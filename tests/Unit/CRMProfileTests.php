<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\CRM\NinepineModels\Profile;
use Faker\Factory as Faker;
use App\CRM\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

class CRMProfileTests extends TestCase{
	public function testIndex(){
		Auth::login(User::all()->random());

		$response = $this->get(route('user.profile', Profile::all()->random()));

		$response->assertStatus(200);
	}

	public function testUpdate(){
		Auth::login(User::all()->random());
		$faker = Faker::create();

		$response = $this->put(route('user.profile.image', Profile::all()->random()), [
			'image' => UploadedFile::fake()->image('file.png', 1024, 1024)
		]);

		$response->assertStatus(200);
	}
}
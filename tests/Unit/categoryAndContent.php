<?php

namespace Tests\Unit;

use App\CRM\NinepineModels\Category;
use App\CRM\NinepineModels\Content;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;

class categoryAndContent extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testCheckIfParentCategoryLinkIsWorking()
    {
        $categories = Category::with('child', 'status')
            ->where('parent_category_id', '=', 0)
            ->orderBy('sort', 'asc')
            ->get();
        $categories = json_decode($categories, true);

        foreach ($categories as $parent_category) {
            $response = $this->get('/category/' . $parent_category['category_id']);
            $response->assertStatus(200);

        }
    }
        public function testCheckIfContentLinkIsWorking()
        {
            $contents = Content::all();

            $categories = json_decode($contents, true);

            foreach ($contents as $content) {
                $response = $this->get('/category/' . $content['content_id']);
                $response->assertStatus(200);


            }
        }

}

<?php

namespace Tests\Unit;

use App\CRM\NinepineModels\Avatar;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\MarketplaceAvatar;
use App\CRM\NinepineModels\Status;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\MarketplaceAvatarRequest;
use Tests\TestCase;

class MarketplaceAvatarTest extends TestCase{
	public function testIndex(){
		Auth::login(User::where('email', 'superadmin@email.com')->first());
		$response = $this->get(route('marketplace_avatar.index'));
		$response->assertStatus(200);
	}

	public function testStore(){
		Auth::login(User::where('email', 'superadmin@email.com')->first());
		$faker = Faker::create();

		$response = $this->post(route('marketplace_avatar.store'), [
			'avatar_amount' => $faker->randomFloat(2, 0.01),
			'avatar_currency' => Currency::where('app_currency', 0)->inRandomOrder()->first()->currency_id,
			'valid_date' => $faker->date,
			'avatar_id' => Avatar::all()->random()->avatar_id,
			'status_id' => Status::where('status_type', 'MarketplaceAvatar')->where('status_name', 'Active')->first()->status_id
		]);

		$response->assertStatus(200);
	}

	public function testUpdate(){
		Auth::login(User::where('email', 'superadmin@email.com')->first());
		$faker = Faker::create();
		$data = MarketplaceAvatar::all()->random();

		$response = $this->put(route('marketplace_avatar.update', $data), [
			'marketplace_avatar_id' => $data->marketplace_avatar_id,
			'avatar_amount' => $faker->randomFloat(2, 0.01),
			'avatar_currency' => Currency::where('app_currency', 0)->inRandomOrder()->first()->currency_id,
			'valid_date' => $faker->date,
			'avatar_id' => Avatar::all()->random()->avatar_id,
			'status_id' => Status::where('status_type', 'MarketplaceAvatar')->where('status_name', 'Active')->first()->status_id
		]);

		$response->assertStatus(200);
	}

	public function testJson(){
		Auth::login(User::where('email', 'superadmin@email.com')->first());
		$response = $this->get(route('marketplace_avatar.json'));
		$response->assertStatus(200);
	}
}

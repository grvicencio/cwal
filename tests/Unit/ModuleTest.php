<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ModuleTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();
    }

    public function tearDown()
    {

    }

    public function testIndex()
    {
        $this->assertDatabaseHas('Roles', [
           'role_name' => 'Administrator'
        ]);
    }
}

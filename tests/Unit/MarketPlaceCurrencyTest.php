<?php

namespace Tests\Unit;

use App\CRM\baccarat\Applications;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\MarketplaceCurrency;
use App\CRM\NinepineModels\Status;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\MarketCurrencyRequest;
use Tests\TestCase;

class MarketPlaceCurrencyTest extends TestCase{
    public function testIndex(){
        Auth::login(User::superAdmin());
        $response = $this->get(route('marketplace_currency.index'));
        $response->assertStatus(200);
    }

    public function testStore(){
        Auth::login(User::superAdmin());
        $faker = Faker::create();

        $response = $this->post(route('marketplace_currency.store'), [
            'item_amount' => $faker->randomFloat(2, 0.01),
            'item_currency' => Currency::where('app_currency', 1)->inRandomOrder()->first()->currency_id,
            'purchase_amount' => $faker->randomFloat(2, 0.01),
            'purchase_currency' => Currency::where('app_currency', 1)->inRandomOrder()->first()->currency_id,
            'expiration_date' => $faker->date,
            'application_id' => Applications::all()->random()->application_id,
            'status_id' => Status::where('status_type', 'MarketplaceCurrency')->where('status_name', 'Active')->first()->status_id,
            'image' => UploadedFile::fake()->image('file.png', 1024, 1024)
        ]);

        $response->assertStatus(201);
    }

    public function testUpdate(){
        Auth::login(User::superAdmin());
        $faker = Faker::create();

        $response = $this->put(route('marketplace_currency.update', MarketplaceCurrency::all()->random()), [
        	'marketplace_currency_id' => MarketplaceCurrency::all()->random()->marketplace_currency_id,
            'item_amount' => $faker->randomFloat(2, 0.01),
            'item_currency' => Currency::where('app_currency', 1)->inRandomOrder()->first()->currency_id,
            'purchase_amount' => $faker->randomFloat(2, 0.01),
            'purchase_currency' => Currency::where('app_currency', 1)->inRandomOrder()->first()->currency_id,
            'expiration_date' => $faker->date,
            'application_id' => Applications::all()->random()->application_id,
            'status_id' => Status::where('status_type', 'MarketplaceCurrency')->where('status_name', 'Active')->first()->status_id,
            'image' => UploadedFile::fake()->image('file.png', 1024, 1024)
        ]);

        $response->assertStatus(200);
    }
}

<?php

namespace Tests\Unit;

use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\Status;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class ValidateSpecialChars extends TestCase{
	public function testUpdate(){
        Auth::login(User::all()->random());

        if(Auth::check()){
            $faker = Faker::create();
            $first_name = $faker->firstname;
            $last_name = $faker->lastname;
            $acc = Accounts::all()->random();

            $response = $this->put(route('accounts.update', $acc->id), [
                "id" => $acc->id,
                "username" => $faker->username . "-'._測試Björk Guðmundsdóttirñ Борис123",
                "email" => $faker->email,
                "mobile" => $faker->phonenumber,
                "first_name" => $first_name . "-'測試Björk Guðmundsdóttirñ Борис",
                "last_name" => $last_name . "-'測試Björk Guðmundsdóttirñ Борис",
                "name" => $first_name . ' ' . $last_name,
                "country" => $faker->country,
                "address" => $faker->address,
                "status_id" => Status::where('status_type', 'Account')->get()->random()->status_id
            ]);

            $response->assertStatus(200);
        }
    }
}
<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthRegisterTest extends TestCase
{
    public function testGetRegister()
    {
        $response = $this->get('register');
        $response->assertStatus(302);
    }

    public function testPostRegister()
    {
        $response = $this->post('register');
        $response->assertStatus(404);
    }
}

<?php

namespace Tests\Feature;

use App\CRM\NinepineModels\ExchangeRate;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class ExchangeRateTest extends TestCase
{
    public function testIndex()
    {
        Auth::login(User::superAdmin());
        $response = $this->get(route('exchange_rates.index'));
        $response->assertStatus(200);
    }

    public function testStore()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();

        $response = $this->post(route('exchange_rates.store'), [
            'from_currency' => ExchangeRate::all()->random()->exchange_rate_id,
            'to_currency' => ExchangeRate::all()->random()->exchange_rate_id,
            'default_amount' => 1,
            'exchange_rate' => doubleval($faker->randomDigit),
        ]);
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();

        $response = $this->put(route('exchange_rates.update', ExchangeRate::all()->random()), [
            'exchange_rate' => doubleval($faker->randomDigit),
        ]);
        $response->assertStatus(200);
    }
}

<?php

namespace Tests\Feature;

use App\CRM\NinepineModels\Registration;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    public function testIndex()
    {
        Auth::login(User::superAdmin());
        $response = $this->get(route('registrations.index'));
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();

        $response = $this->put(route('registrations.update', Registration::all()->random()), [
            'amount' => doubleval($faker->randomDigit),
        ]);
        $response->assertStatus(200);
    }
}

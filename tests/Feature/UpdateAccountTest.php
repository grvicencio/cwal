<?php

namespace Tests\Feature;

use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\Status;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class UpdateAccountTest extends TestCase
{
    public function testUpdate()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();
        $first_name = $faker->firstName;
        $last_name = $faker->lastName;

        $response = $this->put(route('accounts.update', Accounts::all()->random()), [
            "username" => $faker->userName,
            "email" => $faker->email,
            "mobile" => $faker->phoneNumber,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "name" => $first_name . ' ' . $last_name,
            "country" => $faker->country,
            "address" => $faker->address,
            "status_id" => Status::where('status_type', 'Account')->get()->random()->status_id
        ]);
        $response->assertStatus(302);
    }

    public function testTransactionActivity()
    {
        Auth::login(User::superAdmin());
        $response = $this->call('GET', route('accounts.transactionActivityDataTable', Accounts::all()->random()), [
            "start" => 0,
            "draw" => 1,
            "length" => 10,
            "columns" => [
                [
                    "data" => "transaction_log_id",
                    "name" => "",
                    "searchable" => true,
                    "orderable" => true,
                    "search" => [
                        "value" => "",
                        "regex" => false,
                    ]
                ]
            ],
            "order" => [
                [
                    "column" => 0,
                    "dir" => "asc"
                ]
            ]
        ]);
        $response->assertStatus(200);
    }

    public function testDetails()
    {
        Auth::login(User::superAdmin());
        $response = $this->get(route('accounts.details', Accounts::all()->random()));
        $response->assertStatus(200);
    }
}

<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CrmContentUploadTest extends TestCase
{
    private $dir = 'tmp_dir_from_unit_test';
    private $image = 'sample_image_from_unit_test.png';

    public function testImageUpload()
    {
        $response = $this->post(route('contents.imageUpload'), [
            'dir' => $this->dir,
            'content_image' => UploadedFile::fake()->image($this->image, 300, 300)
        ]);
        $response->assertStatus(200);
    }

    public function testGetImage()
    {
        $response = $this->get(route('contents.getImage', [
            'dir' => $this->dir,
            'image' => $this->image
        ]));
        $response->assertStatus($response->getStatusCode() == 404 ? 404 : 200);
    }

    public function testRenameDir()
    {
        $response = $this->post(route('contents.renameDir'), [
            'dir' => $this->dir,
            'content_id' => time()
        ]);
        $response->assertStatus(200);
    }
}

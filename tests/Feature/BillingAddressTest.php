<?php

namespace Tests\Feature;

use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\BillingAddress;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class BillingAddressTest extends TestCase
{
    public function testStore()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();
        $account = Accounts::all()->random();

        if (!$account->billing_address()->count()) {
            $response = $this->post(route('accounts.billing_addresses.store', $account), [
                'ba_first_name' => $faker->firstName,
                'ba_last_name' => $faker->lastName,
                'ba_country' => $faker->country,
                'ba_city' => $faker->city,
                'ba_billing_address' => $faker->address,
                'ba_billing_address_second' => $faker->address,
                'ba_postalcode' => $faker->randomDigit,
                'ba_phone' => $faker->randomDigit
            ]);
            $response->assertStatus(302);
        }
    }

    public function testUpdate()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();
        $billing_address = BillingAddress::all()->random();
        $account = $billing_address->account()->first();

        $response = $this->put(route('accounts.billing_addresses.update', [
            $account, $billing_address
        ]), [
            'ba_first_name' => $faker->firstName,
            'ba_last_name' => $faker->lastName,
            'ba_country' => $faker->country,
            'ba_city' => $faker->city,
            'ba_billing_address' => $faker->address,
            'ba_billing_address_second' => $faker->address,
            'ba_postalcode' => $faker->randomDigit,
            'ba_phone' => $faker->randomDigit
        ]);
        $response->assertStatus(302);
    }
}

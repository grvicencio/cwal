<?php

namespace Tests\Feature;

use App\CRM\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class RolePermissionTest extends TestCase
{
    public function testCanAccessRoute()
    {
        Auth::login(User::superAdmin());
        $response = $this->get('dashboard');
        $response->assertStatus(200);
    }

    public function testCantAccessRoute()
    {
        Auth::login(User::superAdmin());
        $response = $this->get('transactions');
        $response->assertStatus(307);
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;

class checkAccountActivation extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAccountActivation()
    {

        $faker = Faker::create();

        $response = $this->get('/activate/' . md5($faker->word));
        $response->assertStatus(200);

    }
}

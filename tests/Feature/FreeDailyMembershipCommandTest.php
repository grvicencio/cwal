<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class FreeDailyMembershipCommandTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExecFreePoints()
    {
        Artisan::call('command:FreeDailyMembership');
        $this->assertTrue(!empty(Artisan::output()) == true);
    }
}

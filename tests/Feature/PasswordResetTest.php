<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PasswordResetTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->faker = Faker::create();

        // I was hoping there's a test account. You can change this credentials matching your local values.
        // i know this isn't right... i have to search something to access user on test units
        $this->post(route('login'), [
            'username' => 'mark@email.com',
            'password' => 'password'
        ]);
    }


    public function testAccess()
    {
        $this->assertTrue(true);
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Faker\Factory as Faker;

class checkContactUsPage extends TestCase
{
    public function testContactUsPage()
    {
        $faker = Faker::create();

        $response = $this->post('/sendContactUsEmail', [
            'email' => $faker->email,
            'subject' => $faker->sentence,
            'type' => $faker->sentence,
            'name' => $faker->name,
            'isTest' => true,
            'g-recaptcha-response' => md5($faker->word)
        ]);
        $response->assertStatus(200);
    }
}

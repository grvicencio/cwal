<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Ixudra\Curl\Facades\Curl;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;

class PaymentInfoTest extends TestCase
{
    private $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Faker::create();

        // I was hoping there's a test account. You can change this credentials matching your local values.
        $this->post(route('login'), [
            'username' => 'emurmotol',
            'password' => 'nakalimutankona'
        ]);
    }

    public function testUpdate()
    {
        $response = $this->post('transaction/payment_info/update', [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'city' => $this->faker->city,
            'billing_address' => $this->faker->address,
            'billing_address_second' => $this->faker->address,
            'postalcode' => $this->faker->postcode,
            'phone' => '09234234' . rand(100, 999),
            'country' => $this->faker->country
        ]);
        $response->assertSessionHas('message', 'Success');
    }
}

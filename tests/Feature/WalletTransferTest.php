<?php

namespace Tests\Feature;

use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\Currency;
use App\CRM\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class WalletTransferTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testWalletTransfer()
    {
        Auth::login(User::superAdmin());
        $account = Accounts::all()->random();
        $currency_id = Currency::where('app_currency', false)->get()->random()->currency_id;
        $checkWallet = $account->wallet()->where('currency_id', $currency_id)->first();
        $oldBalance = doubleval(0);
        $fakeAmount = doubleval(50);

        if (!is_null($checkWallet)) {
            $oldBalance = doubleval($checkWallet->balance);
        }

        $response = $this->call('POST', '/wallet/transfer', [
            "user_id" => $account->id,
            "currency_id" => $currency_id,
            "transfer_amount" => $fakeAmount,
            "reason" => "Triggered from unit test.",
        ]);
        $response->assertStatus(200);
        $reCheckWallet = Accounts::find($account->id)->wallet()->where('currency_id', $currency_id)->first();

        $newBalance = doubleval($reCheckWallet->balance);
        $transferedAmount = $newBalance - $oldBalance;
        $this->assertTrue($transferedAmount == $fakeAmount);
    }
}

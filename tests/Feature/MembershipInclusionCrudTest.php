<?php

namespace Tests\Feature;

use App\CRM\baccarat\MembershipCharges;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class MembershipInclusionCrudTest extends TestCase
{
    public function testIndex()
    {
        Auth::login(User::superAdmin());
        $membership = MembershipCharges::all()->random();

        if ($membership) {
            $response = $this->get(route('membership.inclusions.index', $membership));
            $response->assertStatus(200);
        }
    }

    public function testStore()
    {
        Auth::login(User::superAdmin());
        $membership = MembershipCharges::all()->random();

        if ($membership) {
            $faker = Faker::create();

            $response = $this->post(route('membership.inclusions.store', $membership), [
                'name' => $faker->word,
                'description' => $faker->sentence,
                'value' => $faker->randomDigit
            ]);
            $response->assertStatus(302);
        }
    }

    public function testUpdate()
    {
        Auth::login(User::superAdmin());
        $membership = MembershipCharges::all()->random();

        if ($membership && $membership->membership_inclusions) {
            $faker = Faker::create();

            $response = $this->put(route('membership.inclusions.update', [$membership, $membership->membership_inclusions->random()]), [
                'name' => $faker->word,
                'description' => $faker->sentence,
                'value' => $faker->randomDigit
            ]);
            $response->assertStatus(302);
        }
    }

    public function testDestroy()
    {
        Auth::login(User::superAdmin());
        $membership = MembershipCharges::all()->random();

        if ($membership && $membership->membership_inclusions) {
            $response = $this->delete(route('membership.inclusions.destroy', [$membership, $membership->membership_inclusions->random()]));
            $response->assertStatus(302);
        }
    }
}

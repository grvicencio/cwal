<?php

namespace Tests\Feature;

use App\CRM\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class PaginationTest extends TestCase
{
    Use WithoutMiddleware;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testResponseStatus()
    {
        Auth::login(User::superAdmin());
        $response = $this->call('GET', '/accounts/datatable', [
            "start" => 0,
            "draw" => 1,
            "length" => 10,
            "columns" => [
                [
                    "data" => "username",
                    "name" => "",
                    "searchable" => true,
                    "orderable" => true,
                    "search" => [
                        "value" => "",
                        "regex" => false,
                    ]
                ]
            ],
            "order" => [
                [
                    "column" => 0,
                    "dir" => "asc"
                ]
            ]
        ]);
        $response->assertStatus(200);
    }

    public function testJsonStruct()
    {
        Auth::login(User::superAdmin());
        $response = $this->call('GET', '/accounts/datatable', [
            "start" => 0,
            "draw" => 1,
            "length" => 10,
            "columns" => [
                [
                    "data" => "username",
                    "name" => "",
                    "searchable" => true,
                    "orderable" => true,
                    "search" => [
                        "value" => "",
                        "regex" => false,
                    ]
                ]
            ],
            "order" => [
                [
                    "column" => 0,
                    "dir" => "asc"
                ]
            ]
        ]);

        $response->assertJsonStructure([
            'draw',
            'recordsTotal',
            'recordsFiltered',
            'data'
        ]);
    }
}

<?php

namespace Tests\Feature;

use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\DailyBonus;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class DailyBonusTest extends TestCase
{
    public function testIndex()
    {
        Auth::login(User::superAdmin());
        $response = $this->get(route('daily_bonuses.index'));
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();

        $response = $this->put(route('daily_bonuses.update', DailyBonus::all()->random()), [
            'points' => doubleval($faker->randomDigit),
        ]);
        $response->assertStatus(200);
    }

    public function testStore()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();
        $type = DailyBonus::$types[0];

        if (is_null(DailyBonus::where('type', $type)->first())) {
            $response = $this->post(route('daily_bonuses.store'), [
                'currency_id' => Currency::where('app_currency', true)->random()->currency_id,
                'type' => DailyBonus::$types[0],
                'points' => doubleval($faker->randomDigit),
            ]);
            $response->assertStatus(200);
        }
    }
}

<?php

namespace Tests\Feature;

use App\CRM\NinepineModels\Currency;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class WalletCurrencyCrudTest extends TestCase
{
    public function testIndex()
    {
        Auth::login(User::superAdmin());
        $response = $this->get(route('currencies.index'));
        $response->assertStatus(200);
    }

    public function testStore()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();

        $response = $this->post(route('currencies.store'), [
            'currency_name' => $faker->word,
            'currency_symbol' => $faker->randomLetter,
            'currency' => $faker->randomDigit,
            'app_currency' => true,
            'default_registration' => false,
        ]);
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        Auth::login(User::superAdmin());
        $response = $this->put(route('currencies.update', Currency::all()->random()), [
            'default_registration' => true,
        ]);
        $response->assertStatus(200);
    }
}

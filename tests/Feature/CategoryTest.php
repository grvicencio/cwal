<?php

namespace Tests\Feature;

use App\CRM\NinepineModels\Category;
use App\CRM\NinepineModels\Status;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    public function testIndex()
    {
        Auth::login(User::superAdmin());
        $response = $this->get(route('categories.index'));
        $response->assertStatus(200);
    }

    public function testJson()
    {
        Auth::login(User::superAdmin());
        $response = $this->get(route('categories.json'));
        $response->assertStatus(200);
    }

    public function testDataTable()
    {
        Auth::login(User::superAdmin());
        $response = $this->call('GET', route('categories.dataTable'), [
            "start" => 0,
            "draw" => 1,
            "length" => 10,
            "columns" => [
                [
                    "data" => "category_name",
                    "name" => "",
                    "searchable" => true,
                    "orderable" => true,
                    "search" => [
                        "value" => "",
                        "regex" => false,
                    ]
                ]
            ],
            "order" => [
                [
                    "column" => 0,
                    "dir" => "asc"
                ]
            ]
        ]);
        $response->assertStatus(200);
    }

    public function testStore()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();
        $category = Category::all()->random();

        $sorts = Category::where('parent_category_id', $category->category_id)
            ->orderBy('sort', 'asc')
            ->pluck('sort')
            ->toArray();

        $response = $this->post(route('categories.store'), [
            'category_name' => $faker->word,
            'parent_category_id' => Category::all()->random()->category_id,
            'status_id' => Status::where('status_type', 'Category')->where('status_name', 'Active')->first()->status_id,
            'sort' => $faker->randomElement($sorts)
        ]);
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();
        $category = Category::all()->random();

        $sorts = Category::where('parent_category_id', $category->category_id)
            ->orderBy('sort', 'asc')
            ->pluck('sort')
            ->toArray();

        $response = $this->put(route('categories.update', $category), [
            'category_name' => $faker->word,
            'parent_category_id' => Category::all()->random()->category_id,
            'status_id' => Status::where('status_type', 'Category')->where('status_name', 'Active')->first()->status_id,
            'sort' => $faker->randomElement($sorts)
        ]);
        $response->assertStatus(200);
    }

    public function testDestroy()
    {
        Auth::login(User::superAdmin());
        $category = Category::all()->random();
        $response = $this->delete(route('categories.destroy', $category));
        $response->assertStatus(200);
    }
}

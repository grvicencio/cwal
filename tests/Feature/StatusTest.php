<?php

namespace Tests\Feature;

use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class StatusTest extends TestCase
{
    protected $faker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUrl()
    {
        Auth::login(User::superAdmin());
        $response = $this->get('status');
        $response->assertStatus(200);
    }

    public function testStatusInsert()
    {
        Auth::login(User::superAdmin());
        $this->faker = Faker::create();
        $status_type = $this->faker->word;
        $status_name = $this->faker->word;
        $data = ['status_type' => $status_type,
            'status_name' => $status_name

        ];
        $response = array('response' => 'success');
        $this->post('status/save', $data)->assertStatus(201)->assertJson($response);
    }

    public function testStatusUpdate()
    {
        Auth::login(User::superAdmin());
        $this->faker = Faker::create();
        $id = rand(1, 10);
        $status_type = $this->faker->word;
        $status_name = $this->faker->word;
        $data = ['status_type' => $status_type,
            'status_name' => $status_name,
            'status_id' => $id
        ];
        $response = array('response' => 'success');
        $this->post('status/update', $data)->assertStatus(201)->assertJson($response);;
    }

    public function testStatusDelete()
    {
        Auth::login(User::superAdmin());
        $id = rand(1, 10);
        $response = array('response' => 'success');
        $this->get("status/delete/$id")->assertStatus(201)->assertJson($response);;
    }
}

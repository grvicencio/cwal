<?php

namespace Tests\Feature;

use App\CRM\NinepineModels\Category;
use App\CRM\NinepineModels\Content;
use App\CRM\NinepineModels\Status;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Tests\TestCase;

class ContentTest extends TestCase
{
    public function testIndex()
    {
        Auth::login(User::superAdmin());
        $response = $this->get(route('contents.index'));
        $response->assertStatus(200);
    }

    public function testCreate()
    {
        Auth::login(User::superAdmin());
        $response = $this->get(route('contents.create'));
        $response->assertStatus(200);
    }

    public function testEdit()
    {
        Auth::login(User::superAdmin());
        $content = Content::all()->random();
        $response = $this->get(route('contents.edit', $content));
        $response->assertStatus(200);
    }

    public function testUpload()
    {
        Auth::login(User::superAdmin());

        $uploadedFile = new UploadedFile(
            public_path('Capital7-1.0.0/img') . '/user2-160x160.jpg',
            'user2-160x160.jpg',
            'image/jpeg',
            null,
            null,
            true
        );

        $response = $this->post(route('contents.upload'), [
            'content_image' => $uploadedFile,
        ]);
        $response->assertStatus(200);
    }

    public function testDataTable()
    {
        Auth::login(User::superAdmin());
        $response = $this->call('GET', route('contents.dataTable'), [
            "start" => 0,
            "draw" => 1,
            "length" => 10,
            "columns" => [
                [
                    "data" => "title",
                    "name" => "",
                    "searchable" => true,
                    "orderable" => true,
                    "search" => [
                        "value" => "",
                        "regex" => false,
                    ]
                ]
            ],
            "order" => [
                [
                    "column" => 0,
                    "dir" => "asc"
                ]
            ]
        ]);
        $response->assertStatus(200);
    }

    public function testStore()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();

        $response = $this->post(route('contents.store'), [
            'title' => $faker->word,
            'content' => $faker->word,
            'category_id' => Category::all()->random()->category_id,
            'status_id' => Status::where('status_type', 'Content')->where('status_name', 'Active')->first()->status_id,
            'path' => '/' . str_slug($faker->word),
            'display_on_homepage' => true,
            'default' => false,
        ]);
        $response->assertStatus(302);
    }

    public function testUpdate()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();
        $content = Content::all()->random();

        $response = $this->put(route('contents.update', $content), [
            'title' => $faker->word,
            'content' => $faker->word,
            'category_id' => Category::all()->random()->category_id,
            'status_id' => Status::where('status_type', 'Content')->where('status_name', 'Active')->first()->status_id,
            'path' => '/' . str_slug($faker->word),
            'display_on_homepage' => true,
            'default' => false,
        ]);
        $response->assertStatus(302);
    }

    public function testDestroy()
    {
        Auth::login(User::superAdmin());
        $content = Content::all()->random();
        $response = $this->delete(route('contents.destroy', $content));
        $response->assertStatus(200);
    }
}

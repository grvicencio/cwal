<?php

namespace Tests\Feature;

use App\CRM\baccarat\MembershipCharges;
use App\CRM\baccarat\Status;
use App\CRM\NinepineModels\Currency;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class MembershipCrudTest extends TestCase
{
    public function testIndex()
    {
        Auth::login(User::superAdmin());
        $response = $this->get(route('membership.index'));
        $response->assertStatus(200);
    }

    public function testStore()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();

        $response = $this->post(route('membership.add'), [
            'name' => $faker->word,
            'description' => $faker->word,
            'status_id' => $faker->randomElement(Status::where('status_type', 'Membership')->pluck('status_id')->toArray()),
            'billing_cycle' => $faker->randomDigit,
            'price' => doubleval($faker->randomDigit),
            'membership_type' => $faker->randomElement(MembershipCharges::$types),
            'no_of_tables' => $faker->randomDigit,
            'banker_rake' => $faker->randomDigit,
            'logo_file' => UploadedFile::fake()->image('file.png', 720, 720),
            'currency_id' => $faker->randomElement(Currency::where('app_currency', false)->orWhere('app_currency', null)->pluck('currency_id')->toArray())
        ]);
        $response->assertStatus(201);
    }

    public function testUpdate()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();

        $response = $this->put(route('membership.update', MembershipCharges::all()->random()), [
            'name' => $faker->word,
            'description' => $faker->word,
            'status_id' => $faker->randomElement(Status::where('status_type', 'Membership')->pluck('status_id')->toArray()),
            'billing_cycle' => $faker->randomDigit,
            'price' => doubleval($faker->randomDigit),
            'membership_type' => $faker->randomElement(MembershipCharges::$types),
            'no_of_tables' => $faker->randomDigit,
            'banker_rake' => $faker->randomDigit,
            'logo_file' => UploadedFile::fake()->image('file.png', 720, 720),
            'currency_id' => $faker->randomElement(Currency::where('app_currency', false)->orWhere('app_currency', null)->pluck('currency_id')->toArray())
        ]);
        $response->assertStatus(200);
    }
}

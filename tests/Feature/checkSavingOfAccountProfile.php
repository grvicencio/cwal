<?php

namespace Tests\Feature;

use App\CRM\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;

class checkSavingOfAccountProfile extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdateProfile()
    {

        $faker = Faker::create();

        $response = $this->post(route('login') , [
            '_token' => csrf_token(),
            'username' => 'wisefajardo',
            'password' => 'password',
        ]);

        $bearer = "Bearer ". Session::get('access_token');
        $response = $this->post('/account/updateprofile', [
            '_token' => csrf_token(),
            'first_name' => $faker->firstNameMale,
            'last_name' => $faker->lastName,
            'gender' => 'Male',
            'country' => $faker->country,
            'birth_date' => $faker->date(),
            'image' => UploadedFile::fake()->image('bear.png', 300, 300)
        ],  ['HTTP_Authorization' =>  $bearer]);
        $response->assertStatus(200);


    }
}

<?php

namespace Tests\Feature;

use App\CRM\baccarat\Status;
use App\CRM\NinepineModels\Role;
use App\CRM\User;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdate()
    {
        Auth::login(User::superAdmin());
        $faker = Faker::create();
        $first_name = $faker->firstName;
        $last_name = $faker->lastName;

        $response = $this->put(route('assignuser.update', User::where('email', '!=', 'superadmin@prototype-crm.dev')->get()->random()), [
            "email" => $faker->email,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "name" => $first_name . ' ' . $last_name,
            "role_id" => Role::all()->random()->role_id,
            "status_id" => Status::where('status_type', 'User')->get()->random()->status_id
        ]);
        $response->assertStatus(200);
    }

    public function testPasswordReset()
    {
        Auth::login(User::superAdmin());
        $response = $this->put(route('assignuser.password_reset', User::where('email', '!=', 'superadmin@prototype-crm.dev')->get()->random()), [
            "password" => 'secret',
        ]);
        $response->assertStatus(200);
    }
}

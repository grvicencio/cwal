<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;
class CheckRegistration extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {

        $faker = Faker::create();
        $password = $faker->password(8);
        $email = $faker->email;
        $name = $faker->name;
        $firstName = $faker->firstName;
        $lastName = $faker->lastName;
        $response = $this->post('/account/register', [
            'name' => $name,
            'lastname' =>$firstName,
            'firstname' => $lastName,
            'email' => $email,
            'password' => $password,
            'repeat_password' => $password,



        ]);
        $response->assertStatus(200);
    }
}

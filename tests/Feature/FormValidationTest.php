<?php

namespace Tests\Feature;

use App\Http\Requests\ApplicationRequest;
use App\Http\Requests\MembershipChargesRequest;
use App\Http\Requests\ModuleRequest;
use App\Http\Requests\RegularChargesRequest;
use App\Http\Requests\RoleRequest;
use App\Http\Requests\StatusRequest;
use App\Http\Requests\UserRequest;
use App\CRM\NinepineModels\Role;
use App\CRM\NinepineModels\Status;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class FormValidationTest extends TestCase
{
    public function testAddStatusForm()
    {
        $rules = (new StatusRequest)->rules();
        $this->assertFalse($this->validateField('status_name', '', $rules));
        $this->assertTrue($this->validateField('status_name', 'Test Status', $rules));
    }

    public function testEditStatusForm()
    {
        $rules = (new StatusRequest)->rules();
        $this->assertFalse($this->validateField('status_name', '', $rules));
        $this->assertTrue($this->validateField('status_name', 'Test Status', $rules));
    }

    public function testAddModuleForm()
    {
        $rules = (new ModuleRequest())->rules();
        $this->assertFalse($this->validateField('module', '', $rules));
        $this->assertTrue($this->validateField('module', 'Test Module', $rules));

        $this->assertFalse($this->validateField('controller', '', $rules));
        $this->assertTrue($this->validateField('controller', 'Test Controller', $rules));

        $this->assertFalse($this->validateField('description', '', $rules));
        $this->assertTrue($this->validateField('description', 'Test Description', $rules));
    }

    public function testAddPermissionForm()
    {
        $rules = (new RoleRequest())->rules();
        $this->assertFalse($this->validateField('role_name', '', $rules));
        $this->assertTrue($this->validateField('role_name', 'Test Role Name', $rules));

        $this->assertFalse($this->validateField('description', '', $rules));
        $this->assertTrue($this->validateField('description', 'Test Description', $rules));
    }

    public function testAddUserForm()
    {
        $rules = (new UserRequest())->rules('POST');
        $this->assertFalse($this->validateField('firstname', '', $rules));
        $this->assertTrue($this->validateField('firstname', 'Test First Name', $rules));

        $this->assertFalse($this->validateField('lastname', '', $rules));
        $this->assertTrue($this->validateField('lastname', 'Test Last Name', $rules));

        $status_id = Status::where('status_type', 'User')->get()->random()->status_id;
        $this->assertFalse($this->validateField('status_id', '', $rules));
        $this->assertTrue($this->validateField('status_id', $status_id, $rules));

        $this->assertFalse($this->validateField('role_id', '', $rules));
        $this->assertTrue($this->validateField('role_id', Role::all()->random()->role_id, $rules));

        $this->assertFalse($this->validateField('email', '', $rules));
        $this->assertTrue($this->validateField('email', 'test@example.com', $rules));

        $this->assertFalse($this->validateField('password', '', $rules));
        $this->assertFalse($this->validateField('password', '1234', $rules));
        $this->assertFalse($this->validateField('password', '12345', $rules));
    }

    public function testEditRegularForm()
    {
        $rules = (new RegularChargesRequest())->rules();
        $this->assertFalse($this->validateField('name', '', $rules));
        $this->assertTrue($this->validateField('name', 'Test Name', $rules));

        $this->assertFalse($this->validateField('description', '', $rules));
        $this->assertTrue($this->validateField('description', 'Test Description', $rules));

        $this->assertFalse($this->validateField('price', '', $rules));
        $this->assertFalse($this->validateField('price', 0.00, $rules));
        $this->assertFalse($this->validateField('price', 'abc', $rules));
        $this->assertTrue($this->validateField('price', 1.00, $rules));

        $this->assertFalse($this->validateField('value', '', $rules));
        $this->assertFalse($this->validateField('value', -1, $rules));
        $this->assertFalse($this->validateField('value', 'abc', $rules));
        $this->assertTrue($this->validateField('value', 0, $rules));
        $this->assertTrue($this->validateField('value', 1, $rules));

        $status_id = Status::where('status_type', 'Charges')->get()->random()->status_id;
        $this->assertFalse($this->validateField('status_id', '', $rules));
        $this->assertTrue($this->validateField('status_id', $status_id, $rules));
    }

    public function testAddRegularForm()
    {
        $rules = (new RegularChargesRequest())->rules();
        $this->assertFalse($this->validateField('name', '', $rules));
        $this->assertTrue($this->validateField('name', 'Test Name', $rules));

        $this->assertFalse($this->validateField('description', '', $rules));
        $this->assertTrue($this->validateField('description', 'Test Description', $rules));

        $this->assertFalse($this->validateField('price', '', $rules));
        $this->assertFalse($this->validateField('price', 0.00, $rules));
        $this->assertFalse($this->validateField('price', 'abc', $rules));
        $this->assertTrue($this->validateField('price', 1.00, $rules));

        $this->assertFalse($this->validateField('value', '', $rules));
        $this->assertFalse($this->validateField('value', -1, $rules));
        $this->assertFalse($this->validateField('value', 'abc', $rules));
        $this->assertTrue($this->validateField('value', 0, $rules));
        $this->assertTrue($this->validateField('value', 1, $rules));

        $status_id = Status::where('status_type', 'Charges')->get()->random()->status_id;
        $this->assertFalse($this->validateField('status_id', '', $rules));
        $this->assertTrue($this->validateField('status_id', $status_id, $rules));
    }

    public function testAddMembershipForm()
    {
        $rules = (new MembershipChargesRequest())->rules();
        $this->assertFalse($this->validateField('name', '', $rules));
        $this->assertTrue($this->validateField('name', 'Test Name', $rules));

        $this->assertFalse($this->validateField('description', '', $rules));
        $this->assertTrue($this->validateField('description', 'Test Description', $rules));

        $this->assertFalse($this->validateField('price', '', $rules));
        $this->assertFalse($this->validateField('price', 0.00, $rules));
        $this->assertFalse($this->validateField('price', 'abc', $rules));
        $this->assertTrue($this->validateField('price', 1.00, $rules));

        $this->assertFalse($this->validateField('billing_cycle', '', $rules));
        $this->assertFalse($this->validateField('billing_cycle', -1, $rules));
        $this->assertFalse($this->validateField('billing_cycle', 'abc', $rules));
        $this->assertTrue($this->validateField('billing_cycle', 0, $rules));
        $this->assertTrue($this->validateField('billing_cycle', 1, $rules));

        $status_id = Status::where('status_type', 'Membership')->get()->random()->status_id;
        $this->assertFalse($this->validateField('status_id', '', $rules));
        $this->assertTrue($this->validateField('status_id', $status_id, $rules));
    }

    public function testAddApplicationForm()
    {
        $rules = (new ApplicationRequest())->rules();
        $this->assertFalse($this->validateField('name', '', $rules));
        $this->assertTrue($this->validateField('name', 'Test Name', $rules));

        $this->assertFalse($this->validateField('description', '', $rules));
        $this->assertTrue($this->validateField('description', 'Test Description', $rules));

        $this->assertFalse($this->validateField('price', '', $rules));
        $this->assertFalse($this->validateField('price', 0.00, $rules));
        $this->assertFalse($this->validateField('price', 'abc', $rules));
        $this->assertTrue($this->validateField('price', 1.00, $rules));

        $image_landscape_valid_file = UploadedFile::fake()->image('file.png', 720, 365);
        $this->assertTrue($this->validateField('image_landscape', $image_landscape_valid_file, $rules));
        $image_landscape_file_exceeded_size = UploadedFile::fake()->create('file.png', 5120); // 5MB
        $this->assertFalse($this->validateField('image_landscape', $image_landscape_file_exceeded_size, $rules));
        $image_landscape_text_file = UploadedFile::fake()->create('file.txt', 32);
        $this->assertFalse($this->validateField('image_landscape', $image_landscape_text_file, $rules));

        $image_square_valid_file = UploadedFile::fake()->image('file.png', 300, 300);
        $this->assertTrue($this->validateField('image_square', $image_square_valid_file, $rules));
        $image_square_file_exceeded_size = UploadedFile::fake()->create('file.png', 5120); // 5MB
        $this->assertFalse($this->validateField('image_square', $image_square_file_exceeded_size, $rules));
        $image_square_text_file = UploadedFile::fake()->create('file.txt', 32);
        $this->assertFalse($this->validateField('image_square', $image_square_text_file, $rules));

        $status_id = Status::where('status_type', 'Application')->get()->random()->status_id;
        $this->assertFalse($this->validateField('status_id', '', $rules));
        $this->assertTrue($this->validateField('status_id', $status_id, $rules));
    }

    protected function validateField($field, $value, $rules)
    {
        $validator = $this->app['validator']->make([$field => $value], [$field => $rules[$field]]);
        return $validator->passes();
    }
}

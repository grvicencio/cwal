<?php

namespace Tests\Feature;

use App\CRM\NinepineModels\Wallet;
use App\CRM\NinepineModels\Wallet_ledger;
use App\CRM\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class SourceInfoTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDataTable()
    {
        Auth::login(User::superAdmin());
        $response = $this->call('GET', route('wallet.dataTable', Wallet::all()->random()), [
            "start" => 0,
            "draw" => 1,
            "length" => 10,
            "columns" => [
                [
                    "data" => "",
                    "name" => "",
                    "searchable" => true,
                    "orderable" => true,
                    "search" => [
                        "value" => "",
                        "regex" => false,
                    ]
                ]
            ],
            "order" => [
                [
                    "column" => 0,
                    "dir" => "asc"
                ]
            ]
        ]);
        $response->assertStatus(200);
    }

    public function testSourceInfo()
    {
        Auth::login(User::superAdmin());
        $response = $this->call('GET', route('wallet.ledgerSourceInfo', Wallet_ledger::all()->random()));
        $response->assertStatus(200);
    }
}

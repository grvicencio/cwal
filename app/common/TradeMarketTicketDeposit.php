<?php

namespace App\common;

use App\CRM\baccarat\Accounts;
use Illuminate\Database\Eloquent\Model;
use App\CRM\NinepineModels\Source;
use Illuminate\Support\Facades\DB;
use App\CRM\NinepineModels\WalletTransfer;

class TradeMarketTicketDeposit extends Model
{
    protected $connection = 'bit';
    protected $table = 'TradeMarketTicketDeposit';
    protected $primaryKey = 'id';

    protected $casts = [
        'attachments' => 'array',
    ];

    protected $fillable = [
        'trademarket_ticket_id',
        'sender_id',
        'receiver_id',
        "bank_id",
        "attachments",
        "notes",
        "offer_notes",
        "wallet_transfer_id",
        "wallet_id",
        "type",
    ];

    public function bank()
    {
        // return $this->belongsTo(BankInfo::class, "bank_id",'bankinfo_id')->select([
        //     "bank_name", "bank_account_name", "bank_account_number", "bank_branch"
        // ]);
        return $this->belongsTo(BankInfo::class, "bank_id",'bankinfo_id');
    }

    public function sender_account()
    {
        return $this->belongsTo(Accounts::class, 'sender_id', 'id');
    }

    public function receiver_account()
    {
        return $this->belongsTo(Accounts::class, 'receiver_id', 'id');
    }

    public function trade_market_ticket()
    {
        return $this->belongsTo(TradeMarketTicket::class, 'trademarket_ticket_id');
    }

    public function wallet_transfer()
    {
        return $this->belongsTo(WalletTransfer::class, 'wallet_transfer_id');
    }

    public static function getTransactions() {
        return DB::table("TradeMarketTicket")->select([
            "crmwalletledger.wallet_ledger_id as wl_id",
            "crmwalletledger.credit as credit",
            "crmwalletledger.debit as debit",
            "crmwalletledger.balance as balance",
            "crmwallet.wallet_account_number as wan",
            "crmcurrency.currency as currency",
            "crmwalletledger.created_at as created_at",
            // "crmstatus.status_name as status_name",
            "TradeMarketTicket.ticket_generated_id as ticket_generated_id",
        ])
        ->join("TradeMarketTicketDeposit", "TradeMarketTicketDeposit.trademarket_ticket_id", "=", "TradeMarketTicket.trademarket_ticket_id")
        ->join("TradeMarketTicketHistory", "TradeMarketTicket.trademarket_ticket_id", "=", "TradeMarketTicketHistory.trademarket_ticket_id")
        ->join("crmstatus", "crmstatus.status_id", "=", "TradeMarketTicketHistory.ticket_status")
        ->join("Tradeprofile", "TradeMarketTicket.trade_profile_id", "=", "Tradeprofile.tradeprofile_id")
        ->join("BankInfo", "BankInfo.bankinfo_id", "=", "TradeMarketTicketDeposit.bank_id")
        ->join("crmcurrency", "crmcurrency.currency_id", "=", "BankInfo.currency_id")
        ->join("crmwalletledger", "TradeMarketTicketDeposit.wallet_id", "=", "crmwalletledger.wallet_id")
        ->join("crmsources", "crmsources.source_id", "=", "crmwalletledger.source_id")
        ->join("crmwallet", "TradeMarketTicketDeposit.wallet_id", "=", "crmwallet.wallet_id")
        ->where("crmwallet.user_id", authUser()->get("id"))
        ->where("crmwalletledger.source_id", Source::getIdByName("WalletTransfer"))
        ->where("TradeMarketTicketDeposit.type", "Wallet")
        ->groupBy("crmwalletledger.wallet_ledger_id")
        ->groupBy("crmwalletledger.created_at")
        ->groupBy("crmwalletledger.credit")
        ->groupBy("crmwalletledger.balance")
        ->groupBy("crmwalletledger.debit")
        ->groupBy("crmwallet.wallet_account_number")
        ->groupBy("crmcurrency.currency")
        // ->groupBy("crmstatus.status_name")
        // ->groupBy("TradeMarketTicketHistory.trademarket_ticket_id")
        ->groupBy("TradeMarketTicket.ticket_generated_id")
        // ->groupBy("TradeMarketTicket.trademarket_ticket_id")
        ->orderBy("crmwalletledger.created_at", "desc")
        ->get();
    }
}

<?php

namespace App\common;

use Illuminate\Database\Eloquent\Model;

/**
 * This model uses db view
 * */
class WalletTransaction extends Model
{
    protected $connection = 'bit';
    protected $table = 'transactionhistory';


}

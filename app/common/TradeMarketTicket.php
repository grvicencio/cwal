<?php

namespace App\common;

use App\API\Tradeprofile;
use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\Source;
use App\CRM\NinepineModels\Status;
use App\CRM\NinepineModels\SubticketCount;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Wallet;
use App\Http\Requests\common\TicketPaymentRequest;
use App\Http\Requests\common\TicketBuyerAccept;
use App\CRM\baccarat\Applications;
use App\Mail\WebTicketHasAnOfferEmail;
use App\Mail\WebTicketHasExpiredEmail;
use App\Mail\WebTicketHasPaymentEmail;
use App\Mail\WebTicketReachesZeroValueEmail;
use App\Mail\WebTicketWasApprovedEmail;
use App\Mail\WebTicketWasClosedEmail;
use App\Mail\WEBTicketHasCompletedEmail;
use App\Mail\WebTicketWasOpenedEmail;
use App\Mail\WebTicketWasPayedEmail;
use App\Mail\WebTicketWasRejectedEmail;
use Carbon\Carbon;
use Cartalyst\Stripe\Exception\MissingParameterException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\CRM\NinepineModels\Wallet_ledger;
use App\CRM\NinepineModels\WalletTransfer;
use App\Jobs\SendTicketEmail;

/**
 * Ticket instance
 *
 * Class TradeMarketTicket
 * @package App\common
 */
class TradeMarketTicket extends Model
{
    /**
     * Db connection
     *
     * @var string
     */
    protected $connection = 'bit';

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'TradeMarketTicket';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = "trademarket_ticket_id";

    /**
     * Mass assignable
     * Only update or create listed
     *
     * @var array
     */
    protected $fillable = [
        'trade_profile_id',
        'user_id',
        'trademarket_ticket_parent_id',
        'inquiry_by',
        'value',
        'amount',
        'text',
        'currency_id',
        'ticket_generated_id',
        'ticket_status',
        "expire_at",
        'bankinfo_id',
        "wallet_id", // ticket-system-14
        "wallet_id_global"
    ];

    /**
     * Auto cast fields to proper types
     *
     * @var array
     */
    protected $casts = [
        'value' => 'double',
        'amount' => 'double',
    ];

    /**
     * Declare all date cols
     *
     * @var array
     */
    protected $dates = [
        "expire_at"
    ];

    /**
     * Used for generating ticket id
     *
     * @var array
     */
    public static $trade_market_codes = [
        "Seller" => 'S',
        "Buyer" => 'B'
    ];

    protected $account_cols = [
        'id',
        'name',
        'email',
        'first_name',
        'last_name',
        'country',
        'address',
        'username',
        'mobile',
        'gender',
    ];

    /**
     * Identify if a ticket is being created as an offer
     *
     */
    const TYPE_OFFER = "OFFER";

    /**
     * Identify if a ticket is new
     *
     */
    const TYPE_NEW = "NEW";

    public static function transactions()
    {
        $all = collect(Tradeprofile::getTransactions());

        foreach (TradeMarketTicketTransfer::getTransactions() as $tmtt) {
            $all->push($tmtt);
        }

        foreach (TradeMarketTicketDeposit::getTransactions() as $tmtd) {
            $all->push($tmtd);
        }
        return $all->sortByDesc("created_at");
    }

    /**
     * Open a new ticket or an offer
     *
     * @param Tradeprofile $trade_profile
     * @param $type
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Collection|Model|null|static|static[]
     */
    public static function open(Tradeprofile $trade_profile, $type, $data = [])
    {
        $ticket = null;

        if ($trade_profile->isOpen()) {
            $trade_market_ticket_parent_id = null;
            $inquiry_by = null;
            $value = doubleval(0);
            $amount = doubleval(0);
            $text = '';
            $currency_id = null;
            $ticket_status = null;
            $expire_at = "";
            $bankinfo_id = "";
            $wallet_id = null; // ticket-system-14

            switch ($type) {
                case self::TYPE_NEW:
                    $trade_market_ticket_parent_id = 0;
                    $inquiry_by = 0;
                    $value = $trade_profile->type == "Seller" ? $trade_profile->sellamount : $trade_profile->amount;
                    $amount = $trade_profile->type == "Seller" ? $trade_profile->amount : $trade_profile->max;
                    $text = $trade_profile->description;
                    $currency_id = $trade_profile->type == "Seller" ? $trade_profile->sellcurrency_id : $trade_profile->globalcurrency_id;
                    $trade_market_code = self::$trade_market_codes[$trade_profile->type];
                    $expire_at = Carbon::createFromFormat("Y-m-d", $trade_profile->expiration_date);
                    $wallet_id = $trade_profile->wallet_id; // ticket-system-14
                    $wallet_id_global = $trade_profile->wallet_id_global; // ticket-system-14
                    $bankinfo_id = $data['bankinfo_id'];
                    break;
                case self::TYPE_OFFER:
                    $offer_data = collect($data);

                    if ($offer_data->isNotEmpty()) {
                        $trade_market_ticket_parent_id = $offer_data->get("parent_id");
                        $inquiry_by = $offer_data->get("inquiry_by");
                        $value = $offer_data->get("value");
                        $amount = $offer_data->get("amount");
                        $text = $offer_data->get("text");
                        $currency_id = $offer_data->get("currency_id");
                        $wallet_id = $offer_data->get("wallet_id"); // ticket-system-14
                        $trade_profile_type = self::invertTradeType($trade_profile->type);
                        $trade_market_code = self::$trade_market_codes[$trade_profile_type];
                        $expire_at = Carbon::now()->addHour(24);
                        $trade_market_parent = self::find($trade_market_ticket_parent_id);
                        $bankinfo_id = $offer_data->get('bankinfo_id');

                        // if (!$trade_market_parent->hasBalance()) {
                        //     $trade_market_parent->close();
                        //     return $trade_market_parent;
                        // }

                        // if ($trade_market_parent->isExpired()) {
                        //     $trade_market_parent->close();
                        //     return $trade_market_parent;
                        // }
                    }
                    break;
            }
            $ticket_status = Status::getIdOf("TradeMarketTicket", "Open");
            $trade_profile_id = $trade_profile->tradeprofile_id;
            $ticket_count = $trade_profile->countTickets();
            $ticket_generated_id = self::createTicketNumber($trade_profile_id, $ticket_count, $trade_market_code);
            $ticket = [
                'trade_profile_id' => $trade_profile_id,
                'user_id' => $trade_profile->user_id,
                'trademarket_ticket_parent_id' => $trade_market_ticket_parent_id,
                'inquiry_by' => $inquiry_by,
                'value' => $value,
                'amount' => $amount,
                'text' => $text,
                'currency_id' => $currency_id,
                'ticket_generated_id' => $ticket_generated_id,
                'ticket_status' => $ticket_status,
                'expire_at' => $expire_at,
                'bankinfo_id' => $bankinfo_id,
                'wallet_id' => $wallet_id, // ticket-system-14
                'wallet_id_global' => $wallet_id_global??NULL,
            ];
        }
        return self::createTicket($ticket);
    }

    private function filterStatus() {
        return [
            Status::getIdOf("TradeMarketTicket", "Close"),
            Status::getIdOf("TradeMarketTicket", "Rejected"),
            Status::getIdOf("TradeMarketTicket", "Completed")
        ];
    }

    public static function active($active = true)
    {
        $relations = [
            "status",
            "trader_account", // not working ugh
            "inquiry_account", // not working ugh
            "parent",
            "parent.currency",
            "trade_profile.crypto_currency",
            "active_offers",
            "active_offers.parent",
            "active_offers.trade_profile.crypto_currency",
            "active_offers.currency",
            "active_offers.status",
            "active_offers.trader_account",
            "active_offers.inquiry_account",
            "active_offers.trade_market_ticket_deposit",
            "currency"
        ];
        $auth_id = Auth::guard("api")->id();
        $a = null;
        $b = null;

        if ($active) {
            $filter_status = [
                Status::getIdOf("TradeMarketTicket", "Close"),
                Status::getIdOf("TradeMarketTicket", "Rejected"),
                Status::getIdOf("TradeMarketTicket", "Cancelled"),
                Status::getIdOf("TradeMarketTicket", "Completed")
            ];

            $a = self::with($relations)
                ->whereNotIn("ticket_status", $filter_status)
                ->where("user_id", $auth_id)
                ->get();
            $b = self::with($relations)
                ->whereNotIn("ticket_status", $filter_status)
                ->where("inquiry_by", $auth_id)
                ->get();
        } else {
            $filter_status = [
                Status::getIdOf("TradeMarketTicket", "Close"),
                Status::getIdOf("TradeMarketTicket", "Rejected"),
                // Status::getIdOf("TradeMarketTicket", "Cancelled"),
                Status::getIdOf("TradeMarketTicket", "Completed")
            ];

            $a = self::with($relations)
                ->whereIn("ticket_status", $filter_status)
                ->where("user_id", $auth_id)
                ->get();
            $b = self::with($relations)
                ->whereIn("ticket_status", $filter_status)
                ->where("inquiry_by", $auth_id)
                ->get();
        }

        foreach ($b as $ticket) {
            $a->push($ticket);
        }
        $tickets = collect();

        if ($a->isEmpty()) {
            return $tickets;
        }

        foreach ($a as $ticket) {
            $ticket = collect($ticket);
            $offers = collect($ticket->get("active_offers"))->sortByDesc("created_at");
            $trader = Auth::guard("api")->user();
            $is_auth_trader = $trader->id == $ticket->get("user_id");

            if ($is_auth_trader && $offers->isNotEmpty()) {
                $tickets->push([
                    "offer" => null,
                    "trade_profile" => [
                        "value" => $ticket->get("value"),
                        "value_currency" => $ticket->get("currency")["currency"],
                        "amount" => $ticket->get("amount"),
                        "initial_amount" =>$ticket->get("trade_profile")["initial_amount"],
                        "amount_currency" => $ticket->get("trade_profile")["crypto_currency"]["currency"],
                    ],
                    "ticket_id" => $ticket->get("trademarket_ticket_id"),
                    "trader" => Accounts::find($ticket->get("user_id"))->getDisplayName(),
                    "trader_id" => $ticket->get("user_id"),
                    "type" => getTicketType($ticket->get("ticket_generated_id")),
                    "status" => $ticket->get("status")["status_name"],
                    "ticket_generated_id" => $ticket->get("ticket_generated_id"),
                    "expire_at" => $ticket->get("expire_at"),
                    "created_at" => $ticket->get("created_at"),
                ]);

                foreach ($offers as $offer) {
                    $offer = collect($offer);

                    $tickets->push([
                        "offer" => [
                            "id" => $offer->get("trademarket_ticket_id"),
                            "ticket_generated_id" => $offer->get("ticket_generated_id"),
                            "value" => $offer->get("value"),
                            "value_currency" => $offer->get("currency")["currency"],
                            "amount" => $offer->get("amount"),
                            "amount_currency" => $ticket->get("trade_profile")["crypto_currency"]["currency"],
                        ],
                        "trade_profile" => null,
                        "ticket_id" => $ticket->get("trademarket_ticket_id"),
                        "trader" => Accounts::find($offer->get("inquiry_by") ? $offer->get("inquiry_by") : $offer->get("user_id"))->getDisplayName(),
                        "trader_id" => $offer->get("inquiry_by") ? $offer->get("inquiry_by") : $offer->get("user_id"),
                        "type" => getTicketType($offer->get("ticket_generated_id")),
                        "status" => $offer->get("status")["status_name"],
                        "ticket_generated_id" => $ticket->get("ticket_generated_id"),
                        "expire_at" => $offer->get("expire_at"),
                        "created_at" => $offer->get("created_at"),
                    ]);
                }
            } else if ($is_auth_trader && $offers->isEmpty()) {
                if ($ticket->get("status")["status_name"] == 'Completed' || $ticket->get("status")["status_name"] == 'Rejected' || $ticket->get("status")["status_name"] == 'Cancelled') {
                    $tickets->push([
                        "offer" => [
                            "id" => $ticket->get("trademarket_ticket_id"),
                            "ticket_generated_id" => $ticket->get("ticket_generated_id"),
                            "value" => $ticket->get("value"),
                            "value_currency" => $ticket->get("currency")["currency"],
                            "amount" => $ticket->get("amount"),
                            "amount_currency" => $ticket->get("trade_profile")["crypto_currency"]["currency"],
                        ],
                        "trade_profile" => null,
                        "ticket_id" => $ticket->get("trademarket_ticket_id"),
                        "trader" => Accounts::find($ticket->get("inquiry_by") ? $ticket->get("inquiry_by") : $ticket->get("user_id"))->getDisplayName(),
                        "trader_id" => $ticket->get("inquiry_by"),
                        "type" => getTicketType($ticket->get("ticket_generated_id")),
                        "status" => $ticket->get("status")["status_name"],
                        "ticket_generated_id" => $ticket->get("ticket_generated_id"),
                        "expire_at" => $ticket->get("expire_at"),
                        "created_at" => $ticket->get("created_at"),
                    ]);
                    continue;
                }
                
                if ($ticket->get("trademarket_ticket_parent_id") != 0) {
                    continue;
                }

                $tickets->push([
                    "offer" => null,
                    "trade_profile" => [
                        "value" => $ticket->get("value"),
                        "value_currency" => $ticket->get("currency")["currency"],
                        "amount" => $ticket->get("amount"),
                        "initial_amount" =>$ticket->get("trade_profile")["initial_amount"],
                        "amount_currency" => $ticket->get("trade_profile")["crypto_currency"]["currency"],
                    ],
                    "ticket_id" => $ticket->get("trademarket_ticket_id"),
                    "trader" => Accounts::find($ticket->get("user_id"))->getDisplayName(),
                    "trader_id" => $ticket->get("user_id"),
                    "type" => getTicketType($ticket->get("ticket_generated_id")),
                    "status" => $ticket->get("status")["status_name"],
                    "ticket_generated_id" => $ticket->get("ticket_generated_id"),
                    "expire_at" => $ticket->get("expire_at"),
                    "created_at" => $ticket->get("created_at"),
                ]);
            } else if ($trader->id == $ticket->get("inquiry_by")) {
                $offer = collect($ticket);
                $ticket = collect($offer->get("parent"));

                $tickets->push([
                    "offer" => [
                        "id" => $offer->get("trademarket_ticket_id"),
                        "ticket_generated_id" => $offer->get("ticket_generated_id"),
                        "value" => $offer->get("value"),
                        "value_currency" => $offer->get("currency")["currency"],
                        "amount" => $offer->get("amount"),
                        "amount_currency" => $offer->get("trade_profile")["crypto_currency"]["currency"],
                    ],
                    "trade_profile" => null,
                    "ticket_id" => $ticket->get("trademarket_ticket_id"),
                    "trader" => Accounts::find($offer->get("inquiry_by"))->getDisplayName(), // This fixed trader name issue.
                    "trader_id" => $offer->get("inquiry_by"),
                    "type" => getTicketType($offer->get("ticket_generated_id")),
                    "status" => $offer->get("status")["status_name"],
                    "ticket_generated_id" => $ticket->get("ticket_generated_id"),
                    "expire_at" => $offer->get("expire_at"),
                    "created_at" => $offer->get("created_at"),
                ]);
            }
        }

        return $tickets->sortByDesc("created_at")->values()->all();
    }

    /**
     * Check if offer is sent
     * Applies on sub-ticket only otherwise will throw a ModelNotFoundException
     *
     * @return bool
     */
    public function isSent()
    {
        $offer = $this->offers()->where($this->primaryKey, $this->trademarket_ticket_id)->first();

        if (is_null($offer)) {
            throw new ModelNotFoundException; //must create custom exception
        }
        return $offer->ticket_status == Status::getIdOf($this->table, "Offer") && Auth::id() == $this->inquiry_by;
    }

    /**
     * Check if offer is received
     * Applies on sub-ticket only otherwise will throw a ModelNotFoundException
     *
     * @return bool
     */
    public function isReceived()
    {
        $offer = $this->offers()->where($this->primaryKey, $this->trademarket_ticket_id)->first();

        if (is_null($offer)) {
            throw new ModelNotFoundException; //must create custom exception
        }
        return $offer->ticket_status == Status::getIdOf($this->table, "Offer") && Auth::id() == $this->user_id;
    }

    /**
     * Create a new ticket in the db
     *
     * @param array $data
     * @return Model|null|static
     */
    public static function createTicket($data = [])
    {
        $trade_market_ticket = self::create($data);
        $trade_market_ticket->sendEmail();
        $trade_market_ticket->addToHistory(['note' => $data['text']]);

        if (!$trade_market_ticket->isParent()) {
            $trade_market_ticket->offer();
        }
        return $trade_market_ticket->details(["status"]);
    }

    /**
     * Send email depending on the ticket status
     *
     * @param array $data
     */
    public function sendEmail($data = [])
    {
        $table = $this->table;
        
        switch ($this->ticket_status) {
            case Status::getIdOf($table, "Open"):
               // Mail::send(new WebTicketWasOpenedEmail($this, $data));
                    $job = (new SendTicketEmail('WebTicketWasOpenedEmail',$this,$data))->delay(Carbon::now()->addMinutes(1))->onConnection('database');
                    dispatch($job);
                break;
            case Status::getIdOf($table, "Offer"):
                //Mail::send(new WebTicketHasAnOfferEmail($this, $data));
                    $job = (new SendTicketEmail('WebTicketHasAnOfferEmail',$this,$data))->delay(Carbon::now()->addMinutes(1))->onConnection('database');
                    dispatch($job);
                break;
            case Status::getIdOf($table, "Rejected"):
                //Mail::send(new WebTicketWasRejectedEmail($this, $data));
                $job = (new SendTicketEmail('WebTicketWasRejectedEmail',$this,$data))->delay(Carbon::now()->addMinutes(1))->onConnection('database');
                 dispatch($job);
                break;
            case Status::getIdOf($table, "Approved"):
                 $job = (new SendTicketEmail('WebTicketWasApprovedEmail',$this,$data))->delay(Carbon::now()->addMinutes(1))->onConnection('database');
                 dispatch($job);
               // Mail::send(new WebTicketWasApprovedEmail($this, $data));
                break;
            case Status::getIdOf($table, "Payment Made"):
                $job = (new SendTicketEmail('WebTicketHasPaymentEmail',$this,$data))->delay(Carbon::now()->addMinutes(1))->onConnection('database');
                 dispatch($job);
               // Mail::send(new WebTicketHasPaymentEmail($this, $data));
                break;
            case Status::getIdOf($table, "Payment Received"):
                  $job = (new SendTicketEmail('WebTicketWasPayedEmail',$this,$data))->delay(Carbon::now()->addMinutes(1))->onConnection('database');
                 dispatch($job);
               // Mail::send(new WebTicketWasPayedEmail($this, $data));
                break;
            case Status::getIdOf($table, "Close"):
                  $job = (new SendTicketEmail('WebTicketWasClosedEmail',$this,$data))->delay(Carbon::now()->addMinutes(1))->onConnection('database');
                 dispatch($job);
               // Mail::send(new WebTicketWasClosedEmail($this, $data));
                break;
            /* ticket system #14 */
            case Status::getIdof($table,'Completed'):
                 $job = (new SendTicketEmail('WEBTicketHasCompletedEmail',$this,$data))->delay(Carbon::now()->addMinutes(1))->onConnection('database');
                 dispatch($job);
                //Mail::send(new WEBTicketHasCompletedEmail($this, $data));
                break;
            /* end */
        }
    }

    /**
     * Check if user has already made an offer from a ticket
     *
     * @return bool
     */
    public function userHasOffer()
    {
        $offer = $this->offers()
            ->whereIn("ticket_status", [
                Status::getIdOf($this->table, "Offer"),
                Status::getIdOf($this->table, "Approved"),
                Status::getIdOf($this->table, "Payment Made")
            ])
            ->where("inquiry_by", Auth::guard("api")->id())
            ->first();
        return !is_null($offer);
    }

    /**
     * Check if a ticket is expired
     *
     * @return bool
     */
    public function isExpired()
    {
        $is_expired = $this->expire_at <= Carbon::now();
        $data = [];

        if ($is_expired) {
            Mail::send(new WebTicketHasExpiredEmail($this, $data));
            $this->close();
        }
        return $is_expired;
    }

    public function isOffer()
    {
        return $this->ticket_status == Status::getIdOf($this->table, "Offer") && !$this->isParent();
    }

    public function isOfferValid()
    {
        return in_array($this->ticket_status, [
            Status::getIdOf($this->table, "Offer"),
            Status::getIdOf($this->table, "Approved"),
            Status::getIdOf($this->table, "Payment Made"),
            Status::getIdOf($this->table, "Payment Received")
        ]);
    }

    /**
     * Check if a ticket is a parent
     *
     * @return bool
     */
    public function isParent()
    {
        return $this->trademarket_ticket_parent_id == 0;
    }

    /**
     * Check if a ticket amount reaches zero value
     *
     * @return bool
     */
    public function hasBalance()
    {
        $has_balance = $this->amount > config("bitcoin.zero_value");
        $data = [];

        if (!$has_balance) {
            Mail::send(new WebTicketReachesZeroValueEmail($this, $data));
            $this->close();
        }
        return $has_balance;
    }

    /**
     * Check if offer payment was received
     *
     * @return bool
     */
    public function isPaymentReceived()
    {
        return $this->ticket_status == Status::getIdOf($this->table, "Payment Received");
    }

    /**
     * Check if offer payment was made
     *
     * @return bool
     */
    public function isPaymentMade()
    {
        return $this->ticket_status == Status::getIdOf($this->table, "Payment Made");
    }

    /**
     * Check if offer was approved
     *
     * @return bool
     */
    public function isApproved()
    {
        return $this->ticket_status == Status::getIdOf($this->table, "Approved");
    }

    /**
     * Check if ticket/offer is open
     *
     * @return bool
     */
    public function isOpen()
    {
        return $this->ticket_status == Status::getIdOf($this->table, "Open");
    }

    /**
     * Check if ticket/offer is close
     *
     * @return bool
     */
    public function isClosed()
    {
        return $this->ticket_status == Status::getIdOf($this->table, "Close");
    }

    /**
     * Check if offer is rejected
     *
     * @return bool
     */
    public function isRejected()
    {
        return $this->ticket_status == Status::getIdOf($this->table, "Rejected");
    }

    /**
     * Check if ticket is dispute
     *
     * @return bool
     */
    public function isDispute()
    {
        return $this->ticket_status == Status::getIdOf($this->table, "Dispute");
    }

    /**
     * Check if a ticket has the given offer
     *
     * @param TradeMarketTicket $offer
     * @return bool
     */
    public function hasOffer(self $offer)
    {
        $filter_status = [
            Status::getIdOf("TradeMarketTicket", "Close"),
            Status::getIdOf("TradeMarketTicket", "Rejected"),
            Status::getIdOf("TradeMarketTicket", "Completed")
        ];

        $ticket = $this->offers()->where($this->primaryKey, $offer->trademarket_ticket_id)
            ->whereNotIn("ticket_status", $filter_status)
            ->first();
        return !is_null($ticket);
    }

    /**
     * List all offers from a ticket
     *
     * @return mixed
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * Add the ticket/offer to history
     *
     * @return $this|Model
     */
    public function addToHistory($data=[])
    {
        \Log::debug($data);
        return TradeMarketTicketHistory::create([
            $this->primaryKey => $this->trademarket_ticket_id,
            'ticket_status' => $this->ticket_status,
            'amount' => $this->amount,
            'note' => isset($data["note"]) ? $data["note"] : $this->notes
        ]);
    }

    /**
     * Set ticket as an offer
     *
     * @return $this
     */
    public function offer()
    {
        $this->ticket_status = Status::getIdOf($this->table, "Offer");
        $this->save();
        $this->sendEmail();
        $this->addToHistory(['note' =>'Set Offer']);
        return $this;
    }

    /**
     * Complete deposit information using attachment and notes to verify the payment
     *
     * @param TicketPaymentRequest $request
     * @return $this
     */
    public function payment(TicketPaymentRequest $request)
    {
        $deposit = TradeMarketTicketDeposit::find($request->deposit_id);

        if ($request->type == "Bank") {
            $attachment_urls = [];

            if ($request->hasFile('attachments')) {

                foreach ($request->attachments as $attachment) {
                    $attachment_url = makeAttachment($attachment, config('ticket.actions.deposit'), $deposit->id);
                    array_push($attachment_urls, $attachment_url);
                }
                $deposit->attachments = $attachment_urls;
            }
            $deposit->offer_notes = $request->offer_notes;
        } else if ($request->type == "Wallet") {
            //in this case
            //sender is the seller and
            //receiver is the buyer

            $transfer = WalletTransfer::create([
                'sender_id' => $deposit->sender_id,
                'sender_wallet_id' => $deposit->bank->wallet->wallet_id,
                'receiver_id' => $deposit->receiver_id,
                'receiver_wallet_id' => $request->wallet_id,
                'amount_transfer' => $this->value,
                'reason' => "TradeMarketTicketDeposit",
                'currency_id' => $deposit->bank->currency_id,
                'status_id' => Status::getIdOf("WalletTransfer", "Pending"),
            ]);
            $receiver = $transfer->receiver;
            $currency = $transfer->currency;
            $source = Source::where('source_name', "WalletTransfer")->first();
            $resource_id = $transfer->wallet_transfer_id;
            $application = Applications::where('name', 'Capital7')->first();
            $type = Wallet::TYPE_DISCHARGE;
            $wallet_account_number = $transfer->receiver_wallet->wallet_account_number;

            $wallet_ledger = Wallet::makeTransaction(
                $receiver,
                $this->value,
                $currency,
                $source,
                $resource_id,
                $application,
                $type,
                $wallet_account_number
            );

            if ($wallet_ledger) {
                $deposit->wallet_transfer_id = $transfer->wallet_transfer_id;
                $deposit->wallet_id = $request->wallet_id;
            }
        }
        $deposit->type = $request->type;
        $deposit->save();
        return $this;
    }

    /**
     * Show information of a ticket
     *
     * @param array $relations
     * @return Model|null|static
     */
    public function details($relations = [])
    {
        $default_relations = [
            "status",
            "trade_market_ticket_deposit",
            "trade_profile.crypto_currency",
            "currency",
            "offers"
        ];

        if (!empty($relations)) {
            $default_relations = $relations;
        }
        return $this->with($default_relations)
            ->latest()
            ->where($this->primaryKey, $this->trademarket_ticket_id)
            ->first();
    }

    /**
     * Reject an offer
     *
     * @return $this
     */
    public function reject($notes = '')
    {
        \Log::debug($this->text);
        $this->ticket_status = Status::getIdOf($this->table, "Rejected");
        $this->save();
        $this->sendEmail();
        $this->addToHistory([
            'note' => $notes != '' ? $notes : $this->text
        ]);
        return $this;
    }

    /**
     * Approve an offer
     *
     * @param BankInfo $bank
     * @return $this
     */
    public function approve(BankInfo $bank, $notes, $resend = false)
    {
        if (!$resend) {
            $offer_type = getTicketType($this->ticket_generated_id);

            if ($offer_type == "Buyer") {
                $this->tempCollectCoins();
            }
        }
        $this->deposit($bank, $notes);
        $this->ticket_status = Status::getIdOf($this->table, "Approved");
        $this->save();
        $this->sendEmail();
        $this->addToHistory(['note'=>$notes]);
        return $this;
    }

    public function tempCollectCoins($is_buyer = true)
    {

        if($is_buyer){
            $parent = $this->parent;
        } else {
            $parent = $this;
        }

        $amount = $this->amount;

        $trade_profile = $parent->trade_profile;
        $trade_profile->amount -= $amount;
        $trade_profile->save();

        // Returns 0 if the two operands are equal, 1 if the left_operand is larger than the right_operand, -1 otherwise.
        if (bccomp($trade_profile->amount, -0.00000000) == 0) {
            $trade_profile->amount = 0.00000000;
            $trade_profile->save();
        }
        $this->parent->amount -= $amount;
        $this->parent->save();

        if (bccomp($this->parent->amount, -0.00000000) == 0) {
            $this->parent->amount = 0.00000000;
            $this->parent->save();
        }
        return $parent;
    }

    public function returnTempCollectedCoins($is_buyer = true)
    {
        if($is_buyer){
            $parent = $this->parent;
        } else {
            $parent = $this;
        }

        $amount = $this->amount;

        $trade_profile = $parent->trade_profile;
        $trade_profile->amount += $amount;
        $trade_profile->save();

        $this->parent->amount += $amount;
        $this->parent->save();

        return $parent;
    }

    /**
     * Add new deposit entry
     * Contains bank information
     *
     * @param BankInfo $bank
     * @return $this|Model
     */
    public function deposit(BankInfo $bank, $notes)
    {
        return TradeMarketTicketDeposit::create([
            $this->primaryKey => $this->trademarket_ticket_id,
            'sender_id' => $this->user_id,
            'receiver_id' => $this->inquiry_by,
            "bank_id" => $bank->bankinfo_id,
            "notes" => $notes,
        ]);
    }

    /**
     * Payment was made
     *
     * @return $this
     */
    public function paymentMade($note)
    {
        $this->ticket_status = Status::getIdOf($this->table, "Payment Made");
        $this->save();
        $this->sendEmail();
        $this->addToHistory(['note' =>$note]);
        return $this;
    }

    /**
     * Payment was received
     *
     * @return $this
     */
    public function paymentReceived()
    {
        $this->ticket_status = Status::getIdOf($this->table, "Payment Received");
        $this->save();
        $this->sendEmail();
        $this->addToHistory(['note' =>'']);
//        $this->updateParentTicket();
        $this->pendingTransfer();
        return $this;
    }

    /**
     * Add entry on transfer
     * This is called automatically if the ticket status is payment received
     *
     * @return $this|Model
     */
    public function pendingTransfer()
    {
        return TradeMarketTicketTransfer::pending($this);
    }

    /**
     * Deduct the offer amount to the parent ticket
     * This also close the ticket if the value reaches 0
     * Handles buyer and seller
     *
     * @return mixed
     */
    public function updateParentTicket()
    {
        $parent = $this->parent;

        switch ($parent->getType()) {
            case "Buyer":
                $trade_profile = $parent->trade_profile;
                $trade_profile->amount += $this->amount;
                $trade_profile->save();

                $parent->amount += $this->amount;
                $parent->save();
                break;
            case "Seller":
                $trade_profile = $parent->trade_profile;
                $trade_profile->amount -= $this->amount;
                $trade_profile->save();

                $parent->amount -= $this->amount;
                $parent->save();

                if (!$parent->hasBalance()) {
                    $parent->close();
                }
                break;
        }
        return $parent;
    }

    /**
     * Close a trade
     *
     * @return $this
     */
    public function close()
    {
        if ($this->isParent()) {
            $trade_profile = $this->trade_profile;
            $trade_profile->status_id = Status::getIdOf("Tradeprofile", "Closed");
            $trade_profile->save();

            // if ($this->offers()->count()) {
            //     foreach ($this->offers as $offer) {
            //         $offer->reject();
            //     }
            // }
        }

        $this->ticket_status = Status::getIdOf($this->table, "Close");
        $this->save();
        $this->sendEmail();
        $this->addToHistory(['note' =>'']);
        return $this;
    }

    /**
     * Invert trade type depending on who's who
     *
     * @param $type
     * @return string
     */
    private static function invertTradeType($type)
    {
        return $type == "Buyer" ? "Seller" : "Buyer";
    }

    /**
     * Get ticket type
     *
     * @param $type
     * @return string
     */
    public function getType()
    {
        return getTicketType($this->ticket_generated_id);
    }

    /**
     * List tickets buy status
     *
     * @param null $status_id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getTicketByStatus($status_id)
    {
        return self::with("status")
            ->where("ticket_status", $status_id)
            ->get();
    }

    /**
     * Generate a ticket number
     * This depends if buyer or seller
     *
     * @param $trade_profile_id
     * @param $counter
     * @param $trade_market_code
     * @return string
     */
    private static function createTicketNumber($trade_profile_id, $counter, $trade_market_code)
    {
        $ticket_format = str_pad($trade_profile_id, 5, 0, STR_PAD_LEFT);
        $ticket_count_format = str_pad($counter, 5, 0, STR_PAD_LEFT);
        $ticket = $trade_market_code . $ticket_format . '-' . $ticket_count_format;
        return $ticket;
    }

    /**
     * Relationship to Tradeprofile
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trade_profile()
    {
        return $this->belongsTo(Tradeprofile::class, 'trade_profile_id', 'tradeprofile_id');
    }

    /**
     * Relationship to Accounts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trader_account()
    {
        return $this->belongsTo(Accounts::class, 'user_id', 'id')->select($this->account_cols);
    }

    /**
     * Relationship to Accounts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inquiry_account()
    {
        return $this->belongsTo(Accounts::class, 'inquiry_by', 'id')->select($this->account_cols);
    }

    /**
     * Relationship to self
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(self::class, 'trademarket_ticket_parent_id', $this->primaryKey);
    }

    /**
     * Relationship to self with status
     *
     * @return $this
     */
    public function offers()
    {
        $filter_status = [
            Status::getIdOf("TradeMarketTicket", "Close"),
            Status::getIdOf("TradeMarketTicket", "Rejected"),
            Status::getIdOf("TradeMarketTicket", "Completed")
        ];

        return $this->hasMany(self::class, 'trademarket_ticket_parent_id', $this->primaryKey)
            ->whereNotIn("ticket_status", $filter_status)
            ->with("status");
    }

    public function active_offers()
    {
        return $this->offers()
            ->where("ticket_status", "!=", Status::getIdOf($this->table, "Close"))
            ->where("ticket_status", "!=", Status::getIdOf($this->table, "Rejected"))
            ->where("ticket_status", "!=", Status::getIdOf($this->table, "Completed"))
        ;
    }

    /**
     * Relationship to Currency
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    /**
     * Relationship to Wallet
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }

    /**
     * Relationship to Status
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class, 'ticket_status', 'status_id')->select([
            "status_name",
            "status_id"
        ]);
    }

    /**
     * Relationship to TradeMarketTicketDeposit
     *
     * @return $this
     */
    public function trade_market_ticket_deposit()
    {
        return $this->hasMany(TradeMarketTicketDeposit::class, $this->primaryKey)
            ->latest()
            ->limit(1);
    }

    /**
     * Relationship to TradeMarketTicketTransfer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trade_market_ticket_transfer()
    {
        return $this->hasMany(TradeMarketTicketTransfer::class, 'trade_market_ticket_id', $this->primaryKey);
    }

    /**
     * Relationship to TradeMarketTicketHistory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(TradeMarketTicketHistory::class, $this->primaryKey);
    }

    public function SettoComplete($note) {
        $this->ticket_status = Status::getIdOf($this->table, "Completed");
        $this->save();
        $this->addToHistory(['note' => $note]);
        $this->sendEmail();
        return $this;
    }

    /* CP 206 */
    /**
     * Set ticket to dispute
     *
     * @param $notes
     * @return $this
     */
    public function createDispute($notes)
    {
        $this->ticket_status = Status::getIdOf($this->table, "Dispute");
        $this->save();
        // $this->sendEmail();
        $this->addToHistory(['note'=>$notes]);
        return $this;
    }

    public function getDisputeMessage() {
        $status =  Status::getIdOf($this->table, "Dispute");
        $history = TradeMarketTicketHistory::where('ticket_status','=',$status)->where('trademarket_ticket_id','=',$this->trademarket_ticket_id)->get();
        return $history;
    }
    public function DisputetoApprove($notes) {
        $depositdata =  $this->trade_market_ticket_deposit()->first();
        $depositdata->notes = $notes;
        $depositdata->save();
        $this->ticket_status = Status::getIdOf($this->table, "Approved");
        $this->save();
        $this->sendEmail();
        $this->addToHistory(['note'=>$notes]);
        return $this;
    }
    /* end CP-206 */
    public function scopeSearch($query, $search, $cols = null){
        return $query->leftJoin('crmstatus AS s', 'TradeMarketTicket.ticket_status', '=', 's.status_id')
            ->leftJoin('users AS u', 'TradeMarketTicket.user_id', '=', 'u.id')
            ->where('s.status_type', 'TradeMarketTicket')
            ->where('TradeMarketTicket.ticket_generated_id', 'ILIKE', "%$search%")
            ->orWhere('s.status_name', 'ILIKE', "%$search%")
            ->orWhere('u.username', 'ILIKE', "%$search%");
    }

    public function subticketcount(){
        return $this->hasMany(SubticketCount::class, 'trademarket_ticket_parent_id');
    }

}

<?php

namespace App\common;

use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Status;
use App\CRM\NinepineModels\Wallet;
use App\CRM\NinepineModels\Wallet_ledger;
use Illuminate\Database\Eloquent\Model;

class WalletWithdrawal extends Model
{
    protected $connection = 'bit';
    protected $table = 'WalletWithdrawal';
    protected $primaryKey = 'wallet_withdrawal_id';

    protected $fillable = [
        'attachments',
        'notes',
        'crm_note',
        'amount',
        'currency_id',
        'user_id',
        'status_id',
        'wallet_id',
        'bankinfo_id',
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function account()
    {
        return $this->belongsTo(Accounts::class, 'user_id');
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'resource_id', 'wallet_withdrawal_id');
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }

    public function bank()
    {
        return $this->belongsTo(BankInfo::class, 'bankinfo_id');
    }

    public static function getBankFromId($wallet_withdrawal_id)
    {
        $wallet_withdrawal = self::find($wallet_withdrawal_id);

        if (is_null($wallet_withdrawal)) {
            return null;
        }
        return $wallet_withdrawal->bank;
    }
}

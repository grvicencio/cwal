<?php

namespace App\common;

use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\CRMDepositInfo;
use App\CRM\NinepineModels\Status;
use App\CRM\NinepineModels\Wallet;
use App\CRM\NinepineModels\Wallet_ledger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WalletDeposit extends Model
{
    protected $connection = 'bit';
    protected $table = 'WalletDeposit';
    protected $primaryKey = 'wallet_deposit_id';

    protected $fillable = [
        'attachments',
        'notes',
        'amount',
        'currency_id',
        'user_id',
        'status_id',
        'crm_note',
        'wallet_id',
        'depositinfo_id',
    ];

    public function scopeSearch($query, $search, $cols = null)
    {
        if (is_null($cols)) {
            $except = [
                array_search('currency_id', $this->fillable),
                array_search('user_id', $this->fillable),
                array_search('status_id', $this->fillable),
                array_search('currency.currency', $this->fillable)
            ];

            $cols = array_except($this->fillable, $except);
        }

        foreach ($cols as $key => $value) {
            if ($key == 0) {
                $query->whereRaw("$value::text ILIKE '%$search%'");
            }
            $query->orWhereRaw("$value::text ILIKE '%$search%'");
        }
        return $query;
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function account()
    {
        return $this->belongsTo(Accounts::class, 'user_id');
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'resource_id', 'wallet_deposit_id');
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }

    public function depositinfo()
    {
        return $this->belongsTo(CRMDepositInfo::class, 'depositinfo_id');
    }
}

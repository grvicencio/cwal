<?php

namespace App\common;

use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Wallet;
use Illuminate\Database\Eloquent\Model;

class BankInfo extends Model
{
    protected $connection = 'bit';
    protected $table = 'BankInfo';
    protected $primaryKey = 'bankinfo_id';

    protected $fillable = [
        'user_id',
        'bank_name',
        'bank_branch',
        'bank_account_name',
        'bank_account_number',
        'bank_swift',
        'created_at',
        'updated_at',
        'is_default',
        'bank_address',
        'currency_id',
        'bank_address_2',
        'country',
        'bank_code',
        'wallet_id',
        'currency_id',
    ];

    public function account()
    {
        return $this->belongsTo(Accounts::class, 'user_id');
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }

    public function wallet_withdrawal()
    {
        return $this->hasMany(WalletWithdrawal::class, 'bankinfo_id');
    }

    public function trade_market_ticket_deposit()
    {
        return $this->hasMany(TradeMarketTicketDeposit::class, "bank_id", 'bankinfo_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}

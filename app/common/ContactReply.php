<?php

namespace App\common;

use Illuminate\Database\Eloquent\Model;

class ContactReply extends Model{
	protected $connection = 'bit';
	protected $table = 'ContactReply';
	protected $primaryKey = 'reply_id';
	protected $fillable = [
		'title',
		'message',
		'contact_id',
		'replied_by',
	];
}
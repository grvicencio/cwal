<?php

namespace App\common;

use App\CRM\baccarat\Accounts;
use Illuminate\Database\Eloquent\Model;

class UserReview extends Model{
	protected $connection = 'bit';
	protected $table = 'UserReview';
	protected $primaryKey = 'review_id';

	protected $fillable = [
		'user_id',
		'title',
		'description',
		'rating',
		'reviewer_id',
	];

	public function account(){
		return $this->belongsTo(Accounts::class, 'user_id');
	}

	public function reviewer(){
		return $this->belongsTo(Accounts::class, 'reviewer_id');
	}
}
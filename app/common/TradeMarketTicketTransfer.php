<?php

namespace App\common;

use App\CRM\NinepineModels\Status;
use App\CRM\NinepineModels\Wallet;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\CRM\NinepineModels\Source;

class TradeMarketTicketTransfer extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'TradeMarketTicketTransfer';
    protected $primaryKey = "trade_market_ticket_transfer_id";

    protected $fillable = [
        'trade_market_ticket_id',
        'sender_wallet_id',
        'receiver_wallet_id',
        'status_id',
    ];

    public static function getTransactions() {
        return DB::table("TradeMarketTicket")->select([
            "crmwalletledger.wallet_ledger_id as wl_id",
            "crmwalletledger.credit as credit",
            "crmwalletledger.debit as debit",
            "crmwalletledger.balance as balance",
            "crmwallet.wallet_account_number as wan",
            "crmcurrency.currency as currency",
            "crmwalletledger.created_at as created_at",
            // "crmstatus.status_name as status_name",
            "TradeMarketTicket.ticket_generated_id as ticket_generated_id",
        ])
        ->join("Tradeprofile", "TradeMarketTicket.trade_profile_id", "=", "Tradeprofile.tradeprofile_id")
        ->join("TradeMarketTicketHistory", "TradeMarketTicket.trademarket_ticket_id", "=", "TradeMarketTicketHistory.trademarket_ticket_id")
        ->join("crmstatus", "crmstatus.status_id", "=", "TradeMarketTicketHistory.ticket_status")
        ->join("crmcurrency", "crmcurrency.currency_id", "=", "Tradeprofile.cryptocurrency_id")
        ->join("crmwalletledger", "Tradeprofile.wallet_id", "=", "crmwalletledger.wallet_id")
        ->join("crmsources", "crmsources.source_id", "=", "crmwalletledger.source_id")
        ->join("crmwallet", "Tradeprofile.wallet_id", "=", "crmwallet.wallet_id")
        ->where("crmwallet.user_id", authUser()->get("id"))
        ->where("crmwalletledger.source_id", Source::getIdByName("TradeMarketTicketTransfer"))
        ->groupBy("crmwalletledger.wallet_ledger_id")
        ->groupBy("crmwalletledger.created_at")
        ->groupBy("crmwalletledger.credit")
        ->groupBy("crmwalletledger.balance")
        ->groupBy("crmwalletledger.debit")
        ->groupBy("crmwallet.wallet_account_number")
        ->groupBy("crmcurrency.currency")
        // ->groupBy("crmstatus.status_name")
        // ->groupBy("TradeMarketTicketHistory.trademarket_ticket_id")
        ->groupBy("TradeMarketTicket.ticket_generated_id")
        // ->groupBy("TradeMarketTicket.trademarket_ticket_id")
        ->orderBy("crmwalletledger.created_at", "desc")
        ->get();
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id')->select([
            "status_id", "status_name"
        ]);
    }

    public function trade_market_ticket()
    {
        return $this->belongsTo(TradeMarketTicket::class, 'trade_market_ticket_id', 'trademarket_ticket_id');
    }

    public function sender_wallet()
    {
        return $this->belongsTo(Wallet::class, 'sender_wallet_id', 'wallet_id')->select([
            "wallet_id",
            "currency_id",
            "user_id",
            "wallet_account_number",
            "wallet_alias"
        ]);
    }

    public function receiver_wallet()
    {
        return $this->belongsTo(Wallet::class, 'receiver_wallet_id', 'wallet_id')->select([
            "wallet_id",
            "currency_id",
            "user_id",
            "wallet_account_number",
            "wallet_alias"
        ]);
    }

    /**
     * Add new transfer entry with status pending by default
     *
     * @param TradeMarketTicket $ticket
     * @return $this|Model
     */
    public static function pending(TradeMarketTicket $ticket)
    {
        return self::create([
            'trade_market_ticket_id' => $ticket->trademarket_ticket_id,
            'sender_wallet_id' => $ticket->trade_profile->wallet_id,
            'receiver_wallet_id' => $ticket->wallet_id, // ticket-system-14
            'status_id' => Status::getIdOf((new self)->table, "Pending"), 
        ]);
    }
     /**
     * update status to Approve
     *
     * @param TradeMarketTicket $ticket
     * @return $this|Model
     */
    public static function Approve(TradeMarketTicket $ticket)
    {
         $status_id = Status::getIdOf('TradeMarketTicketTransfer', "Approve");
         
         $transfer =  TradeMarketTicketTransfer::where('trade_market_ticket_id','=',$ticket->trademarket_ticket_id)->first();
         $transfer->status_id = $status_id;
         $transfer->save();
       
    }
}

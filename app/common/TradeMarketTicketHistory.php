<?php

namespace App\common;

use App\CRM\NinepineModels\Status;
use Illuminate\Database\Eloquent\Model;

class TradeMarketTicketHistory extends Model
{
    protected $connection = 'bit';
    protected $table = 'TradeMarketTicketHistory';
    protected $primaryKey = 'trade_market_history_id';

    protected $fillable = [
        'trademarket_ticket_id',
        'ticket_status',
        'amount',
        'note',
    ];

    public function trade_market_ticket()
    {
        return $this->belongsTo(TradeMarketTicket::class, 'trademarket_ticket_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'ticket_status', 'status_id');
    }
}

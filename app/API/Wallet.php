<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    //
    protected $connection = 'bit_crm';
    protected $table = 'Wallet';
    protected $primaryKey = 'wallet_id';
    
    
}

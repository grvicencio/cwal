<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    //
    protected $connection = 'bit_crm';
    protected $table = 'Avatar';
    protected $primaryKey = 'avatar_id';
}

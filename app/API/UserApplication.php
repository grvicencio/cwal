<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class UserApplication extends Model
{
    //
    protected $table = 'UserApplications';
    protected $primaryKey = 'user_application_id';
   
    
    public function detail() {
        return $this->belongsto('App\API\Application', 'application_id' );
    }
}

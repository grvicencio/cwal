<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class MembershipInclusion extends Model
{
    //
     protected $table = 'MembershipInclusions';
    protected $primaryKey = 'membership_inclusion_id';
}

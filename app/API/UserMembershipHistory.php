<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class UserMembershipHistory extends Model
{
    //
     protected $table = 'UserMembershipHistory';
     protected $primaryKey = 'user_membership_history_id';
}

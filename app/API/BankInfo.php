<?php

namespace App\API;

use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Wallet;
use Illuminate\Database\Eloquent\Model;

class BankInfo extends Model
{
    protected $table = 'BankInfo';
    protected $primaryKey = 'bankinfo_id';

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }
}

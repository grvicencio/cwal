<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    //
    protected $table = 'Charges';
    protected $primaryKey = 'charge_id';
    
    public function Details() {
        
       return $this->hasMany('App\ChargeDetail','charge_id');
 
    }
}

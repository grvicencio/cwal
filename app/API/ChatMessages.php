<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class ChatMessages extends Model
{
     protected $table = 'ChatMessages';
    protected $primaryKey = 'chat_id';
    protected $fillable = [
        'sender_id', 'receiver_id', 'message'
    ];
}

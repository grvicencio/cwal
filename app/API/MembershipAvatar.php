<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class MembershipAvatar extends Model
{
    //
    protected $connection = 'bit_crm';
    protected $table = 'MembershipAvatar';
    protected $primaryKey = 'membreship_avatar_id';
}

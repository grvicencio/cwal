<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class TransactionStatus extends Model
{
    //
    protected $table = 'TransactionStatuses';
    protected $primaryKey = 'transaction_status_id';
}

<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class ChargeDetail extends Model
{
    //
    protected $table = 'ChargeDetails';
    protected $primaryKey = 'charge_details_id';
}

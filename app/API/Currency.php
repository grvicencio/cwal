<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    //
     protected $connection = 'bit_crm';
     protected $table = 'Currency';
    protected $primaryKey = 'currency_id';
   
}

<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class WalletLedger extends Model
{
    //
    protected $connection = 'bit_crm';
    protected $table = 'Wallet_ledger';
    protected $primaryKey = 'wallet_ledger_id';
}

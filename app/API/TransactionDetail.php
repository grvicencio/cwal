<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    //
    protected $table = 'TransactionDetails';
    protected $primaryKey = 'transaction_detail_id';
    
    public function Transaction() {
        return $this->hasOne('App\API\Transaction', 'transaction_id');
    }
}

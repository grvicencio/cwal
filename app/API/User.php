<?php

namespace App\API;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Laravel\Cashier\Billable;
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    use Billable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username','mobile','status_id','validation_key','first_name','last_name', 'birth_date','google2fa_secret', 'use_authenticator'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','google2fa_secret',
    ];
    
    public function Membership() {
         return $this->hasMany('App\API\UserMembership','user_id');
       
    }
    
    public function UserApplication() {
        return $this->hasMany('App\API\USerApplication','user_id');
        //return $this->hasone('App\UserApplication','user_id');
    }
    public function Paymentinfo() {
        return $this->hasone('App\API\PaymentInfo', 'user_id');
    }
    public function belongMembership() {
        return $this->belongsTo('App\API\UserMembership','user_id');
    }   
    public function findForPassport($identifier)
    {
        $d = $identifier;
       
        // $this->where('status_id','1')->Where('email', $identifier)->orWhere('username', $identifier)->orWhere('mobile', $identifier)->first();
        return $this->where('status_id','1')->Where( function($query) use ($d){
                        $query->orWhere('email', $d)->orWhere('username',$d)->orWhere('mobile', $d);
                 })->first();
                
    }
    
    public function BankInfo() {
         return $this->hasMany('App\API\BankInfo','user_id', "id");
    }

    public function Wallet() {
        return $this->hasMany(\App\CRM\NinepineModels\Wallet::class,'user_id', "id");
    }

    public function SecurityQuestion()
    {
        return $this->hasOne('App\API\SecurityQuestion', 'id', 'security_question_id');
    }
    public function isOnline()
    {
        return \Cache::has('user-is-online-' . $this->id);
    }
    
    /**
     * Ecrypt the user's google_2fa secret.
     *
     * @param  string  $value
     * @return string
     */
    public function setGoogle2faSecretAttribute($value)
    {
         $this->attributes['google2fa_secret'] = encrypt($value);
    }

    /**
     * Decrypt the user's google_2fa secret.
     *
     * @param  string  $value
     * @return string
     */
    public function getGoogle2faSecretAttribute($value)
    {
        return decrypt($value);
    }

    public function is2faAttribute()
    {
        return (bool) $this->attributes['google2fa_secret'];
    }
}

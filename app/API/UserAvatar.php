<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class UserAvatar extends Model
{
    //
    protected $table = 'User_Avatar';
    protected $primaryKey = 'user_avatar_id';
}

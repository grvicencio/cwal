<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class UserMembership extends Model
{
    //
    protected $table = 'UserMemberships';
    protected $primaryKey = 'user_membership_id';
    
    public function HasMembership() {
         return $this->hasMany('App\API\Membership','membership_id','membership_id');
    }
    public function belongstoMembership() {
        return $this->belongsTo('App\API\MembershipInclusion', 'membership_id');
    }
    
    
}

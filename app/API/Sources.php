<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Sources extends Model
{
    //
    protected $connection = 'bit_crm';
    protected $table = 'Sources';
    protected $primaryKey = 'source_id';
}

<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class ChargeType extends Model
{
    protected $table = 'ChargeTypes';
    protected $primaryKey = 'charge_type_id';
}

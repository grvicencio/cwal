<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;
use Sagalbot\Encryptable\Encryptable;
class PaymentInfo extends Model
{
    //prote
    use Encryptable;
    protected $table = 'PaymentInfo';
    protected $primaryKey = 'payment_info_id';
    
    protected $fillable = [
        'cvc', 'valid_thru', 'card_no','card_name','payment_method_id','user_id'
    ];
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'cvc', 'valid_thru', 'user_id', 'payment_info_id','payment_method_id'
    ];
    /**
     *  The attributes that need to be encrypted 
     */
    
    protected $encryptable = [
        'cvc','valid_thru','card_no'
    ];
    public function Method () {
        return $this->belongsto('App\API\PaymentMethod', 'payment_method_id' );
    }
    
    
}

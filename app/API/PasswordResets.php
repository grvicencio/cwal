<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class PasswordResets extends Model
{
    //
  	protected $connection = 'bit';
  	public $timestamps = false;
  	protected $autoincrement = false;
  	protected $primaryKey  = null;
    protected $table = 'password_resets';
 protected $fillable = array('email', 'token', 'created_at');

}

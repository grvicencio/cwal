<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class RegisterWallet extends Model
{
    //
    protected $connection = 'bit_crm';
    protected $table = 'RegisterWallet';
    protected $primaryKey = 'registration_id';
}

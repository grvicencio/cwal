<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Application extends Model{
	protected $table = 'Applications';
	protected $primaryKey = 'application_id';

	public static function getIdOf($name){
		return self::where('name', $name)
			->first()
			->application_id;
	}
}
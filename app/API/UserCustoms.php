<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class UserCustoms extends Model
{
    //
    protected $table = 'User_Customs';
    protected $primaryKey = 'user_custom_id';
}

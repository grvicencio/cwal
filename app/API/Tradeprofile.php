<?php

namespace App\API;

use App\common\TradeMarketTicket;
use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Status;
use App\CRM\NinepineModels\Wallet_ledger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\CRM\NinepineModels\Source;

class Tradeprofile extends Model{

    protected $connection = 'bit';
	protected $table = 'Tradeprofile';
	protected $primaryKey = 'tradeprofile_id';
	protected $fillable = [
		'type',
		'title',
		'description',
		'min',
		'max',
		'globalcurrency_id',
		'amount',
		'cryptocurrency_id',
		'user_id',
		'status_id',
		'expiration_date',
		'sellcurrency_id',
		'sellamount',
		'wallet_account_number',
		'parent_id',
		'wallet_id',
        'initial_amount',
        'wallet_id_global',
    ];
    
    public static function getTransactions() {
        return DB::table("TradeMarketTicket")->select([
            "crmwalletledger.wallet_ledger_id as wl_id",
            "crmwalletledger.credit as credit",
            "crmwalletledger.debit as debit",
            "crmwalletledger.balance as balance",
            "crmwallet.wallet_account_number as wan",
            "crmcurrency.currency as currency",
            "crmwalletledger.created_at as created_at",
            // "crmstatus.status_name as status_name",
            "TradeMarketTicket.ticket_generated_id as ticket_generated_id",
        ])
        ->join("Tradeprofile", "TradeMarketTicket.trade_profile_id", "=", "Tradeprofile.tradeprofile_id")
        // ->join("TradeMarketTicketHistory", "TradeMarketTicket.trademarket_ticket_id", "=", "TradeMarketTicketHistory.trademarket_ticket_id")
        ->join("crmstatus", "TradeMarketTicket.ticket_status", "=", "crmstatus.status_id")
        ->join("crmcurrency", "crmcurrency.currency_id", "=", "Tradeprofile.cryptocurrency_id")
        ->join("crmwalletledger", "Tradeprofile.wallet_id", "=", "crmwalletledger.wallet_id")
        ->join("crmsources", "crmsources.source_id", "=", "crmwalletledger.source_id")
        ->join("crmwallet", "Tradeprofile.wallet_id", "=", "crmwallet.wallet_id")
        ->where("crmwallet.user_id", authUser()->get("id"))
        ->where("crmwalletledger.source_id", Source::getIdByName("Tradeprofile"))
        ->groupBy("crmwalletledger.wallet_ledger_id")
        ->groupBy("crmwalletledger.created_at")
        ->groupBy("crmwalletledger.credit")
        ->groupBy("crmwalletledger.balance")
        ->groupBy("crmwalletledger.debit")
        ->groupBy("crmwallet.wallet_account_number")
        ->groupBy("crmcurrency.currency")
        // ->groupBy("crmstatus.status_name")
        // ->groupBy("TradeMarketTicketHistory.trademarket_ticket_id")
        ->groupBy("TradeMarketTicket.ticket_generated_id")
        // ->groupBy("TradeMarketTicket.trademarket_ticket_id")
        ->orderBy("crmwalletledger.created_at", "desc")
        ->get();
    }

    protected $casts = [
        "amount" => "double",
        "sellamount" => "double"
    ];

	public function global_currency() {
        return $this->belongsTo(Currency::class, 'globalcurrency_id', 'currency_id');
    }

	public function crypto_currency() {
        return $this->belongsTo(Currency::class, 'cryptocurrency_id', 'currency_id');
    }

	public function user() {
        return $this->belongsTo(Accounts::class, 'user_id', 'id');
    }

	public function status() {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'resource_id', 'tradeprofile_id');
    }

    public function trade_market_ticket()
    {
        return $this->hasMany(TradeMarketTicket::class, 'trade_profile_id', 'tradeprofile_id');
    }

    public function isOpen()
    {
        return $this->status_id == Status::getIdOf($this->table, "Open");
    }

    public function isClosed()
    {
        return $this->status_id == Status::getIdOf($this->table, "Closed");
    }

    public function countTickets()
    {
        return TradeMarketTicket::where("trade_profile_id", $this->tradeprofile_id)->count();
    }
}
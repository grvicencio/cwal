<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    //
    protected $table ='PaymentMethods';
    protected $primaryKey = 'payment_method_id';
    

}

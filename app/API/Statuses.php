<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Statuses extends Model
{
    //
    protected $connection = 'bit_crm';
    protected $table = 'Statuses';
    protected $primaryKey = 'status_id';
    
    
    public function scopeGetStatus($query,$status_type, $status_name) {
        
        return $query->where('status_type','=', $status_type)->where('status_name','=',$status_name);
    }
}

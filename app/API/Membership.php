<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    //
    protected $table = 'Memberships';
    protected $primaryKey = 'membership_id';
    
    public function Inclusion() {
        
       return $this->hasMany('App\API\MembershipInclusion','membership_id','membership_id');
 
    }
}

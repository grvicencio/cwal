<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class TransactionLog extends Model
{
    //
    protected $table = 'TransactionLogs';
    protected $primaryKey = 'transaction_log_id';
}

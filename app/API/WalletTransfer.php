<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class WalletTransfer extends Model
{
    //
    protected $connection = 'bit_crm';
    protected $table = 'WalletTransfer';
    protected $primaryKey = 'wallet_transfer_id';
}

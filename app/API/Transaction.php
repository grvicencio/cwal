<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
      protected $table = 'Transactions';
      protected $primaryKey = 'transaction_id';
      
      public function Detail() {
          return $this->hasOne('App\API\TransactionDetail', 'transaction_id');
      }
}

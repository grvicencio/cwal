<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestion extends Model
{
    protected $table = 'SecurityQuestions';
    protected $primaryKey = 'id';
}

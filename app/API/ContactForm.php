<?php

namespace App\API;

use App\CRM\baccarat\Accounts;
use Illuminate\Database\Eloquent\Model;

class ContactForm extends Model{
    protected $connection = 'bit';
    protected $table = 'ContactForm';
    protected $primaryKey = 'contact_id';
    protected $fillable = [
        'subject',
        'type',
        'message',
        'user_id',
        'source'
    ];

    public function account(){
        return $this->belongsTo(Accounts::class, 'user_id');
    }

    public function scopeSearch($query, $search, $cols = null){
        return $query->where('type', 'ILIKE', "%$search%")
            ->orWhere('subject', 'ILIKE', "%$search%")
            ->orWhere('message', 'ILIKE', "%$search%");
    }

    public function getTypeAttribute($value){
    	return strtoupper($value);
    }

    public function getCreatedAtAttribute($value){
    	return date("F d, Y", strtotime($value)) . " &nbsp; &nbsp; " . date("h:i A", strtotime($value));
    }

    public function getMessageAttribute($value){
    	$str = substr($value, 0, 35);
    	$str .= strlen($value) > 35 ? "..." : "";

    	return $str;
    }
}
<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'crmstatus';
    protected $primaryKey = 'status_id';
}

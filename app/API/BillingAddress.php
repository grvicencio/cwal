<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class BillingAddress extends Model
{
    //
    protected $table = 'BillingAddress';
    protected $primaryKey = 'billing_address_id';
}

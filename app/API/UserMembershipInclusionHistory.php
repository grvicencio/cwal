<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class UserMembershipInclusionHistory extends Model
{
    //
      protected $table = 'UserMembreshipInclusionHistory';
      protected $primaryKey = 'user_membership_inclusion_history_id';
}

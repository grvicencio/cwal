<?php

namespace App\Mail;

use App\API\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WebSubmitReview extends Mailable{
	use Queueable, SerializesModels;

	protected $user;

	public function __construct($user){
		$this->user = $user;
	}

	public function build(){
		$test = array(
			'from' => config('mail.from.address'), config('mail.from.name'),
			'to' => $this->user->email
		);

		\Log::info(json_encode($test));

		return $this->view('layouts.email')
			->from(config('mail.from.address'), config('mail.from.name'))
			->subject('Capital7 | User Review')
			->to($this->user->email);
	}
}
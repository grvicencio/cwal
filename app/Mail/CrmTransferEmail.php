<?php

namespace App\Mail;

use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CrmTransferEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $sender;
    protected $receiver;
    protected $amount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Accounts $sender, Accounts $receiver, $amount)
    {
        $this->sender = $sender;
        $this->receiver = $receiver;
        $this->amount = $amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = EmailTemplate::where("email_type", "CRM_TRANSFER")->first();

        $content = render($email_template->content, [
            'sender' => $this->sender->getDisplayName(),
            'receiver' => $this->receiver->getDisplayName(),
            'amount' => $this->amount,
            'app_url' => config("app.url") . '/admin/login',
            'app_name' => config("app.name")
        ]);

        return $this->view("layouts.email", compact('content'))
            ->subject($email_template->subject)
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->to(config('mail.from.transfer.address'), config('mail.from.transfer.name'));
    }
}

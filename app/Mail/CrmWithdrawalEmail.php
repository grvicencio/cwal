<?php

namespace App\Mail;

use App\common\WalletWithdrawal;
use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CrmWithdrawalEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $account;
    protected $bank;
    protected $amount;
    protected $notes;
    protected $symbol;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(WalletWithdrawal $withdrawal)
    {
       
        $amount = $withdrawal->currency->currency_symbol . ' ' . number_format($withdrawal->amount, 8);
        $this->account = $withdrawal->account;
        $this->bank = $withdrawal->bank;
        $this->amount = $amount;
        $this->notes = $withdrawal->notes;
        $this->symbol = $withdrawal->currency->currency_symbol;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = EmailTemplate::where("email_type", "CRM_WITHDRAWAL")->first();

        $content = render($email_template->content, [
            'name' => $this->account->getDisplayName(),
            'amount' => $this->amount,
            'notes' => $this->notes,
            'app_url' => config("app.url") . '/admin/login',
            'app_name' => config("app.name"),
            'bank_name' => $this->bank->bank_name,
            'bank_branch' => $this->bank->bank_branch,
            'bank_address' => $this->bank->bank_address,
            'bank_address_2' => $this->bank->bank_address_2,
            'bank_account_name' => $this->bank->bank_account_name,
            'bank_account_number' => $this->bank->bank_account_number,
            'bank_swift' => $this->bank->bank_swift,
            'bank_currency' =>$this->symbol,
           // 'bank_currency' => $this->bank->wallet->currency->currency,
            'bank_code' => $this->bank->bank_code,
            'bank_country' => $this->bank->bank_country,
        ]);

        return $this->view("layouts.email", compact('content'))
            ->subject($email_template->subject)
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->to(config('mail.from.withdrawal.address'), config('mail.from.withdrawal.name'));
    }
}

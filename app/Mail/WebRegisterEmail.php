<?php

namespace App\Mail;

use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WebRegisterEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $receiver;
    protected $message;
    protected $amount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Accounts $receiver, $message, $amount)
    {
        $this->receiver = $receiver;
        $this->message = $message;
        $this->amount = $amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = EmailTemplate::where("email_type", "WEB_REGISTER")->first();

        $content = render($email_template->content, [
            'receiver' => $this->receiver->getDisplayName(),
            'message' => $this->message,
            'amount' => $this->amount,
            'app_url' => config("app.url"),
            'app_name' => config("app.name")
        ]);

        return $this->view("layouts.email", compact('content'))
            ->subject($email_template->subject)
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->to($this->receiver->email, $this->receiver->getDisplayName());
    }
}

<?php

namespace App\Mail;

use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WebAccountActivationEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;
    protected $activation_code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $activation_code)
    {
        $this->data = $data;
        $this->activation_code = $activation_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = EmailTemplate::where("email_type", "WEB_ACCOUNT_ACTIVATION")->first();
        $account = Accounts::where('email', $this->data["email"])->first();
        $link = config('app.url') . "/activate/" . $this->activation_code;

        $content = render($email_template->content, [
            'name' => $account->getDisplayName(),
            'link' => $link,
            'app_name' => config("app.name")
        ]);

        return $this->view("layouts.email", compact('content'))
            ->subject($email_template->subject)
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->to($account->email, $account->getDisplayName());
    }
}

<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\API\User;
class ChatEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $data;
    public $receiver;
    public $sender_profile;
    public function __construct($data, $receiver,  $user)
    {
        //
        $this->receiver = $receiver;
        $this->sender_profile = array('id'=>$user->id, 'first_name'=>$user->first_name, 'last_name'=>$user->last_name, 'avatar' => $user->account_image);
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        //return new PrivateChannel('test-channel');
        return ['private_chat.'. $this->receiver];
    }
    
    
}

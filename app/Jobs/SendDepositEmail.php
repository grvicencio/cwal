<?php

namespace App\Jobs;
use App\CRM\baccarat\Accounts;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\CrmDepositEmail;
class SendDepositEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $deposit;
    protected $amount;
    protected $notes;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($deposit, $amount, $notes)
    {
        $this->deposit = $deposit;
        $this->amount = $amount;
        $this->notes = $notes;
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
         \Mail::send(new CrmDepositEmail($this->deposit->account, $this->amount, $this->notes));
    }
}

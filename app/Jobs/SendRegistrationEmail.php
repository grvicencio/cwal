<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\WebAccountActivationEmail;
class SendRegistrationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $body_data;
    protected $activation_code;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($body_data,$activation_code)
    {
        //
        $this->body_data = $body_data;
        $this->activation_code = $activation_code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        \Mail::send(new WebAccountActivationEmail($this->body_data, $this->activation_code));
    }
}

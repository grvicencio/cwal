<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\WebTicketHasAnOfferEmail ;
use App\Mail\WebTicketWasOpenedEmail;
class SendTicketEmail implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $thisobject;
    protected $data;
    protected $object;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($object,$thisobject,$data)
    {
        //
        $this->thisobject = $thisobject;
        $this->data = $data;
        $this->object = $object;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $string = 'App\\Mail\\'.$this->object; //$this->object
        \Mail::send(new $string($this->thisobject, $this->data));
        //Mail::send(new WebTicketHasAnOfferEmail($this->thisobject, $this->data));
    }
}

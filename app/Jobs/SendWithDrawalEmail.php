<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\CrmWithdrawalEmail;
use App\common\WalletWithdrawal;
class SendWithDrawalEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $withdrawal;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( WalletWithdrawal $withdrawal)
    {
        
        $this->withdrawal = $withdrawal;
        
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        \Mail::send(new CrmWithdrawalEmail($this->withdrawal));
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('usercan', function ($expression) {
            list($permission, $route_name) = explode(', ', $expression);
            return "<?php if (Auth::guard('crm')->user()->canAccess($permission, $route_name)) { ?>";
        });

        Blade::directive('cant', function () {
            return "<?php } else { ?>";
        });

        Blade::directive('endusercan', function () {
            return "<?php } ?>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

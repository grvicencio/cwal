<?php
/**
 * Created by PhpStorm.
 * User: NPT 8
 * Date: 2/13/2018
 * Time: 10:48 AM
 */

namespace App\Providers;
namespace App\Validators;


class ReCaptcha
{

    public function validate(
        $attribute,
        $value,
        $parameters,
        $validator
    ){

        $client = new Client();

        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            ['form_params'=>
                [
                    'secret'=>env('GOOGLE_RECAPTCHA_SECRET'),
                    'response'=>$value
                ]
            ]
        );

        $body = json_decode((string)$response->getBody());
        return $body->success;
    }
}
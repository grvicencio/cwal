<?php

namespace App\Cards;

use Illuminate\Database\Eloquent\Model;

class Shoes extends Model
{
    //
     protected $table = 'Shoe';
      protected $primaryKey = 'shoe_id';
}

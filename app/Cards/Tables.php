<?php

namespace App\Cards;

use Illuminate\Database\Eloquent\Model;

class Tables extends Model
{
    //
     protected $table = 'Tables';
      protected $primaryKey = 'table_id';
}

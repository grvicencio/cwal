<?php

namespace App\Cards;

use Illuminate\Database\Eloquent\Model;

class TableShoeResult extends Model
{
    //
     protected $table = 'TableResult';
      protected $primaryKey = 'table_result_id';
}
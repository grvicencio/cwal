<?php

namespace App\Cards;

use Illuminate\Database\Eloquent\Model;

class TableShoes extends Model
{
    //
     protected $table = 'TableShoes';
      protected $primaryKey = 'tableshoe_id';
}

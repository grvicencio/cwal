<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Session;
use Closure;

class CheckSession
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (!session()->has('access_token')) {
            return redirect('/logout');
        }

        return $next($request);
    }
}

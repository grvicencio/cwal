<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Response;
class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'username' => 'min:8|max:16|unique:users',
            'email' =>'email|unique:users',
             
            //
        ];
    }
     public function response(array $errors) {
        $err['error'] = $errors;
        return Response::json($err, 400);
        
    }
}

<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Response;
class WebRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'repeat_password' =>'required|same:password',
            // 'first_name' =>'required',
            // 'last_name'=>'required',
            // 'username' =>'required|min:8|unique:users'
            //
        ];
    }
    
    public function response(array $errors) {
        $err['error'] = $errors;
        return Response::json($err, 400);
        
    }
}

<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Response;
class BillingAddress extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' =>'reauired',
            'country' =>'required',
            'city' =>'required',
            'billing_address' =>'required',
            'postalcode' =>'requried',
            'phone' =>"required"
            //
        ];
    }
    public function response(array $errors) {
        $err['error'] = $errors;
        return Response::json($err, 400);
       
    } 
}

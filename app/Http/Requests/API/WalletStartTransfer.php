<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Response;
class WalletStartTransfer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'uid' =>'required',
            'wallet_account_number' =>'required',
            'amount' => 'required',
            'password' =>'required',
            'time' =>"required"
        ];
    }
     public function response(array $errors) {
        $err['error'] = $errors;
        return Response::json($err);
        
    }
}

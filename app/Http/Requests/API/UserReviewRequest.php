<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class UserReviewRequest extends FormRequest{
	public function authorize(){
		 return true;
	}

	public function rules(){
		return [
			'title' =>'required',
			'description' =>'required',
			'rating' => 'required'
		];
	}

	public function response(array $errors){
		return response()->json([
			config('response.status') => config('response.type.error'),
			config('response.errors') => $errors
		], 400);
	}
}
<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class GetResetToken extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'birth_date' => 'required',
        ];
    }

//    public function message() {
//        return [
//            //
//        ];
//    }

    public function response(array $errors){
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ], 400);
    }
}

<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Response;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->has_changed_username){
            return [
                'username' => 'min:8|unique:users,username,' . $this->id . ',id'
                // 'email' =>'email|unique:users',
                //
            ];
        } else{
            return [];
        }
    }
    public function response(array $errors) {
        $err['error'] = $errors;

        return Response::json($err, 400);
    }
    
}

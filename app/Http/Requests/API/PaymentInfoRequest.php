<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Response;
class PaymentInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_no' =>'required',
            'card_name' =>'required',
            'cvc'=>'required|min:2',
            'valid_thru' => 'required',
            //'payment_method' => 'required|integer'
            //
        ];
    }
    public function message() {
        return [
            'card_no.required' =>' Card Number is required',
            'card_name.required' =>'Card Name is requried',
            'cvc.required' =>'CVC is required',
            'cvc.min' =>' CVC minimum is 2',
            'valid_thru' =>'Valid Thru is required',
            //'payment_method.required' =>' Payment method is required',
            //'oayment_method_id.integer' => 'Payment method should be numeric'
        ];
    }
    public function response(array $errors) {
        $err['error'] = $errors;
        return Response::json($err, 400);
        
    }
}

<?php

namespace App\Http\Requests\API;

use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Wallet;
use App\common\BankInfo;
use App\CRM\baccarat\Accounts;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Response;
class BankUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(!$this->wallet_account_number){
            $inapp = '';
        } else {
            // currency will not be saved on bank, it will be now linked on wallet page
            $inapp = 'required';
        }


        $store = [
            'bankinfo_id' => '',
            'is_default' => '',
            'bank_name' => $inapp . '',
            'bank_branch' =>'',
            'bank_address' => $inapp . '',
            'bank_address_2' =>'',
            'country' =>'',
            'bank_code' =>'',
            'bank_account_name' =>'required',
            'bank_swift' => 'unique:bit.BankInfo,bank_swift',
            'wallet_account_number' => '',
        ];

        $update = [
            'bankinfo_id' => 'required',
            'is_default' => '',
            'bank_name' => $inapp . '',
            'bank_branch' =>'',
            'bank_address' => $inapp . '',
            'bank_address_2' =>'',
            'country' =>'',
            'bank_code' =>'',
            'bank_account_name' =>'required',
            'bank_swift' => '|unique:bit.BankInfo,bank_swift,' . $this->bankinfo_id . ',bankinfo_id',
            'wallet_account_number' => '',
        ];

        $bank_not_in = BankInfo::where('bank_account_number', '!=', $this->bank_account_number)
            ->where('bank_name', '!=', $this->bank_name)
            ->pluck('bank_account_number')
            ->toArray();
        $implode_not_in = implode(',', $bank_not_in);

        $store['bank_account_number'] = "required|not_in:$implode_not_in";
        $update['bank_account_number'] = "required|not_in:$implode_not_in";

        $bank_in = BankInfo::where('bank_account_number', $this->bank_account_number)->first();
        $bank_account_numbers_in = [];

        if (!is_null($bank_in)) {
            array_push($bank_account_numbers_in, $bank_in->bank_account_number);
            $implode_in = implode(',', $bank_account_numbers_in);

            if ($bank_in->bank_name != $this->bank_name) {
                $store['bank_account_number'] = "required|in:$implode_in";
                $update['bank_account_number'] = "required|in:$implode_in";
            }
        }

        if (empty($this->bankinfo_id)) {
            return $store;
        }
        return $update;
    }
      public function message() {
        return [
            'bank_name.required' =>'Bank Name  is required',
            'bank_address.required' =>'Bank Address is required',
            'bank_account_name.required' =>'Account Name is required',
            'bank_account_number.required' =>'Account Number is required',
            'bank_swift.required' =>'Bank Swift is required',
           
        ];
    }
     public function response(array $errors) {
        $err['error'] = $errors;
        return Response::json($err, 400);
       
    } 
}

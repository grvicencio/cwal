<?php

namespace App\Http\Requests\API;

use App\common\TradeMarketTicket;
use App\CRM\NinepineModels\Status;
use App\CRM\NinepineModels\Wallet;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Response;

class TradeprofileRequest extends FormRequest{
    public function authorize(){
        return true;
    }

    public function rules(){
        if ($this->type == "Seller") {
            $wallet = Wallet::findByAccountNumber($this->wallet_account_number);
            $max = $wallet->balance;

            return [
                'type' => 'required',
                'min' => 'numeric',
                'max' => 'numeric',
                'wallet_account_number' => 'required',
                'sellamount' => 'numeric|min:0.00000001|between:0.00000000,1000000000000.00000000',
                'amount' => "numeric|min:0.00000001|max:$max",
            ];
        }

        return [
            'type' => 'required',
            // 'min' => 'numeric',
            'max' => 'numeric',
            'amount' => 'required|numeric|min:0.00000001|between:0.00000000,1000000000000.00000000',
            'wallet_account_number' => 'required',
            // 'sellamount' => 'numeric|min:0.00000001|between:0.00000000,1000000000000.00000000',
        ];
    }

    public function response(array $error){
        $err['error'] = $error;

        return Response::json($err, 400);
    }
}
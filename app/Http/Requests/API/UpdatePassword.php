<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Response;
class UpdatePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|min:8',
            'new_password' => 'required|min:8|different:old_password',
            'repeat_password' =>'same:new_password'
            //
        ];
    }
     public function response(array $errors) {
        $err['error'] = $errors;
        return Response::json($err, 400);
        
    }
}

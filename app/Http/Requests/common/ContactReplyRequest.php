<?php

namespace App\Http\Requests\common;

use Illuminate\Foundation\Http\FormRequest;

class ContactReplyRequest extends FormRequest{
	public function authorize(){
		return true;
	}

	public function rules(){
		return [
			'message' => 'required',
			'contact_id' => 'required',
		];
	}

	public function response(array $errors){
		return response()->json([
			config('response.status') => config('response.type.error'),
			config('response.errors') => $errors
		]);
	}
}
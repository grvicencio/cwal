<?php

namespace App\Http\Requests\common;

use Illuminate\Foundation\Http\FormRequest;

class DepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric|min:0.00000001|between:0.00000000,1000000000000.00000000',
            // 'notes' => 'required',
            'wallet_id' => 'required',
            // 'attachments' => 'image|mimes:jpeg,jpg,png|max:2000'
//            'attachments.*' => 'image|mimes:jpeg,jpg,png|max:2000'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }
}

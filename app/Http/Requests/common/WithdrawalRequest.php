<?php

namespace App\Http\Requests\common;

use App\CRM\baccarat\Accounts;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class WithdrawalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // empty? WEB else API
        $user_id = authUser()->isEmpty() ? Auth::guard("api")->id() : authUser()->get("id");
        $balance = Accounts::find($user_id)->wallet()->where("wallet_id", $this->wallet_id)->first()->balance;
        $max = number_format($balance, 8, '.', '');
//        $min = config("bitcoin.min_value");

        return [
            'amount' => "required|regex:/^\d*(\.\d{1,8})?$/|numeric|min:0.00000001|max:$max",
            'bankinfo_id' => 'required',
            'notes' => '',
            'wallet_id' => 'required'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }

    public function messages() {
        return [
            'amount.max' => 'Insufficient funds.',
        ];
    }
}

<?php

namespace App\Http\Requests\common;

use Illuminate\Foundation\Http\FormRequest;

class TicketBuyerAccept extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $attachments = count($this->input('attachments'));

        foreach(range(0, $attachments) as $index) {
            $rules['attachments.' . $index] = 'image|mimes:jpeg,bmp,png,jpg|max:2000';
        }
        return $rules;
    }

    public function response(array $errors)
    {
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }

    public function messages()
    {
        return [
            "attachments.*.required" => "The attachment field is required.",
            "attachments.*.image" => "The attachment field must be an image.",
            "attachments.*.mimes" => "The attachment field must be a file type of jpeg ,bmp ,png ,jpg.",
            "attachments.*.max" => "The attachment field must not be greater than 2 megabytes.",
        ];
    }
}

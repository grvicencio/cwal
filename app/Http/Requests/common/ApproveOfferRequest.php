<?php

namespace App\Http\Requests\common;

use Illuminate\Foundation\Http\FormRequest;

class ApproveOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "bank_id" => "required",
            "notes" => ""
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }

    public function messages()
    {
        return [
            "bank_id.required" => "Please select a bank."
        ];
    }
}

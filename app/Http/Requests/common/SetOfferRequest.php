<?php

namespace App\Http\Requests\common;

use App\common\TradeMarketTicket;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SetOfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ticket = $this->route("ticket");
        $inquiry_by_in = implode(',', [Auth::guard("api")->id()]);
        $max_amount = $ticket->amount;
        $min_amount = config("bitcoin.min_value");
        $min_value = 1;

        return [
            "inquiry_by" => "required|in:$inquiry_by_in",
            "value" => "required|numeric|min:$min_value",
            "amount" => "required|numeric|max:$max_amount|min:$min_amount",
            "text" => "required",
            "currency_id" => "required",
            "wallet_id" =>"required"
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }

    public function messages()
    {
        return [
            "inquiry_by.in" => "You can't make offer to yourself.",
        ];
    }
}

<?php

namespace App\Http\Requests\common;

use Illuminate\Foundation\Http\FormRequest;

class TransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return authUser()->isNotEmpty();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wallet_account_number' => 'required',
            'amount' => 'required|numeric|min:0.00000001|between:0.00000000,1000000000000.00000000',
            'email' => 'required|email|not_in:' . authUser()->get('email'),
            'recipient_wallet_account_number' => 'required',
            'password' => '',
            'reason' => 'required',
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }
}

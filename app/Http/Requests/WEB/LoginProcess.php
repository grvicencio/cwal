<?php

namespace App\Http\Requests\WEB;

use Illuminate\Foundation\Http\FormRequest;

class LoginProcess extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'username' => "required|email",
            'password' => "required"
        ];
    }

    public function messages(){
        return [
            // 'username'  => 'Invalid username or password!',
            // 'password.min'  => 'Password minimum of 8 characters!'
        ];
    }

    public function response(array $error){
        return $error;
    }
}

<?php

namespace App\Http\Requests\WEB;

use Illuminate\Foundation\Http\FormRequest;

class UserRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'mobile' =>'required|min:3',
            'email' =>'required|email',
            // 'password' =>' required|confirmed',
            // 'repeat_password' =>' required|same:password'
            //
        ];
    }
    public function messages() {
        return [
            // 'mobile.required' =>'Mobile is required',
            // 'mobile.min'    =>'Mobile nummber minimum character is 11',
            // 'password.required' =>'Password is required',
            // 'password.min' => 'Password Minimum character is 8'
        ];
    }

    public function response(array $error) {
        return $error;
    }
}

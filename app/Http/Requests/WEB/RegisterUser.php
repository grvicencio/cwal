<?php

namespace App\Http\Requests\WEB;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'username' => 'required|min:8',
            'email' =>'required|email',
            'birthdate' =>'required',
            // 'lastname' =>'required',
            'password'=>'required|min:8',
            'repeat_password'=>'required|min:8'
            //
        ];
    }
    
    public function messages() {
        return [
            // 'username.required' =>'Username is required',
            // 'username.min' => 'Username mininum character is 8',
            'email.required' =>'Email is required',
            'birthdate.required' => 'Birth Date is required',
            'password.required' =>'Password is required',
            'password.min' =>'Password Minimum character is 8',
            
            
        ];
    }

    public function response(array $error){
        return compact('error');
    }
}

<?php

namespace App\Http\Requests\CRM;

use App\NinepineModels\MarketplaceAvatar;
use Illuminate\Foundation\Http\FormRequest;

class MarketplaceAvatarRequest extends FormRequest{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return \Auth::guard('crm')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'avatar_id' => "required",
            'avatar_amount' => 'required|numeric|min:0.10|regex:/^[\d\.]+$/u',
            'avatar_currency' => 'required',
            'valid_date' => 'required',
            'status_id' => 'required'
        ];
    }

    public function response(array $errors){
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }
}

<?php

namespace App\Http\Requests\CRM;

use App\CRM\NinepineModels\ExchangeRate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ExchangeRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('crm')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $existing_exchange_rate = ExchangeRate::where('from_currency', $this->input('from_currency'))
            ->where('to_currency', $this->input('to_currency'))
            ->first();

        switch ($this->method()) {
            case 'POST': {
                $unique = is_null($existing_exchange_rate) ? '' : '|unique:ExchangeRates';

                return [
                    'from_currency' => "required$unique",
                    'to_currency' => "required|different:from_currency$unique",
                    'default_amount' => 'required|regex:/^\d*(\.\d{1,2})?$/|numeric|min:1.00',
                    'exchange_rate' => 'required|regex:/^\d*(\.\d{1,12})?$/|numeric|min:0.000000000001'
                ];
            }
            case 'PUT': {
                $exchange_rate = $this->route('exchange_rate');
                $unique_from = is_null($existing_exchange_rate) ? '' : '|unique:ExchangeRates,from_currency,' . $exchange_rate->exchange_rate_id . ',exchange_rate_id';
                $unique_to = is_null($existing_exchange_rate) ? '' : '|unique:ExchangeRates,to_currency,' . $exchange_rate->exchange_rate_id . ',exchange_rate_id';

                return [
                    'from_currency' => "required$unique_from",
                    'to_currency' => "required|different:from_currency$unique_to",
                    'default_amount' => 'required|regex:/^\d*(\.\d{1,2})?$/|numeric|min:1.00',
                    'exchange_rate' => 'required|regex:/^\d*(\.\d{1,12})?$/|numeric|min:0.000000000001'
                ];
            }
            default:
                break;
        }
    }

    public function response(array $errors)
    {
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }
}

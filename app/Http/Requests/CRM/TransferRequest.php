<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\CRM\NinepineModels\Wallet;

class TransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('crm')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = Wallet::where('wallet_account_number', $this->wallet_account_number)
            ->join('Currency', 'Wallet.currency_id', '=', 'Currency.currency_id')
            ->pluck('Currency.app_currency');
        $data = json_decode($data);
        $range = ($data[0]) ? "regex:/^\d*(\.\d{1,8})?$/|min:0.00000001|between:0.00000000,1000000000000.00000000" : "regex:/^\d*(\.\d{1,2})?$/|min:1.00|max:99999.99";

        return [
            'wallet_account_number' => 'required',
            'user_id' => 'required',
            'transfer_amount' => 'required|numeric|' . $range,
            'reason' => 'required|max:255'
        ];
    }

    public function response(array $errors)
    {
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }
}

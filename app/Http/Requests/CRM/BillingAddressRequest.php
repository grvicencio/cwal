<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class BillingAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('crm')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'ba_first_name' => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:100',
                        'ba_last_name' => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:100',
                        'ba_country' => 'required',
                        'ba_city' => 'required',
                        'ba_billing_address' => 'required|max:255',
                        'ba_billing_address_second' => 'max:255',
                        'ba_postalcode' => 'required|numeric',
                        'ba_phone' => 'required|regex:/(?:\(?\+\d{2}\)?\s*)?\d+(?:[ -]*\d+)*$/|min:4|max:20|unique:bit.BillingAddress,phone'
                    ];
                }
            case 'PUT':
                {
                    $billing_address_id = $this->route('billing_address')->billing_address_id;

                    return [
                        'ba_first_name' => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:100',
                        'ba_last_name' => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:100',
                        'ba_country' => 'required',
                        'ba_city' => 'required',
                        'ba_billing_address' => 'required|max:255',
                        'ba_billing_address_second' => 'max:255',
                        'ba_postalcode' => 'required|numeric',
                        'ba_phone' => "required|regex:/(?:\(?\+\d{2}\)?\s*)?\d+(?:[ -]*\d+)*$/|min:4|max:20|unique:bit.BillingAddress,phone,$billing_address_id,billing_address_id"
                    ];
                }
            default:
                break;
        }
    }
}

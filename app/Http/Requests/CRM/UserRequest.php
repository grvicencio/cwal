<?php

namespace App\Http\Requests\CRM;

use App\CRM\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return Auth::guard('crm')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($method = null){
        $method = is_null($method) ? $this->method() : $method;

        switch($method){
            case 'POST': {
                return [
                    'firstname' => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:100',
                    'lastname' => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:100',
                    'status_id' => 'required',
                    'role_id' => 'required',
                    'email' => 'required|unique:bit_crm.Users',
                    'password' => 'required|min:8|confirmed'
                ];
            } case 'PUT': {
                $user = $this->route('assignuser');

                return [
                    'firstname' => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:100',
                    'lastname' => 'required|regex:/^[\pL\s\-]+$/u|min:2|max:100',
                    'status_id' => 'required',
                    'role_id' => 'required',
                    'email' => 'required|unique:bit_crm.Users,email,' . $user->id
                ];
            } default: break;
        }
    }

    public function response(array $errors){
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }
}
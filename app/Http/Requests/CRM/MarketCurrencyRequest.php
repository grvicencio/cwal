<?php

namespace App\Http\Requests\CRM;

use App\NinepineModels\MarketplaceCurrency;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MarketCurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('crm')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $required = $this->method() == 'PUT' ? '' : 'required|';
        $size = collect(MarketplaceCurrency::$size);
        $width = $size->get('width');
        $height = $size->get('height');

        return [
            'image' => $required . "image|max:2048|mimes:png|dimensions:width=$width,height=$height",
            'item_amount' => 'required|integer|min:1',
            'item_currency' => 'required',
            'purchase_amount' => 'required|integer|min:1',
            'purchase_currency' => 'required',
            'expiration_date' => 'required',
            'status_id' => 'required',
            'application_id' => 'required',
            'marketplace_currency_id' => '',
        ];
    }

    public function response(array $errors){
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }
}

<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('crm')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'title' => 'required|min:2|max:100',
                        'content' => 'required',
                        'category_id' => 'required',
                        'status_id' => 'required',
                        'display_on_homepage' => '',
                        'default' => '',
//                        'path' => 'required'
                    ];
                }
            case 'PUT':
                {
                    return [
                        'title' => 'required|min:2|max:100',
                        'content' => 'required',
                        'category_id' => 'required',
                        'status_id' => 'required',
                        'display_on_homepage' => '',
                        'default' => '',
//                        'path' => 'required'
                    ];
                }
            default:
                break;
        }
    }
}

<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AccountsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('crm')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $account = $this->route('account');

        return [
            "first_name" => "required|regex:/^[\pL\s\-']+$/u|min:2|max:100",
            "last_name" => "required|regex:/^[\pL\s\-']+$/u|min:2|max:100",
            "status_id" => "required",
            "email" => "required|max:100|email|unique:bit.users,email," . $account->id,
            "birth_date" => "",
            "gender" => ""
        ];
    }
}

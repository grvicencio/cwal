<?php

namespace App\Http\Requests\CRM;

use App\CRM\NinepineModels\Profile;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProfileRequest extends FormRequest{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return Auth::guard('crm')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $required = $this->method() == 'PUT' ? '' : 'required|';
        $size = collect(Profile::$size);
        $width = $size->get('width');
        $height = $size->get('height');

        return [
            'image' => $required . "image|max:2048|mimes:png|dimensions:width=$width,height=$height",
        ];
    }

    public function response(array $errors){
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }
}

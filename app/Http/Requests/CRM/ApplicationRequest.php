<?php

namespace App\Http\Requests\CRM;

use App\CRM\baccarat\Applications;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('crm')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $max = 2048;
        $mimes = 'mimes:jpg,png,jpeg';
        $sizes = collect(Applications::$sizes);
        $landscape_width = $sizes->get('landscape')['width'];
        $landscape_height = $sizes->get('landscape')['height'];
        $square_width = $sizes->get('square')['width'];
        $square_height = $sizes->get('square')['height'];

        switch ($this->method()) {
            case 'POST': {
                return [
                    'name' => 'required|max:100',
                    'description' => 'required|max:255',
                    'image_landscape' => "required|image|$mimes|dimensions:width=$landscape_width,height=$landscape_height|max:$max",
                    'image_square' => "required|image|$mimes|dimensions:width=$square_width,height=$square_height|max:$max",
                    'status_id' => 'required'
                ];
            }
            case 'PUT': {
                return [
                    'name' => 'required|max:100',
                    'description' => 'required|max:255',
                    'image_landscape' => "image|$mimes|dimensions:width=$landscape_width,height=$landscape_height|max:$max",
                    'image_square' => "image|$mimes|dimensions:width=$square_width,height=$square_height|max:$max",
                    'status_id' => 'required'
                ];
            }
            default:
                break;
        }
    }

    public function response(array $errors)
    {
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }
}

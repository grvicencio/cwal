<?php

namespace App\Http\Requests\CRM;

use App\baccarat\MembershipCharges;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MembershipChargesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('crm')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $max = 1024;
        $mimes = 'mimes:png';
        $size = collect(MembershipCharges::$logo_size);
        $width = $size->get('width');
        $height = $size->get('height');

        switch ($this->method()) {
            case 'POST': {
                return [
                    'name' => 'required|unique:bit.Memberships',
                    'description' => 'required',
                    'price' => 'required|regex:/^\d*(\.\d{1,2})?$/|numeric|min:1.00',
                    'billing_cycle' => 'required|integer|min:0',
                    'status_id' => 'required',
                    'membership_type' => 'required',
                    'no_of_tables' => 'required|integer|min:2',
                    'banker_rake' => 'required|numeric|min:0.90',
                    'currency_id' => 'required',
                    'logo_file' => "required|image|$mimes|dimensions:width=$width,height=$height|max:$max"
                ];
            }
            case 'PUT': {
                $membership_id = $this->route('membership')->membership_id;

                return [
                    'name' => "required|unique:bit.Memberships,name,$membership_id,membership_id",
                    'description' => 'required',
                    'price' => 'required|regex:/^\d*(\.\d{1,2})?$/|numeric|min:1.00',
                    'billing_cycle' => 'required|integer|min:0',
                    'status_id' => 'required',
                    'membership_type' => 'required',
                    'no_of_tables' => 'required|integer|min:2',
                    'banker_rake' => 'required|numeric|min:0.90',
                    'currency_id' => 'required',
                    'logo_file' => "image|$mimes|dimensions:width=$width,height=$height|max:$max"
                ];
            }
            default:
                break;
        }
    }

    public function response(array $errors)
    {
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: NPT 8
 * Date: 10/23/2017
 * Time: 2:16 PM
 */

namespace App\Http\Controllers\CRM\PaymentMethods;


use App\CRM\baccarat\PaymentMethods;
use App\Http\Controllers\CRM\NinePineController;
use Illuminate\Http\Request;

class PaymentMethodsController extends NinePineController
{


    public function index()
    {
        $data['charges_menu'] = true;
        $data['payment_methods'] = true;
        $data['page_title'] = 'Payment Methods';
        $data['sub_title'] = 'Payment Methods';
        $PaymentMethods = PaymentMethods::all();

        $page_title = "Payment Methods";
        return view('CRM.payment_methods.index', compact('page_title', 'PaymentMethods', 'data'));

    }

    public function add(Request $request)
    {


    }


    public function update(Request $request, $id)
    {
        $PaymentMethod = PaymentMethods::where('payment_method_id', $id)->first();
        if (!$PaymentMethod) {
            abort(404);
        }
        $PaymentMethod->name = $request->input('name');
        $PaymentMethod->description = $request->input('description');
        $PaymentMethod->created_at = $request->input('created_at');
        $PaymentMethod->updated_at = $request->input('updated_at');
        $PaymentMethod->save();
        return redirect('payment_methods.index');

    }

//    public function destroy(){
//
//
//    }

}
<?php

namespace App\Http\Controllers\CRM\Profile;

use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\ProfileRequest;
use App\CRM\NinepineModels\Role;
use App\CRM\NinepineModels\Status;
use App\CRM\NinepineModels\Profile;
use App\CRM\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends NinePineController{
	public function index(){
		$data = array();

		$data['pagetitle'] = "Profile";

		return view('CRM.profile.index')
			->with($data);
	}

	public function update(ProfileRequest $request){
		$data = array();
		$user = Profile::find(Auth::guard('crm')->user()->id);

		if($request->hasFile('image')){
			if(!$user->makeImage($request->file('image'))){
				$swal = trans('swal.marketplace_currency.update.image.error');

				return response()->json([
					config('response.status') => config('response.type.fail'),
					config('response.data') => compact('swal')
				]);
			}
		}

		return response()->json([
			config('response.status') => config('response.type.success'),
			config('response.data') => $user
		]);
	}
}
<?php

namespace App\Http\Controllers\CRM\Inquiry;

use App\API\ContactForm;
use App\common\ContactReply;
use App\Http\Requests\common\ContactFormRequest;
use App\Http\Requests\common\ContactReplyRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\CRM\Controller;
use Illuminate\Support\Facades\Auth;

class InquiryController extends Controller{
	public function index(){
		$data['page_title'] = "Contact Inquiries";
        $data['inquiry_menu'] = true;

        return view('CRM.inquiry.index')
        	->with($data);
	}

	public function datatable(Request $request){
		return dataTable($request, ContactForm::where('user_id', '!=', 0)->with('account'));
	}

	public function postInquiryInfo(Request $request){
		$data = array();
		$info = \DB::table('ContactForm AS c')
			->leftJoin('users AS u', 'c.user_id', '=', 'u.id')
			->where('c.contact_id', $request->id)
			->select(
				'c.subject',
				\DB::raw('TO_CHAR("c"."created_at", \'Month DD, YYYY\') AS "date"'),
				\DB::raw('TO_CHAR("c"."created_at", \'HH12:MI AM\') AS "time"'),
				'c.message',
				'c.type',
				'u.username',
				\DB::raw('CASE
							WHEN LOWER("c"."type") = \'inquiry\' THEN
								\'primary\'
							WHEN LOWER("c"."type") = \'account\' THEN
								\'danger\'
							WHEN LOWER("c"."type") = \'wallet\' THEN
								\'info\'
							WHEN LOWER("c"."type") = \'market\' THEN
								\'success\'
							WHEN LOWER("c"."type") = \'others\' THEN
								\'dark\'
							WHEN LOWER("c"."type") = \'dispute\' THEN
								\'warning\'
						END AS "status"')
			)
			->first();
		$data['info'] = $info;

		return response()->json($data, 200);
	}

	public function postCRMReply(ContactReplyRequest $request){
		$info = ContactForm::where('user_id', '!=', 0)
			->where('contact_id', $request->contact_id)
			->with('account')
			->first();
		$getFullMessage = \DB::table('ContactForm AS c')->where('contact_id', $info->contact_id)->first()->message;
		$data = $request->all();
		$data['title'] = "Re: " . $info->subject;
		$data['replied_by'] = Auth::user()->id;
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');
		ContactReply::create($data);

		// Mail user
		\Mail::send('CRM.email_templates.reply', ['content' => "<em>" . $getFullMessage . "</em><hr />" . $request->message, 'email' => $info->account->email, 'username' => $info->account->username], function($message) use($info){
			$message->subject("Re: " . $info->subject);
            $message->from(Auth::user()->email, Auth::user()->name);
            $message->to($info->account->email);
        });

		return response()->json($data, 200);
	}

	public function postCRMReplyHistory(Request $request){
		$data = ContactReply::where('contact_id', $request->id)
			->get();

		return response()->json($data, 200);
	}
}
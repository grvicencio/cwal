<?php

namespace App\Http\Controllers\CRM\Application;

use App\CRM\baccarat\Applications;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\ApplicationRequest;
use App\CRM\NinepineModels\Status;
use Illuminate\Support\Facades\File;

class ApplicationController extends NinePineController
{
    public function index()
    {
        $data['page_title'] = 'Applications';
        $data['sub_title'] = '';
        $data['application_menu'] = true;

        $applications = Applications::join('Statuses', 'Applications.status_id', '=', 'Statuses.status_id')->get();
        $data['applications'] = $applications;

        $statuses = Status::where('status_type', 'Application')->get(['status_id', 'status_name'])->toArray();
        $data['statuses'] = $statuses;
        return view('CRM.application.application')->with($data);
    }

    public function store(ApplicationRequest $request)
    {
        $application = Applications::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'status_id' => $request->input('status_id')
        ]);

        if ($request->hasFile('image_landscape') && $request->hasFile('image_square')) {
            $application->app_image_470_260 = Applications::makeImage($request->file('image_landscape'), $application->application_id, Applications::$sizes['landscape']['width'], Applications::$sizes['landscape']['height']);
            $application->app_image_300_300 = Applications::makeImage($request->file('image_square'), $application->application_id, Applications::$sizes['square']['width'], Applications::$sizes['square']['height']);
        } else {
            $swal = trans('swal.application.create.image.error');

            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => compact('swal')
            ]);
        }
        $application->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }

    public function update(ApplicationRequest $request, Applications $application)
    {
        $application->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'status_id' => $request->input('status_id')
        ]);

        if ($request->hasFile('image_landscape')) {
            $application->app_image_470_260 = Applications::makeImage($request->file('image_landscape'), $application->application_id, Applications::$sizes['landscape']['width'], Applications::$sizes['landscape']['height']);
        }

        if ($request->hasFile('image_square')) {
            $application->app_image_300_300 = Applications::makeImage($request->file('image_square'), $application->application_id, Applications::$sizes['square']['width'], Applications::$sizes['square']['height']);
        }
        $application->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function destroy(Applications $application)
    {
        $affected = $application->getAffectedRelations();

        if (count($affected)) {
            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => compact('affected')
            ]);
        }
        $dir = storage_path("app/public/applications/$application->application_id");

        if (File::exists($dir)) {
            deleteDir($dir);
        }
        $application->delete();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function getImage($dir, $size, $image)
    {
        $file = storage_path("app/public/applications/$dir/$size/$image");

        if (!File::exists($file)) {
            abort(404);
        }
        return response()->file($file);
    }
}

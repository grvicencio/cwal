<?php

namespace App\Http\Controllers\CRM\Status;

use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\StatusRequest;
use App\CRM\NinepineModels\Status;
use Illuminate\Http\Request;

class StatusController extends NinePineController
{
    public function index(Request $request)
    {
        $types = new Status;
        $types = $types->ListByType();
        $first_key = $types[0];

        $data['secured'] = array('Active', 'Inactive');
        $data['type_selected'] = (!empty($request->type)) ? $request->type : $first_key;
        $data['statuses'] = !empty($request->type) ? Status::where('status_type', $request->type)->get() : Status::where('status_type', $first_key)->get();
        $data['page_title'] = 'Status';
        $data['types'] = $types;
        $data['status_menu'] = true;

        return view('CRM.status.index')->with($data);
    }

    public function store(StatusRequest $request)
    {
        $status_old = Status::where('status_type', $request->status_type)->first();

        if (is_null($status_old->status_name)) {
            $status_old->status_name = $request->status_name;
            $status_old->save();
        } else {
            $status = new Status();
            $status->status_type = $request->status_type;
            $status->status_name = $request->status_name;
            $status->save();
        }
        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }

    public function update(StatusRequest $request)
    {
        $status_id = $request->status_id;

        if (!is_numeric($status_id)) {
            $response = array('response' => 'failed');
            return response()->json($response, 500);
        }
        $status = Status::find($status_id);

        if (!$status) {
            $response = array('response' => 'failed');
            return response()->json($response, 500);
        }
        $status->status_name = $request->status_name;
        $status->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }
}

<?php

namespace App\Http\Controllers\CRM\Charts;

use App\CRM\baccarat\Accounts;
use App\Http\Controllers\CRM\Controller;

class UserChargeController extends Controller
{
    public function user_charge_range($from, $to)
    {
        $user_charge_data = Accounts::getUserChargeDataFromDateRange($from, $to);

        return response()->json([
            config('response.status') => config('response.type.success'),
            config('response.data') => compact('user_charge_data')
        ]);
    }
}

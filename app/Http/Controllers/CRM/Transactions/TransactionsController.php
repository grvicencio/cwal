<?php

namespace App\Http\Controllers\CRM\Transactions;


use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\Transaction;
use App\Http\Controllers\CRM\NinePineController;
use Illuminate\Http\Request;

class TransactionsController extends NinePineController
{
    public function index(){
        $data['transactions'] = Transaction::with([
            'transaction_logs.transaction_status',
            'transaction_detail.charge',
            'transaction_detail.charge.currency',
            'account',
            'payment_info'
        ])->get(); // Must have currency_id on TransactionDetails

        $data['page_title']  ="Transactions";
        $data['transactions_menu'] = true;

        return view('CRM.transactions/index')->with($data);
    }

    public function updateNotes(Request $request, $id){
        $transaction = Transaction::where('transaction_id', $id)->first();
        if(! $transaction){
            abort(404);
        }
        $transaction->notes = $request->input('notes');
        $transaction->save();

        return redirect('/transactions')->with('msg', 'Success!');;
    }

    public function activityDataTable(Request $request, Accounts $account)
    {
        return dataTable($request, $account->getTransactionActivity());
    }
}
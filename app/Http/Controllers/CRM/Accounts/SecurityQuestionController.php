<?php

namespace App\Http\Controllers\CRM\Accounts;

use App\CRM\baccarat\SecurityQuestion;
use App\CRM\NinepineModels\Status;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\SecurityQuestionRequest;
use Illuminate\Http\Request;

class SecurityQuestionController extends NinePineController
{
    public function index()
    {
        $page_title = "Security Question";

        $data['page_title'] = $page_title;
        $data['security_question'] = true;

        return view('CRM.security_question.index')->with($data);
    }

    public function fetchSecurityQuestionDatable(Request $request)
    {
        return dataTable($request, (new SecurityQuestion()));
    }


    public function create(SecurityQuestionRequest $request)
    {
        $question = $request->input('security_question');


        // get active status for security question
        $status_id = Status::where('status_type', 'SecurityQuestion')
            ->where('status_name', 'Active')
            ->first();
        $status_id = $status_id->status_id;

        $security_question = new SecurityQuestion();
        $security_question->security_question = $question;
        $security_question->status_id = $status_id;
        $security_question->save();

        return redirect()->back();
    }

    public function delete($id)
    {
        $sec_ques = SecurityQuestion::find($id);
        if(!$sec_ques){
            abort(404);
        }
        $sec_ques->delete();

        return redirect()->back();
    }

    public function update(SecurityQuestionRequest $request, $id)
    {
        $security_question = SecurityQuestion::find($id);
        if(!$security_question){
            abort(404);
        }

        $security_question->security_question = $request->input('security_question');
        $security_question->save();
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\CRM\Accounts;

use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\BankInfo;
use App\CRM\baccarat\Status;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\AccountsRequest;
use Illuminate\Http\Request;

class AccountsController extends NinePineController
{
    public function index()
    {
        $data['page_title'] = "Accounts";
        $data['accounts_menu'] = true;
        $data['statuses'] = Status::of("Account");
        return view('CRM.accounts.index')->with($data);
    }

    public function dataTable(Request $request)
    {
        return dataTable($request, Accounts::withoutGuest()->with('status'));
    }

    public function update(AccountsRequest $request, Accounts $account){
        $account->update($request->all());
        $account->birth_date = $request->birth_date ? date("Y-m-d", strtotime($request->birth_date)) : null;
        $account->save();

        $swal = trans('swal.account.update.success');

        return redirect()->back()->with(compact('swal'));
    }

    public function details(Request $request, Accounts $account)
    {
        $data['page_title'] = 'Account Information';
        $data['account'] = $account;
        $data['account_membership'] = $account->memberships()->first();
        $data['statuses'] = Status::of("Account");
        $banks = $account->bank_info;
        $bankinfo_id = $request->query('bankinfo_id');

        if ($bankinfo_id) {
            $banks = $account->bank_info()->where('bankinfo_id', $bankinfo_id)->get();
        }
        $data['banks'] = $banks;
        $data['accounts_menu'] = true;
        $data['genders'] = Accounts::$genders;
        return view('CRM.accounts.details.index')->with($data);
    }
}

<?php

namespace App\Http\Controllers\CRM;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NinePineController extends Controller{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct($bypass = false){
        parent::__construct();

        $this->middleware(function(Request $request, $next) use($bypass){
            $guard = 'crm';

            if(!$bypass){
                if(!Auth::guard($guard)->check()){
                    return abort(401);
                }

                $permission = $this->permission($request->route()->getName());

                if(!is_null($permission)){
                    $can_access = Auth::guard($guard)->user()->canAccess($permission, $request->route()->getName());

                    if(!$can_access){
                        $swal = trans('swal.permission.forbidden');

                        if($request->ajax()){
                            return response()->json(compact('swal'), 403);
                        }

                        if(Auth::guard($guard)->user()->canAccess('view', 'crm.dashboard.index')){
                            return redirect('admin/dashboard', 307)->with(compact('swal'));
                        }

                        return abort(403);
                    }
                }
            }

            return $next($request);
        });
    }

    private function permission($route_name){
        $action_name = getActionName($route_name);
        $action_method = getActionMethod($action_name);

        return getPermission($action_method);
    }
}

<?php

namespace App\Http\Controllers\CRM\Charges;

use App\CRM\baccarat\ChargeDetail;
use App\CRM\baccarat\RegularCharges;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Status;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\RegularChargesRequest;

class RegularChargesController extends NinePineController
{
    // this are seeded to eliminate unnecessary query
    const CHARGE_REG = 1;
    const CHARGE_MEM = 1;
    const CHARGE_PRO = 3;

    public function index()
    {
        $data['charges_menu'] = true;
        $data['reg_menu'] = true;
        $data['page_title'] = 'Charges';
        $data['sub_title'] = 'Regular';

        // seems hasOne() on eloquent is not working with dblink, will change this if solved
        // $regular_statuses = Status::where('status_id', '51')->get(['status_id', 'status_name'])->toArray();
        $data['regular_charges'] = RegularCharges::join('Statuses', 'Charges.status_id', '=', 'Statuses.status_id')
            ->where('Charges.charge_type_id', $this::CHARGE_REG)->get();
            dd(RegularCharges::join('Statuses', 'Charges.status_id', '=', 'Statuses.status_id')
            ->where('Charges.charge_type_id', $this::CHARGE_REG)->get());

        // possible regular charges Status:
        $data['statuses'] = Status::where('status_type', 'Charges')->get(['status_id', 'status_name'])->toArray();
        $data['global_currencies'] = Currency::where('app_currency', false)->orWhere('app_currency', null)->get();

        return view('CRM.charges.regular.index')->with($data);
    }

    public function edit(RegularChargesRequest $request, RegularCharges $charge)
    {
        $charge->name = $request->input('name');
        $charge->description = $request->input('description');
        $charge->price = $request->input('price');
        $charge->status_id = $request->input('status_id');
        $charge->currency_id = $request->input('currency_id');
        $charge->wallet_value = $request->input('value');
        $charge->save();

        $charge_details = ChargeDetail::where('charge_id', $charge->charge_id)->first();
        $charge_details->value = $request->input('value');
        $charge_details->name = $request->input('name');
        $charge_details->description = $request->input('description');
        $charge_details->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function add(RegularChargesRequest $request)
    {
        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $value = $request->input('value');
        $status_id = $request->input('status_id');
        $currency_id = $request->input('currency_id');

        // TODO: transaction on insert
        // TODO: link this two into one save
        $regular_charge = new RegularCharges();
        $regular_charge->name = $name;
        $regular_charge->description = $description;
        $regular_charge->price = $price;
        $regular_charge->status_id = $status_id;
        $regular_charge->currency_id = $currency_id;
        $regular_charge->wallet_value = $value;
        $regular_charge->charge_type_id = $this::CHARGE_REG;
        $regular_charge->save();

        $charge_details = new ChargeDetail();
        $charge_details->charge_id = $regular_charge->charge_id;
        $charge_details->name = $name;
        $charge_details->description = $description;
        $charge_details->value = $value;
        $charge_details->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }
}

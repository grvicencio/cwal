<?php

namespace App\Http\Controllers\CRM\Charges;

use App\CRM\baccarat\MembershipCharges;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\MembershipAvatarRequest;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\MembershipAvatar;
use App\CRM\NinepineModels\Status;

class MembershipAvatarController extends NinePineController
{
    public function index(MembershipCharges $membership)
    {
        $data['charges_menu'] = true;
        $data['mem_menu'] = true;
        $data['membership_charges'] = $membership;
        $data['page_title'] = 'Membership Inclusions';
        $data['in_app_currencies'] = Currency::where('app_currency', true)->get();
        $data['active_id'] = Status::where('status_type', 'Avatar')
            ->where('status_name', 'Active')
            ->first()->status_id;
        $data['excluded_avatar_ids'] = MembershipAvatar::where('membership_id', $membership->membership_id)->pluck('avatar_id');
        return view('CRM.charges.membership.inclusions.index')->with($data);
    }

    public function store(MembershipAvatarRequest $request, MembershipCharges $membership)
    {
        $membership->membership_avatar()->create($request->all());

        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }

    public function update(MembershipAvatarRequest $request, MembershipCharges $membership, MembershipAvatar $avatar)
    {
        $avatar->update($request->all());
        $avatar->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function destroy(MembershipCharges $membership, MembershipAvatar $avatar)
    {
        $avatar->delete();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }
}

<?php

namespace App\Http\Controllers\CRM\Charges;

use App\CRM\baccarat\MembershipCharges;
use App\CRM\NinepineModels\Status;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\MembershipChargesRequest;
use App\CRM\NinepineModels\Currency;

class MembershipChargesController extends NinePineController
{
    public function index()
    {
        $data['charges_menu'] = true;
        $data['mem_menu'] = true;
        $data['page_title'] = 'Charges';
        $data['sub_title'] = 'Membership';

        $membership_charges = MembershipCharges::join('Statuses', 'Memberships.status_id', '=', 'Statuses.status_id')->get();
        $data['membership_charges'] = $membership_charges;

        // possible regular charges Status:
        $statuses =  Status::where('status_type', 'Membership')->get(['status_id', 'status_name'])->toArray();
        $data['statuses'] = $statuses;
        $data['types'] = MembershipCharges::$types;
        $data['global_currencies'] = Currency::where('app_currency', false)->orWhere('app_currency', null)->get();

        return view('CRM.charges.membership.index')->with($data);
    }

    public function add(MembershipChargesRequest $request)
    {
        $request_values = $request->all();
        $request_values['logo'] = MembershipCharges::DEFAULT_LOGO;
        $membership = MembershipCharges::create($request_values);

        if (!$request->hasFile('logo_file') || !$membership->makeLogo($request->file('logo_file'))) {
            $swal = trans('swal.membership.create.logo.error');

            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => compact('swal')
            ]);
        }
        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }

    public function update(MembershipChargesRequest $request, MembershipCharges $membership)
    {
        $membership->update($request->all());

        if ($request->hasFile('logo_file')) {
            if (!$membership->makeLogo($request->file('logo_file'))) {
                $swal = trans('swal.membership.update.logo.error');

                return response()->json([
                    config('response.status') => config('response.type.fail'),
                    config('response.data') => compact('swal')
                ]);
            }
        }
        $membership->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }
}

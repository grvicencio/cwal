<?php

namespace App\Http\Controllers\CRM\Security;

use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\RoleRequest;
use App\CRM\NinepineModels\Module;
use App\CRM\NinepineModels\Role;
use App\CRM\NinepineModels\RoleModule;
use Illuminate\Support\Facades\Auth;

class PermissionController extends NinePineController
{

    public function index()
    {
        $data['permissions'] = Role::all();
        $data['roles'] = Module::all();
        $data['page_title'] = 'Permissions';
        $data['security_menu'] = true;
        $data['permission_menu'] = true;
        return view('CRM.security.permissions.index')->with($data);
    }

    public function add(RoleRequest $request)
    {
        Role::create([
            'role_name' => $request->input('role_name'),
            'description' => $request->input('description'),
            'created_by' =>  Auth::id()
        ]);

        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }

    public function edit(RoleRequest $request)
    {
        $role_id = $request->input('permission_id');
        $permissions = $request->input('permissions');
        $check_parent = Module::checkParentRelationships($permissions);

        if (!is_null($check_parent) && $request->input('warning') == 'true') { // parse boolean
            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => swal(
                    trans('swal.permission.warning.title'),
                    trans('swal.permission.warning.html', $check_parent),
                    trans('swal.permission.warning.type')
                )
            ]);
        }

        if ($permissions) {
            $role_module_ids = collect($permissions)->pluck('role_module_id');
            RoleModule::where('role_id', $role_id)
                ->whereNotIn('role_module_id', $role_module_ids)
                ->delete();

            foreach ($permissions as $permission) {
                $role_module = RoleModule::where('role_id', $role_id)
                    ->where('module_id', $permission['module_id'])
                    ->first();

                if (!is_null($role_module)) {
                    $role_module->can_add = $permission['can_add'];
                    $role_module->can_edit = $permission['can_edit'];
                    $role_module->can_delete = $permission['can_delete'];
                    $role_module->admin = $permission['admin'];
                    $role_module->readonly = $permission['readonly'];
                    $role_module->save();
                } else {
                    RoleModule::create([
                        'role_id' => $role_id,
                        'module_id' => $permission['module_id'],
                        'can_add' => $permission['can_add'],
                        'can_edit' => $permission['can_edit'],
                        'can_delete' => $permission['can_delete'],
                        'admin' => $permission['admin'],
                        'readonly' => $permission['readonly']
                    ]);
                }
            }
            $swal = trans('swal.permission.update.success');

            return response()->json([
                config('response.status') => config('response.type.success'),
                config('response.data') => compact('swal')
            ]);
        }
    }
}

<?php

namespace App\Http\Controllers\CRM\Security;

use App\CRM\User;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\UserPasswordResetRequest;
use App\Http\Requests\CRM\UserRequest;
use App\CRM\NinepineModels\Role;
use App\CRM\NinepineModels\Status;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

class AssignUserController extends NinePineController{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $data['users'] = User::all();
        $data['roles'] = Role::all('role_name', 'role_id');
        $data['statuses'] =  Status::where('status_type', 'User')->get();
        $data['security_menu'] = true;
        $data['assignuser_menu'] = true;
        $data['page_title'] = 'User Management';

        return view('CRM.security.user.index')->with($data);
    }

    public function add(UserRequest $request){
        $user = User::create([
            'name' => $request->input('firstname') . ' ' . $request->input('lastname'),
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'status_id' => $request->input('status_id')
        ]);
        $user->user_role()->create([
            'role_id' => $request->input('role_id')
        ]);

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function update(UserRequest $request, User $assignuser){
        $assignuser->update($request->all());
        $assignuser->name = $request->input('firstname') . ' ' . $request->input('lastname');
        $assignuser->user_role()->update([
            'role_id' => $request->input('role_id')
        ]);

        if($request->hasFile('image')){
            if(!$assignuser->makeImage($request->file('image'))){
                $swal = trans('swal.marketplace_currency.update.image.error');

                return response()->json([
                    config('response.status') => config('response.type.fail'),
                    config('response.data') => compact('swal')
                ]);
            }
        }

        $assignuser->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function passwordReset(UserPasswordResetRequest $request, User $assignuser){
        if (Hash::check($request->get('password'), $assignuser->password)) {
            return response()->json([
                config('response.status') => config('response.type.error'),
                config('response.errors') => [
                    'password' => [
                        'The password field can\'t be same as old.'
                    ]
                ]
            ]);
        }
        $assignuser->password = bcrypt($request->get('password'));
        $assignuser->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function delete(User $assignuser){
        $affected = $assignuser->getAffectedRelations();

        if (count($affected)) {
            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => compact('affected')
            ]);
        }
        $assignuser->delete();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function getImage($dir, $image){
        $file = storage_path("app/public/crmuser/$dir/$image");

        if(!File::exists($file))
            abort(404);

        return response()->file($file);
    }
}

<?php

namespace App\Http\Controllers\CRM\Wallet;

use App\common\WalletWithdrawal;
use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Source;
use App\CRM\NinepineModels\Status;
use App\CRM\NinepineModels\Wallet;
use App\CRM\baccarat\Applications;
use App\Mail\WebWithdrawalEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\CRM\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class WalletWithdrawalController extends Controller
{
    public function index(Request $request)
    {
        $data['wallet_menu'] = true;
        $data['withdrawal_menu'] = true;
        $data['page_title'] = 'Withdrawal';
        $data['in_app_currencies'] = Currency::where('app_currency', true)->get();
        $data['global_currencies'] = Currency::where('app_currency', false)
            ->orWhere('app_currency', null)->get();
        $data['statuses'] = Status::where('status_type', '=', 'WalletWithdrawal')->get();
        $query = WalletWithdrawal::latest()->with('status', 'currency', 'account');

        if ($request->query('wallet_withdrawal_id')) {
            $data['wallet_withdrawals'] =  $query->where('wallet_withdrawal_id', $request->query('wallet_withdrawal_id'))->get();
        } else {
            $data['wallet_withdrawals'] = $query->get();
        }
        $data['pending'] = Status::where('status_type', 'WalletWithdrawal')->where('status_name', 'Pending')->first();
        $data['approve'] = Status::where('status_type', 'WalletWithdrawal')->where('status_name', 'Approve')->first();
        $data['deny'] = Status::where('status_type', 'WalletWithdrawal')->where('status_name', 'Denied')->first();
        return view('CRM.wallet.withdrawal.index')->with($data);
    }

    public function set_status(Request $request)
    {
        $password = $request->input('password');

        if  (!\Hash::check($password, Auth::user()->password)) {
            return response()->json([
                config('response.status') => config('response.type.error'),
                config('response.information') => "Invalid password"
            ]);
        }

        $wallet_withdrawal = WalletWithdrawal::find($request->get('wallet_withdrawal_id'));

        $account = $wallet_withdrawal->account;

        if ($request->get('status_id') == Status::where('status_type', 'WalletWithdrawal')->where('status_name', 'Approve')->first()->status_id) {
            $currency = Currency::find($wallet_withdrawal->currency_id);
            $balance = $wallet_withdrawal->wallet->balance;

            if ($balance < $wallet_withdrawal->amount) {
                return response()->json([
                    config('response.status') => config('response.type.fail'),
                    config('response.data') => "User has insufficient balance."
                ]);
            }
            $source = Source::where('source_name', 'WalletWithdrawal')->first();
            $application = Applications::where('name', 'Capital7')->first();
            Wallet::makeTransaction($account, $wallet_withdrawal->amount, $currency, $source, $wallet_withdrawal->wallet_withdrawal_id, $application, Wallet::TYPE_DISCHARGE, $wallet_withdrawal->wallet->wallet_account_number);
        }
        $wallet_withdrawal->status_id = $request->get('status_id');
        $wallet_withdrawal->crm_note = $request->input('crm_notes');
        $wallet_withdrawal->save();

        $_amount = $wallet_withdrawal->currency->currency_symbol . ' ' . number_format($wallet_withdrawal->amount, 8);
        Mail::send(new WebWithdrawalEmail($account, $_amount, $wallet_withdrawal->crm_note, Status::find($request->get('status_id'))->status_name));

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function set_attachments(Request $request)
    {
        $wallet_withdrawal = WalletWithdrawal::find($request->get('wallet_withdrawal_id'));
        $wallet_withdrawal->attachments = makeAttachment($request->file('attachments'), config('wallet.actions.withdrawal'), $request->get('wallet_withdrawal_id'));
        $wallet_withdrawal->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function set_notes(Request $request)
    {
        $wallet_withdrawal = WalletWithdrawal::find($request->get('wallet_withdrawal_id'));
        $wallet_withdrawal->notes = $request->get('notes');
        $wallet_withdrawal->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function updateCrmNotes(Request $request){
        $note = $request->input('withdrawal-note');
        $withdrawal_id  = $request->input('withdrawal-id');
        $walletWithdrawal = WalletWithdrawal::find($withdrawal_id);
        $walletWithdrawal->update(['crm_note' => $note]);
        $walletWithdrawal->save();
        $swal = trans('swal.wallet.withdrawal.success');
        return response()->json(compact('swal'));
    }
}

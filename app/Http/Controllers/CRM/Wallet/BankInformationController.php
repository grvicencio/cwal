<?php

namespace App\Http\Controllers\CRM\Wallet;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\BankInfoRequest;
use App\CRM\NinepineModels\BankInformation;
use App\CRM\NinepineModels\Currency;
use App\CRM\baccarat\Status;


class BankInformationController extends NinePineController{
    public function index(){
        $data['bankinformation_menu'] = true;
        $data['page_title'] = 'Bank Information';
        $data['wallet_menu'] = true;
        $data['statuses'] = Status::of("BankInfo");
        $status_id = $data['statuses']->where('status_name', 'Active')->first()->status_id;
        $data['currencies'] = Currency::get();
        $currency_id = Currency::where('app_currency', 't')->pluck('currency_id')->toArray();
        $defaultAccount = BankInformation::where('default', true)->count();

        if($defaultAccount <= 0){
            /**
             *  Initially set all `Depositinfo` records `status_id` as `Active`
            **/
            \DB::connection('bit_crm')
                ->table('Depositinfo')
                ->update([
                    'status_id' => $status_id,
                    'default' => false,
                    'updated_at' => Carbon::now(),
                ]);

            $default = BankInformation::whereNotIn('currency_id', $currency_id)->first();
            $default->default = true;
            $default->updated_at = Carbon::now();
            $default->save();
        }

        return view('CRM.wallet.bankinfo.index')->with($data);
    }

    public function dataTable(Request $request){
        return dataTable($request,BankInformation::query());
    }

    public function store(BankInfoRequest $request){
       if($request->default_account == true){
        BankInformation::where('default','true')->update([
            'default' => 'false'
            ]);
            $default_account='true';
       }else  $default_account='false';
        $bankinfo=BankInformation::insert([
            'information'=> $request->information,
            'bank_name'=> $request->bank_name,
            'currency_id'=> $request->currency_id,
            'status_id'=> $request->status_id,
            'created_at'=> Carbon::now(),
            'updated_at'=> Carbon::now(),
            'default'=> $default_account
        ]);
        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);

    }
    public function update(BankInfoRequest $request){
       // $today = date("Y-m-d H:i:s");
       if($request->default_account == true){
        BankInformation::where('default','true')->update([
            'default' => 'false'
            ]);
            $default_account='true';
       }else  $default_account='false';
        BankInformation::where('depositinfo_id',$request->depositinfo_id)->update([
            'bank_name' => $request->bank_name,
            'information' => $request->information,
            'currency_id'=> $request->currency_id,
            'status_id'=> $request->status_id,
            'updated_at'=> Carbon::now(),
            'default'=> $default_account
            ]);

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);

    }

}

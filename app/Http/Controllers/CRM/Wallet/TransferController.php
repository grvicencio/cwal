<?php

namespace App\Http\Controllers\CRM\Wallet;

use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\Applications;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\TransferRequest;
use App\Mail\WebTransferFromCrmEmail;
use App\CRM\NinepineModels\CrmTransfer;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Source;
use App\CRM\NinepineModels\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class TransferController extends NinePineController
{
    public function index()
    {
        $data['wallet_menu'] = true;
        $data['transfer_menu'] = true;
        $data['page_title'] = 'Transfer';
        $data['in_app_currencies'] = Currency::crypto();
        $data['global_currencies'] = Currency::crypto(false);
        return view('CRM.wallet.transfer.index')->with($data);
    }

    public function dataTable(Request $request)
    {
        return dataTable($request, Accounts::withoutGuest(), ['username', 'email', 'mobile']);
    }

    public function get_currencies(Request $request)
    {
        $account = Accounts::find($request->get('user_id'));

        return response()->view('CRM.wallet.transfer.forms.currency_options', [
            'crypto' => $account->wallet()
                ->whereIn('currency_id', Currency::crypto()->pluck('currency_id'))
                ->orderBy('Wallet.currency_id', 'asc')
                ->orderBy('Wallet.wallet_account_number', 'asc')
                ->get(),
            'global' => $account->wallet()
                ->whereIn('currency_id', Currency::crypto(false)->pluck('currency_id'))
                ->orderBy('Wallet.currency_id', 'asc')
                ->orderBy('Wallet.wallet_account_number', 'asc')
                ->get()
        ]);
    }

    public function transfer(TransferRequest $request)
    {
        $sender = Auth::guard('crm')->user();
        try {
            $password = $request->password;
            if  (!\Hash::check($password, Auth::user()->password)) {
                return response()->json(swal("Transfer Rejected", "Invalid password.", 'error'));
            }
            $receiver = Accounts::findOrFail($request->user_id);
            $currency = Wallet::findByAccountNumber($request->wallet_account_number)->currency;
        } catch (\Exception $e) {
            return response()->json(swal(
                trans('swal.exception.title'),
                $e->getMessage(),
                trans('swal.exception.type')
            ));
        }
        if (!$receiver->bank_info()->count()) {
            $acct_name = $receiver->getDisplayName();
            return response()->json(swal("Transfer Rejected", "$acct_name has incomplete banking information.", 'error'));
        }

        $crm_transfer = CrmTransfer::create([
            'transfer_amount' => $request->transfer_amount,
            'currency_id' => $currency->currency_id,
            'crm_user_id' => $sender->id,
            'reason' => $request->reason,
            'user_id' => $receiver->id
        ]);

        $application = Applications::where('name', 'Capital7')->first();
        $source = Source::where('source_name', 'CRM')->first();
        $resource_id = $crm_transfer->crm_transfer_id;
        Wallet::makeTransaction($receiver, $request->transfer_amount, $currency, $source, $resource_id, $application, Wallet::TYPE_CHARGE, $request->wallet_account_number);
        $amount = $currency->currency_symbol . ' ' . number_format($request->transfer_amount, 8);

        if (!is_null($receiver->email)) {
            Mail::send(new WebTransferFromCrmEmail($receiver, $amount));
        }

        return response()->json(swal(
            trans('swal.transfer.success.title'),
            trans('swal.transfer.success.html', [
                'amount' => $amount,
                'link_to_route' => link_to_route('accounts.details', $receiver->getDisplayName(), [
                    $receiver, '#wallet'
                ])
            ]),
            trans('swal.transfer.success.type')
        ));
    }
}
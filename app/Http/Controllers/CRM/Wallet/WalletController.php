<?php

namespace App\Http\Controllers\CRM\Wallet;

use App\CRM\NinepineModels\Registration;
use App\Http\Controllers\CRM\NinePineController;
use App\CRM\NinepineModels\Source;
use App\CRM\NinepineModels\Wallet;
use App\CRM\NinepineModels\Wallet_ledger;
use Illuminate\Http\Request;

class WalletController extends NinePineController
{
    public function dataTable(Request $request, Wallet $wallet)
    {
        $query = $wallet->wallet_ledger()->with('wallet', 'wallet.account', 'wallet.currency', 'source');
        return dataTable($request, $query, ['debit', 'credit', 'balance', 'created_at']);
    }

    public function ledgerSourceInfo(Request $request, Wallet_ledger $ledger)
    {
        $html = null;

        switch ($ledger->source_id) {
            case Source::getIdByName('Charges'):
                $transaction_detail = $ledger->transaction_resource->transaction_detail;
                $currency_symbol = $transaction_detail->charge->currency->currency_symbol;

                $price = is_null($transaction_detail->price) ? 0 : $transaction_detail->price;
                $values = is_null($transaction_detail->values) ? 0 : $transaction_detail->values;

                $html = (string)view('CRM.accounts.details.tabs.wallet.sources.charges', [
                    'charge_description' => $transaction_detail->charge->description,
                    'transaction_detail_price' => $currency_symbol . ' ' . number_format($price, 8),
                    'transaction_detail_values' => $currency_symbol . ' ' . number_format($values, 8),
                    'created_at' => $ledger->created_at->toDayDateTimeString()
                ]);
                break;
            case Source::getIdByName('Membership'):
                $membership = $ledger->membership_resource;
                $currency_symbol = $membership->currency->currency_symbol;

                $html = (string)view('CRM.accounts.details.tabs.wallet.sources.membership', [
                    'membership_name' => $membership->name,
                    'membership_price' => $currency_symbol . ' ' . number_format($membership->price, 8),
                    'created_at' => $ledger->created_at->toDayDateTimeString()
                ]);
                break;
            case Source::getIdByName('CRM'):
                $crm_transfer = $ledger->crm_transfer_resource;
                $currency_symbol = $crm_transfer->currency->currency_symbol;

                $html = (string)view('CRM.accounts.details.tabs.wallet.sources.crm', [
                    'sender' => $crm_transfer->user->email,
                    'amount' => $currency_symbol . ' ' . number_format($crm_transfer->transfer_amount, 8),
                    'created_at' => $ledger->created_at->toDayDateTimeString(),
                    'reason' => $crm_transfer->reason
                ]);
                break;
            case Source::getIdByName('WalletTransfer'):
                $wallet_transfer = $ledger->wallet_transfer_resource;
                $currency_symbol = $wallet_transfer->currency->currency_symbol;

                $html = (string)view('CRM.accounts.details.tabs.wallet.sources.wallet_transfer', [
                    'sender' => $wallet_transfer->sender->email,
                    'receiver' => $wallet_transfer->receiver->email,
                    'amount' => $currency_symbol . ' ' . number_format($wallet_transfer->amount_transfer, 8),
                    'created_at' => $ledger->created_at->toDayDateTimeString(),
                    'reason' => $wallet_transfer->reason
                ]);
                break;
            case Source::getIdByName('DailyBonuses'):
                $daily_bonus = $ledger->daily_bonus_resource;
                $currency_symbol = $daily_bonus->currency->currency_symbol;
                $currency_name = $daily_bonus->currency->currency_name;

                $html = (string)view('CRM.accounts.details.tabs.wallet.sources.daily_bonus', [
                    'daily_bonus_name' => 'Free ' . $currency_name,
                    'daily_bonus_price' => $currency_symbol . ' ' . number_format($daily_bonus->points, 8),
                    'daily_bonus_type' => $daily_bonus->type,
                    'created_at' => $ledger->created_at->toDayDateTimeString()
                ]);
                break;
            case Source::getIdByName('Register'):
                $register_wallet = null;
                $currency_symbol = null;
                $currency_name = null;
                $amount = null;

                if (Registration::count()) {
                    $register_wallet = $ledger->register_wallet_resource();
                    $currency_symbol = $register_wallet->currency->currency_symbol;
                    $currency_name = $register_wallet->currency->currency_name;
                    $amount = $register_wallet->amount;
                } else {
                    $register_wallet = $ledger->wallet;
                    $currency_symbol = $register_wallet->currency->currency_symbol;
                    $currency_name = $register_wallet->currency->currency_name;
                    $amount = $ledger->debit;
                }
                $html = (string)view('CRM.accounts.details.tabs.wallet.sources.register_wallet', [
                    'register_wallet_name' => 'Registration ' . $currency_name,
                    'register_wallet_amount' => $currency_symbol . ' ' . number_format($amount, 8),
                    'register_wallet_type' => 'Registration',
                    'created_at' => $ledger->created_at->toDayDateTimeString()
                ]);
                break;
            case Source::getIdByName('WalletDeposit'):
                $wallet_deposit = $ledger->wallet_deposit_resource;
                $currency_symbol = $wallet_deposit->currency->currency_symbol;

                $html = (string)view('CRM.accounts.details.tabs.wallet.sources.wallet_deposit', [
                    'wallet_account_number' => empty($ledger->wallet->wallet_account_number) ? 'N/A' : $ledger->wallet->wallet_account_number,
                    'amount' => $currency_symbol . ' ' . number_format($wallet_deposit->amount, 8),
                    'created_at' => $ledger->created_at->toDayDateTimeString()
                ]);
                break;
            case Source::getIdByName('WalletWithdrawal'):
                $wallet_withdrawal = $ledger->wallet_withdrawal_resource;
                $currency_symbol = $wallet_withdrawal->currency->currency_symbol;

                $html = (string)view('CRM.accounts.details.tabs.wallet.sources.wallet_withdrawal', [
                    'wallet_account_number' => empty($ledger->wallet->wallet_account_number) ? 'N/A' : $ledger->wallet->wallet_account_number,
                    'amount' => $currency_symbol . ' ' . number_format($wallet_withdrawal->amount, 8),
                    'created_at' => $ledger->created_at->toDayDateTimeString()
                ]);
                break;
            case Source::getIdByName('Tradeprofile'):
                $trade_profile = $ledger->trade_profile_resource;
                $currency_symbol = $trade_profile->crypto_currency->currency_symbol;
                $amount = $ledger->credit;

                if (!$amount) {
                    $amount = $ledger->debit;
                }

                $html = (string)view('CRM.accounts.details.tabs.wallet.sources.trade_profile', [
                    'type' => $trade_profile->type,
                    'amount' => $currency_symbol . ' ' . number_format($amount, 8),
                    'created_at' => $ledger->created_at->toDayDateTimeString()
                ]);
                break;
            case Source::getIdByName('TradeMarketTicketTransfer');
                $transfer = $ledger->trademarket_ticket_transfer_resource;
                $currency_symbol = $transfer->trade_market_ticket->wallet->currency->currency_symbol;

                $html = (string)view('CRM.accounts.details.tabs.wallet.sources.trademarket_ticket_transfer', [
                    'sender' => $transfer->sender_wallet->account->email,
                    'receiver' => $transfer->receiver_wallet->account->email,
                    'amount' => $currency_symbol . ' ' . number_format($transfer->trade_market_ticket->amount, 8),
                    'created_at' => $ledger->created_at->toDayDateTimeString(),
                    'note' => $transfer->note,
                    'ledger' => json_encode($ledger),
                    'transfer' => json_encode($transfer->trade_market_ticket),
                    'ticket' => $transfer->trade_market_ticket->ticket_generated_id,
                ]);
                break;
        }
        return response()->json(compact('html'));
    }
}
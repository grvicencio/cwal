<?php

namespace App\Http\Controllers\CRM\Marketplace;

use App\Http\Requests\CRM\MarketplaceAvatarRequest;
use App\Http\Controllers\CRM\NinePineController;
use App\CRM\NinepineModels\Avatar;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\MarketplaceAvatar;
use App\CRM\NinepineModels\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Ixudra\Curl\Facades\Curl;

class MarketplaceAvatarController extends NinePineController{
	public function index(){
		$data = array();

		$currencies = Currency::get(['currency_id', 'currency_name', 'currency']);
		$globalcurrencies = Currency::where('app_currency', 0)->orWhere('app_currency', null)->get(['currency_id', 'currency_name', 'currency']);
		$appcurrencies = Currency::where('app_currency', 1) ->get(['currency_id', 'currency_name', 'currency']);

		$data['avatars'] = MarketplaceAvatar::join('Avatar', 'MarketplaceAvatar.avatar_id', '=', 'Avatar.avatar_id')
			->join('Currency', 'MarketplaceAvatar.avatar_currency', '=', 'Currency.currency_id')
			->select('*', 'MarketplaceAvatar.status_id AS marketplace_status')
			->get();
		$data['selectavatars'] = Avatar::where('type', 'Market')->get();
		$data['status'] = Status::where('status_type', 'MarketplaceAvatar')->get();
		$data['currencies'] = $currencies;
		$data['globalcurrencies'] = $globalcurrencies;
		$data['appcurrencies'] = $appcurrencies;
    		$data['pagetitle'] = 'Marketplace Avatar';
		$data['marketplace_menu'] = true;
		$data['marketplace_avatar_menu'] = true;

		return view('CRM.marketplace_avatar.index')
			->with($data);
	}

	public function json(Request $request){
		$per_page = $request->query('per_page');
		$readonly = false;
		// $status_id = $request->query('status_id');
		$query = MarketplaceAvatar::join('Avatar', 'Avatar.avatar_id', '=', 'MarketplaceAvatar.avatar_id')
			->join('Currency', 'Currency.currency_id', '=', 'MarketplaceAvatar.avatar_currency')
			->join('Statuses', 'Statuses.status_id', '=', 'MarketplaceAvatar.status_id')
			->select('*', 'MarketplaceAvatar.status_id AS marketplace_status');

		if($request->status_id != "")
			$query->where('MarketplaceAvatar.status_id', $request->status_id);

		if($request->currency_id != "")
			$query->where('MarketplaceAvatar.avatar_currency', $request->currency_id);

		$avatars = $query->paginate($per_page)->appends([
			'per_page' => $per_page,
			'readonly' => $readonly
		]);

		$avatars = [
			'html' => (string) view('marketplace_avatar.avatars')->with([
				'avatars' => $avatars,
				'readonly' => $readonly == 'true' ? true : false
			]),
			'links' => (string) $avatars->links()
		];

		return response()->json([
			config('response.status') => config('response.type.success'),
			config('response.data') => compact('avatars'),
		]);
	}

	public function selectAvatars(Request $request){
		$readonly = $request->query('readonly');
		$per_page = $request->query('per_page');
		$query = Avatar::join('Statuses', 'Avatar.status_id', '=', 'Statuses.status_id')
			->where('Avatar.type', 'Market')
			->where('Statuses.status_name', 'Active');

		$selectavatars = $query->paginate($per_page)->appends([
			'per_page' => $per_page,
			'readonly' => $readonly
		]);

		$selectavatars = [
			'html' => (string)view('marketplace_avatar.avatars')->with([
				'avatars' => $selectavatars,
				'readonly' => $readonly == 'true' ? true : false
			]),
			'links' => (string)$selectavatars->links()
		];

		return response()->json([
			config('response.status') => config('response.type.success'),
			config('response.data') => compact('selectavatars'),
		]);
	}

	public function store(MarketplaceAvatarRequest $request){
		$inputs = $request->all();
		$inputs['avatar_amount'] = number_format($request->avatar_amount, 2, '.', '');
		$inputs['valid_date'] = date('Y-m-d', strtotime($request->valid_date));
		$inputs['updated_at'] = null;
		$res = MarketplaceAvatar::create($inputs);

		return response()->json([
			config('response.status') => config('response.type.success'),
			config('response.data') => compact('avatars'),
		]);
	}

	public function update(MarketplaceAvatarRequest $request){
		$data = MarketplaceAvatar::where('marketplace_avatar_id', $request->marketplace_avatar_id)->first();
		$inputs = $request->all();
		$inputs['avatar_amount'] = number_format($request->avatar_amount, 2, '.', '');
		$inputs['valid_date'] = date('Y-m-d', strtotime($request->valid_date));
		$data->update($inputs);
		$data->save();

		return response()->json([
			config('response.status') => config('response.type.success'),
			config('response.data') => compact('avatars'),
		]);
	}
}
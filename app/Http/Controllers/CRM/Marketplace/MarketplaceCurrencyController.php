<?php

namespace App\Http\Controllers\CRM\Marketplace;

use App\CRM\baccarat\Applications;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\MarketCurrencyRequest;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\MarketplaceCurrency;
use App\CRM\NinepineModels\Status;
use Illuminate\Http\Request;

class MarketplaceCurrencyController extends NinePineController{
    public function index(){
    	$data = array();

		$status = new Status();
		$status = $status->getStatusOf('MarketplaceCurrency');

		$apps = new Applications();
		$apps = $apps->get([
			'application_id',
			'name'
		]);

		$currencies = Currency::where('app_currency', 1)
			->get([
				'currency_id',
				'currency_name',
				'currency'
			]);

		$data['apps'] = $apps;
		$data['currencies'] = $currencies;
		$data['status'] = $status;
    	$data['pagetitle'] = 'Market Currency';
		$data['marketplace_menu'] = true;
		$data['marketplace_currency_menu'] = true;

    	return view('CRM.marketplace_currency.index')
    		->with($data);
    }

	public function dataTable(Request $request){
		return dataTable($request, MarketplaceCurrency::with([
			'status',
			'applications',
			'itemcurrencies',
			'purchasecurrencies'
		]));
	}

	public function store(MarketCurrencyRequest $request){
        $marketplace_currency = MarketplaceCurrency::create([
            'item_amount' => $request->item_amount,
            'item_currency' => $request->item_currency,
            'purchase_amount' => $request->purchase_amount,
            'purchase_currency' => $request->purchase_currency,
            'application_id' => $request->application_id,
            'status_id' => $request->status_id,
            'expiration_date' => date('Y-m-d H:i:s', strtotime($request->expiration_date))
        ]);

        if (!$request->hasFile('image') || !$marketplace_currency->makeImage($request->file('image'))) {
            $swal = trans('swal.marketplace_currency.create.image.error');

            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => compact('swal')
            ]);
        }
        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
	}

	public function update(MarketCurrencyRequest $request){
        $market = MarketplaceCurrency::find($request->marketplace_currency_id);

        $market->update([
            'item_amount' => $request->item_amount,
            'item_currency' => $request->item_currency,
            'purchase_amount' => $request->purchase_amount,
            'purchase_currency' => $request->purchase_currency,
            'application_id' => $request->application_id,
            'status_id' => $request->status_id,
            'expiration_date' => date('Y-m-d H:i:s', strtotime($request->expiration_date)),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        if ($request->hasFile('image')) {
            if (!$market->makeImage($request->file('image'))) {
                $swal = trans('swal.marketplace_currency.update.image.error');

                return response()->json([
                    config('response.status') => config('response.type.fail'),
                    config('response.data') => compact('swal')
                ]);
            }
        }
        $market->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
	}
}
<?php

namespace App\Http\Controllers\CRM\CMS;

use App\Http\Controllers\CRM\Controller;
use App\Http\Requests\CRM\ContentImageRequest;
use App\Http\Requests\CRM\ContentRequest;
use App\CRM\NinepineModels\Category;
use App\CRM\NinepineModels\Content;
use App\CRM\NinepineModels\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

class ContentController extends Controller
{
    public function dataTable(Request $request)
    {
        return dataTable($request, Content::with('category', 'status'));
    }

    public function index()
    {
        $data['cms_menu'] = true;
        $data['content_menu'] = true;
        $data['page_title'] = 'Contents';
        return view('CRM.cms.content.index')->with($data);
    }

    public function create(Request $request)
    {
        $data['cms_menu'] = true;
        $data['content_menu'] = true;
        $data['page_title'] = 'Add Content';
        $data['statuses'] = Status::where('status_type', 'Content')->get();
        $data['categories'] = Category::all();
        $data['dir'] = 'tmp_' . time();
        return view('CRM.cms.content.create')->with($data);
    }

    public function store(ContentRequest $request)
    {
        $request_values = $request->all();
        $request_values['path'] = '/' . str_slug($request->get('title'));
        $request_values['display_on_homepage'] = !is_null($request->get('display_on_homepage'));
        $request_values['default'] = !is_null($request->get('default'));
        $request_values['content'] = Content::DEFAULT_CONTENT;
        $dir = $request->get('dir');
        $content = Content::create($request_values);
        $content->setAsDefault($request_values['default']);
        $content->content = str_replace($dir, $content->content_id, $request->get('content'));
        $content->save();

        $response = Curl::to(config('web.url') . "/contents/rename_dir")
            ->withData([
                'dir' => $dir,
                'content_id' => $content->content_id
            ])
            ->returnResponseObject()
            ->post();
        $_content = collect(json_decode($response->content, true));
        Log::info($_content);
        return redirect()->route('contents.index');
    }

    public function edit(Request $request, Content $content)
    {
        $data['cms_menu'] = true;
        $data['content_menu'] = true;
        $data['page_title'] = 'Edit Content';
        $data['content'] = $content;
        $data['statuses'] = Status::where('status_type', 'Content')->get();
        $data['categories'] = Category::all();
        return view('CRM.cms.content.edit')->with($data);
    }

    public function update(ContentRequest $request, Content $content)
    {
        $request_values = $request->all();
        $request_values['path'] = '/' . str_slug($request->get('title'));
        $request_values['display_on_homepage'] = !is_null($request->get('display_on_homepage'));
        $request_values['default'] = !is_null($request->get('default'));
        $content->update($request_values);
        $content->save();
        $content->setAsDefault($request_values['default']);
        return redirect()->back();
    }

    public function upload(ContentImageRequest $request)
    {
        $url = Content::makeImage($request->file('content_image'), $request->get('dir'));

        return response()->json([
            config('response.status') => config('response.type.success'),
            config('response.data') => $url
        ]);
    }

    public function destroy(Content $content)
    {
        $dir = storage_path("app/public/contents/$content->content_id");

        if (File::exists($dir)) {
            deleteDir($dir);
        }
        $content->delete();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }
}

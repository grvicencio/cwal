<?php

namespace App\Http\Controllers\CRM\Dashboard;

use App\Http\Controllers\CRM\NinePineController;

class DashboardController extends NinePineController{
    public function index(){
        $data['page_title'] = 'Dashboard';
        $data['dashboard_menu'] = true;

        return view('CRM.dashboard.index')
        	->with($data);
    }
}
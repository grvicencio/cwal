<?php

namespace App\Http\Controllers\CRM\Dashboard;

use App\common\WalletDeposit;
use App\CRM\NinepineModels\Status;
use App\Http\Controllers\CRM\NinePineController;
use Illuminate\Http\Request;
use App\Http\Controllers\CRM\Controller;

class DepositTicketsController extends NinePineController
{
    public function tickets()
    {
        $status_id = Status::where('status_type', 'WalletDeposit')
            ->where('status_name', 'Pending')
            ->first()
            ->status_id;

        $pending_deposits = WalletDeposit::where('status_id', $status_id)->get();

        return response()->view('CRM.dashboard.deposit_tickets', compact('pending_deposits'));
    }
}

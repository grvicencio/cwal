<?php

namespace App\Http\Controllers\CRM\Dashboard;

use App\common\WalletWithdrawal;
use App\CRM\NinepineModels\Status;
use App\Http\Controllers\CRM\NinePineController;
use Illuminate\Http\Request;
use App\Http\Controllers\CRM\Controller;

class WithdrawalTicketsController extends NinePineController
{
    public function tickets()
    {
        $status_id = Status::where('status_type', 'WalletWithdrawal')
            ->where('status_name', 'Pending')
            ->first()
            ->status_id;

        $pending_withdrawals = WalletWithdrawal::where('status_id', $status_id)->get();

        return response()->view('CRM.dashboard.withdrawal_tickets', compact('pending_withdrawals'));
    }
}

<?php

namespace App\Http\Controllers\common;

use Illuminate\Http\Request;
use App\Http\Controllers\CRM\Controller;
use Illuminate\Support\Facades\File;

class AttachmentsController extends Controller
{
    public function getFile($action, $id, $file)
    {
        $_file = storage_path("app/public/attachments/$action/$id/$file");

        if (!File::exists($_file)) {
            abort(404);
        }
        return response()->file($_file);
    }
}

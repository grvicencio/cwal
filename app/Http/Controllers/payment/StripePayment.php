<?php

namespace App\Http\Controllers\payment;

trait StripePayment {
    
    
    public function UpdateCard($customerId, $creditcard = array()) {
        if (empty($customerId)) {
          throw new \Exception('Customer is Requried');
        }
        if (count($creditcard)==0) {
              throw new \Exception('Credit Card info missing ');
         }
        try {
            $expiration = explode("/", $creditcard['valid_thru']);
            $cards = $this->RetrieveCard($customerId);
            foreach ($cards['data'] as $card) {
                $cardid = $card['id'];
                
                $card = \Stripe::cards()->update($customerId, $cardid,[
                            'exp_month' => $expiration[0],
                            'exp_year' => $expiration[1],
                            ]
                        
                        );
            }
            
            
        } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
        }

    }
    
    
    public function CreateCard($email, $creditcard = array()) {
        $customerId ='';
         if (empty($email))
         {
             throw new \Exception('Email is requried');
         }
         if (count($creditcard)==0) {
              throw new \Exception('Credit Card info missing ');
         }
         try {
            $customerId = $this->CreateUser($email);
         } catch (\Excepton $e)  {
             throw new \Exception($e->getMessage());
         }
       

        try {
           $expiration = explode("/", $creditcard['valid_thru']);
            $token  =\Stripe::tokens()->create(
                    [
                      'card' => [
                          'number'    => $creditcard['card_no'],
                          'exp_month' => $expiration[0],
                          'cvc'       => $creditcard['cvc'],
                          'exp_year'  => $expiration[1],
                      ],
                  ]
                 );
        
        } catch (\Exception $e) {
            
             throw new \Exception($e->getMessage());
        }
        try {
            $card = \Stripe::cards()->create($customerId, $token['id']);
           
        } catch (\Exception $e) {
             throw new \Exception($e->getMessage());
        }
        return $customerId;
    }
    
    public function CreateUser($email='') {
         if (empty($email))
         {
             throw new \Exception('Email is required');
         }
         $customer = \Stripe::customers()->create([
            'email' => $email,
           ]);
         return $customer['id'];
    }
    
    public function RetrieveCard($customerId ='') {
        $allcards =array();
        if (empty($customerId)) {
             throw new \Exception('Customer is Requried');
        }
       try {
           $cards = \Stripe::cards()->all($customerId);
           $allcards = $cards;
           
            
       } catch (\Exception $ex) {
           throw new \Exception($e->getMessage());
       }
       return $allcards;
    }
    
    public function Makecharges($customerId, $amount, $currency ='USD', $description, $capture = true, $receipt_email) {
        if (empty($customerId)) {
             throw new \Exception('Customer is Requried');
        }
        $response = '';
        try {
            $charge = \Stripe::charges()->create([
                'customer' => $customerId,
                'currency' => $currency,
                'amount'   => $amount,
                'description' => $description,
                'receipt_email' => $receipt_email,
                'capture' => $capture
            ]);
            $response = $charge;
        } catch (\Exception $ex) {
            throw new \Exception($e->getMessage());
        }
        
        return $response;
    
    }
}
<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class TransactionLog extends Model
{
    protected $connection = 'bit';
    protected $table = 'TransactionLogs';
    protected $primaryKey = 'transaction_log_id';

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }

    public function transaction_status()
    {
        return $this->belongsTo(TransactionStatus::class, 'transaction_status_id');
    }
}

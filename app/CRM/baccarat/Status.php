<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $connection = 'bit';
    protected $table = 'crmstatus';
    protected $primaryKey = 'status_id';

    public function accounts()
    {
        return $this->hasMany(Accounts::class);
    }

    public function memberships()
    {
        return $this->hasMany(MembershipCharges::class);
    }

    public function security_question()
    {
        return $this->hasMany(SecurityQuestion::class);
    }

    public function charge()
    {
        return $this->belongsTo(RegularCharges::class);
    }

    public function user_application()
    {
        return $this->hasMany(UserApplication::class);
    }

    public static function of($status_type)
    {
        return self::where("status_type", $status_type)
            ->whereIn("status_name", [
                "Active",
                "Inactive"
            ])->get();
    }
}

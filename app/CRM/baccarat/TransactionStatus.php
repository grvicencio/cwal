<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class TransactionStatus extends Model
{
    protected $connection = 'bit';
    protected $table = 'TransactionStatuses';
    protected $primaryKey = 'transaction_status_id';

    public function transaction_logs()
    {
        return $this->hasMany(TransactionLog::class);
    }
}

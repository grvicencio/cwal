<?php

namespace App\CRM\baccarat;

use App\CRM\NinepineModels\Currency;
use Illuminate\Database\Eloquent\Model;

class MembershipInclusions extends Model
{
    protected $connection = 'bit';
    protected $table = 'MembershipInclusions';
    protected $primaryKey = 'membership_inclusion_id';

    protected $fillable = [
        'name',
        'description',
        'value',
        'currency_id',
        'type',
        'membership_id'
    ];

    const TYPE_DAILY = 'Daily';
    const TYPE_ONETIME = 'Onetime';

    public static $types = [
        self::TYPE_DAILY,
        self::TYPE_ONETIME
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function membership_charges()
    {
        return $this->belongsTo(MembershipCharges::class, 'membership_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}

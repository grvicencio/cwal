<?php

namespace App\CRM\baccarat;

use App\CRM\NinepineModels\Avatar;
use App\CRM\NinepineModels\Registration;
use App\CRM\NinepineModels\Wallet_ledger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Ixudra\Curl\Facades\Curl;

class Applications extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Applications';
    protected $primaryKey = 'application_id';

    protected $fillable = [
        'name',
        'description',
        'app_image_300_300',
        'app_image_470_260',
        'status_id'
    ];

    public static $sizes = [
        'landscape' => [
            'width' => 470,
            'height' => 260
        ],
        'square' => [
            'width' => 300,
            'height' => 300
        ]
    ];

    public function getImageLandscapeAttribute()
    {
        return $this->app_image_470_260;
    }

    public function getImageSquareAttribute()
    {
        return $this->app_image_300_300;
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'application_id');
    }

    public function user_application()
    {
        return $this->hasMany(UserApplication::class, 'application_id');
    }

    public function accounts()
    {
        return $this->belongsToMany(Accounts::class, 'UserApplications', 'application_id', 'user_id');
    }

    public function registration()
    {
        return $this->hasMany(Registration::class, 'application_id');
    }

    public function avatar()
    {
        return $this->hasMany(Avatar::class, 'application_id');
    }

    public function getAffectedRelations()
    {
        $affected = [];

        if ($this->wallet_ledger()->count()) {
            array_push($affected, [
                'relation' => 'wallet_ledger',
                'count' => $this->wallet_ledger()->count()
            ]);
        }

        if ($this->avatar()->count()) {
            array_push($affected, [
                'relation' => 'avatar',
                'count' => $this->avatar()->count()
            ]);
        }

        if ($this->registration()->count()) {
            array_push($affected, [
                'relation' => 'registration',
                'count' => $this->registration()->count()
            ]);
        }

        if ($this->user_application()->count()) {
            array_push($affected, [
                'relation' => 'user_application',
                'count' => $this->user_application()->count()
            ]);
        }
        return $affected;
    }

    public static function makeImage(UploadedFile $image, $application_id, $width, $height)
    {
        if ($image->isValid()) {
            $size = $width . "x" . $height;
            $dir = "public/applications/$application_id/$size";
            $path = storage_path("app/") . $image->store("$dir");
            $filename = basename($path);

            if (!File::exists(dirname($path))) {
                File::makeDirectory(dirname($path), 0775, true);
            }
            $_image = Image::make($path)
                ->resize($width, $height)
                ->save($path);

            $response = Curl::to(config('web.url') . "/applications/upload")
                ->withData([
                    'application_id' => $application_id,
                    'size' => $size
                ])
                ->withFile('application_image', $path, $_image->mime(), $filename)
                ->returnResponseObject()
                ->post();
            $content = collect(json_decode($response->content, true));

            if ($content->get('result') == "success") {
                deleteDir(str_replace('/' . $size, '', storage_path("app/$dir")));
            }
            return $content->get('data')['url'];
        }
        return null;
    }
}

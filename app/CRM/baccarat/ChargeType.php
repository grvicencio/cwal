<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class ChargeType extends Model
{
    protected $connection = 'bit';
    protected $table = 'ChargeTypes';
    protected $primaryKey = 'charge_type_id';

    public function charge()
    {
        return $this->hasMany(RegularCharges::class);
    }
}

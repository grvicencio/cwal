<?php

namespace App\CRM\baccarat;

use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\MembershipAvatar;
use App\CRM\NinepineModels\Wallet_ledger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Ixudra\Curl\Facades\Curl;

class MembershipCharges extends Model
{
    protected $connection = 'bit';
    protected $table = 'Memberships';
    protected $primaryKey = 'membership_id';

    protected $fillable = [
        'name',
        'description',
        'status_id',
        'billing_cycle',
        'price',
        'membership_type',
        'no_of_tables',
        'banker_rake',
        'logo',
        'currency_id'
    ];

    public static $types = [
        'Basic',
        'Bronze',
        'Gold',
        'Premium'
    ];

    public static $logo_size = [
        'width' => 720,
        'height' => 720
    ];

    const DEFAULT_LOGO = 'DEFAULT';
    const DEFAULT_LOGO_720x720 = '/CRM/Capital7-1.0.0/img/membership/logo/default/720x720/LrnTmiytr2oWsCyuqDZz3TIbInDRGL6geMpAjStu.png';

    public function membership_inclusions()
    {
        return $this->hasMany(MembershipInclusions::class, 'membership_id');
    }

    public function accounts()
    {
        return $this->belongsToMany(Accounts::class, 'UserMemberships', 'membership_id', 'user_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'resource_id', 'membership_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function membership_avatar()
    {
        return $this->hasMany(MembershipAvatar::class, 'membership_id');
    }

    public function user_membership_history()
    {
        return $this->hasMany(UserMembershipHistory::class, 'membership_id');
    }

    public function user_membership()
    {
        return $this->hasMany(UserMembership::class, 'membership_id');
    }

    public function user_membership_inclusion_history()
    {
        return $this->hasMany(UserMembershipInclusionHistory::class, 'membership_id');
    }

    public function makeLogo(UploadedFile $image)
    {
        if ($image->isValid()) {
            $dir = "public/membership/logo/$this->membership_id";
            $original_path = storage_path("app/") . $image->store("$dir/original");
            $filename = basename($original_path);
            $width = self::$logo_size['width'];
            $height = self::$logo_size['height'];

            $_720x720_path = storage_path("app/$dir/$width" . "x$height/") . $filename;

            if (!File::exists(dirname($_720x720_path))) {
                File::makeDirectory(dirname($_720x720_path), 0775, true);
            }
            $_720x720 = Image::make($original_path)
                ->resize($width, $height)
                ->save($_720x720_path);

            $_100x100_path = storage_path("app/$dir/100x100/") . $filename;

            if (!File::exists(dirname($_100x100_path))) {
                File::makeDirectory(dirname($_100x100_path), 0775, true);
            }
            $_100x100 = Image::make($original_path)
                ->resize(100, 100)
                ->save($_100x100_path);

            $response = Curl::to(config('api.url') . "membership/logo/upload")
                ->withData([
                    'membership_id' => $this->membership_id
                ])
                ->withFile('logo_720x720', $_720x720_path, $_720x720->mime(), $filename)
                ->withFile('logo_100x100', $_100x100_path, $_100x100->mime(), $filename)
                ->returnResponseObject()
                ->post();
            $content = collect(json_decode($response->content, true));

            if ($content->get('result') == "success") {
                deleteDir(storage_path("app/$dir"));
                $this->logo = $content->get('data')['url'];
                return $this->save();
            }
            Log::error('API: ' . title_case($content->get('result')) . ': ' . $content->get('message'));
        }
        return false;
    }
}

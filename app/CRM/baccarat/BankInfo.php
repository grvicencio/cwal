<?php
/**
 * Created by PhpStorm.
 * User: NPT 8
 * Date: 3/7/2018
 * Time: 10:53 PM
 */

namespace App\CRM\baccarat;
use Illuminate\Database\Eloquent\Model;

class BankInfo extends Model
{

    protected $connection = 'bit';
    protected $table = 'BankInfo';
    protected $primaryKey = 'bankinfo_id';

    protected $fillable = [
        'user_id',
        'bank_name',
        'bank_branch',
        'bank_account_name',
        'bank_account_number',
        'bank_swift',
        'created_at',
        'updated_at',
        'is_default',
    ];

    public function account()
    {
        return $this->belongsTo(Accounts::class, 'user_id');
    }
}
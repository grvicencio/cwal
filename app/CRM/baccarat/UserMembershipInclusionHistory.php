<?php

namespace App\CRM\baccarat;

use App\CRM\NinepineModels\Currency;
use Illuminate\Database\Eloquent\Model;

class UserMembershipInclusionHistory extends Model
{
    protected $connection = 'bit';
    protected $table = 'UserMembershipInclusionHistory';
    protected $primaryKey = 'user_membership_inclusion_history_id';

    public function user_membership()
    {
        return $this->belongsTo(UserMembership::class, 'user_membership_id');
    }

    public function user_membership_history()
    {
        return $this->belongsTo(UserMembershipHistory::class, 'user_membership_history_id');
    }

    public function membership()
    {
        return $this->belongsTo(MembershipCharges::class, 'membership_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}

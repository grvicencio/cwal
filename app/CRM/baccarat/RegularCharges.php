<?php

namespace App\CRM\baccarat;

use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Wallet_ledger;
use Illuminate\Database\Eloquent\Model;

class RegularCharges extends Model
{
    protected $connection = 'bit';
    protected $table = 'Charges';
    protected $primaryKey = 'charge_id';

    protected $fillable = [
        'name',
        'description',
        'price',
        'charge_type_id',
        'currency_id',
        'wallet_value',
        'status_id'
    ];

    public function transaction_detail()
    {
        return $this->hasOne(TransactionDetail::class, 'charge_id');
    }

    public function charge_type()
    {
        return $this->belongsTo(ChargeType::class, 'charge_type_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'resource_id', 'charge_id');
    }

    public function charge_details()
    {
        return $this->hasOne(ChargeDetail::class, 'charge_id');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'status_id');
    }
}

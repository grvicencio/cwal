<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'ExchangeRates';
    protected $primaryKey = 'exchange_rate_id';

    protected $fillable = [
        'from_currency',
        'to_currency',
        'default_amount',
        'exchange_rate'
    ];

    public static $default_amount = 1.00;

    public function currency_from()
    {
        return $this->belongsTo(Currency::class, 'from_currency', 'currency_id');
    }

    public function currency_to()
    {
        return $this->belongsTo(Currency::class, 'to_currency', 'currency_id');
    }
}

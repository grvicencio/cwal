<?php
/**
 * Created by PhpStorm.
 * User: NPT 8
 * Date: 3/6/2018
 * Time: 2:24 PM
 */

namespace App\CRM\NinepineModels;
use Illuminate\Database\Eloquent\Model;
class WalletDeposit extends Model
{
    protected $connection = 'bit';
    protected $table = 'WalletDeposit';
    protected $primaryKey = 'wallet_deposit_id';
    protected $fillable = ['status_id', 'updated_at', 'notes'];



    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }



}
<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class RoleModule extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'RoleModules';
    protected $primaryKey = 'role_module_id';

    protected $maps = [
        'can_view' => 'readonly',
    ];

    protected $hidden = array(
        'created_at',
        'updated_at'
    );

    protected $fillable = [
        'role_id',
        'module_id',
        'can_add',
        'can_edit',
        'can_delete',
        'admin',
        'readonly'
    ];

    //doens't work :(
    public function getCanViewAttribute($value)
    {
        return $this->attributes['readonly'];
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function module()
    {
        return $this->belongsTo(Module::class, 'module_id');
    }
}

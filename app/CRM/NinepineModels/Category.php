<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Category';
    protected $primaryKey = 'category_id';

    protected $fillable = [
        'category_name',
        'parent_category_id',
        'sort',
        'status_id'
    ];

    public function scopeSearch($query, $search, $cols = null)
    {
        if (is_null($cols)) {
            $except = [
                array_search('parent_category_id', $this->fillable),
                array_search('status_id', $this->fillable)
            ];

            $cols = array_except($this->fillable, $except);
        }

        foreach ($cols as $key => $value) {
            if ($key == 0) {
                $query->where($value, 'ILIKE', "%$search%");
            }
            $query->orWhere($value, 'ILIKE', "%$search%");
        }
        return $query;
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_category_id', 'category_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_category_id', 'category_id');
    }

    public function content()
    {
        return $this->hasMany(Content::class, 'category_id');
    }

    public static function toTree()
    {
        $categories = [];

        foreach (self::all() as $category) {
            array_push($categories, [
                'id' => $category->category_id,
                'category_id' => $category->category_id,
                'name' => $category->category_name,
                'category_name' => $category->category_name,
                'parent_id' => $category->parent_category_id,
                'parent_category_id' => $category->parent_category_id,
                'status_id' => $category->status_id
            ]);
        }
        return buildTree($categories);
    }

    public static function reorder($parent, $from, $to) {
        $categories = Category::where('parent_category_id', $parent)
            ->orderBy('sort', 'asc')
            ->get()
            ->toArray();
        $moved_categories = moveElement($categories, $from, $to);

        for ($i = 0; $i < count($moved_categories); $i++) {
            $_category = self::find($moved_categories[$i]['category_id']);
            $_category->sort = $i;
            $_category->save();
        }
    }

    public function getAffectedRelations()
    {
        $affected = [];

        if ($this->content()->count()) {
            array_push($affected, [
                'relation' => 'content',
                'count' => $this->content()->count()
            ]);
        }

        if ($this->children()->count()) {
            array_push($affected, [
                'relation' => 'children',
                'count' => $this->children()->count()
            ]);
        }
        return $affected;
    }
}

<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class SubticketCount extends Model{
	protected $connection = 'bit';
	protected $table = 'TradeMarketTicket';
	protected $primaryKey = "trademarket_ticket_parent_id";

	public function subticketcount(){
		return $this->belongsTo(TradeMarketTicket::class, 'trademarket_ticket_parent_id');
	}
}
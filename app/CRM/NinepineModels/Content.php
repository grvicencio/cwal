<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Ixudra\Curl\Facades\Curl;

class Content extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Contents';
    protected $primaryKey = 'content_id';

    protected $fillable = [
        'title',
        'content',
        'category_id',
        'path',
        'default',
        'display_on_homepage',
        'status_id'
    ];

    const DEFAULT_CONTENT = 'DEFAULT';

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function scopeSearch($query, $search, $cols = null)
    {
        if (is_null($cols)) {
            $except = [
                array_search('category_id', $this->fillable),
                array_search('display_on_homepage', $this->fillable),
                array_search('default', $this->fillable),
                array_search('status_id', $this->fillable)
            ];

            $cols = array_except($this->fillable, $except);
        }

        foreach ($cols as $key => $value) {
            if ($key == 0) {
                $query->where($value, 'ILIKE', "%$search%");
            }
            $query->orWhere($value, 'ILIKE', "%$search%");
        }
        return $query;
    }

    public function setAsDefault($is_default)
    {
        if ($is_default) {
            $contents = $this->getDefaults();

            if ($contents->count()) {
                $contents->each(function ($content) {
                    $content->default = false;
                    $content->save();
                });
            }
            return;
        }
        $this->setDefaultIfNone();
    }

    private function getDefaults()
    {
        return self::where('default', true)
            ->where('category_id', $this->category_id)
            ->where('content_id', '!=', $this->content_id)
            ->get();
    }

    private function setDefaultIfNone()
    {
        $contents = $this->getDefaults();

        if (!$contents->count()) {
            $this->default = true;
            $this->save();
        }
    }

    public static function makeImage(UploadedFile $image, $dir)
    {
        if ($image->isValid()) {
            $_dir = "public/contents/$dir";
            $path = storage_path("app/") . $image->store("$_dir");
            $filename = basename($path);

            if (!File::exists(dirname($path))) {
                File::makeDirectory(dirname($path), 0775, true);
            }
            $_image = Image::make($path)->save($path);

            $response = Curl::to(config('web.url') . "/contents/upload")
                ->withData([
                    'dir' => $dir
                ])
                ->withFile('content_image', $path, $_image->mime(), $filename)
                ->returnResponseObject()
                ->post();
            $content = collect(json_decode($response->content, true));

            if ($content->get('result') == "success") {
                deleteDir(storage_path("app/$_dir"));
                return $content->get('data')['url'];
            }
            Log::error('WEBCORE: ' . title_case($content->get('result')) . ': ' . $content->get('message'));
        }
        return null;
    }
}

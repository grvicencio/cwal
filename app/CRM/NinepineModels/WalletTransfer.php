<?php

namespace App\CRM\NinepineModels;

use App\CRM\baccarat\Accounts;
use Illuminate\Database\Eloquent\Model;

class WalletTransfer extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'WalletTransfer';
    protected $primaryKey = 'wallet_transfer_id';

    protected $fillable = [
        'sender_id',
        'sender_wallet_id',
        'receiver_id',
        'receiver_wallet_id',
        'amount_transfer',
        'reason',
        'currency_id',
        'status_id',
    ];

    public function sender()
    {
        return $this->belongsTo(Accounts::class, 'sender_id', 'id');
    }

    public function receiver()
    {
        return $this->belongsTo(Accounts::class, 'receiver_id', 'id');
    }

    public function sender_wallet()
    {
        return $this->belongsTo(Wallet::class, 'sender_wallet_id', 'wallet_id');
    }

    public function receiver_wallet()
    {
        return $this->belongsTo(Wallet::class, 'receiver_wallet_id', 'wallet_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'resource_id', 'wallet_transfer_id');
    }
}

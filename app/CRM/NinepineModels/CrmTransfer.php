<?php

namespace App\CRM\NinepineModels;

use App\CRM\baccarat\Accounts;
use App\CRM\User;
use Illuminate\Database\Eloquent\Model;

class CrmTransfer extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'CrmTransfer';
    protected $primaryKey = 'crm_transfer_id';

    protected $fillable = [
        'transfer_amount',
        'currency_id',
        'crm_user_id',
        'reason',
        'user_id'
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function account()
    {
        return $this->belongsTo(Accounts::class, 'user_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'crm_user_id', 'id');
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'resource_id', 'crm_transfer_id');
    }
}

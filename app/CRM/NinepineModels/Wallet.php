<?php

namespace App\CRM\NinepineModels;

use App\common\BankInfo;
use App\common\TradeMarketTicketTransfer;
use App\common\WalletDeposit;
use App\common\WalletWithdrawal;
use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\Applications;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Wallet';
    protected $primaryKey = 'wallet_id';

    protected $fillable = [
        'balance',
        'currency_id',
        'source_id',
        'user_id',
        'wallet_account_number'
    ];

    protected $casts = [
        'balance' => 'double',
    ];

    const TYPE_CHARGE = 'CHARGE';
    const TYPE_DISCHARGE = 'DISCHARGE';

    public function account()
    {
        return $this->belongsTo(Accounts::class, 'user_id')->select([
            "id",
            "name",
            "email",
            "username",
            "account_image"
        ]);
    }

    public function source()
    {
        return $this->belongsTo(Source::class, 'source_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'wallet_id');
    }

    public function bank()
    {
        return $this->hasMany(BankInfo::class, 'bankinfo_id', 'bank_id');
    }

    public static function makeTransaction(Accounts $receiver, $amount, Currency $currency, Source $source, $resource_id, Applications $application, $type, $wallet_account_number = null)
    {
        $wallet = null;
        $debit = doubleval(0);
        $credit = doubleval(0);

        if (!$receiver->wallet()->count()) {
            $wallet = $receiver->wallet()->create([
                'balance' => $amount,
                'currency_id' => $currency->currency_id,
                'wallet_account_number' => GenerateAccount()
            ]);
        } else {
            if (!is_null($wallet_account_number)) {
                $wallet = self::findByAccountNumber($wallet_account_number);
            } else {
                $wallet = $receiver->wallet()->where('currency_id', $currency->currency_id)->first();
            }

            if (is_null($wallet)) {
                $wallet = $receiver->wallet()->create([
                    'balance' => $amount,
                    'currency_id' => $currency->currency_id,
                    'wallet_account_number' => GenerateAccount()
                ]);
            } else {
                if ($type == self::TYPE_CHARGE) {
                    $wallet->balance += $amount;
                } else {
                    $wallet->balance -= $amount;
                }
                $wallet->save();
            }
        }

        if ($type == self::TYPE_CHARGE) {
            $debit = $amount;
        } else {
            $credit = $amount;
        }
        return Wallet_ledger::create([
            'wallet_id' => $wallet->wallet_id,
            'source_id' => $source->source_id,
            'resource_id' => $resource_id,
            'debit' => $debit,
            'credit' => $credit,
            'application_id' => $application->application_id,
            'balance' => $wallet->balance
        ]);
    }

    public static function findByAccountNumber($wallet_account_number)
    {
        return self::where("wallet_account_number", $wallet_account_number)->first();
    }

    public function wallet_deposit()
    {
        return $this->hasMany(WalletDeposit::class, 'wallet_id');
    }

    public function wallet_withdrawal()
    {
        return $this->hasMany(WalletWithdrawal::class, 'wallet_id');
    }

    public function trade_market_ticket_transfer_sender()
    {
        return $this->hasMany(TradeMarketTicketTransfer::class, 'sender_wallet_id', 'wallet_id');
    }

    public function trade_market_ticket_transfer_receiver()
    {
        return $this->hasMany(TradeMarketTicketTransfer::class, 'receiver_wallet_id', 'wallet_id');
    }
}

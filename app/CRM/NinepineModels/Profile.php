<?php

namespace App\CRM\NinepineModels;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class Profile extends Model{
	protected $connection = 'bit_crm';
	protected $table = 'Users';
	protected $primaryKey ='id';
	protected $fillable = [
		'image',
	];

	public static $size = [
		'width' => 1024,
		'height' => 1024
	];

	public function makeImage(UploadedFile $image){
		if($image->isValid()){
			$dir = "crmuser/$this->id";
			$original_path = storage_path("app/public/$dir/") . $image->store("public/$dir");
			$filename = basename($original_path);
			$path = storage_path("app/public/$dir/") . $filename;

			if(!File::exists(dirname($path))){
				File::makeDirectory(dirname($path), 0775, true);
			}

			Image::make($image->getRealPath())->save($path);
			$this->image = "admin/security/userimg/$this->id/$filename";

			return $this->save();
		}

		return false;
	}
}
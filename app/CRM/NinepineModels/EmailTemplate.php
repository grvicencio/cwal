<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'EmailTemplates';
    protected $primaryKey = 'email_template_id';

    protected $fillable = [
        'email_type',
        'subject',
        'content'
    ];

    public static $email_types = [
        [
            "name" => "WEB_WITHDRAWAL",
            "subject" => "Withdrawal Request Status",
            "variables" => [
                "name",
                "amount",
                "status",
                "notes",
                "app_url",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_DEPOSIT",
            "subject" => "Deposit Request Status",
            "variables" => [
                "name",
                "amount",
                "status",
                "notes",
                "app_url",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_TRANSFER_FROM_CRM",
            "subject" => "Wallet Transfer",
            "variables" => [
                "receiver",
                "amount",
                "app_url",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_TRANSFER_SENDER",
            "subject" => "Wallet Transfer Sent",
            "variables" => [
                "sender",
                "receiver",
                "amount",
                "message",
                "app_url",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_TRANSFER_RECEIVER",
            "subject" => "Wallet Transfer Received",
            "variables" => [
                "sender",
                "receiver",
                "amount",
                "message",
                "app_url",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_REGISTER",
            "subject" => "Wallet Registration",
            "variables" => [
                "receiver",
                "amount",
                "message",
                "app_url",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_FORGOT_PASSWORD",
            "subject" => "Password Reset Link",
            "variables" => [
                "name",
                "link",
                "app_name",
            ],
        ],
        [
            "name" => "CRM_WITHDRAWAL",
            "subject" => "Withdrawal Request",
            "variables" => [
                "name",
                "amount",
                "notes",
                "app_url",
                "app_name",
                'bank_name',
                'bank_branch',
                'bank_address',
                'bank_account_name',
                'bank_account_number',
                'bank_swift',
                'bank_code',
                'bank_currency',
                'bank_address_2',
                'bank_country',
            ],
        ],
        [
            "name" => "CRM_DEPOSIT",
            "subject" => "Deposit Request",
            "variables" => [
                "name",
                "amount",
                "notes",
                "app_url",
                "app_name",
            ],
        ],
        [
            "name" => "CRM_TRANSFER",
            "subject" => "Wallet Transfer Information",
            "variables" => [
                "sender",
                "receiver",
                "amount",
                "app_url",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_ACCOUNT_ACTIVATION",
            "subject" => "Account Activation Link",
            "variables" => [
                "name",
                "link",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_TICKET_HAS_AN_OFFER",
            "subject" => "Ticket Has An Offer",
            "variables" => [
                "trader",
                "offer",
                "offer_type",
                "offer_ticket_generated_id",
                "offer_value",
                "offer_amount",
                "offer_text",
                "app_url",
                "ticket_generated_id",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_TICKET_HAS_EXPIRED",
            "subject" => "Ticket Has Expired",
            "variables" => [
                "trader",
                "app_url",
                "ticket_generated_id",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_TICKET_HAS_PAYMENT",
            "subject" => "Ticket Has Payment",
            "variables" => [
                "trader",
                "offer",
                "offer_type",
                "offer_ticket_generated_id",
                "offer_value",
                "offer_amount",
                "app_url",
                "ticket_generated_id",
                "app_name",
                "notes",
                "deposit_id",
            ],
        ],
        [
            "name" => "WEB_TICKET_REACHES_ZERO_VALUE",
            "subject" => "Ticket Reaches Zero Value",
            "variables" => [
                "trader",
                "app_url",
                "ticket_generated_id",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_TICKET_WAS_APPROVED",
            "subject" => "Ticket Was Approved",
            "variables" => [
                "trader",
                "offer",
                "offer_ticket_generated_id",
                "app_url",
                "ticket_generated_id",
                "app_name",
                'bank_name',
                'bank_account_name',
                'bank_account_number',
                'deposit_id',
                'notes',
            ],
        ],
        [
            "name" => "WEB_TICKET_WAS_CLOSED",
            "subject" => "Ticket Was Closed",
            "variables" => [
                "trader",
                "app_url",
                "ticket_generated_id",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_TICKET_WAS_OPENED",
            "subject" => "Ticket Was Opened",
            "variables" => [
                "trader",
                "app_url",
                "ticket_generated_id",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_TICKET_WAS_PAYED",
            "subject" => "Ticket Was Payed",
            "variables" => [
                "trader",
                "offer",
                "offer_ticket_generated_id",
                "app_url",
                "app_name",
            ],
        ],
        [
            "name" => "WEB_TICKET_WAS_REJECTED",
            "subject" => "Ticket Was Rejected",
            "variables" => [
                "trader",
                "offer",
                "offer_ticket_generated_id",
                "app_url",
                "app_name",
            ],
        ],
         [
            "name" => "WEB_TICKET_COMPLETED",
            "subject" => "Ticket Completed",
            "variables" => [
                "trader",
                "offer",
                "offer_ticket_generated_id",
                "app_url",
                "app_name",
            ],
        ],
    ];

    public static $email_variables = [
        [
            "key" => 'deposit_id',
            "value" => '{{$deposit_id}}',
        ],
        [
            "key" => 'notes',
            "value" => '{{$notes}}',
        ],
        [
            "key" => 'ticket_generated_id',
            "value" => '{{$ticket_generated_id}}',
        ],
        [
            "key" => 'offer_text',
            "value" => '{{$offer_text}}',
        ],
        [
            "key" => 'offer_amount',
            "value" => '{{$offer_amount}}',
        ],
        [
            "key" => 'offer_value',
            "value" => '{{$offer_value}}',
        ],
        [
            "key" => 'offer_ticket_generated_id',
            "value" => '{{$offer_ticket_generated_id}}',
        ],
        [
            "key" => 'offer_type',
            "value" => '{{$offer_type}}',
        ],
        [
            "key" => 'offer',
            "value" => '{{$offer}}',
        ],
        [
            "key" => 'trader',
            "value" => '{{$trader}}',
        ],
        [
            "key" => 'name',
            "value" => '{{$name}}',
        ],
        [
            "key" => 'amount',
            "value" => '{{$amount}}'
        ],
        [
            "key" => 'notes',
            "value" => '{{$notes}}'
        ],
        [
            "key" => 'app_url',
            "value" => '{{$app_url}}'
        ],
        [
            "key" => 'app_name',
            "value" => '{{$app_name}}'
        ],
        [
            "key" => 'sender',
            "value" => '{{$sender}}'
        ],
        [
            "key" => 'receiver',
            "value" => '{{$receiver}}'
        ],
        [
            "key" => 'status',
            "value" => '{{$status}}'
        ],
        [
            "key" => 'message',
            "value" => '{{$message}}'
        ],
        [
            "key" => 'link',
            "value" => '{{$link}}'
        ],
        [
            "key" => 'bank_name',
            "value" => '{{$bank_name}}'
        ],
        [
            "key" => 'bank_branch',
            "value" => '{{$bank_branch}}'
        ],
        [
            "key" => 'bank_address',
            "value" => '{{$bank_address}}'
        ],
        [
            "key" => 'bank_address_2',
            "value" => '{{$bank_address_2}}'
        ],
        [
            "key" => 'bank_account_name',
            "value" => '{{$bank_account_name}}'
        ],
        [
            "key" => 'bank_account_number',
            "value" => '{{$bank_account_number}}'
        ],
        [
            "key" => 'bank_swift',
            "value" => '{{$bank_swift}}'
        ],
        [
            "key" => 'bank_currency',
            "value" => '{{$bank_currency}}'
        ],
        [
            "key" => 'bank_code',
            "value" => '{{$bank_code}}'
        ],
        [
            "key" => 'bank_country',
            "value" => '{{$bank_country}}'
        ],
    ];
}

<?php

namespace App\CRM\NinepineModels;

use App\API\Tradeprofile;
use App\common\BankInfo;
use App\common\TradeMarketTicket;
use App\common\WalletDeposit;
use App\common\WalletWithdrawal;
use App\CRM\baccarat\MembershipCharges;
use App\CRM\baccarat\MembershipInclusions;
use App\CRM\baccarat\RegularCharges;
use App\CRM\baccarat\UserMembership;
use App\CRM\baccarat\UserMembershipHistory;
use App\CRM\baccarat\UserMembershipInclusionHistory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Currency';
    protected $primaryKey = 'currency_id';

    protected $fillable = [
        'currency_id',
        'currency_name',
        'currency_symbol',
        'currency',
        'app_currency',
        'default_registration'
    ];

    public function scopeSearch($query, $search, $cols = null)
    {
        if (is_null($cols)) {
            $except = [
                array_search('currency_id', $this->fillable),
                array_search('app_currency', $this->fillable),
                array_search('default_registration', $this->fillable)
            ];

            $cols = array_except($this->fillable, $except);
        }

        foreach ($cols as $key => $value) {
            if ($key == 0) {
                $query->where($value, 'ILIKE', "%$search%");
            }
            $query->orWhere($value, 'ILIKE', "%$search%");
        }
        return $query;
    }

    public function wallet()
    {
        return $this->hasMany(Wallet::class);
    }

    public function bank_info()
    {
        return $this->hasMany(BankInfo::class);
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class);
    }

    public function daily_bonus()
    {
        return $this->hasMany(DailyBonus::class);
    }

    public function wallet_deposit()
    {
        return $this->hasMany(WalletDeposit::class);
    }

    public function trade_profile_global_currency()
    {
        return $this->hasMany(Tradeprofile::class, 'globalcurrency_id', 'currency_id');
    }

    public function trade_profile_crypto_currency()
    {
        return $this->hasMany(Tradeprofile::class, 'cryptocurrency_id', 'currency_id');
    }

    public function wallet_withdrawal()
    {
        return $this->hasMany(WalletWithdrawal::class);
    }

    public function crm_transfer()
    {
        return $this->hasMany(CrmTransfer::class);
    }

    public function wallet_transfer()
    {
        return $this->hasMany(WalletTransfer::class);
    }

    public function exchange_rate()
    {
        return $this->hasMany(ExchangeRate::class);
    }

    public function charge()
    {
        return $this->hasMany(RegularCharges::class);
    }

    public function membership()
    {
        return $this->hasMany(MembershipCharges::class);
    }

    public function membership_inclusion()
    {
        return $this->hasMany(MembershipInclusions::class);
    }

    public function user_membership_history()
    {
        return $this->hasMany(UserMembershipHistory::class, 'currency_id');
    }

    public function user_membership()
    {
        return $this->hasMany(UserMembership::class, 'currency_id');
    }

    public function user_membership_inclusion_history()
    {
        return $this->hasMany(UserMembershipInclusionHistory::class, 'currency_id');
    }

    public static function getDefault()
    {
        return self::where('default_registration', true)->first();
    }

    public function registration()
    {
        return $this->hasMany(Registration::class);
    }

    public function trade_market_ticket()
    {
        return $this->hasMany(TradeMarketTicket::class);
    }

    public static function resetDefaultRegistration($default_registration)
    {
        if (boolval($default_registration)) {
            $currencies = self::where('default_registration', true);

            if ($currencies->count()) {
                $currencies->each(function ($currency) {
                    $currency->default_registration = false;
                    $currency->save();
                });
            }
        }
    }

    public static function crypto($only = true) {
        if (!$only) {
            return self::where('app_currency', $only)
                ->orWhere('app_currency', null)
                ->get();
        }
        return self::where('app_currency', $only)->get();
    }
}

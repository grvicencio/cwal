<?php

namespace App\CRM;

use App\CRM\NinepineModels\CrmTransfer;
use App\CRM\NinepineModels\Module;
use App\CRM\NinepineModels\Role;
use App\CRM\NinepineModels\Status;
use App\CRM\NinepineModels\UserRole;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use InvalidArgumentException;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection = 'bit_crm';

    protected $table = 'Users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'firstname',
        'lastname',
        'status_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $append = ['superuser'];

    public static $size = [
        'width' => 1024,
        'height' => 1024
    ];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function user_role()
    {
        return $this->hasOne(UserRole::class, 'user_id', 'id');
    }

    public function crm_transfer()
    {
        return $this->hasMany(CrmTransfer::class, 'crm_user_id', 'id');
    }

    public function role()
    {
        return $this->hasMany(Role::class, 'created_by', 'id');
    }

    public function getSuperuserAttribute()
    {
        try {
            $attribute = $this->user_role->role->role_name;
            if (empty($attribute)) {
                $attribute = 'superadmin';
            }

        } catch (\Exception $e) {
            $attribute = 'superadmin';
        }
        return $this->attributes['superuser'] = $attribute;
    }

    public function getStatusNameAttribute()
    {
        //don't know why this wont work
        // return $this->hasOne('App\NinepineModels\Status', 'status_id');
        $status_name = Status::where('status_id', $this->status_id)->first()->status_name;
        return $status_name;
    }

    public function canAccess($permission, $route_name)
    {
        if (!in_array($permission, config('permissions'))) {
            throw new InvalidArgumentException("Permission [{$permission}] not defined.");
        }

        if (in_array('admin', $this->permissions($route_name)) || Auth::guard('crm')->user()->superuser === "superadmin") {
            return true;
        }
        return in_array($permission, $this->permissions($route_name));
    }

    private function permissions($route_name)
    {
        $action_name = getActionName($route_name);
        $controller_name = getControllerName($action_name);
        $module_name = getModuleName($action_name);
        $user_role = $this->user_role()->first();

        if (is_null($user_role)) {
            return array();
        }
        $module = Module::where('module_name', $module_name)
            ->where('controller_name', $controller_name)
            ->first();

        if (is_null($module)) {
            return array();
        }
        $role_module = $user_role->role->role_module()
            ->where('module_id', $module->module_id)
            ->first();

        if (is_null($role_module)) {
            return array();
        }
        $permissions = [];

        if ($role_module->admin) {
            array_push($permissions, 'admin');
        }

        if ($role_module->readonly) {
            array_push($permissions, 'view');
        }

        if ($role_module->can_add) {
            array_push($permissions, 'add');
        }

        if ($role_module->can_edit) {
            array_push($permissions, 'edit');
        }

        if ($role_module->can_delete) {
            array_push($permissions, 'delete');
        }
        return $permissions;
    }

    public function getAffectedRelations()
    {
        $affected = [];

        if ($this->status()->count()) {
            array_push($affected, [
                'relation' => 'status',
                'count' => $this->status()->count()
            ]);
        }

        if ($this->user_role()->count()) {
            array_push($affected, [
                'relation' => 'user_role',
                'count' => $this->user_role()->count()
            ]);
        }

        if ($this->crm_transfer()->count()) {
            array_push($affected, [
                'relation' => 'crm_transfer',
                'count' => $this->crm_transfer()->count()
            ]);
        }
        return $affected;
    }

    public static function superAdmin() {
        return self::whereIn('email', [
            'superadmin@email.com',
            'superadmin@ninepinetech.com'
        ])->first();
    }

    public function makeImage(UploadedFile $image){
        if($image->isValid()){
            $dir = "crmuser/$this->id";
            $original_path = storage_path("app/public/$dir/") . $image->store("public/$dir");
            $filename = basename($original_path);
            $path = storage_path("app/public/$dir/") . $filename;

            if(!File::exists(dirname($path))){
                File::makeDirectory(dirname($path), 0775, true);
            }

            Image::make($image->getRealPath())->save($path);
            $this->image = "admin/security/userimg/$this->id/$filename";

            return $this->save();
        }

        return false;
    }
}

<?php

namespace App\Console\Commands;

use App\baccarat\Accounts;
use App\baccarat\Applications;
use App\baccarat\MembershipInclusions;
use App\baccarat\Status;
use App\NinepineModels\Currency;
use App\NinepineModels\Source;
use App\NinepineModels\Wallet;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class FreeDailyMembership extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:FreeDailyMembership';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give membership to user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Do strict checks

        $status = Status::where('status_type', 'Account')
            ->where('status_name', 'Active')
            ->first();

        if (is_null($status)) {
            Log::error('Status is NULL');
            return;
        }
        $source = Source::where('source_name', 'Membership')->first();

        if (is_null($source)) {
            Log::error('Source is NULL');
            return;
        }
        $application = Applications::where('name', 'Baccarat')->first();

        if (is_null($application)) {
            Log::error('Application is NULL');
            return;
        }
        Log::useDailyFiles(storage_path() . '/logs/free-daily-membership-cron.log');
        $accounts = Accounts::where('status_id', $status->status_id)
            ->whereNotNull('validation_key')
            ->get();
        $bar = $this->output->createProgressBar($accounts->count());

        foreach ($accounts as $account) {
            $membership = $account->memberships()->first();

            if (is_null($membership)) {
                continue;
            }
            $type = Wallet::TYPE_CHARGE;
            $resource_id = $membership->membership_id;
            $membership_inclusions = $membership->membership_inclusions()->where('type', MembershipInclusions::TYPE_DAILY)->get();

            foreach ($membership_inclusions as $inclusion) {
                $currency = Currency::find($inclusion->currency_id);

                if (is_null($currency)) {
                    Log::error('Currency is NULL');
                    continue;
                }
                $amount = $inclusion->value;
                $wallet_ledger = Wallet::makeTransaction($account, $amount, $currency, $source, $resource_id, $application, $type);

                if ($wallet_ledger) {
                    $log_entry = [
                        'FREE_DAILY_MEMBERSHIP' => compact(
                            'wallet_ledger',
                            'status',
                            'account',
                            'source',
                            'application',
                            'amount',
                            'currency',
                            'resource_id',
                            'type'
                        )
                    ];
                    Log::info($log_entry);
                }
            }
            $bar->advance();
        }
        $bar->finish();
        $this->line(' DONE');
    }
}

<?php

namespace App\Console\Commands;

use App\baccarat\Accounts;
use App\baccarat\Applications;
use App\baccarat\Status;
use App\NinepineModels\Currency;
use App\NinepineModels\DailyBonus;
use App\NinepineModels\Source;
use App\NinepineModels\Wallet;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class FreePoints extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:FreePoints';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give free points to user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Do strict checks

        $status = Status::where('status_type', 'Account')
            ->where('status_name', 'Active')
            ->first();

        if (is_null($status)) {
            Log::error('Status is NULL');
            return;
        }
        $source = Source::where('source_name', 'DailyBonuses')->first();

        if (is_null($source)) {
            Log::error('Source is NULL');
            return;
        }
        $daily_bonuses = DailyBonus::where('type', 'Register')->get();

        if (!$daily_bonuses->count()) {
            Log::error('DailyBonuses is EMPTY');
            return;
        }
        $application = Applications::where('name', 'Baccarat')->first();

        if (is_null($application)) {
            Log::error('Application is NULL');
            return;
        }
        Log::useDailyFiles(storage_path() . '/logs/daily-bonus-cron.log');
        $accounts = Accounts::where('status_id', $status->status_id)
            ->whereNotNull('validation_key')
            ->get();
        $type = Wallet::TYPE_CHARGE;
        $bar = $this->output->createProgressBar($accounts->count());

        foreach ($accounts as $account) {
            foreach ($daily_bonuses as $daily_bonus) {
                $currency = Currency::find($daily_bonus->currency_id);

                if (is_null($currency)) {
                    Log::error('Currency is NULL');
                    break;
                }
                $amount = $daily_bonus->points;
                $resource_id = $daily_bonus->daily_bonus_id;
                $wallet_ledger = Wallet::makeTransaction($account, $amount, $currency, $source, $resource_id, $application, $type);

                if ($wallet_ledger) {
                    $log_entry = [
                        'FREE_POINTS' => compact(
                            'wallet_ledger',
                            'status',
                            'account',
                            'source',
                            'daily_bonus',
                            'application',
                            'amount',
                            'currency',
                            'resource_id',
                            'type'
                        )
                    ];
                    Log::info($log_entry);
                }
                $bar->advance();
            }
        }
        $bar->finish();
        $this->line(' DONE');
    }
}

<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\Config;
use Illuminate\Console\Command;
use App\CRM\NinepineModels\ExchangeRate;
use App\CRM\NinepineModels\Currency;
use Ixudra\Curl\Facades\Curl;
class ExchangeRateConsole extends Command
{
    private $allCurrencies;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ExchangeRateConsole';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Real Time exchange rate mining';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        // let us delete first exchangerate 
        try {
            $this->allCurrencies = $this->getAllCurrency();
            $deleteE = ExchangeRate::all();
            if (count($deleteE) <>0) {
                foreach($deleteE as $e) {
                    $e->delete();
                }    
            }
                    
            $allCurrency = Currency::where('app_currency','=',false)->get();
            $allCurrencyparent = $allCurrency;
            $totalCurrency = $allCurrency->count();

            $defaultCurrency='USD';
            foreach($allCurrencyparent as $currencyParent){
                $from_id = $currencyParent->currency_id;
                $from_currency_symbol = $currencyParent->currency_symbol;
                foreach($allCurrency as $currency) {
                    $to_id = $currency->currency_id;//to
                    $to_currency_symbol = $currency->currency_symbol;
                    // let us check if same currency conversion 
                    
                   // if ($from_id !== $to_id){
                    // let us try inserting 
                   $exchange_rate = $this->GetExchange($from_currency_symbol, $to_currency_symbol);
                    $exchange=new ExchangeRate();
                    $exchange->from_currency = $from_id;
                    $exchange->to_currency = $to_id;
                    $exchange->default_amount =1;
                    $exchange->exchange_rate =$exchange_rate;
                    $exchange->save();
                  //  }


                }
            }
        } catch (\Exception $e) {
            \Log::debug( $e->getMessage());
        }    
        
        
    }
    
    private function getAllCurrency() {
        // this freakin url value should be put in .env and config, the hell with console not getting data from config file
        $url = 'http://apilayer.net/api/live?access_key=0372154f2eb874dc625bcf0bb79a8737&format=1';
       $response = Curl::to($url)
                    ->withHeader('Accept: application/json')
                    ->get();
       $response = json_decode($response,true);
       return $response;
    }
    
    public  function GetExchange($from_currency, $to_currency) {
        $currencies = $this->allCurrencies['quotes'];
        $fromconversion = $currencies['USD'. $from_currency];
        $toconversion = $currencies['USD'. $to_currency];
        $conversion =  $toconversion / $fromconversion;
        return $conversion;
        
    }
}

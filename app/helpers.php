<?php
/**
 * Custom Helpers (simple way)
 * will change to more organize if the need arise
 *
 */

use App\API\Wallet;
use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\EmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Ixudra\Curl\Facades\Curl;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\Debug\Exception\UndefinedMethodException;

function dataTable(Request $request, $query, $cols = null)
{
    $order = collect($request->input('order')[0]);
    $col = collect($request->input('columns')[$order->get('column')])->get('data');
    $dir = $order->get('dir');

    $q = trim($request->input('search')['value']);
    $len = $request->input('length');
    $page = ($request->input('start') / $len) + 1;

    Paginator::currentPageResolver(function () use ($page) {
        return $page;
    });

    $pagin = null;

    if (!empty($q)) {
        $pagin = $query->search($q, $cols)->orderBy($col, $dir)->paginate($len);
    } else {
        $pagin = $query->orderBy($col, $dir)->paginate($len);
    }

    return response()->json([
        "draw" => intval($request->input('draw')),
        "recordsTotal" => $pagin->total(),
        "recordsFiltered" => $pagin->total(),
        "data" => $pagin->items()
    ]);
}

function swal($title, $html, $type)
{
    $swal = [
        'title' => $title,
        'html' => $html,
        'type' => $type
    ];

    return compact('swal');
}

function getPermission($action)
{
    foreach (config('actionmethods') as $key => $value) {
        if (in_array($action, $value)) {
            return $key;
        }
    }

    return null;
}

function getControllerName($action_name)
{
    $matches = array();
    preg_match('/.*\\\([a-zA-z]+)/', $action_name, $matches);

    return substr($matches[1], 0, -10);
}

function getActionMethod($action_name)
{
    $matches = array();
    preg_match('/@([a-zA-z]+)/', $action_name, $matches);

    return $matches[1];
}

function getActionName($route_name)
{
    $route = collect(Route::getRoutes())->where('action.as', $route_name)->first();

    if (is_null($route)) {
        throw new InvalidArgumentException("Route [{$route_name}] not defined.");
    }

    return $route->action['uses'];
}

function getModuleName($action_name)
{
    $matches = array();
    preg_match('/.*\\\([a-zA-z]+)\\\/', $action_name, $matches);

    return $matches[1];
}

function removeArrKeyPrefix($input_arr, $prefix)
{
    $map = array_map(function ($key) use ($prefix) {
        return str_replace($prefix, '', $key);
    }, array_keys($input_arr));

    return array_combine($map, $input_arr);
}

function spaceCase($string)
{
    $pattern = '/(.*?[a-z]{1})([A-Z]{1}.*?)/';
    $replace = '${1} ${2}';

    return preg_replace($pattern, $replace, $string);
}

function moduleExists($module_name, $controller_name)
{
    $loader = require base_path('vendor/autoload.php');
    $modules = [];

    foreach ($loader->getClassMap() as $class => $file) {
        if (preg_match('/[a-z]+Controller$/', $class)) {
            $reflection = new ReflectionClass($class);
            $methods = [];

            foreach ($reflection->getMethods() as $method) {
                if ($method->class == $reflection->getName()) {
                    $methods[] = $method->name;
                }
            }

            array_push($modules, getModuleName($class) . '/' . getControllerName($class));
        }
    }

    return in_array($module_name . '/' . $controller_name, $modules);
}

function buildTree(array &$elements, $parentId = 0)
{
    $branch = [];

    foreach ($elements as &$element) {

        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['id']);

            if ($children) {
                $element['children'] = $children;
            }

            $branch[$element['id']] = $element;
            unset($element);
        }
    }

    return $branch;
}

function moveElement(&$array, $a, $b)
{
    $p1 = array_splice($array, $a, 1);
    $p2 = array_splice($array, 0, $b);
    $array = array_merge($p2, $p1, $array);

    return $array;
}

function deleteDir($dir)
{
    if (!is_dir($dir)) {
        throw new InvalidArgumentException("$dir must be a directory");
    }

    $files = array_diff(scandir($dir), array('.', '..'));

    foreach ($files as $file) {
        is_dir("$dir/$file") ? deleteDir("$dir/$file") : unlink("$dir/$file");
    }

    return rmdir($dir);
}

function makeAttachment(UploadedFile $file, $action, $id)
{
    $dir = "public/attachments/$action/$id";
    $path = storage_path("app/$dir");
    $filename = $file->getClientOriginalName();

    if (!File::exists($path)) {
        File::makeDirectory($path, 0775, true);
    }
    $replace = str_replace(storage_path('app/public'), '', $file->move($path, $filename));
    return config('app.url') . $replace;
}

function api($url, $method, $data = [], $type = 'application/json')
{
    if (empty($type)) {
        throw new InvalidArgumentException("parameter 'type' is not valid.");
    }

    $response = null;
    $access_token = session('access_token');

    $responseObj = Curl::to($url)
        ->withHeader("Authorization: Bearer $access_token")
        ->withHeader("Accept: $type")
        ->withData($data);

    switch ($type) {
        case "multipart/form-data":
            $responseObj->containsFile();
            break;
    }
    $responseObj->returnResponseObject();

    switch (strtolower($method)) {
        case 'get':
            $response = $responseObj->get();
            break;
        case 'post':
            $response = $responseObj->post();
            break;
        case 'put':
            $response = $responseObj->put();
            break;
    }

    if (!isValidJson($response->content)) {
        \Log::error("api: Invalid JSON string.");
    } 
    $content = json_decode($response->content, true);
    return collect($content);
}

function isValidJson($string){
    $json = json_decode($string);
    return (is_object($json) && json_last_error() == JSON_ERROR_NONE) ? true : false;
}

/**
 * Example usage:
 *
 * $url = endpoint("tickets.offers_show", [
 *      "ticket" => $ticket_id_here,
 *      "offer" => $offer_id_here
 * ]);
 *
 * @param $path
 * @param array $params
 * @return string
 */
function endpoint($path, $params = [])
{
    $endpoint = config("api.endpoints.$path");
    preg_match_all("/\\{(.*?)\\}/", $endpoint, $matches);

    if (count($matches[1])) {
        $replace = [];
        $invalid = [];

        if (!empty($params)) {
            foreach ($params as $param_key => $param_value) {
                if (!in_array($param_key, $matches[1])) {
                    array_push($invalid, $param_key);
                    continue;
                }
                array_push($replace, '{' . $param_key . '}');
            }

            if (count($invalid)) {
                throw new InvalidArgumentException("Invalid parameters: " . implode(', ', $invalid));
            }
        } else {
            throw new InvalidArgumentException("Required parameters: " . implode(', ', $matches[1]));
        }
        $missing = [];

        foreach ($matches[1] as $match) {
            if (!in_array($match, array_keys($params))) {
                array_push($missing, $match);
            }
        }

        if (count($missing)) {
            throw new InvalidArgumentException("Missing parameters: " . implode(', ', $missing));
        }

        foreach ($replace as $key) {
            $endpoint = str_replace($key, $params[substr($key, 1, -1)], $endpoint);
        }
    }
    return config('api.url') . $endpoint;
}

function authUser()
{
    $user = null;

    if (session()->has('user')) {
        $user = Accounts::find(session('user')["id"])->toArray();
    }
    return collect($user);
}

function randstring($length, $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890')
{
    $str = '';
    $count = strlen($charset);
    while ($length--) {
        $str .= $charset[mt_rand(0, $count - 1)];
    }
    return $str;
}

function GenerateAccount()
{
    $theAccount = randstring(3) . "-" . randString(3) . "-" . randString(3);
    $check = Wallet::where('wallet_account_number', '=', $theAccount)->first();
    if ($check) {
        GenerateAccount();
    } else {
        return $theAccount;
    }
}

function render($string, $data)
{
    $php = Blade::compileString($string);

    $obLevel = ob_get_level();
    ob_start();
    extract($data, EXTR_SKIP);

    try {
        eval('?' . '>' . $php);
    } catch (Exception $e) {
        while (ob_get_level() > $obLevel) ob_end_clean();
        throw $e;
    } catch (Throwable $e) {
        while (ob_get_level() > $obLevel) ob_end_clean();
        throw new FatalThrowableError($e);
    }

    return ob_get_clean();
}

function getEmailTemplateVars($email_type)
{
    $variables = collect(EmailTemplate::$email_types)
        ->where("name", $email_type)
        ->first()["variables"];
    return implode(',', $variables);
}

function linkify($value, $protocols = array('http', 'mail'), array $attributes = array())
{
    // Link attributes
    $attr = '';
    foreach ($attributes as $key => $val) {
        $attr = ' ' . $key . '="' . htmlentities($val) . '"';
    }

    $links = array();

    // Extract existing links and tags
    $value = preg_replace_callback('~(<a .*?>.*?</a>|<.*?>)~i', function ($match) use (&$links) {
        return '<' . array_push($links, $match[1]) . '>';
    }, $value);

    // Extract text links for each protocol
    foreach ((array)$protocols as $protocol) {
        switch ($protocol) {
            case 'http':
            case 'https':
                $value = preg_replace_callback('~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) {
                    if ($match[1]) $protocol = $match[1];
                    $link = $match[2] ?: $match[3];
                    return '<' . array_push($links, "<a $attr href=\"$protocol://$link\">$link</a>") . '>';
                }, $value);
                break;
//            case 'mail':
//                $value = preg_replace_callback('~([^\s<]+?@[^\s<]+?\.[^\s<]+)(?<![\.,:])~', function ($match) use (&$links, $attr) {
//                    return '<' . array_push($links, "<a $attr href=\"mailto:{$match[1]}\">{$match[1]}</a>") . '>';
//                }, $value);
//                break;
            case 'twitter':
                $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) {
                    return '<' . array_push($links, "<a $attr href=\"https://twitter.com/" . ($match[0][0] == '@' ? '' : 'search/%23') . $match[1] . "\">{$match[0]}</a>") . '>';
                }, $value);
                break;
            default:
                $value = preg_replace_callback('~' . preg_quote($protocol, '~') . '://([^\s<]+?)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) {
                    return '<' . array_push($links, "<a $attr href=\"$protocol://{$match[1]}\">{$match[1]}</a>") . '>';
                }, $value);
                break;
        }
    }

    // Insert all link
    return preg_replace_callback('/<(\d+)>/', function ($match) use (&$links) {
        return $links[$match[1] - 1];
    }, $value);
}

function getTicketType($ticket_generated_id)
{
    return substr($ticket_generated_id, 0, 1) === 'B' ? "Buyer" : "Seller";
}

/**
 * Check NodeJS Socket.IO connection if it is `ON` or `OFF`
 * 
 * @return boolean;
**/
function checkSocketConnection(){
    $connection = @fsockopen('localhost', '3000');
    $checkSocket = "";

    if(is_resource($connection)){
        $checkSocket = true;
        fclose($connection);
    } else{
        $checkSocket = false;
    }

    return $checkSocket;
}
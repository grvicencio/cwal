<?php

/*
 * Static `config` variables for Foreign Exchange.
 * Fetched May 5, 2018 @ 1:51 PM
 * Source: Google Search @ https://www.google.com
 */

return [
    'hkd' => 1.23,
    'usd' => 0.16,
    'sgd' => 0.21,
    'btc' => 0.000017,
    'cny' => 1,
];
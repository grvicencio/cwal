<?php

return [

    'Membership' =>'Membership', // wallet Credit to Membership 
    'WalletTransfer' =>'WalletTransfer',  // Wallet credit to transfer
    'Charges' =>'Charges', // WAllet debit
    'Register' =>'Register', // Wallet Initializaiton
    'CRM' =>'CRM', // Wallet debited from CRM 
    'Wallet' =>'Wallet', // unknwon workflow 
    'Friend' =>'Friend', // unknown workflow 
    'RedisWalletTransferName' =>'WalletTransfer',
    'Guest' =>'Guest',

    'actions' => [
        'deposit' => 'deposit',
        'withdrawal' => 'withdrawal',
        'transfer' => 'transfer',
    ],
    
];


<?php


return [

    'Basic' =>'Basic',
    'Bronze' =>'Bronze',
    'Silver' =>'Silver',
    'Gold' =>'Gold',
    'Premium' =>'Premium',
    'Basic_Allow' =>array('Bronze', 'Silver', 'Gold', 'Premium'),
    'Bronze_Allow' =>array('Silver', 'Gold', 'Premium'),
    'Silver_Allow' =>array('Gold', 'Premium'),
    'Gold_Allow' =>array('Premium'),
    'Onetime' =>'Onetime',
    'Daily' =>'Daily'

];


<?php

return [

    'url' => env('API_URL', 'http://bitcoin.ninepine/api/'),
    'endpoints' => [
        'user' => [
            'login' => 'v2/user/login',
            'register' => 'user/web_register',
            'registerv2' => 'v2/user/web_register',
            '2fa'   => 'v2/user/google2fa',
            'update2fa'   => 'v2/user/update_google2fa',
            'fetchQR'   => 'v2/user/fetchQR',
            'validate2fa'   => 'v2/user/validate2fa',
            'token_refresh' => 'user/refresh',
            'logout' => 'user/logout',
            'profile' => 'user/profile',
            'userprofile' => 'user/userprofile',
            'update_profile' => 'user/update',
            'update_security' => 'user/security',
            'fetch_security_question_all' => 'user/security_question',
            'security_question_update' => 'user/security_question/update',
            'billing_save' => 'user/billing/update',
            'billing' => 'user/billing',
            'bank_info' => 'user/bank/checkinfo/',
            'bank_info_id' => 'user/bank/{bank_id}',
            'uploadprofile' => 'user/uploadprofile',
            'activate' => 'user/activate/',
            'bank' =>'user/bank',
            'bankupdate'=>'user/bank/update',
            //added chat
            'sendchat' =>'user/chat',
            'getchat' => 'user/getchat',
            'getallonline' =>'user/online',
            'myinbox' => 'user/inbox'

            // end chat
        ],
        //RESTful endpoints
        'ticket' => [
            'active' => 'tickets/active', //METHOD: GET=List all not closed
            'inactive' => 'tickets/inactive', //METHOD: GET=List all closed and rejected
            'open' => 'tickets/open', //METHOD: GET=List all
            'show' => 'tickets/{ticket}', //METHOD: GET=Show ticket
            'close' => 'tickets/{ticket}/close', //METHOD: POST=Close ticket
            'offers' => 'tickets/{ticket}/offers', //METHOD: GET=List all, POST=Create new
            'offer_show' => 'tickets/{ticket}/offers/{offer}', //METHOD: GET=Show offer
            'offer_reject' => 'tickets/{ticket}/offers/{offer}/reject', //METHOD: POST=Reject offer
            'offer_approve' => 'tickets/{ticket}/offers/{offer}/approve', //METHOD: POST=Approve offer
            'offer_pay' => 'tickets/{ticket}/offers/{offer}/pay', //METHOD: POST=Pay offer
            'buyer_pay' => 'tickets/{ticket}/offers/{offer}/buyer_pay', //METHOD: POST=Pay offer
            'offer_payed' => 'tickets/{ticket}/offers/{offer}/payed', //METHOD: POST=Verify payed offer
            'offer_close' => 'tickets/{ticket}/offers/{offer}/close', //METHOD: POST=Close offer
            'offer_payment_info' => 'tickets/{ticket}/offers/{offer}/payment_info',
            'offer_transfer_info' => 'tickets/{ticket}/offers/{offer}/transfer_info',
            'transfer' => 'tickets/{ticket}/offers/{offer}/ticket_transfer',
            /* cp-206 */
            'submitdispute' =>'tickets/{ticket}/offers/{offer}/dispute',
            'disputemessage' =>'tickets/{ticket}/offers/{offer}/disputemessage',
            'disputetopaymentmade' =>'tickets/{ticket}/offers/{offer}/disputetopaymentmade',
            /* Cp -206*/

        ],
        'review' => [
            'send' => 'review/send',
            'overall' => 'review/overall', // USER OVERALL COMPUTED RATING
            'get_review' => 'review/get_review', // GET LOGGED USER REVIEW
            'user_review' => 'review/get_reviews', // GET USER-SPECIFIC REVIEWS
            'review_detail' => 'review/review_detail', // GET USER-SPECIFIC REVIEW DETAILS
        ],
        'charges' => [
            'all' => 'charges/all',
            'view' => 'charges/detail',
        ],
        'wallet' => [
            'mywallet' => 'wallet/mywallet',
            'transfer_step1' => 'wallet/transferto',
            'transfer_step2' => 'wallet/transferprocess/baccarat',
            'showCurrency' => 'wallet/currency/',
            'deposit' => 'wallet/deposit',
            'withdrawal' => 'wallet/withdrawal',
            'check_balance' => 'wallet/check_balance',
            'availablecurrency' =>'wallet/availablecurrency',
            'createnewcurrency' =>"wallet/newaccount",
            'generalwallet' => 'wallet/generalwallet'
        ],
        'v2' => [
            'wallet' => [
                'mywallet' => 'v2/wallet/mywallet',
                'transfer_step1' => 'v2/wallet/transferto',
                'transfer_step2' => 'v2/wallet/transferprocess/baccarat',
                'showCurrency' => 'v2/wallet/currency/',
                'deposit' => 'v2/wallet/deposit',
                'withdrawal' => 'v2/wallet/withdrawal',
                'check_balance' => 'v2/wallet/check_balance',
                'availablecurrency' =>'v2/wallet/availablecurrency',
                'createnewcurrency' =>"v2/wallet/newaccount",
                'change_alias' =>"v2/wallet/alias",
                'mycurrency' =>"v2/wallet/mycurrency",
                'exchangerate' => 'v2/wallet/exchangerate/forex'
            ],
        ],
        'payment' => [
            'charge' => 'payment/charge/',
            'history' => 'payment/transaction',
            'create' => 'payment/create',
            'update' => 'payment/update',
            'user' => 'payment/user'
        ],
        'membership' => [
            'all' => 'membership/all',
            'view' => 'membership/detail',
            'user' => 'membership/user',
            'topup' => 'membership/topup/'
        ],
        'application' => [
            'user' => 'application/user',
            'all' => 'application/all'
        ],
        'password' => [
            'email' => 'password/email',
            'reset' => 'password/reset',
            'security_question' => 'password/security_question_reset',
            'fetch_security_question' => 'password/security_question_fetch',
            'security_question_password_reset' => 'password/security_question_reset',
            'checkIfTokenIsValid' => 'password/checkIfTokenIsValid',
            'checkSecurityQuestionAnswer' => 'password/checkSecurityQuestionAnswer'
        ],
        'tradeprofile' => [
            'all' => 'tradeprofile/all',
            'usertrade' => 'tradeprofile/usertrade',
            'store' => 'tradeprofile/store',
            'update' => 'tradeprofile/update',
            'delete' => 'tradeprofile/delete'
        ],
        'market' => [
            'getprofile' => 'market/getprofile',
        ],
        'contact' =>[
            'submit' =>'contact/submit'
        ]
    ],
    'assets' => [
        'avatar' => 'http://api-ninepine.dev/storage/avatar',
        'app_image' => 'http://api-ninepine.dev/storage/app_image'
    ]

];
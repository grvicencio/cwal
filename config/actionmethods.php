<?php

return [

    'view' => [
        'view',
        'index',
        'show',
        'dataTable',
        'details',
        'activityDataTable',
        'fetchSecurityQuestionDatable',
        'ledgerSourceInfo',
        'registered_users_range',
        'sales_range',
        'user_charge_range',
        'tickets',
        'avatars'
    ],
    'add' => [
        'add',
        'create',
        'store',
        'transfer',
    ],
    'edit' => [
        'set_status',
        'set_notes',
        'set_attachments',
        'updateCrmNotes',
        'chageStatus',
        'processApproval',
        'edit',
        'modify',
        'alter',
        'change',
        'update',
        'passwordReset',
        'updateNotes',
    ],
    'delete' => [
        'del',
        'delete',
        'destroy',
        'remove',
    ],

];
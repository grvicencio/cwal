<?php

return [

    'Application' =>'Application',
    'Application_Active' =>'Active',
    'Membership' =>'Membership',
    'Membership_Active' => 'Active',
    'Charges' =>'Charges',
    'Charges_Active' =>'Active',
    'Regitration_Active' =>1,
    'Registration_In_Active' =>2,
    'Registration_Verification' =>3,
    'UserApplication' =>'UserApplication',
    'UserApplication_Active' =>'Active'
    
];

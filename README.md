# CRM with Application Levels (CWAL)
This Repository contains source codes for the Ninepine application CRM.


***


# Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.


## Prerequisites
*Project `NinepineCore API` with **migrations** and **seeders**.*


## Clone Repository
### Terminal:
1. In your project folder, run `git clone git@bitbucket.org:grvicencio/cwal.git`.

### SourceTree:
1. Under New SourceTree Project Tab, click `Clone`
2. Fill in the necessary fields *(Source Path, Destination, Name)*
3. Click `Clone`


## Setting up your Laravel
1. After successful clone of the repository, add your `.env` file
2. Your Local Machine must have `ninepinecore` and `ninepinecore_crm` databases
3. Run `php artisan config:clear`.
	*Should an error occur, run `composer update` and `composer dump-autoload` from your terminal*


## You're all set up!
Log in to the application using your seeded CRM credentials.


***


# Acknowledgments
Hat tip to these `README.md` references:
> * https://gist.github.com/PurpleBooth/109311bb0361f32d87a2
> * https://bitbucket.org/tutorials/markdowndemo/src/master/
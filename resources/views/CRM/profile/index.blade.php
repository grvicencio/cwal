@extends('CRM.layouts.dashboard')

@section('styles')
	<style>
		@import url('https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700');

		.control-label.align-left{
			text-align: left !important;
		}

		.edit-img{
			left: 60%;
			margin-top: -60px;
			padding: 12.5px 13px;
			position: absolute;
			z-index: 99;

			background: #FFF;
			border-radius: 25px;
			box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.2) !important;
			cursor: pointer;

			transition: 0.2s ease;
			-webkit-transition: 0.2s ease;
			-moz-transition: 0.2s ease;
			-ms-transition: 0.2s ease;
			-o-transition: 0.2s ease;
		}
			.edit-img:hover{
				background: #EEE;

				transition: 0.2s ease;
				-webkit-transition: 0.2s ease;
				-moz-transition: 0.2s ease;
				-ms-transition: 0.2s ease;
				-o-transition: 0.2s ease;
			}

		.box,
		.box-body,
		.panel,
		.panel-heading,
		.panel-body,
		.panel-footer{
			border-radius: 0px !important;
		}
			.box,
			.panel{
				border: none !important;
				box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.2) !important;
			}
				.panel-body,
				.panel-heading{
					padding: 15px 25px !important;
				}
				.panel-heading{
					box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.2) !important;
					font-family: "Raleway", sans-serif !important;
					letter-spacing: 1px;
					text-align: center !important;
					text-transform: uppercase !important;
				}

		.profile-img{
			display: block;
			left: 50%;
			margin-left: -150px;
			margin-top: 50px;
			padding: 25px;
			position: relative;
			width: 300px;
			z-index: 99;

			border: 3px solid #3c8dbc;
			border-radius: 100%;
			cursor: pointer;
		}

		.profile-main{
			font-family: "Raleway", sans-serif !important;
			font-size: 3rem;
			font-weight: 500;
			text-transform: uppercase;
		}
		.profile-sub{
			margin-top: -10px;

			font-size: 1.75rem;
			font-weight: 300;
		}

		.span-profile{
			display: block;
			margin-bottom: 10px;
		}
	</style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ $pagetitle }}</li>
    </ol>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6">
			<div class="box">
				<div class="box-body">
					<form action="image/update/{{ Auth::guard('crm')->user()->id }}" class="frm-profile-img" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						{{ method_field('PUT') }}
						<img src="{{ asset(Auth::guard('crm')->user()->image) }}" class="profile-img">
						<i class="fa fa-pencil edit-img"></i>
						<input type="file" name="image" accept="image/x-png" style="visibility: hidden;">
					</form>

					<p align="center">
						<span class="span-profile profile-main">{{ Auth::guard('crm')->user()->firstname }} {{ Auth::guard('crm')->user()->lastname }}</span>
						<span class="span-profile profile-sub">{{ Auth::guard('crm')->user()->email }}</span>
					</p>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">Edit Information</div>

				<div class="panel-body">
					<form class="form-horizontal frm-edit-profile" action="/admin/security/assignuser/{{ Auth::guard('crm')->user()->id }}" method="POST">
						{{ csrf_field() }}
						{{ method_field('PUT') }}

						<div class="form-group">
							<label for="firstname" class="col-md-3 control-label">Role</label>
							<label class="col-md-8 control-label align-left">{{ Auth::guard('crm')->user()->superuser }}</label>
							<input type="hidden" name="role_id" value="{{ Auth::guard('crm')->user()->user_role->user_role_id }}">
							<input type="hidden" name="status_id" value="{{ Auth::guard('crm')->user()->status_id }}">
						</div>

						<div class="form-group">
							<label for="firstname" class="col-md-3 control-label">First Name</label>

							<div class="col-md-8">
								<input type="text" class="form-control" name="firstname" id="firstname" value="{{ Auth::guard('crm')->user()->firstname }}">
							</div>
						</div>

						<div class="form-group">
							<label for="lastname" class="col-md-3 control-label">Last Name</label>

							<div class="col-md-8">
								<input type="text" class="form-control" name="lastname" id="lastname" value="{{ Auth::guard('crm')->user()->lastname }}">
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="col-md-3 control-label">E-mail Address</label>

							<div class="col-md-8">
								<input type="text" class="form-control" name="email" id="email" value="{{ Auth::guard('crm')->user()->email }}">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12" align="center">
								<button type="submit" role="button" class="btn btn-primary" data-loading-text="{{ trans('loading.please_wait') }}">SUBMIT</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">Reset Password</div>

				<div class="panel-body">
					<form class="form-horizontal frm-password-reset" action="/admin/security/assignuser/{{ Auth::guard('crm')->user()->id }}/password/reset" method="POST">
						{{ csrf_field() }}
						{{ method_field('PUT') }}

						<div class="form-group">
							<label for="password" class="col-md-3 control-label">New Password</label>

							<div class="col-md-8">
								<input type="password" class="form-control" name="password" id="password">
							</div>
						</div>

						<div class="form-group">
							<label for="password_confirmation" class="col-md-3 control-label">Confirm Password</label>

							<div class="col-md-8">
								<input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12" align="center">
								<button type="submit" role="button" class="btn btn-primary" data-loading-text="{{ trans('loading.please_wait') }}">SUBMIT</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('CRM/Capital7-1.0.0/js/form-validation.js') }}"></script>

	<script>
		$(function(){
			$('.frm-edit-profile').submit(function(e){
				e.preventDefault();
				var form = $(this);
				var btn = form.find(':submit').button('loading');

				$.ajax({
					type: form.prop('method'),
					url: form.prop('action'),
					data: form.serialize(),
					success: function(response){
						if(response.status == "{{ config('response.type.success') }}"){
							swal('Edit Information', 'Record Successfully Updated!', 'success').then($res => {
								location.reload();
							});
						} else if(response.status == "{{ config('response.type.error') }}"){
							assocErr(response.errors, form);
						}
					}
				}).done(function(){
					btn.button('reset');
				});
			});

			$('.profile-img, .edit-img').click(function(){
				$('input[name="image"]').trigger('click');
			});

			$('.frm-password-reset').submit(function (e) {
				e.preventDefault();
				var form = $(this);
				var btn = form.find(':submit').button('loading');

				$.ajax({
					type: form.prop('method'),
					url: form.prop('action'),
					data: form.serialize(),
					success: function(response){
						if(response.status == '{{ config('response.type.success') }}'){
							swal('Reset Password', 'Password Reset Successful!', 'success').then($res => {
								location.href = "{{ route('logout') }}";
							});
						} else if (response.status == '{{ config('response.type.error') }}'){
							assocErr(response.errors, form);
						}
					}
				}).done(function(){
					btn.button('reset');
				});
			});

			$('input[name="image"]').change(function(e){
				e.preventDefault();
				var form = $(this).closest('form');

				$.ajax({
					type: form.prop('method'),
					url: form.prop('action'),
					data: new FormData(form[0]),
					contentType: false,
					processData: false,
					success: function (response){
						console.log(response.data);
						if(response.status == "{{ config('response.type.success') }}"){
							swal('Edit Profile Picture', 'Image Successfully Updated!', 'success').then($res => {
								location.reload();
							});
						} else if(response.status == "{{ config('response.type.error') }}"){
							assocErr(response.errors, form);
						} else if(response.status == "{{ config('response.type.fail') }}"){
							swal({
								title: response.data.swal.title,
								html: response.data.swal.html,
								type: response.data.swal.type
							});
						}
					}
				});;
			});
		});
	</script>
@endsection
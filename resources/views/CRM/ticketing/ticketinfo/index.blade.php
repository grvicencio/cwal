@extends('CRM.layouts.dashboard')

@section('styles')
	<link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">

	<style>
		.bubble{
			padding: 20px;

			border-radius: 20px;
		}
			tr.Offer div.bubble,
			tr.Open div.bubble{
				background: #e3f5ff;
				box-shadow: 0px 5px 20px rgba(19, 113, 187, 0.3);
				color: #1371bb;
			}
				tr.Offer div.bubble hr,
				tr.Open div.bubble hr{
					border-top: 1px solid #1371bb;
				}
				tr.Offer div.bubble span.badge,
				tr.Open div.bubble span.badge{
					background: #1371bb;
					color: #e3f5ff;
				}
			tr.Approved div.bubble,
			tr.Completed div.bubble,
			tr.PaymentMade div.bubble{
				background: #dbffea;
				box-shadow: 0px 5px 20px rgba(17, 195, 156, 0.3);
				color: #11c39c;
			}
				tr.Approved div.bubble hr,
				tr.Completed div.bubble hr,
				tr.PaymentMade div.bubble hr{
					border-top: 1px solid #11c39c;
				}
				tr.Approved div.bubble span.badge,
				tr.Completed div.bubble span.badge,
				tr.PaymentMade div.bubble span.badge{
					background: #11c39c;
					color: #dbffea;
				}
			tr.Dispute div.bubble{
				background: #fffce3;
				box-shadow: 0px 5px 20px rgba(173, 161, 17, 0.3);
				color: #ada111;
			}
				tr.Dispute div.bubble hr{
					border-top: 1px solid #ada111;
				}
				tr.Dispute div.bubble span.badge{
					background: #ada111;
					color: #fffce3;
				}
			tr.Rejected div.bubble{
				background: #ffe3e3;
				box-shadow: 0px 5px 20px rgba(187, 19, 19, 0.3);
				color: #bb1313;
			}
				tr.Rejected div.bubble hr{
					border-top: 1px solid #bb1313;
				}
				tr.Rejected div.bubble span.badge{
					background: #bb1313;
					color: #ffe3e3;
				}

		.dataTables_wrapper .dataTables_paginate .paginate_button{
			margin: 0px !important;
			padding: 0px !important;
		}
			.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
				background: none !important;
				border: 1px solid #EEEEEE !important;
			}

		.dropdown-header{
			color: #222;
			font-size: 14px;
			font-weight: 500;
		}

		.dropdown-menu > li > a{
			padding: 3px 30px;

			color: #999;
		}

		.dropdown-menu > .disabled > a,
		.dropdown-menu > .disabled > a:focus,
		.dropdown-menu > .disabled > a:hover{
			text-decoration: line-through;
		}

		.modal-body{
			max-height: 600px;
			overflow-y: scroll;
		}
			.modal-body::-webkit-scrollbar{
				width: 10px;
			}
			.modal-body::-webkit-scrollbar-track{
				-webkit-box-shadow: inset 0 0 0px rgba(0, 0, 0, 0.3);
			}
			.modal-body::-webkit-scrollbar-thumb{
				background: #888888;
				outline: 1px solid #888888;
			}

		.modal-content{
			max-width: 1000px !important;
			width: 100% !important;
		}

		.modal-loader{
			padding: 30px 0px;

			color: #d2d2d2;
			font-size: 2rem;
			text-align: center;
		}

		span.date{
			color: #444444;
			font-size: 20px;
			font-weight: bold;
		}

		span.month-year{
			color: #888888;
			font-size: 12px;
		}

		.table.ticket-track p{ margin: 0px; }
		.table.ticket-track td{ vertical-align: top; }

		.table > tbody > tr > td,
		.table > tbody > tr > th,
		.table > tfoot > tr > td,
		.table > tfoot > tr > th,
		.table > thead > tr > td,
		.table > thead > tr > th{
			vertical-align: middle;
		}

		.table-borderless, .table-borderless td{ border: none !important; }

		textarea{
			resize: none;
		}

		.well-presentation{
			display: -webkit-box;

			background: linear-gradient(5deg, #45a0d4 10%, #78c6f3 80%);
			color: #FFFFFF;
			font-family: "Lato", sans-serif;
		}
			.well-primary{ background: linear-gradient(5deg, #45a0d4 10%, #78c6f3 80%); }
			.well-danger{ background: linear-gradient(5deg, #d44545 10%, #f37878 80%); }
			.well-success{ background: linear-gradient(5deg, #3cb382 10%, #88e0b7 80%); }
			.well-presentation span[class^="ticket-"]{
				text-shadow: 2px 2px 2px rgba(0, 0, 0, 0.4) !important;
			}
			.well-presentation .ticket-id,
			.well-presentation .ticket-status{
				display: block;

				font-family: "Nova Square", cursive;
				font-size: 3rem;
				text-transform: uppercase;
			}

		@media (min-width: 992px)
			.modal-lg{
				width: 1000px !important;
			}
		}
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tickets</li>
        <li class="active">Ticket Details</li>
    </ol>

    <br />

    <a href="{{ route('ticketing.index') }}" class="btn btn-default"><i class="ion ion-android-arrow-back"></i> &nbsp; Back</a>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Ticket Details</h3>
				</div>

				<div class="box-body">
					<div class="well well-presentation well-{{ $ticket->status->status_name == "Close" ? "danger" : ($ticket->status->status_name == "Open" ? "primary" : "success") }}">
						<div class="col-md-6">
							<span class="ticket-id">{{ $ticket->ticket_generated_id }}</span>
							<span class="ticket-amount">{{ number_format($ticket->amount, 8) }} {{ $ticket->ccsymbol }}</span>
							&nbsp; for &nbsp;
							<span class="ticket-value">{{ number_format($ticket->value, 2) }} {{ $ticket->gcsymbol }}</span>
						</div>

						<div class="col-md-6" align="right">
							<span class="ticket-status pull-right">{{ $ticket->status->status_name }}</span>
						</div>
					</div>

					<hr />

					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Sub Ticket ID</th>
								<th>Ticket Description</th>
								<th>Date Offered</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($subticket AS $row)
								<tr>
									<td>
										{{ $row->ticket_generated_id }}
										<span class="status status-{{ $flags[strtolower($row->status->status_name)] }} pull-right">{{ $row->status->status_name }}</span>
									</td>
									<td align="center">
										{{ number_format($row->amount, 8) }} &nbsp; {{ $row->ccsymbol }} &nbsp; &nbsp; for &nbsp; &nbsp; {{ $row->value }} &nbsp; {{ $row->gcsymbol }}
									</td>
									<td>{{ $row->created_at }}</td>
									<td>
										<div class="dropdown">
											<a href="#" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">Actions &nbsp; <i class="fa fa-angle-down"></i></a>

											<ul class="dropdown-menu">
												<li class="dropdown-header">{{ $row->status->status_name }}</li>
												@if($row->status->status_name == "Offer")
													<li class="{{ $row->status->status_name == "Offer" ? "" : "disabled" }}">
														<a href="#" class="btn-action" data-tid="{{ $row->trademarket_ticket_id }}" data-ticket-type="{{ $row->trade_profile->type }}" data-ticket-action="reward" data-type="offer"><i class="fa fa-check"></i> Accept</a>
													</li>
													<li class="{{ $row->status->status_name == "Offer" ? "" : "disabled" }}">
														<a href="#" class="btn-action" data-tid="{{ $row->trademarket_ticket_id }}" data-ticket-type="{{ $row->trade_profile->type }}" data-ticket-action="revert" data-type="offer"><i class="fa fa-times"></i> Reject</a>
													</li>
												@elseif($row->status->status_name == "Dispute")
													<li class="{{ $row->status->status_name == "Dispute" ? "" : "disabled" }}">
														<a href="#" class="btn-action" data-tid="{{ $row->trademarket_ticket_id }}" data-ticket-type="{{ $row->trade_profile->type }}" data-ticket-action="reward" data-type="dispute"><i class="fa fa-check"></i> Reward</a>
													</li>
													<li class="{{ $row->status->status_name == "Dispute" ? "" : "disabled" }}">
														<a href="#" class="btn-action" data-tid="{{ $row->trademarket_ticket_id }}" data-ticket-type="{{ $row->trade_profile->type }}" data-ticket-action="revert" data-type="dispute"><i class="fa fa-times"></i> Reject</a>
													</li>
												@elseif($row->status->status_name == "Payment Made")
													<li class="{{ $row->status->status_name == "Payment Made" ? "" : "disabled" }}">
														<a href="#" class="btn-action" data-tid="{{ $row->trademarket_ticket_id }}" data-ticket-type="{{ $row->trade_profile->type }}" data-ticket-action="reward" data-type="paymentmade"><i class="fa fa-check"></i> Reward</a>
													</li>
												@elseif($row->status->status_name == "Payment Received")
													<li class="{{ $row->status->status_name == "Payment Received" ? "" : "disabled" }}">
														<a href="#" class="btn-action" data-tid="{{ $row->trademarket_ticket_id }}" data-ticket-type="{{ $row->trade_profile->type }}" data-ticket-action="reward" data-type="paymentreceived"><i class="fa fa-check"></i> Reward</a>
													</li>
												@endif
												<li class="divider"></li>
												<li class="">
													<a href="#" class="btn-ticket-history" data-id="{{ $row->trademarket_ticket_id }}"><i class="fa fa-clock-o"></i> Show History</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="history-modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">Ticket History</div>

				<div class="modal-body">
					<div class="modal-loader">
						<i class="ion ion-load-c fa-spin fa-5x fa-fw"></i>
						<br /><br />
						Fetching data
					</div>

					<table class="table table-borderless ticket-track hidden">
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="action-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">Ticket Action: <span id="action"></span></div>

				<div class="modal-body">
					<form class="form-horizontal" id="action-frm">
						{{ csrf_field() }}
						<input type="hidden" name="tid">
						<input type="hidden" name="type">
						<input type="hidden" name="tickettype">
						<input type="hidden" name="ticketaction">

						<section id="notes">
							<label class="control-label">Notes</label>
							<div class="row">
								<div class="col-sm-12">
									<textarea class="form-control" name="notes" rows="5"></textarea>
								</div>
							</div>
						</section>

						<section id="attachments">
							<label class="control-label">Attachments</label>
							<div class="row">
								<div class="col-sm-12">
									<input type="file" name="attachments">
								</div>
							</div>
						</section>

						<hr />

						<section align="right">
							<button type="submit" class="btn btn-primary" data-loading="{{ trans('loading.please_wait') }}">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</section>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/datatables-ellipsis.js") }}"></script>
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

	<script>
		$(function(){
			$('.btn-ticket-history').on('click', function(){
				var $id = $(this).data('id');

				$('#history-modal').modal('show').find('.modal-loader').removeClass('hidden');
				$('.ticket-track').addClass('hidden').find('tbody').empty();

				$.post("{{ route('ticketing.track') }}", { _token: "{{ csrf_token() }}", id: $id }, function($data, $status){
					if($status == "success"){
						$('.ticket-track').removeClass('hidden');
						$('.modal-loader').addClass('hidden');

						var $arr = [];

						for(var $i = 0; $i < $data.track.length; $i++){
							var $cond = parseInt($.inArray($data.track[$i].created_at_month + ' ' + $data.track[$i].created_at_year, $arr)) + 1;
							var $append = $cond ? '&nbsp;' : '<br /><br /><span class="month-year">' + $data.track[$i].created_at_month + ' ' + $data.track[$i].created_at_year + '</span><br /><span class="date">' + $data.track[$i].created_at_date + '</span>';

							$('.ticket-track tbody').append('\
								<tr class="' + ($data.track[$i].status_name).replace(/\s/g, '') + '">\
									<td align="center" width="20%">' + $append + '</td>\
									<td>\
										<div class="bubble">\
											<small>' + $data.track[$i].username + '</small>\
											<span class="badge pull-right">' + ($data.track[$i].status_name).toUpperCase() + '</span>\
											<span class="badge pull-right">' + $data.track[$i].created_at_time + '</span>\
											<br />\
											<hr />\
											<p>' + $data.track[$i].note + '</p>\
										</div>\
										<br />\
									</td>\
								</tr>');

							$arr.push($data.track[$i].created_at_month + ' ' + $data.track[$i].created_at_year);
						}

						// setTimeout(function(){ $('#history-modal .modal-body').animate({ scrollTop: 9999 }); }, 100);
						$('#history-modal .modal-body').animate({ scrollTop: 9999 });
					}
				});
			});

			$('.btn-action').on('click', function(){
				var $this = $(this);
				var $form = $('form#action-frm');

				$form.find('input[type="hidden"][name="tid"]').val($this.data('tid'));
				$form.find('input[type="hidden"][name="type"]').val($this.data('type'));
				$form.find('input[type="hidden"][name="tickettype"]').val($this.data('ticket-type'));
				$form.find('input[type="hidden"][name="ticketaction"]').val($this.data('ticket-action'));

				if(($this.data('ticket-action') == "reward") && ($this.data('type') == "offer")){
					$('#action').html('Accept');

					if($this.data('ticket-type') == "Buyer")
						$('section#attachments').show();
					else
						$('section#attachments').hide();
				} else if(($this.data('ticket-action') == "reward") && ($this.data('type') == "paymentmade")){
					$('#action').html('Reward');
					$('section#attachments').hide();
				} else if(($this.data('ticket-action') == "reward") && ($this.data('type') == "paymentreceived")){
					$('#action').html('Reward');
					$('section#attachments').hide();
				} else if(($this.data('ticket-action') == "reward") && ($this.data('type') == "dispute")){
					$('#action').html('Reward');
					$('section#attachments').hide();
				} else if(($this.data('ticket-action') == "revert") && ($this.data('type') == "offer")){
					$('#action').html('Reject');
					$('section#attachments').hide();
				} else if(($this.data('ticket-action') == "revert") && ($this.data('type') == "dispute")){
					$('#action').html('Reject');
					$('section#attachments').hide();
				}

        		$('#action-modal').modal('show');

				return false;
			});

			$('#action-frm').on('submit', function($e){
				$e.preventDefault();

				var $this = $(this);
				var $btn = $this.find('button[type="submit"]');

				$btn.button('loading');
				$.ajax({
					type: "POST",
					url: "{{ route('ticketing.action') }}",
					data: new FormData($this[0]),
					contentType: false,
					processData: false,
					success: function ($data){
						console.log($data);
						swal({
							title: $data.response.title,
							text: $data.response.message,
							type: $data.response.swal,
							allowOutsideClick: false,
						}).then($res => {
							$('#action-modal').modal('hide');
							$btn.button('reset');
							location.reload();
						});
					}
				});
			});
		});
	</script>
@endsection
@extends('CRM.layouts.dashboard')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/bootstrap-datetimepicker/datetimepicker.css") }}">

    <style type="text/css">
        .form-group-amount{ width: 50% !important; }
        .push-left{
            margin-left: 5px;
        }

        .push-right{
            margin-right: 5px;
        }

        .readonly-clean{
            padding: 6px 12px !important;

            background-color: #FFF !important;
            border-radius: 0px !important;
        }

        .datatable-img{
            width: 100px !important;
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Marketplace</li>
        <li class="active">{{ $pagetitle }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">All Market Currencies</h3>
                </div>

                <div class="box-body">
                    <table id="marketplace-currency-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Application</th>
                                <th>Item Currency</th>
                                <th>Item Amount</th>
                                <th>Purchase Currency</th>
                                <th>Purchase Amount</th>
                                <th>Expiration Date</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="marketplace-currency-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" name="marketplace-currency-modal-form" id="marketplace-currency-modal-form" action="" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <h4 class="modal-title">Add {{ $pagetitle }}</h4>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-3">
                                <img src="{{ str_replace('1024x1024', '100x100', MarketplaceCurrency::DEFAULT_IMAGE_1024x1024) }}" data-old-src="{{ str_replace('1024x1024', '100x100', MarketplaceCurrency::DEFAULT_IMAGE_1024x1024) }}" id="marketplace-currency-image" class="img-responsive" height="100" width="100">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="image" class="col-sm-3 control-label">Image <span class="text-muted">(1024x1024)</span></label>
                                
                            <div class="col-md-8">
                                <input type="file" class="form-control" id="image" name="image" accept="image/x-png">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="application_id" class="col-sm-3 control-label">Application</label>

                            <div class="col-sm-8">
                                <select class="form-control" name="application_id" id="application_id">
                                    <option value="" style="display: none;"> -- Select Application -- </option>
                                    @foreach($apps AS $row)
                                        <option value="{{ $row->application_id }}">{{ $row->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="item_currency" class="col-sm-3 control-label">Item Amount</label>

                            <div class="col-sm-8">
                                <div class="input-group col-md-12">
                                    <select class="form-control form-group-amount" name="item_currency" id="item_currency">
                                        @foreach($currencies AS $row)
                                            <option value="{{ $row->currency_id }}">{{ $row->currency_name }} ({{ $row->currency }})</option>
                                        @endforeach
                                    </select>
                                    <input type="text" class="form-control form-group-amount" name="item_amount" id="item_amount">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="purchase_currency" class="col-sm-3 control-label">Purchase Amount</label>

                            <div class="col-sm-8">
                                <div class="input-group col-md-12">
                                    <select class="form-control form-group-amount" name="purchase_currency" id="purchase_currency">
                                        @foreach($currencies AS $row)
                                            <option value="{{ $row->currency_id }}">{{ $row->currency_name }} ({{ $row->currency }})</option>
                                        @endforeach
                                    </select>
                                    <input type="text" class="form-control form-group-amount" name="purchase_amount" id="purchase_amount">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="expiration_date" class="col-sm-3 control-label">Expiration Date</label>

                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control datepicker readonly-clean" name="expiration_date" id="expiration_date">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status_id" class="col-sm-3 control-label">Status</label>

                            <div class="col-sm-8">
                                <select class="form-control" name="status_id" id="status_id">
                                    <option value="" style="display: none;"> -- Select Status -- </option>
                                    @foreach($status AS $row)
                                        <option value="{{ $row->status_id }}">{{ $row->status_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

                        @usercan('add', 'marketplace_currency.store')
                            <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary">
                                Save
                            </button>
                        @endusercan
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-sm-12">
            <a href="#" class="btn btn-primary btn-form-modal" id="add-marketplace-currency"><i class="fa fa-plus"></i> Add {{ $pagetitle }}</a>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
    <!-- BOOTSTRAP DATEPICKER -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/bootstrap-datetimepicker/datetimepicker.js") }}"></script>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/datatables-ellipsis.js") }}"></script>
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function(){
            @if(session('message'))
                swal({
                    title: 'Success!',
                    text: '{{ session("message") }}'
                });
            @endif

            $('.btn-form-modal').click(function(){
                var $checkid = $(document).find('#marketplace-currency-modal-form > input[name^="id"]'),
                    $checkmethod = $(document).find('#marketplace-currency-modal-form > input[name^="_method"]');

                // CLEAR MODAL FORM INPUT VALUES
                $('#marketplace-currency-modal-form').find('input[name!="_token"], textarea, select').each(function(){
                    $(this).val('');
                    $(this).prop('disabled', false);
                });

                $('#marketplace-currency-modal-form').find('input[type="checkbox"]')
                    .prop('checked', false)
                    .prop('name', '');

                // SET PROPER FORM ACTION
                $('#marketplace-currency-modal-form').prop('action', '{{ route("marketplace_currency.store") }}');
                $('#marketplace-currency-modal').modal('show');

                // CHECK IF HIDDEN INPUT "ID" EXISTS
                if($checkid.length){
                    $checkid.remove();
                }

                // CHECK IF HIDDEN INPUT "_METHOD" EXISTS
                if($checkmethod.length){
                    $checkmethod.remove();
                }

                return false;
            });

            $('.cb-isparent input[type="checkbox"]').click(function(){
                var $ischecked = $(this).is(':checked')
                    $parent = ($ischecked) ? "parent" : "";

                $('select#parent').prop('disabled', ($ischecked));
                $(this).prop('name', $parent);
            });

            $(document).on('click', '.btn-edit-marketplace-currency', function(){
                var $tdcount = $(this).closest('tr').find('td').length,
                    $test = 0,
                    $td = $(this).closest('tr').find('td'),
                    $checkid = $(document).find('#marketplace-currency-modal-form > input[name^="id"]'),
                    $checkmethod = $(document).find('#marketplace-currency-modal-form > input[name^="_method"]')
                    $isparent = $(this).data('parent'),
                    $parent = (!$isparent) ? "parent" : "";

                $('#marketplace-currency-image').attr('src', $td.eq(0).find('img').prop('src'));

                $('#application_id').val($td.eq(1).find('span').data('id')).trigger('change');
                $('#item_currency').val($td.eq(2).find('span').data('id')).trigger('change');
                $('#item_amount').val($td.eq(3).text());
                $('#purchase_currency').val($td.eq(4).find('span').data('id')).trigger('change');
                $('#purchase_amount').val($td.eq(5).text());
                $('#expiration_date').val($td.eq(6).text());
                $('#status_id').val($td.eq(7).find('span').data('id')).trigger('change');

                // for(var $i = 0; $i < ($tdcount - 1); $i++){
                //     var $value = ($td.eq($i).has('span').length) ? $td.eq($i).find('span').data('id') : $td.eq($i).text();
                //
                //     $('#marketplace-currency-modal-form').find('input[type="text"], textarea, select').eq($i).val($value);
                // }

                // APPEND "ID" HIDDEN INPUT IF NOT EXIST
                if($checkid.length == 0){
                    $('#marketplace-currency-modal-form').append('<input type="hidden" name="marketplace_currency_id" value="' + $(this).data('id') + '">');
                }

                // APPEND "_METHOD" HIDDEN INPUT IF NOT EXIST
                if($checkmethod.length == 0){
                    $('#marketplace-currency-modal-form').append('{{ method_field("PUT") }}');
                }

                $('select#parent').prop('disabled', (!$isparent));
                $('#isparent').prop('checked', (!$isparent));
                $(this).prop('name', $parent);

                // SET PROPER FORM ACTION
                $('#marketplace-currency-modal-form').prop('action', '{{ route("marketplace_currency.update") }}');
                $('#marketplace-currency-modal').modal('show');

                return false;
            });

            $('#marketplace-currency-modal-form').submit(function(e){
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                clearErr(form);

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: new FormData(form[0]),
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response.status == '{{ config('response.type.success') }}'){
                            swal('Currency', 'Record Successfully Saved!', 'success').then($res => {
                                location.reload();
                            });
                        } else if(response.status == '{{ config('response.type.error') }}'){
                            assocErr(response.errors, form);
                        } else if(response.status == '{{ config('response.type.fail') }}'){
                            swal({
                                title: response.data.swal.title,
                                html: response.data.swal.html,
                                type: response.data.swal.type
                            });
                        }
                    }
                }).done(function(){
                    btn.button('reset');
                });
            });

            $('#marketplace-currency-table').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "responsive": true,
                "ajax": "/marketplace/marketplace_currency/datatable",
                columnDefs: [
                    {
                        targets: [0, 8],
                        orderable: false
                    }
                ],
                "columns": [
                    {
                        "data": "image",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                if (!row.image) {
                                    data = '<a href="{{ MarketplaceCurrency::DEFAULT_IMAGE_1024x1024 }}"><img src="{{ str_replace('1024x1024', '100x100', MarketplaceCurrency::DEFAULT_IMAGE_1024x1024) }}" width="25" height="25"></a>';
                                } else {
                                    data = '<a href="' + row.image + '"><img src="' + row.image.replace('1024x1024', '100x100') + '" width="25" height="25"></a>';
                                }
                            }

                            return data;
                        }
                    },
                    {
                        "data": "application_id",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                // data = !row.application_id ? 'N/A' : row.application_id;
                                data = !row.application_id ? 'N/A' : '<span data-id="' + row.application_id + '"></span>' + row.applications.name;
                            }

                            return data;
                        }
                    },
                    {
                        "data": "item_currency",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = !row.item_currency ? 'N/A' : '<span data-id="' + row.item_currency + '"></span>' + row.itemcurrencies.currency_name + " (" + row.itemcurrencies.currency + ")";
                            }

                            return data;
                        }
                    },
                    {
                        "data": "item_amount",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = !row.item_amount ? 'N/A' : row.item_amount;
                            }

                            return data;
                        }
                    },
                    {
                        "data": "purchase_currency",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = !row.purchase_currency ? 'N/A' : '<span data-id="' + row.purchase_currency + '"></span>' + row.purchasecurrencies.currency_name + " (" + row.purchasecurrencies.currency + ")";
                            }

                            return data;
                        }
                    },
                    {
                        "data": "purchase_amount",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = !row.purchase_amount ? 'N/A' : row.purchase_amount;
                            }

                            return data;
                        }
                    },
                    {
                        "data": "expiration_date",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = !row.applications.name ? 'N/A' : row.expiration_date;
                            }

                            return data;
                        }
                    },
                    {
                        "data": "status_id",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = !row.status.status_name ? 'N/A' : '<span data-id="' + row.status_id + '"></span>' + row.status.status_name;
                            }

                            return data;
                        }
                    },
                    {
                        "data": null,
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = '<a href="#" data-id="' + row.marketplace_currency_id + '" class="btn btn-xs btn-warning btn-edit-marketplace-currency push-right"><i class="fa fa-edit"></i></a>';
                            }

                            return data;
                        }
                    }
                ]
            });

            $('.datepicker').datetimepicker({
                format: "MM d, yyyy HH:ii P",
                autoclose: true,
                startDate: '+1d',
                todayHighlight: false
            });

            $('select.form-group-amount').on('change', function(){
                $('select.form-group-amount').not(this).find('option').show();
                $('select.form-group-amount').not(this).find('option[value="' + $(this).val() + '"]').hide();
            });

            $('#image').on('change', function () {

                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#marketplace-currency-image').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(this.files[0]);
                }
            });

            $('#marketplace-currency-modal').on("hidden.bs.modal", function () {
                var img = $('#marketplace-currency-image');
                img.attr('src', img.data('old-src'));
                $('#marketplace-currency-modal-form').trigger('reset');
            });

            $('#add-marketplace-currency').on("click", function () {
                $('#purchase_currency option').show();
                $('#item_currency option').show();
            });

        });
    </script>
@endsection
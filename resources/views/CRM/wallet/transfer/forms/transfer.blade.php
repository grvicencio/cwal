<div class="modal fade" id="modal-transfer" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="form-transfer" id="form-transfer" action="/admin/wallet/transfer" method="POST">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Transfer funds to <strong id="email"></strong></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="user-id-input" name="user_id">
                    <div class="form-group">
                        <label for="currency-id-select" class="col-sm-2 control-label">Account</label>

                        <div class="col-sm-9">
                            <select class="form-control" id="currency-id-select" name="wallet_account_number" data-width="100%"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="transfer-amount-input" class="col-sm-2 control-label" >Amount</label>

                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="transfer-amount-input" step="0.00000001" name="transfer_amount"
                                   placeholder="Enter amount to transfer" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="reason-textarea" class="col-sm-2 control-label">Reason</label>

                        <div class="col-sm-9">
                            <textarea class="form-control" name="reason" id="reason-textarea" rows="3"
                                      placeholder="Enter reason" style="resize:none;" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    @usercan('add', 'wallet.transfer.transfer')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary">
                        Transfer
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function () {
            $("#modal-transfer").on("hidden.bs.modal", function () {
                var form = $('#form-transfer');
                form.find('#email').empty();
                form.find('[name=user_id]').val('');
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-transfer').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                var url = form.prop('action');

                swal({
                    title: 'Confirm Transfer',
                    text: 'Please input your password to proceed.',
                    input: 'password',
                    showCancelButton: true,
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    confirmButtonText: 'Proceed',
                    showLoaderOnConfirm: true,
                    autocomplete: "new-password",
                    preConfirm: function (password) {
                        return new Promise(function (resolve) {
                            if (password === false || password === "") {
                                swal.showValidationError('Password is required!');
                                resolve();
                            } else if (password) {
                                $.post(url, form.serialize() + '&password=' + password, function (data) {
                                    if(data.errors){
                                        assocErr(data.errors, form);
                                        resolve();
                                    }
                                    if (!data.errors) {
                                        if (data.swal) {
                                            swal({
                                                title: data.swal.title,
                                                html: data.swal.html,
                                                type: data.swal.type
                                            });
                                            btn.button('reset');
                                            $('#modal-transfer').modal('hide');
                                        }
                                        return;
                                    }
                                });
                                btn.button('reset');
                            }
                        });
                    }
                }).then(function(result){
                    if (result.dismiss === swal.DismissReason.cancel){
                        btn.button('reset');
                    }
                });;

            $('#currency-id-select').select2({
                placeholder: "Select currency"
            });
        });
            });
    </script>

@endsection
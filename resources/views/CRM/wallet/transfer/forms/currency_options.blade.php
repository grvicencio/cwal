@if($global->count())
    <optgroup label="Global">
        @foreach($global as $wallet)
            <option value="{{ $wallet->wallet_account_number }}">
                {{ $wallet->currency->currency }} ({{ $wallet->wallet_account_number }})
            </option>
        @endforeach
    </optgroup>
@endif
@if($crypto->count())
    <optgroup label="Cryptocurrency">
        @foreach($crypto as $wallet)
            <option value="{{ $wallet->wallet_account_number }}">
                {{ $wallet->currency->currency }} ({{ $wallet->wallet_account_number }})
            </option>
        @endforeach
    </optgroup>
@endif
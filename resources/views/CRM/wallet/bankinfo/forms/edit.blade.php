<div class="modal fade" id="modal-edit-bankinfo" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="form-edit-bankinfo" id="form-edit-bankinfo">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Bank Information</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="edit-bank-name-input" class="col-sm-3 control-label">Bank Name</label>

                        <div class="col-sm-8">
                        <input name="bank_name" id="edit-bank-name-input" class="form-control" required></input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-bankinfo-input" class="col-sm-3 control-label">Bank Information</label>

                        <div class="col-sm-8">
                        <textarea name="information" id="edit-informations-input"class="form-control" rows="5" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-bankinfo-code-input" class="col-sm-3 control-label">Currency Code</label>

                        <div class="col-sm-8">
                            <!-- <input type="text" class="form-control" name="currency_id" id="edit-currency-id-input"
                                   placeholder="Currency ID"> -->
                            <select id="edit-status-input" class="form-control"
                                    name="currency_id" required>
                                   
                                    @foreach ($currencies as $currency)
                                        <option value='{{ $currency->currency_id}}'> {{ $currency->currency }}</option>
                                    @endforeach
                               
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-status-input" class="col-sm-3 control-label">Status</label>

                        <div class="col-sm-8">
                            <select id="edit-status-input" class="form-control"
                                    name="status_id" required>
                                    @foreach ($statuses as $status)
                                        <option value='{{ $status->status_id}}'> {{ $status->status_name }}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-status-input" class="col-sm-3 control-label">Default Account?</label>

                        <div class="col-sm-8" id="edit-default-account-holder">
                                <input  type="checkbox" id="edit-default-account" name="default_account" value=true>
                                <span class="label label-default" id="default-account-message" >A default account is required!</span>
                        </div>
                       
                    </div>
                    
                    <input type="hidden" name="confirm">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('edit', 'currencies.store')
                    <button type="submit" id="edit_submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary">
                        Save
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>

@section('script')
    @parent

    <script>
     
    function defaultAccountChecker(){
        
        var formAddForm = $('#form-add-bankinfo');
        var form = $('#form-edit-bankinfo');
        statOption=form.find('select[name=status_id]');
        statOptionAdd=formAddForm.find('select[name=status_id]');
        var statIndex =statOption.prop('selectedIndex');
        var statIndexAdd =statOptionAdd.prop('selectedIndex');
        if (statIndex == 1){
        form.find('input[name=default_account]').attr('disabled',true);
        form.find('input[name=default_account]').prop('checked', false); 
        }
        else
        form.find('input[name=default_account]').removeAttr("disabled");
        if(form.find('input[name=default_account]').prop('checked')==true) {
            $('#default-account-message').show();
            statOption.attr('disabled',true);
        }else $('#default-account-message').hide();
        
        if (statIndexAdd == 1 ){
        formAddForm.find('input[name=default_account]').attr('disabled',true);
        formAddForm.find('input[name=default_account]').prop('checked', false); 
        }
        else
        formAddForm.find('input[name=default_account]').removeAttr("disabled");
        
    }
     function setFormEditBankInfo(src){
        var form = $('#form-edit-bankinfo');
            var tr = $(src).closest('tr');
             var wid= tr.data('bankinfo-id');
                $('<input />').attr('type', 'hidden')
                .attr('name', "depositinfo_id")
                .attr('value', wid)
                .appendTo(form);
                var info = formatTextInput(tr.find('td:eq(1)').text());
                form.find('input[name=bank_name]').val(tr.find('td:eq(0)').text());
                form.find('textarea[name=information]').val(info);
                currencyOption=form.find('select[name=currency_id]');
                var curIndex =currencyOption.find('option[value="'+tr.data('currency-id')+'"]').index();
                currencyOption.prop('selectedIndex',curIndex).change();
                statOption=form.find('select[name=status_id]');
                var statIndex =statOption.find('option[value="'+tr.data('status-id')+'"]').index();
                statOption.prop('selectedIndex',statIndex).change();
                form.find('input[name=default_account]').prop('checked',tr.data('default-account'));
                
                $('#modal-edit-bankinfo').modal('show');
                defaultAccountChecker();
                
                if(tr.data('default-account')==true) {
                form.find('input[name=default_account]').attr('disabled',true);
                   if(form.find('input[name=default_account]').prop('checked')==true){
                            console.log(form.find('input[name=default_account]').prop('checked'));
                            // swal({
                            //             title: '{{ trans('swal.content.set_as_default_error.title') }}',
                            //             html: '{!! trans('swal.content.set_as_default_error.html') !!}',
                            //             type: '{{ trans('swal.content.set_as_default_error.type') }}'
                            //     });
                                // $('#edit-default-account-holder').mouseenter(function() {
                                   
                                // });
                       }
                    
                }else console.log(tr.data('default-account') +": "+ tr.index());  
        }

        $(function () {
           
            var form = $('#form-edit-bankinfo');
            $("#modal-edit-bankinfo").on("hidden.bs.modal", function () {
                $(this).removeData('bs.modal');
                 form = $('#form-edit-bankinfo');
                form.trigger('reset');
                clearErr(form);
                $(this).removeData('bs.modal');
                statOption=form.find('select[name=status_id]');
                statOption.removeAttr("disabled");
               
            });
            $('select[name=status_id]').change(function(){
                defaultAccountChecker();
            });
           
            $('#form-edit-bankinfo').submit(function (e) {
                form.find('select[name=status_id]').prop("disabled", false);
                form.find('input[name=default_account]').prop("disabled", false);
                $.post('/admin/wallet/bankinformation/update',form.serialize(),function(res,er){
                    if(res.errors != null){
                        var msg="";
                        $.each( res.errors, function( key, value ) {
                            msg+=value;
                            });
                        alert(msg);
                    }else{
                    reloadTbl();
                    $('#modal-edit-bankinfo').modal('hide');
                    }
                   
                });
                return false;
            });
        });
    </script>

@endsection

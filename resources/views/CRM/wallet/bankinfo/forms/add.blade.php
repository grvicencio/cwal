<div class="modal fade" id="modal-add-bankinfo" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="form-add-bankinfo" id="form-add-bankinfo">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Bank</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="add-bank-name-input" class="col-sm-3 control-label">Bank Name</label>

                        <div class="col-sm-8">
                        <input name="bank_name" id="add-bank-name-input" class="form-control" required></input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-bankinfo-input" class="col-sm-3 control-label">Bank Information</label>

                        <div class="col-sm-8">
                        <textarea name="information" id="add-informations-input" class="form-control" rows="5" required></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-bankinfo-code-input" class="col-sm-3 control-label">Currency Code</label>

                        <div class="col-sm-8">
                          
                            <select id="add-status-input" class="form-control"
                                    name="currency_id" required>
                                   
                                    @foreach ($currencies as $currency)
                                        <option value='{{ $currency->currency_id}}'> {{ $currency->currency }}</option>
                                    @endforeach
                               
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-status-input" class="col-sm-3 control-label">Status</label>

                        <div class="col-sm-8">
                            <select id="add-status-input" class="form-control"
                                    name="status_id" required>
                                   
                                    @foreach ($statuses as $status)
                                        <option value='{{ $status->status_id}}'> {{ $status->status_name }}</option>
                                    @endforeach
                               
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-status-input" class="col-sm-3 control-label">Default Account?</label>

                        <div class="col-sm-8">
                                <input  type="checkbox" id="add-default-account" name="default_account" value=true>
                        </div>
                    </div>

                    <input type="hidden" name="confirm">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'wallet.bankinformation.store')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary">
                        Save
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>

@section('script')
    @parent

    <script>
        
        $(function () {
            var form = $('#form-add-bankinfo');
            $("#modal-add-bankinfo").on("hidden.bs.modal", function () {
                var form = $('#form-add-bankinfo');
                form.trigger('reset');
                clearErr(form);
            });

           $('#add-status-input').change(function(){
            defaultAccountChecker();
           });

            $('#form-add-bankinfo').submit(function (e) {
                $.post('/admin/wallet/bankinformation/store',form.serialize(),function(res,er){
                    if(res.errors != null){
                        console.log(res.errors);
                        var msg="res.errors";
                        $.each( res.errors, function( key, value ) {
                            msg+=value;
                            });
                        alert(msg);
                    }else{
                    reloadTbl();
                    $('#modal-add-bankinfo').modal('hide');
                    }
                });
                return false;
            });
        });
    </script>

@endsection
@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Wallet</li>
        <li class="active">Currency</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="currrency-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Symbol</th>
                            <th>Currency Code</th>
                            <th>Type</th>
                            <th>Default?</th>
                            @usercan('edit', 'currencies.update')
                            <th>Actions</th>
                            @endusercan
                        </tr>
                        </thead>
                    </table>
                </div>
                @usercan('add', 'currencies.store')
                <div class="box-footer">
                    <button type="button" class="btn btn-success btn-xs btn-add-currency"
                            data-toggle="modal"
                            data-target="#modal-add-currency" title="Add">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
                @endusercan
            </div>
        </div>
    </div>

    @include('CRM.wallet.currency.forms.add')
    @include('CRM.wallet.currency.forms.edit')
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        var setFormEditCurrency = function (src) {
            var form = $('#form-edit-currency');
            var tr = $(src).closest('tr');
            form.attr('data-currency-id', tr.data('currency-id'));
            form.find('input[name=currency_name]').val(tr.find('td:eq(0)').text());
            form.find('input[name=currency_symbol]').val(tr.find('td:eq(1)').text());
            form.find('input[name=currency]').val(tr.find('td:eq(2)').text());
            form.find('input[name=app_currency]').prop('checked', tr.find('td:eq(3)').data('app-currency')).trigger('change');
            form.find('select[name=default_registration]').val(tr.find('td:eq(4)').data('default-registration')).trigger('change');
            $('#modal-edit-currency').modal('show');
        };

        $(function () {
            $('#currrency-table').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "responsive": true,
                "ajax": {
                    "url": "/admin/wallet/currencies/datatable",
                    "error": function($err){
                        if($err.status == 401){
                            alert('Your session is expired! Please login again.');
                            window.location.href = '/login';
                        }
                    }
                },
                'createdRow': function (tr, data, dataIndex) {
                    $(tr).attr('data-currency-id', data.currency_id);
                },
                columnDefs: [{
                    'targets': 3,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('data-app-currency', cellData ? 1 : 0);
                    }
                }, {
                    'targets': 4,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('data-default-registration', cellData ? 1 : 0);
                    }
                },
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'currencies.update'))
                    {
                        targets: 5,
                        orderable: false
                    }
                    @endif
                ],
                "columns": [
                    {"data": "currency_name"},
                    {"data": "currency_symbol"},
                    {"data": "currency"},
                    {
                        "data": "app_currency",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = row.app_currency ? 'Cryptocurrency' : 'Global';
                            }
                            return data;
                        }
                    },
                    {
                        "data": "default_registration",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = row.default_registration ? 'Yes' : 'No';
                            }
                            return data;
                        }
                    },
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'currencies.update'))
                    {
                        "data": null,
                        "render": function (data, type, row, meta) {
                            data = '';

                            if (type === 'display') {
                                @if(Auth::guard('crm')->user()->canAccess('edit', 'currencies.update'))
                                    data += '<button type="button" onclick="setFormEditCurrency(this);" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-edit"></i></button>';
                                @endif
                            }
                            return data;
                        }
                    }
                    @endif
                ]
            });

            $('input[name=app_currency]').change(function (e) {
                if (!$(this).is(':checked')) {
                    $('select[name=default_registration]').removeAttr('readonly');
                    $('select[name=default_registration]').removeAttr('onmousedown');
                    $(this).val(0);
                } else {
                    $('select[name=default_registration]').attr('onmousedown', '(function(e){ e.preventDefault(); })(event, this)');
                    $('select[name=default_registration]').attr('readonly', true);
                    $('select[name=default_registration]').val(0).trigger('change');
                    $(this).val(1);
                }
            });
        });
    </script>
@endsection
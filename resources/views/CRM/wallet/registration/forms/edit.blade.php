<div class="modal fade" id="modal-edit-registration" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="form-edit-registration" id="form-edit-registration"
                  data-temp-action="/admin/wallet/registrations"
                  method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Registration</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="edit-type-input" class="col-sm-3 control-label">Type</label>

                        <div class="col-sm-8">
                            <p class="form-control-static" id="edit-type-static"></p>
                            <input type="hidden" name="type">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="edit-avatar-id-input" class="col-sm-3 control-label">Avatar</label>

                        <div class="col-sm-8">
                            <img src="http://placehold.it/100x100" alt="100x100"
                                 data-src="http://placehold.it/100x100" data-alt="100x100"
                                 data-title="100x100" title="100x100" height="100"
                                 class="img-responsive avatar-placeholder">
                            <input type="hidden" name="avatar_id">
                            <br>
                            <button type="button" role="button" class="btn btn-default select-avatar-btn">
                                <i class="fa fa-smile-o"></i> Select avatar
                            </button>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="edit-currency-id-input" class="col-sm-3 control-label">Currency</label>

                        <div class="col-sm-8">
                            <select id="edit-currency-id-input" class="form-control" name="currency_id">
                                @foreach($in_app_currencies as $currency)
                                    <option value="{{ $currency->currency_id }}">{{ $currency->currency }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="edit-amount-input" class="col-sm-3 control-label">Amount</label>

                        <div class="col-sm-8">
                            <input type="number" step='0.01' class="form-control" name="amount"
                                   id="edit-amount-input"
                                   placeholder="Amount">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('edit', 'registrations.update')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">
                        Save changes
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>

@section('script')
    @parent

    <script>
        $(function () {
            $("#modal-edit-registration").on("hidden.bs.modal", function () {
                var form = $('#form-edit-registration');
                form.removeAttr('data-registration-id');
                form.find('.select-avatar-btn').prop('disabled', false);
                var img = form.find('.avatar-placeholder');
                img.prop('src', img.data('src'));
                img.prop('alt', img.data('alt'));
                img.prop('title', img.data('title'));
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-edit-registration').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                var url = form.data('temp-action') + '/' + form.data('registration-id');

                $.post(url, form.serialize(), function (response) {
                    if (response.status == '{{ config('response.type.success') }}') {
                        location.href = form.data('temp-action');
                        return;
                    } else if (response.status == '{{ config('response.type.error') }}') {
                        assocErr(response.errors, form);
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });
    </script>

@endsection

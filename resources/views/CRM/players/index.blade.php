@extends('layouts.default')
@section('title', 'Players')
  @section('scripts')
            <script type="text/javascript" >
            $(document).ready(function() {
              $('#table').DataTable();
          } );

            </script>
  @stop    


                
   @section('content')

          <table class="table" id="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
     
            <th class="text-center">Email</th>
            <th class="text-center">Mobile</th>
            <th class="text-center">Username</th>
            
        </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            
        </tr>
    </tbody>
    <tbody>
        @foreach($players as $player)
        <tr class="item{{$player->id}}">
            <td>{{$player->id}}</td>
            <td>{{$player->email}}</td>
            <td>{{$player->mobile}}</td>
            <td>{{$player->username}}</td>
            
            <td><button class="edit-modal btn btn-info">
                    
                    <span class="glyphicon glyphicon-edit"></span> Edit
                </button>
                <button class="delete-modal btn btn-danger">
                    
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </button></td>
        </tr>
        @endforeach
       </tbody>
</table>

   @endsection

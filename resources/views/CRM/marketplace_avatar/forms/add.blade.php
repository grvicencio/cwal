<div class="modal fade avatar-frm" id="modal-add-avatar" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form name="form-add-avatar" id="form-add-avatar" class="form-horizontal" action="{{ route('marketplace_avatar.store') }}" method="post">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Avatar</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="add-avatar-id-input" class="col-sm-3 control-label">Avatar</label>

                            <div class="col-sm-8">
                                <img src="http://via.placeholder.com/100/d3d3d3/696969?text=1024%20x%201024" alt="1024x1024" data-src="http://via.placeholder.com/100/d3d3d3/696969?text=1024%20x%201024" data-alt="1024x1024" data-title="1024x1024" title="1024x1024" style="width: 100px !important;" class="img-responsive avatar-placeholder">
                                <input type="hidden" name="avatar_id">
                                <br />
                                <button type="button" role="button"  class="btn btn-default select-avatar-btn">
                                    <i class="fa fa-smile-o"></i> Select avatar
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="avatar_amount" class="control-label col-sm-3">Amount</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="avatar_amount" name="avatar_amount" placeholder="Amount">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="currency" class="control-label col-sm-3">Currency</label>

                            <div class="col-sm-8">
                                <select class="form-control" name="avatar_currency" id="avatar_currency">
                                    <option value="" style="display: none;"></option>
                                    <optgroup label="Global Currencies">
                                        @foreach($globalcurrencies AS $row)
                                            <option value="{{ $row->currency_id }}">{{ $row->currency_name }} ({{ $row->currency }})</option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="In-Ap Currencies">
                                        @foreach($appcurrencies AS $row)
                                            <option value="{{ $row->currency_id }}">{{ $row->currency_name }} ({{ $row->currency }})</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="valid_date" class="control-label col-sm-3">Valid Until</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control datepicker" readonly data-date-start-date="+1d" id="valid_date" name="valid_date" placeholder="Valid Until">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status_id" class="control-label col-sm-3">Status</label>

                            <div class="col-sm-8">
                                <select id="status_id" class="form-control" name="status_id">
                                    @foreach($status as $row)
                                        <option value="{{ $row->status_id }}">{{ $row->status_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'marketplace_avatar.store')
                        <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                                class="btn btn-primary">Save
                        </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>
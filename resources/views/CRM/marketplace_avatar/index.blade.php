@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Market Place</li>
        <li class="active">Avatar</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            @usercan('add', 'marketplace_avatar.store')
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-add-avatar" title="Add">
                    <i class="fa fa-plus"></i> New Market Avatar
                </button>
            @endusercan

            <div class="box">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-12 form-inline">
                            <span class="ml">Show</span>
                            <select name="per_page" id="avatars-per-page" class="form-control input-sm">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>entries</span>

                            <span class="ml">Status: </span>
                            <select name="type" id="avatars-status-id" class="form-control input-sm">
                                <option value="" selected>All</option>
                                @foreach($status AS $row)
                                    <option value="{{ $row->status_id }}">{{ $row->status_name }}</option>
                                @endforeach
                            </select>

                            <span class="ml">Currency Type: </span>
                            <select name="currency_id" id="avatars-currency-id" class="form-control input-sm">
                                <option value="" selected>All</option>
                                @foreach($currencies AS $row)
                                    <option value="{{ $row->currency_id }}">{{ $row->currency_name }} ({{ $row->currency }})</option>
                                @endforeach
                            </select>

                            <input type="hidden" id="current-url">
                            <input type="hidden" id="select-avatar-url">
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="avatars"></div>
                </div>

                <div class="box-footer avatar-links text-center"></div>
            </div>
        </div>
    </div>

    @include('CRM.marketplace_avatar.forms.add')
    @include('CRM.marketplace_avatar.forms.edit')
    @include('CRM.marketplace_avatar.select_avatar')
@endsection

@section('style')
    @parent

    <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}">

    <style>
        .ml {
            padding-left: 10px;
        }

        .avatars {
            padding: 20px 20px 0px 20px;
        }

        .avatar-links {
            padding-top: 0;
            padding-bottom: 0;
        }

        .avatars .caption p {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

        .btn-round{
            padding: 13px 17px !important;
            position: absolute;

            border: none !important;
            border-radius: 23px;
            box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3);
        }
            .btn-round.btn-edit-avatar{
                margin-top: -42.5px;
                right: 40px;
            }
            .btn-round.btn-remove-avatar{
                margin-top: -20px;
                right: 0px;
            }

        #avatars-per-page, #avatars-type {
            width: auto;
        }

        #modal-select-avatar .modal-dialog {
            overflow-y: initial !important
        }

        #modal-select-avatar .modal-body {
            height: 520px;
            overflow-y: auto;
        }

        #modal-select-avatar .modal-footer div {
            padding: 0;
        }

        #modal-select-avatar .modal-footer div .pagination {
            margin: 0;
        }

        .avatar-card{
            margin-bottom: 20px;
            overflow: hidden;

            border-radius: 5px;
            box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.3);
        }
            .avatar-card span{
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: no-wrap !important;
            }
            .avatar-card .caption{
                padding: 20px;
            }
                .avatar-card .caption span{
                    display: block;
                }
                    .avatar-card .caption span.badge-success{
                        background: #00a65a !important;
                    }
                    .avatar-card .caption span.badge-danger{
                        background: #dd4b39 !important;
                    }
                    .avatar-card .caption span > em{
                        color: #CCC;
                        font-size: 1.25rem;
                    }
                .avatar-card .caption .caption-title{
                    font-weight: 700;
                    font-size: 1.75rem;
                }

        div.thumbnail{
            border-radius: 0px !important;
            cursor: hand !important;

            transition: 0.1s ease;
            -webkit-transition: 0.1s ease;
            -moz-transition: 0.1s ease;
            -ms-transition: 0.1s ease;
            -o-transition: 0.1s ease;
        }
            div.thumbnail:hover{
                border: 2px solid #3c8dbc;

                transform: scale(1.05, 1.05);

                transition: 0.1s ease;
                -webkit-transition: 0.1s ease;
                -moz-transition: 0.1s ease;
                -ms-transition: 0.1s ease;
                -o-transition: 0.1s ease;
            }

        .form-control.datepicker{
            padding: 6px 12px !important;

            background: #FFF !important;
            border-radius: 0px !important;
        }
    </style>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset('CRM/Capital7-1.0.0/js/form-validation.js') }}"></script>
    <script src="{{ asset('CRM/Capital7-1.0.0/js/params.js') }}"></script>
    <script src="{{ asset('AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('AdminLTE-2.4.2/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script>
        function getAvatars(url){
            $('#current-url').val(url);

            $.getJSON(url, function(){
                $('.box-body .avatars').html('<p class="text-center">' + '{!! trans('loading.processing') !!}' + '</p>');
            }).done(function(response){
                $('.box-body .avatars').html(response.data.avatars.html);
                $('.avatar-links').html(response.data.avatars.links);
            });
        }

        function selectAvatars(url){
            $('#select-avatar-url').val(url);

            $.getJSON(url, function(){
                $('#modal-select-avatar .avatars').html('<p class="text-center">' + '{!! trans('loading.processing') !!}' + '</p>');
            }).done(function(response){
                $('#modal-select-avatar .avatars').html(response.data.selectavatars.html);
                $('.avatar-links').html(response.data.selectavatars.links);
            });
        }

        $(function(){
            getAvatars('{{ route("marketplace_avatar.json") }}?per_page=' + $('#avatars-per-page').val() + '&readonly=false');
            selectAvatars('{{ route("marketplace_avatar.selectavatars") }}?per_page=' + $('#avatars-per-page').val() + '&readonly=true');

            $("#modal-add-avatar").on("shown.bs.modal", function(){
                var form = $('#form-add-avatar');
                form.trigger('reset');
                form.find('.avatar-placeholder').prop('src', 'http://via.placeholder.com/100/d3d3d3/696969?text=1024%20x%201024');
                clearErr(form);
            });

            $(document).on('click', '.btn-edit-avatar', function(){
                var $form = $('#modal-edit-avatar');

                console.log($(this).data('status'));

                $form.find('.avatar-placeholder').prop('src', $(this).data('avatar-href'));
                $form.find('input[name="marketplace_avatar_id"]').val($(this).data('id'));
                $form.find('input[name="avatar_id"]').val($(this).data('avatar-id'));
                $form.find('select#status_id').val($(this).data('status'));
                $form.find('input#avatar_amount').val($(this).closest('.caption').find('span').eq(1).data('amount'));
                $form.find('select#avatar_currency').val($(this).closest('.caption').find('span').eq(1).data('currency'));
                $form.find('input#valid_date').val($(this).closest('.caption').find('span').eq(2).data('date'));

                clearErr($form);
            });

            $('#form-add-avatar').submit(function(e){
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                clearErr(form);

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    cache: false,
                    data: $('#form-add-avatar').serialize(),
                    success: function(response){
                        if(response.status == '{{ config('response.type.success') }}'){
                            swal('Add Avatar', 'Record Successfully Added!', 'success').then($res => {
                                location.reload();
                            });
                        } else if(response.status == '{{ config('response.type.error') }}'){
                            assocErr(response.errors, form);
                        } else if(response.status == '{{ config('response.type.fail') }}'){
                            swal({
                                title: response.data.swal.title,
                                html: response.data.swal.html,
                                type: response.data.swal.type
                            });
                        }
                    }
                }).done(function(){
                    btn.button('reset');
                });
            });

            $('#form-edit-avatar').submit(function(e){
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                clearErr(form);

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    cache: false,
                    data: $('#form-edit-avatar').serialize(),
                    success: function(response){
                        if(response.status == '{{ config('response.type.success') }}'){
                            swal('Edit Avatar', 'Record Successfully Updated!', 'success').then($res => {
                                location.reload();
                            });
                        } else if(response.status == '{{ config('response.type.error') }}'){
                            assocErr(response.errors, form);
                        } else if(response.status == '{{ config('response.type.fail') }}'){
                            swal({
                                title: response.data.swal.title,
                                html: response.data.swal.html,
                                type: response.data.swal.type
                            });
                        }
                    }
                }).done(function(){
                    btn.button('reset');
                });
            });

            $('.select-avatar-btn').on('click', function(){
                selectAvatars('{{ route("marketplace_avatar.selectavatars") }}?per_page=' + $('#avatars-per-page').val() + '&readonly=true');
                $('#modal-select-avatar').modal('show');
            });

            $('#modal-select-avatar .avatars').on('click', '.div-select-avatar', function (e) {
                e.preventDefault();
                var form = $('.avatar-frm');
                form.find('input[name=avatar_id]').val($(this).data('avatar-id'));
                form.find('.avatar-placeholder').prop('src', $(this).data('avatar'));
                $('#modal-select-avatar').modal('hide');
            });

            $('.avatar-links').on('click', '.pagination a', function(e){
                e.preventDefault();
                var url_lvl1 = setParam($(this).prop('href'), 'per_page', $('#avatars-per-page').val());
                var url_lvl2 = setParam(url_lvl1, 'currency_id', $('#avatars-currency-id').val());
                var url_lvl3 = setParam(url_lvl2, 'status_id', $('#avatars-status-id').val());
                getAvatars(url_lvl3);
            });

            $("#avatars-status-id").on("change", function(){
                var url_lvl1 = setParam($('#current-url').val(), 'status_id', $(this).val());
                var url_lvl2 = setParam(url_lvl1, 'page', 1);
                getAvatars(url_lvl2);
            });

            $("#avatars-currency-id").on("change", function(){
                var url_lvl1 = setParam($('#current-url').val(), 'currency_id', $(this).val());
                var url_lvl2 = setParam(url_lvl1, 'page', 1);
                getAvatars(url_lvl2);
            });

            $("#avatars-search").on("keyup", function(){
                getAvatars('{{ route('marketplace_avatar.json') }}?search=' + $(this).val() + '&per_page=' + $('#avatars-per-page').val() + "&currency_id=" + $('#avatars-currency-id').val() + "&status_id=" + $('#avatars-status-id').val() + '&readonly=false');
            });

            $("#avatars-per-page").on("change", function(){
                var url_lvl1 = setParam($('#current-url').val(), 'per_page', $(this).val());
                var url_lvl2 = setParam(url_lvl1, 'page', 1);
                getAvatars(url_lvl2);
            });

            $('.datepicker').datepicker({
                autoclose: true,
                clearBtn: true,
                format: "MM d, yyyy"
            });
        });
    </script>
@endsection
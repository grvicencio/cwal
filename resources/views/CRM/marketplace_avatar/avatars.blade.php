@forelse($avatars as $avatar)
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 @if($readonly) div-select-avatar @endif" @if($readonly) data-avatar-id="{{ $avatar->avatar_id }}" data-avatar="{{ $avatar->avatar }}" @endif>
        <div class="avatar-card">
            @if(!$readonly)
                <div class="caption">
                    @usercan('edit', 'marketplace_avatar.update')
                        <a href="#" class="btn btn-warning btn-edit-avatar pull-right" data-target="#modal-edit-avatar" data-toggle="modal" data-avatar-href="{{ $avatar->avatar }}" data-status="{{ $avatar->marketplace_status }}" data-id="{{ $avatar->marketplace_avatar_id }}" data-avatar-id="{{ $avatar->avatar_id }}"><i class="fa fa-pencil-square-o"></i></a>
                    @endusercan

                    <span class="caption-title text-primary">{{ $avatar->name }}</span>
                    <span data-currency="{{ $avatar->avatar_currency }}" data-amount="{{ number_format($avatar->avatar_amount, 2, '.', '') }}"><em>Amount</em> {{ $avatar->currency_name }} ({{ $avatar->currency }}) {{ number_format($avatar->avatar_amount, 2, '.', ',') }}</span>
                    <span data-date="{{ $avatar->valid_date }}"><em>Valid until</em> {{ $avatar->valid_date }}</span>
                </div>
            @endif

            <img src="{{ $avatar->avatar }}" class="img-responsive" alt="{{ $avatar->name }}">

            @if(!$readonly)
                <div class="caption">
                    <span class="badge badge-{{ $avatar->status_name == 'Active' ? 'success' : 'danger' }}">{{ $avatar->status_name }}</span>
                </div>
            @endif
        </div>
    </div>
@empty
    <div class="col-md-12"><i>No records found.</i></div>
@endforelse
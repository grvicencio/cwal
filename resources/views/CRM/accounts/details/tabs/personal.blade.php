<div id="personal" class="tab-pane fade">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group text-center">
                <img src="{{ isset($account->account_image) ? $account->account_image : asset('images/WEB/default_avatar.png') }}"  class="img-circle img-thumbnail"   style="width:200px;" id="img-thumbnail" alt="User Image">
            </div>
            <form name="formeditaccount" id="formeditaccount" action="{{ route('accounts.update', $account) }}#personal"
                  method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                    <label for="first-name-input" class="control-label">First Name</label>
                    <input type="text" class="form-control" id="first-name-input" name="first_name"
                           placeholder="First Name" value="{{ old('first_name', $account->first_name) }}">

                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label for="last-name-input" class="control-label">Last Name</label>
                    <input type="text" class="form-control" id="last-name-input" name="last_name"
                           placeholder="Last Name" value="{{ old('last_name', $account->last_name) }}">

                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>


                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email-input" class="control-label">Email</label>
                    <input type="text" class="form-control" id="email-input"
                           name="email" placeholder="Email" value="{{ old('email', $account->email) }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>


                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                    <label for="gender-input" class="control-label">Gender</label>
                    <select class="form-control" id="gender-input" name="gender">
                        <option value=""> -- Select Gender -- </option>
                        @foreach($genders as $gender)
                            <option {{ old('gender', $account->gender) == $gender ? "selected" : "" }}>{{ $gender }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('gender'))
                        <span class="help-block">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
                    <label for="birth_date-input" class="control-label">Date of Birth</label>
                    <input type="text" class="form-control" data-date-end-date="-18y" id="birth-date-input" required name="birth_date" placeholder="Select Date of Birth" value="{{ old('birth_date', $account->birth_date) }}">

                    @if ($errors->has('birth_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('birth_date') }}</strong>
                        </span>
                    @endif
                </div>


                <div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
                    <label for="modal-edit-status" class="control-label">Status</label>
                    <select id="status-id-select" class="form-control" name="status_id">
                        @foreach($statuses as $status)
                            <option value="{{ $status->status_id }}"{{ $status['status_id'] == old('status_id', $account->status_id) ? ' selected' : '' }}>{{ $status->status_name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('status_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('status_id') }}</strong>
                        </span>
                    @endif
                </div>

                @usercan('edit', 'accounts.update')
                <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary">Save</button>
                @endusercan
            </form>
        </div>
    </div>
</div>

@section('scripts')
    @parent
    <script src="{{ asset('CRM/AdminLTE-2.4.2/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('CRM/AdminLTE-2.4.2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">
@endsection

@section('script')
    @parent
    <script src="{{ asset('CRM/Capital7-1.0.0/js/form-validation.js') }}"></script>

    <script>
    var loadThumbnail = document.getElementById("img-thumbnail").complete;
    var imgsrc= document.getElementById("img-thumbnail").src;
    function loadImg(imgholder){
       $('#img-thumbnail').attr('src',imgsrc);
       if( $('#img-thumbnail').complete) $('#img-thumbnail').trigger('load');
    }
        $(function () {
            $('#birth-date-input').datepicker({
                clearBtn: true,
                format: "MM d, yyyy"
            });

            $('#formeditaccount').find('#country-select').select2({
                placeholder: "Select a country",
                data: countries
            }).val("{{ old('country', $account->country) }}").trigger('change');

            $('#formeditaccount').submit(function () {
                $(this).find(':submit').button('loading');
            });
            if(loadThumbnail == false){
                setTimeout(function(){ loadImg(); }, 2000);
                }
        });
    </script>
@endsection
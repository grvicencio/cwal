<div class="wallet-source text-center">
    <h2>
        <small>TRADE TYPE</small>
        <br>
        <span class="label label-success">
            {{ $type }}
        </span>
    </h2>

    <h3>
        <small>AMOUNT</small>
        <br>
        <code>{{ $amount }}</code>
    </h3>

    <br>

    <p>
        <i class="fa fa-calendar-check-o"></i> {{ $created_at }}
    </p>
</div>
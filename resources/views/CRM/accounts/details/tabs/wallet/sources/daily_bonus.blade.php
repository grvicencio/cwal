<div class="wallet-source text-center">
    <h3>
        <span class="label label-success">{{ $daily_bonus_name }}</span>
    </h3>

    <h3>
        <small>AMOUNT</small>
        <br>
        <code>{{ $daily_bonus_price }}</code>
    </h3>

    <h3>
        <small>TYPE</small>
        <br>
        <code>{{ $daily_bonus_type }}</code>
    </h3>

    <br>

    <p>
        <i class="fa fa-calendar-check-o"></i> {{ $created_at }}
    </p>
</div>
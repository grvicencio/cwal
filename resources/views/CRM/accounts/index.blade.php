@extends('CRM.layouts.dashboard')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Accounts</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">All Accounts</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="accounts-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>

                            <th>Email</th>
                            <th>First Name</th>
                            <th>Last Name</th>

                            <th>Status</th>
                            <th>Gender</th>
                            <th>Birthdate</th>
                            <th>Registered At</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/datatables-ellipsis.js") }}"></script>
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function(){
            $('#accounts-table').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "responsive": true,
                "ajax": {
                    "url": "/admin/accounts/datatable",
                    "error": function($err){
                        if($err.status == 401){
                            alert('Your session is expired! Please login again.');
                            window.location.href = '/admin/login';
                        }
                    }
                },
                columnDefs: [{
                    targets: 2,
                    render: $.fn.dataTable.render.ellipsis(16, true)
                }, {
                    targets: 5,
                    render: $.fn.dataTable.render.ellipsis(16, true)
                },  {
                    targets: 7,
                    orderable: false
                }],
                "columns": [
                    {
                        "data": "email",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = !row.email ? 'N/A' : row.email;
                            }
                            return data;
                        }
                    },

                    {
                        "data": "first_name",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = !row.first_name ? 'N/A' : row.first_name;
                            }
                            return data;
                        }
                    },
                    {
                        "data": "last_name",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = !row.last_name ? 'N/A' : row.last_name;
                            }
                            return data;
                        }
                    },


                    {
                        "data": "status_id",
                        "render": function (data, type, row, meta) {
                            console.log(data);
                            console.log(row);
                            if (type === 'display') {
                                data = !row.status_id ? 'N/A' : row.status.status_name;
                            }
                            return data;
                        }
                    },
                    {
                        "data": "gender",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = !row.gender ? 'N/A' : row.gender;
                            }

                            return data;
                        }
                    },
                    {
                        "data": "birth_date",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = !row.birth_date ? 'N/A' : row.birth_date;
                            }

                            return data;
                        }
                    },
                    {"data": "created_at"},
                    {
                        "data": null,
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = '<a href="/admin/accounts/' + row.id + '#personal"><i class="fa fa-user-o"></i> Details</a>';
                            }
                            return data;
                        }
                    }
                ]
            });
        });
    </script>
@endsection
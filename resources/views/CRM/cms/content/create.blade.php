@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>CMS</li>
        <li class="active">Contents</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <form name="form-add-content" id="form-add-content"
                          action="{{ route('contents.store') }}"
                          method="POST">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="control-label">Title</label>
                            <input type="text" class="form-control" id="title" name="title"
                                   placeholder="Title" value="{{ old('title') }}">

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="category_id" class="control-label">Category</label>
                            <select name="category_id" class="form-control" id="category_id">
                                @foreach($categories as $category)
                                    <option value="{{ $category->category_id }}"{{ $category->category_id == old('category_id') ? ' selected' : '' }}>{{ $category->category_name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('category_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('category_id') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
                            <label for="status_id" class="control-label">Status</label>
                            <select name="status_id" class="form-control" id="status_id">
                                @foreach($statuses as $status)
                                    <option value="{{ $status->status_id }}"{{ $status['status_id'] == old('status_id') ? ' selected' : '' }}>{{ $status->status_name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('status_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status_id') }}</strong>
                                </span>
                            @endif
                        </div>

                        {{--<div class="form-group{{ $errors->has('path') ? ' has-error' : '' }}">--}}
                        {{--<label for="path" class="control-label">Link Path</label>--}}
                        {{--<input type="text" class="form-control" id="path" name="path"--}}
                        {{--placeholder="Link Path" value="{{ old('path') }}">--}}

                        {{--@if ($errors->has('path'))--}}
                        {{--<span class="help-block">--}}
                        {{--<strong>{{ $errors->first('path') }}</strong>--}}
                        {{--</span>--}}
                        {{--@endif--}}
                        {{--</div>--}}

                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="control-label">Content</label>
                            <textarea name="content" id="content" class="form-control" cols="30"
                                      rows="30">{{ old('content') }}</textarea>

                            @if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="display_on_homepage"
                                           name="display_on_homepage" {{ old('display_on_homepage') ? 'checked' : '' }}>
                                    Display on Homepage?
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="default" name="default"> Set as Default?
                                </label>
                            </div>
                        </div>
                        <input type="hidden" name="dir" id="dir" value="{{ $dir }}">

                        <a href="{{ route('contents.index') }}" class="btn btn-default">
                            <i class="fa fa-long-arrow-left"></i> Back to Contents
                        </a>
                        @usercan('add', 'contents.store')
                        <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                                class="btn btn-primary">
                            Save
                        </button>
                        @endusercan
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- TinyMCE -->
    <script src="{{ asset("CRM/Capital7-1.0.0/plugins/tinymce/js/tinymce/tinymce.js") }}"></script>
@endsection

@section('script')
    <script>
        $(function () {
            tinymce.init({
                selector: 'textarea#content',
                theme: 'modern',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking',
                    'save table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic sizeselect fontselect fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
                relative_urls: false,
                remove_script_host: false,
                convert_urls: true,
                images_upload_handler: function (blobInfo, success, failure) {
                    var xhr, formData;

                    xhr = new XMLHttpRequest();
                    xhr.withCredentials = false;
                    xhr.open('POST', '{{ route('contents.upload') }}');

                    xhr.onload = function() {
                        var response;
                        response = JSON.parse(xhr.responseText);

                        if (response.status == '{{ config('response.type.success') }}') {
                            tinymce.activeEditor.insertContent('<img src="' + response.data + '"/>');
                            tinymce.activeEditor.windowManager.close();
                            // success(response.data);
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            var err = '';

                            $.each(response.errors.content_image, function (k, v) {
                                err += v + '\n';
                            });
                            failure(err);
                            return;
                        }
                    };
                    formData = new FormData();
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('dir', '{{ $dir }}');
                    formData.append('content_image', blobInfo.blob(), blobInfo.filename());
                    xhr.send(formData);
                }
            });

            $('#form-add-content').submit(function () {
                $(this).find(':submit').button('loading');
            });
        });
    </script>
@endsection

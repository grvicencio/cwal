@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>CMS</li>
        <li class="active">Contents</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="content-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Link</th>
                            <th>Displayed on Homepage</th>
                            <th>Default</th>
                            @if(Auth::guard('crm')->user()->canAccess('edit', 'contents.update') || Auth::guard('crm')->user()->canAccess('delete', 'contents.destroy'))
                                <th>Actions</th>
                            @endif
                        </tr>
                        </thead>
                    </table>
                </div>
                @usercan('add', 'contents.store')
                <div class="box-footer">
                    <a href="{{ route('contents.create') }}" class="btn btn-success btn-xs" id="btn-add-content" title="Add">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
                @endusercan
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <script>
        $(function () {
            $('#content-table').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "responsive": true,
                "ajax": "{{ route('contents.dataTable') }}",
                'createdRow': function (tr, data, dataIndex) {
                    $(tr).attr('data-content-id', data.content_id);
                },
                columnDefs: [
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'contents.update') || Auth::guard('crm')->user()->canAccess('delete', 'contents.destroy'))
                    {
                        targets: 6,
                        orderable: false
                    }
                    @endif
                ],
                "columns": [
                    {"data": "title"},
                    {
                        "data": "category_id",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = row.category.category_name;
                            }
                            return data;
                        }
                    },
                    {
                        "data": "status_id",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = row.status.status_name;
                            }
                            return data;
                        }
                    },
                    {{--{--}}
                        {{--"data": "path",--}}
                        {{--"render": function (data, type, row, meta) {--}}
                            {{--if (type === 'display') {--}}
                                {{--data = '<a href="{{ config('web.url') }}/content/' + row.content_id + '" target="_">' + row.path + '</a>';--}}
                            {{--}--}}
                            {{--return data;--}}
                        {{--}--}}
                    {{--},--}}
                    {
                        "data": null,
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = '<a href="{{ config('web.url') }}/content/' + row.content_id + '" target="_">Preview <i class="fa fa-external-link"></i></a>';
                            }
                            return data;
                        }
                    },
                    {
                        "data": "display_on_homepage",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = row.display_on_homepage ? 'Yes' : 'No';
                            }
                            return data;
                        }
                    },
                    {
                        "data": "default",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = row.default ? 'Yes' : 'No';
                            }
                            return data;
                        }
                    },
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'contents.update') || Auth::guard('crm')->user()->canAccess('delete', 'contents.destroy'))
                    {
                        "data": null,
                        "render": function (data, type, row, meta) {
                            data = '';

                            if (type === 'display') {
                                @if(Auth::guard('crm')->user()->canAccess('edit', 'contents.update'))
                                    data += '<a href="/admin/cms/contents/' + row.content_id + '/edit" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;';
                                @endif

                                @if(Auth::guard('crm')->user()->canAccess('delete', 'contents.destroy'))
                                    data += '<button type="button" onclick="deleteContent(this);" data-content-id="' + row.content_id + '" data-default="' + row.default + '" class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o"></i></button>';
                                @endif
                            }
                            return data;
                        }
                    }
                    @endif
                ]
            });
        });

        function deleteContent(src) {
            if ($(src).data('default')) {
                swal({
                    title: '{{ trans('swal.content.delete.default.title') }}',
                    html: '{{ trans('swal.content.delete.default.html') }}',
                    type: '{{ trans('swal.content.delete.default.type') }}'
                });
                return;
            }

            swal({
                title: '{{ trans('swal.content.delete.confirm.title') }}',
                html: '{{ trans('swal.content.delete.confirm.html') }}',
                type: '{{ trans('swal.content.delete.confirm.type') }}',
                showCancelButton: true,
                confirmButtonText: '{{ trans('swal.content.delete.confirm.confirmButtonText') }}'
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: "/admin/cms/contents/" + $(src).data('content-id'),
                        data: {'_token': '{{ csrf_token() }}'},
                        success: function (response) {
                            if (response.status == '{{ config('response.type.success') }}') {
                                location.reload();
                            } else if (response.status == '{{ config('response.type.fail') }}') {
                                if (response.data.affected) {
                                    var html = '{{ trans('swal.content.delete.fail.html') }}';

                                    $.each(response.data.affected, function (key, val) {
                                        var str = ' ' + val.count + ' ' + val.relation;
                                        html += str.replace(/_/g, ' ');
                                    });

                                    swal({
                                        title: '{{ trans('swal.content.delete.fail.title') }}',
                                        html: html,
                                        type: '{{ trans('swal.content.delete.fail.type') }}'
                                    });
                                }
                            }
                        }
                    });
                }
            });
            return false;
        }
    </script>
@endsection

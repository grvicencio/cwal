<div class="modal fade" id="modal-add-category" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="form-add-currency" id="form-add-category"
                  action="{{ route('categories.store') }}"
                  method="POST">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Category</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="add-currency-name-input" class="col-sm-3 control-label">Name</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="category_name"
                                   id="add-category-name-input"
                                   placeholder="Name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-parent-category-id-input" class="col-sm-3 control-label">Parent</label>

                        <div class="col-sm-8">
                            <select id="add-parent-category-id-input" class="form-control"
                                    name="parent_category_id">
                                <option value="0">None</option>

                                @foreach($categories as $category)
                                    <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-sort-input" class="col-sm-3 control-label">Position</label>

                        <div class="col-sm-8">
                            <select id="add-sort-input" class="form-control" name="sort">
                                <option value="0">1st</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-status-id-input" class="col-sm-3 control-label">Status</label>

                        <div class="col-sm-8">
                            <select id="add-status-id-input" class="form-control"
                                    name="status_id">
                                @foreach($statuses as $status)
                                    <option value="{{ $status->status_id }}">{{ $status->status_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'categories.store')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">
                        Save
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>

@section('script')
    @parent

    <script>
        var addCategory = function (form) {
            var btn = form.find(':submit').button('loading');
            var url = form.prop('action');

            $.post(url, form.serialize(), function (response) {
                if (response.status == '{{ config('response.type.success') }}') {
                    location.href = url;
                    return;
                } else if (response.status == '{{ config('response.type.error') }}') {
                    assocErr(response.errors, form);
                }
            }).done(function () {
                btn.button('reset');
            });
        };

        $(function () {
            $("#modal-add-category").on("hidden.bs.modal", function () {
                var form = $('#form-add-category');
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-add-category').submit(function (e) {
                e.preventDefault();
                var form = $(this);

                if (form.find('#add-sort-input').data('positions-count') && form.find('#add-sort-input').val() != form.find('#add-sort-input').data('old-sort')) {
                    swal({
                        title: '{{ trans('swal.category.create.confirm.title') }}',
                        html: '{{ trans('swal.category.create.confirm.html') }}',
                        type: '{{ trans('swal.category.create.confirm.type') }}',
                        showCancelButton: true,
                        confirmButtonText: '{{ trans('swal.category.create.confirm.confirmButtonText') }}'
                    }).then(function (result) {
                        if (result.value) {
                            addCategory(form);
                        }
                    });
                } else {
                    addCategory(form);
                }
            });

            $('#add-parent-category-id-input').on('change', function () {
                $('#add-sort-input').empty();

                $.get('categories/parent/' + $(this).val() + '/positions', function (response) {
                    if (response.status == '{{ config('response.type.success') }}') {
                        var count = 0;
                        $('#add-sort-input').attr('data-positions-count', response.data.positions.length);

                        if (!response.data.positions.length) {
                            $('#add-sort-input').append('<option value="0">' + getGetOrdinal(1) + '</option>');
                        } else {
                            $.each(response.data.positions, function (i, v) {
                                count = v + 1;
                                $('#add-sort-input').append('<option value="' + v + '">' + getGetOrdinal(v + 1) + '</option>');
                            });
                            $('#add-sort-input').attr('data-old-sort', count);
                            $('#add-sort-input').append('<option value="' + count + '" selected>' + getGetOrdinal(count + 1) + '</option>');
                        }
                    }
                });
            });
            $('#add-parent-category-id-input').val(0).trigger('change');
        });
    </script>
@endsection
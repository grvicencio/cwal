<div class="modal fade" id="modal-edit-category" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="form-edit-category" id="form-edit-category"
                  data-temp-action="/cms/categories"
                  method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Category</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="edit-currency-name-input" class="col-sm-3 control-label">Name</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="category_name"
                                   id="edit-category-name-input"
                                   placeholder="Name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="edit-parent-category-id-input" class="col-sm-3 control-label">Parent</label>

                        <div class="col-sm-8">
                            <select id="edit-parent-category-id-input" class="form-control"
                                    name="parent_category_id">
                                <option value="0">None</option>

                                @foreach($categories as $category)
                                    <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="edit-sort-input" class="col-sm-3 control-label">Position</label>

                        <div class="col-sm-8">
                            <select id="edit-sort-input" class="form-control" name="sort">
                                <option value="0">1st</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="edit-status-id-input" class="col-sm-3 control-label">Status</label>

                        <div class="col-sm-8">
                            <select id="edit-status-id-input" class="form-control"
                                    name="status_id">
                                @foreach($statuses as $status)
                                    <option value="{{ $status->status_id }}">{{ $status->status_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('edit', 'categories.update')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">
                        Save changes
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>

@section('script')
    @parent

    <script>
        $(function () {
            $("#modal-edit-category").on("hidden.bs.modal", function () {
                var form = $('#form-edit-category');
                form.removeAttr('data-category-id');
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-edit-category').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                var url = form.data('temp-action') + '/' + form.data('category-id');

                $.post(url, form.serialize(), function (response) {
                    if (response.status == '{{ config('response.type.success') }}') {
                        location.href = form.data('temp-action');
                        return;
                    } else if (response.status == '{{ config('response.type.error') }}') {
                        assocErr(response.errors, form);
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $('#edit-parent-category-id-input').on('change', function () {
                setEditSortOptions($(this).val());
            });
            $('#edit-parent-category-id-input').val(0).trigger('change');
        });
    </script>
@endsection
@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>CMS</li>
        <li class="active">Category</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="category-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Parent</th>
                            <th>Status</th>
                            @if(Auth::guard('crm')->user()->canAccess('edit', 'categories.update') || Auth::guard('crm')->user()->canAccess('delete', 'categories.destroy'))
                            <th>Actions</th>
                            @endif
                        </tr>
                        </thead>
                    </table>
                </div>
                @usercan('add', 'categories.store')
                <div class="box-footer">
                    <button type="button" class="btn btn-success btn-xs" id="btn-add-category" onclick="setFormAddCategory(this);" title="Add">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
                @endusercan
            </div>
        </div>
    </div>

    @include('CRM.cms.category.forms.add')
    @include('CRM.cms.category.forms.edit')
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('style')
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function () {
            $('#category-table').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "responsive": true,
                "ajax": "{{ route('categories.dataTable') }}",
                'createdRow': function (tr, data, dataIndex) {
                    $(tr).attr('data-category-id', data.category_id);
                },
                columnDefs: [
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'categories.update') || Auth::guard('crm')->user()->canAccess('delete', 'categories.destroy'))
                    {
                        targets: 3,
                        orderable: false
                    }
                    @endif
                ],
                "columns": [
                    {"data": "category_name"},
                    {
                        "data": "parent_category_id",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = row.parent == null ? 'None' : row.parent.category_name;
                            }
                            return data;
                        }
                    },
                    {
                        "data": "status_id",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = row.status.status_name;
                            }
                            return data;
                        }
                    },
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'categories.update') || Auth::guard('crm')->user()->canAccess('delete', 'categories.destroy'))
                    {
                        "data": null,
                        "render": function (data, type, row, meta) {
                            data = '';

                            if (type === 'display') {
                                @if(Auth::guard('crm')->user()->canAccess('edit', 'categories.update'))
                                    data += '<button type="button" onclick="setFormEditCategory(this);" data-sort="' + row.sort + '" data-status-id="' + row.status_id + '" data-category-name="' + row.category_name + '" data-category-id="' + row.category_id + '" data-parent-category-id="' + row.parent_category_id + '" class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-edit"></i></button>&nbsp;';
                                @endif

                                @if(Auth::guard('crm')->user()->canAccess('delete', 'categories.destroy'))
                                    data += '<button type="button" onclick="deleteCategory(' + row.category_id + ');" class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o"></i></button>';
                                @endif
                            }
                            return data;
                        }
                    }
                    @endif
                ]
            });
        });

        var setFormEditCategory = function (src) {
            var form = $('#form-edit-category');
            form.attr('data-category-id', $(src).data('category-id'));
            form.find('input[name=category_name]').val($(src).data('category-name'));
            form.find('select[name=parent_category_id]').val($(src).data('parent-category-id'));
            form.find('select[name=parent_category_id]').find('option').show();
            form.find('select[name=parent_category_id]').find('option[value=' + $(src).data('category-id') + ']').hide();
            form.find('select[name=status_id]').val($(src).data('status-id')).trigger('change');
            setEditSortOptions($(src).data('parent-category-id')).done(function () {
                form.find('select[name=sort]').val($(src).data('sort')).trigger('change');
                $('#modal-edit-category').modal('show');
            });
        };

        var setFormAddCategory = function (src) {
            $('#modal-add-category').modal('show');
        };

        var getGetOrdinal = function(n) {
            var s=["th","st","nd","rd"],
                v=n%100;
            return n+(s[(v-20)%10]||s[v]||s[0]);
        };

        var setEditSortOptions = function (val) {
            $('#edit-sort-input').empty();

            return $.get('categories/parent/' + val + '/positions', function (response) {
                if (response.status == '{{ config('response.type.success') }}') {

                    if (!response.data.positions.length) {
                        $('#edit-sort-input').append('<option value="0">' + getGetOrdinal(1) + '</option>');
                    } else {
                        $.each(response.data.positions, function (i, v) {
                            count = v + 1;
                            $('#edit-sort-input').append('<option value="' + v + '">' + getGetOrdinal(v + 1) + '</option>');
                        });
                    }
                }
            });
        };

        function deleteCategory(id) {
            swal({
                title: '{{ trans('swal.category.delete.confirm.title') }}',
                html: '{{ trans('swal.category.delete.confirm.html') }}',
                type: '{{ trans('swal.category.delete.confirm.type') }}',
                showCancelButton: true,
                confirmButtonText: '{{ trans('swal.category.delete.confirm.confirmButtonText') }}'
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: "/cms/categories/" + id,
                        data: {'_token': '{{ csrf_token() }}'},
                        success: function (response) {
                            if (response.status == '{{ config('response.type.success') }}') {
                                location.reload();
                            } else if (response.status == '{{ config('response.type.fail') }}') {
                                if (response.data.affected) {
                                    var html = '{{ trans('swal.category.delete.fail.html') }}';

                                    $.each(response.data.affected, function (key, val) {
                                        var str = ' ' + val.count + ' ' + val.relation;
                                        html += str.replace(/_/g, ' ');
                                    });

                                    swal({
                                        title: '{{ trans('swal.category.delete.fail.title') }}',
                                        html: html,
                                        type: '{{ trans('swal.category.delete.fail.type') }}'
                                    });
                                }
                            }
                        }
                    });
                }
            });
            return false;
        }
    </script>
@endsection

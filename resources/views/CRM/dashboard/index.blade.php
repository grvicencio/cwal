@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Home</li>
        <li class="active">Dashboard</li>
    </ol>
@endsection

@section('content')
    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">TOTAL ACCOUNTS</span>
                    <span class="info-box-number">{{ number_format(Accounts::withoutGuest()->count()) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-android-checkmark-circle"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">REGISTERED TODAY</span>
                    <span class="info-box-number">{{ number_format(Accounts::getRegisteredToday()->count()) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 temp-hide">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">SALES</span>
                    <span class="info-box-number">$ {{ number_format(Transaction::getAllSales(), 2) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->


    <div class="row">
        @usercan('view', 'crm.dashboard.deposit.tickets')
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title form-inline">
                        Pending Deposits
                    </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="deposit-tickets-container">
                        <div class="text-center">
                            {!! trans('loading.please_wait') !!}
                        </div>
                    </div>
                </div>
                <!-- ./box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        @endusercan

        @usercan('view', 'crm.dashboard.withdrawal.tickets')
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title form-inline">
                        Pending Withdrawals
                    </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="withdrawal-tickets-container">
                        <div class="text-center">
                            {!! trans('loading.please_wait') !!}
                        </div>
                    </div>
                </div>
                <!-- ./box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        @endusercan
    </div>
    <!-- /.row -->

    @usercan('view', 'charts.user_charge_range')
    <div class="row">
        <div class="col-md-12 temp-hide">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title form-inline">
                        <span>User Charge Recap &nbsp;</span>
                        <input type="text" id="users-charge-range" class="form-control text-center input-sm"
                               title="Pick date range">
                    </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="chart">
                        <!-- Sales Chart Canvas -->
                        <canvas id="users-charge-chart" height="360" class="fade in"></canvas>
                    </div>
                    <!-- /.chart-responsive -->
                </div>
                <!-- ./box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    @endusercan

    @usercan('view', 'charts.registered_users_range')
    <div class="row">
        <div class="col-md-12 temp-hide">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title form-inline">
                        <span>Registered Users Recap &nbsp;</span>
                        <input type="text" id="users-range" class="form-control text-center input-sm"
                               title="Pick date range">
                    </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <div class="chart">
                        <!-- Sales Chart Canvas -->
                        <canvas id="users-chart" height="360" class="fade in"></canvas>
                    </div>
                    <!-- /.chart-responsive -->
                </div>
                <!-- ./box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="description-block border-right">
                                <h5 class="description-header">
                                    {{ number_format(Accounts::getRegisteredFromDate(date('Y-m-d'))->count()) }}
                                </h5>
                                <span class="description-text">REGISTERED TODAY</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="description-block">
                                <h5 class="description-header">
                                    {{ number_format(Accounts::getRegisteredFromWeek(date('l'))->count()) }}
                                </h5>
                                <span class="description-text">REGISTERED THIS WEEK</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="description-block border-right">
                                <h5 class="description-header">
                                    {{ number_format(Accounts::getRegisteredFromMonth(date('Y-m'))->count()) }}
                                </h5>
                                <span class="description-text">REGISTERED THIS MONTH</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="description-block border-right">
                                <h5 class="description-header">
                                    {{ number_format(Accounts::getRegisteredFromYear(date('Y'))->count()) }}
                                </h5>
                                <span class="description-text">REGISTERED THIS YEAR</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    @endusercan

    @usercan('view', 'charts.sales_range')
    <div class="row">
        <div class="col-md-12 temp-hide">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title form-inline">
                        <span>Daily Sales Recap &nbsp;</span>
                        <input type="text" id="sales-range" class="form-control text-center input-sm"
                               title="Pick date range">
                    </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="chart">
                        <!-- Sales Chart Canvas -->
                        <canvas id="sales-chart" height="360" class="fade in"></canvas>
                    </div>
                    <!-- /.chart-responsive -->
                </div>
                <!-- ./box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="description-block border-right">
                                <h5 class="description-header">
                                    $ {{ number_format(Transaction::getSalesFromDate(date('Y-m-d')), 2) }}</h5>
                                <span class="description-text">SALES TODAY</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="description-block">
                                <h5 class="description-header">
                                    $ {{ number_format(Transaction::getSalesFromWeek(date('l')), 2) }}</h5>
                                <span class="description-text">SALES THIS WEEK</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="description-block border-right">
                                <h5 class="description-header">
                                    $ {{ number_format(Transaction::getSalesFromMonth(date('Y-m')), 2) }}</h5>
                                <span class="description-text">SALES THIS MONTH</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="description-block border-right">
                                <h5 class="description-header">
                                    $ {{ number_format(Transaction::getSalesFromYear(date('Y')), 2) }}</h5>
                                <span class="description-text">SALES THIS YEAR</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    @endusercan
@endsection

@section('styles')
    <!-- DateRagePicker -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/bootstrap-daterangepicker/daterangepicker.css") }}">
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('style')
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }

        .chart {
            height: 360px;
        }

        .ellipted {
            max-width: 100px !important;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        .btn-link {
            padding: 0;
        }
    </style>
@endsection

@section('scripts')
    <!-- ChartJS -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/chart.js/dist/Chart.min.js") }}"></script>
    <script src="{{ asset("CRM/Capital7-1.0.0/js/utils.js") }}"></script>
    <!-- MomentJS -->
    <script src="{{ asset("CRM/Capital7-1.0.0/plugins/moment/moment.min.js") }}"></script>
    <!-- DateRagePicker -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/bootstrap-daterangepicker/daterangepicker.js") }}"></script>
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <script>
        $(function () {
            @usercan('view', 'charts.user_charge_range')
            getUserChargeDataRange(moment().startOf('month').format('YYYY-MM-DD'), '{{ date('Y-m-d') }}');

            $('#users-charge-range').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                startDate: moment().startOf('month').format('YYYY-MM-DD'),
                endDate: '{{ date('Y-m-d') }}',
                'autoApply': true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }).on('apply.daterangepicker', function (ev, picker) {
                var start = picker.startDate.format('YYYY-MM-DD');
                var end = picker.endDate.format('YYYY-MM-DD');
                getUserChargeDataRange(start, end);
            });
            @endusercan

            @usercan('view', 'charts.registered_users_range')
            getRegisteredUsersDataRange(moment().startOf('month').format('YYYY-MM-DD'), '{{ date('Y-m-d') }}');

            $('#users-range').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                startDate: moment().startOf('month').format('YYYY-MM-DD'),
                endDate: '{{ date('Y-m-d') }}',
                'autoApply': true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }).on('apply.daterangepicker', function (ev, picker) {
                var start = picker.startDate.format('YYYY-MM-DD');
                var end = picker.endDate.format('YYYY-MM-DD');
                getRegisteredUsersDataRange(start, end);
            });
            @endusercan

            @usercan('view', 'charts.sales_range')
            getSalesDataRange(moment().startOf('month').format('YYYY-MM-DD'), '{{ date('Y-m-d') }}');

            $('#sales-range').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                startDate: moment().startOf('month').format('YYYY-MM-DD'),
                endDate: '{{ date('Y-m-d') }}',
                'autoApply': true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }).on('apply.daterangepicker', function (ev, picker) {
                var start = picker.startDate.format('YYYY-MM-DD');
                var end = picker.endDate.format('YYYY-MM-DD');
                getSalesDataRange(start, end);
            });
            @endusercan
        });

        @usercan('view', 'charts.registered_users_range')
        var getRegisteredUsersDataRange = function (start, end) {
            var days = Math.abs(moment(start).diff(moment(end), 'days')) + 1;

            if (days > 90) {
                swal({
                    title: '{{ trans('swal.charts.max_range.title') }}',
                    html: '{{ trans('swal.charts.max_range.html') }}',
                    type: '{{ trans('swal.charts.max_range.type') }}'
                });
                return false;
            }
            var config = null;
            var div = $("canvas#users-chart").parent('.chart');

            $.getJSON('/admin/charts/registered_users_range/' + start + '/' + end, function (response) {
                config = {
                    type: 'bar',
                    data: {
                        labels: response.data.registered_data.dates,
                        datasets: [{
                            label: "Registered Users",
                            backgroundColor: Samples.utils.transparentize(window.chartColors.red),
                            borderColor: window.chartColors.red,
                            data: response.data.registered_data.count,
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                            display: true,
                            text: moment(start).format("MMMM DD, YYYY") + ' - ' + moment(end).format("MMMM DD, YYYY")
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Date'
                                },
                                ticks: {
                                    autoSkip: false
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: '# of Users'
                                }
                            }]
                        }
                    }
                };
            }).done(function (response) {
                if (response.status == '{{ config('response.type.success') }}') {
                    $("canvas#users-chart").remove();
                    div.append('<canvas id="users-chart" height="360" class="fade in"></canvas>');
                    var ctx = document.getElementById('users-chart').getContext('2d');
                    window.myBar = new Chart(ctx, config);
                }
            });
        };
        @endusercan

        @usercan('view', 'charts.sales_range')
        var getSalesDataRange = function (start, end) {
            var days = Math.abs(moment(start).diff(moment(end), 'days')) + 1;

            if (days > 90) {
                swal({
                    title: '{{ trans('swal.charts.max_range.title') }}',
                    html: '{{ trans('swal.charts.max_range.html') }}',
                    type: '{{ trans('swal.charts.max_range.type') }}'
                });
                return false;
            }
            var config = null;
            var div = $("canvas#sales-chart").parent('.chart');

            $.getJSON('/admin/charts/sales_range/' + start + '/' + end, function (response) {
                if (response.status == '{{ config('response.type.success') }}') {
                    config = {
                        type: 'line',
                        data: {
                            labels: response.data.sales_data.dates,
                            datasets: [{
                                label: "Daily Sales",
                                backgroundColor: Samples.utils.transparentize(window.chartColors.blue),
                                borderColor: window.chartColors.blue,
                                data: response.data.sales_data.sales,
                                fill: true
                            }]
                        },
                        options: {
                            responsive: true,
                            title: {
                                display: true,
                                text: moment(start).format("MMMM DD, YYYY") + ' - ' + moment(end).format("MMMM DD, YYYY")
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Date'
                                    },
                                    ticks: {
                                        autoSkip: false
                                    }
                                }],
                                yAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Value ($)'
                                    }
                                }]
                            }
                        }
                    };
                }
            }).done(function () {
                $("canvas#sales-chart").remove();
                div.append('<canvas id="sales-chart" height="360" class="fade in"></canvas>');
                var ctx = document.getElementById('sales-chart').getContext('2d');
                window.myLine = new Chart(ctx, config);
            });
        };
        @endusercan

        @usercan('view', 'charts.user_charge_range')
        var getUserChargeDataRange = function (start, end) {
            var days = Math.abs(moment(start).diff(moment(end), 'days')) + 1;

            if (days > 90) {
                swal({
                    title: '{{ trans('swal.charts.max_range.title') }}',
                    html: '{{ trans('swal.charts.max_range.html') }}',
                    type: '{{ trans('swal.charts.max_range.type') }}'
                });
                return false;
            }
            var config = null;
            var div = $("canvas#users-charge-chart").parent('.chart');

            $.getJSON('/admin/charts/user_charge_range/' + start + '/' + end, function (response) {
                if (response.status == '{{ config('response.type.success') }}') {
                    config = {
                        type: 'bar',
                        data: {
                            labels: response.data.user_charge_data.dates,
                            datasets: [{
                                label: "Charge",
                                backgroundColor: Samples.utils.transparentize(window.chartColors.yellow),
                                borderColor: window.chartColors.yellow,
                                data: response.data.user_charge_data.amount,
                                borderWidth: 1
                            }]
                        },
                        options: {
                            responsive: true,
                            title: {
                                display: true,
                                text: moment(start).format("MMMM DD, YYYY") + ' - ' + moment(end).format("MMMM DD, YYYY")
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Date'
                                    },
                                    ticks: {
                                        autoSkip: false
                                    }
                                }],
                                yAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Value ($)'
                                    }
                                }]
                            }
                        }
                    };
                }
            }).done(function () {
                $("canvas#users-charge-chart").remove();
                div.append('<canvas id="users-charge-chart" height="360" class="fade in"></canvas>');
                var ctx = document.getElementById('users-charge-chart').getContext('2d');
                window.myLine = new Chart(ctx, config);
            });
        };
        @endusercan

        @usercan('view', 'crm.dashboard.deposit.tickets')
        $.get("{{ route('crm.dashboard.deposit.tickets') }}",function (view) {
            $("#deposit-tickets-container").html(view);
        }).done(function () {
            $('#deposit-tickets-table').DataTable({
                "ordering": true,
                "searching": true,
                "responsive": true,
                columnDefs: [{
                    targets: 3,
                    orderable: false
                }]
            });
        });
        @endusercan

        @usercan('view', 'crm.dashboard.withdrawal.tickets')
        $.get("{{ route('crm.dashboard.withdrawal.tickets') }}", function (view) {
            $("#withdrawal-tickets-container").html(view);
        }).done(function () {
            $('#withdrawal-tickets-table').DataTable({
                "ordering": true,
                "searching": true,
                "responsive": true,
                columnDefs: [{
                    targets: 3,
                    orderable: false
                }]
            });
        });
        @endusercan
    </script>
@endsection



@if($pending_withdrawals->count())
    <table id="withdrawal-tickets-table" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Notes</th>
            <th>Date</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pending_withdrawals as $pending_withdrawal)
            <tr data-withdrawal-id="{{ $pending_withdrawal->wallet_withdrawal_id }}">
                <td data-name="{{ $pending_withdrawal->account->getDisplayName() }}">
                    {{ $pending_withdrawal->account->getDisplayName() }}
                </td>
                <td data-notes="{{ $pending_withdrawal->notes }}" title="{{ $pending_withdrawal->notes }}" class="ellipted">
                    {{ $pending_withdrawal->notes }}
                </td>
                <td data-date="{{ $pending_withdrawal->created_at }}">{{ $pending_withdrawal->created_at }}</td>
                <td>
                    <a href="{{ route('wallet.withdrawal.index') }}?wallet_withdrawal_id={{ $pending_withdrawal->wallet_withdrawal_id }}" class="btn btn-link">View</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <p class="text-muted text-center">{{ trans('validation.custom.pending_withdrawal.empty') }}</p>
@endif
@extends('CRM.layouts.dashboard')
@section('styles')
    <link href="{{ asset ("bower_components/AdminLTE/plugins/select2/select2.min.css") }}" rel="stylesheet"
          type="text/css"/>
    <script src="{{ asset ("bower_components/AdminLTE/plugins/select2/select2.min.js") }}"></script>

@endsection

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $data['sub_title'] }}</h3>
                    <div class="box-tools pull-right">

                    </div>
                </div>
                <div class="box-body">
                    <table id="charge_table" class="table hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Name</td>
                            <td>Description</td>
                            <td>Created At</td>
                            <td>Updated At</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($PaymentMethods  as $pm)
                            <div>
                                <td>{{ $pm ->payment_method_id }}</td>
                                <td>{{ $pm ->name }}</td>
                                <td>{{ $pm ->description }}</td>
                                <td>{{ $pm ->created_at}}</td>
                                <td>{{ $pm ->updated_at}}</td>

                                {{-- TODO: refactor the editing method --}}
                                <td>
                                    <div class="btn-group">
                                        <button type="button" data-toggle="modal" data-backdrop="static"
                                                data-keyboard="false" class="btn_edit_pm btn btn-success"
                                                data-target="#modal-add-edit-pm"
                                                data-pm_id="{{ $pm ->payment_method_id }}"
                                                data-pm_name="{{ $pm->name }}"
                                                data-pm_description="{{ $pm->description }}"
                                                data-pm_created_at="{{ $pm->created_at}}"
                                                data-pm_updated_at="{{ $pm->updated_at}}" class="btn-xs btn-warning"><i
                                                    class="fa fa-edit"></i> Edit
                                        </button>
                                        <button type="button" class="btn-danger delete_button btn btn-success "><i
                                                    class="fa fa-trash"></i> Delete
                                        </button>
                                    </div>
                                    {{--<form method="post" action="/charges/regular/delete/{{ $pm ->payment_method_id }}"> {{ csrf_field() }} <button type="submit" class="btn-danger delete_button btn btn-success btn-block" ><i class="fa fa-trash"></i></button></form>--}}
                                </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="button" id="add_pm" class="btn btn-success" data-toggle="modal" data-backdrop="static"
                            data-keyboard="false" data-target="#modal-add-edit-pm"><i class="fa fa-plus"></i></button>
                </div><!-- /.box-footer-->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    {{-- TODO: AVOID COPYING SIMILAR ELEMENTS, USE THE ADD MODAL AND CHANGE VIA JAVASCRIPT --}}
    <div class="modal fade" id="modal-add-edit-pm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">[ <span id="modal-title"></span>] </h4>
                </div>
                <div class="modal-body">


                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" name="form_add_edit_pm" id="form_add_edit_pm" action="/paymentmethods"
                          method="post">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="modal-edit-name" class="col-sm-2 control-label">Name</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="pm_name" name="name"
                                           placeholder="Charge Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="modal-edit-description" class="col-sm-2 control-label">Description</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="pm_description" name="description"
                                           placeholder="Description">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="modal-edit-price" class="col-sm-2 control-label">Created At</label>

                                <div class="col-sm-5">
                                    <input type="date" class="form-control" id="pm_created_at" name="created_at"
                                           placeholder="0.00">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="modal-edit-value" class="col-sm-2 control-label">Updated At</label>

                                <div class="col-sm-5">
                                    <input type="date" class="form-control" id="pm_updated_at" name="updated_at"
                                           placeholder="0">
                                </div>
                            </div>


                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="form_add_edit_pm.submit()">Save changes
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection


@section('scripts')
    <script src="{{ asset ("bower_components/AdminLTE/plugins/iCheck/icheck.min.js") }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#charge_table').DataTable({
                "paging": true,
                "ordering": true,
                "search": true,
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false
                    }
                ]
            });

            $("#status-input").select2();
            var select_box = $("#modal-edit-status").select2();

            $(".delete_button").click(function () {
                if (confirm("Are you sure you want to delete this?")) {
                    return true;
                } else {
                    return false;
                }
            });


            $('#modal-add-edit-pm').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                $('#modal-title').html('Add New Payment Method');
                var pm_id = button.data('pm_id');
                if (pm_id) {
                    $('#modal-title').html('Edit Payment Method');
                    $("#form_add_edit_pm").attr("action", "/paymentmethods/update/" + pm_id);
                    var pm_name = button.data('pm_name');
                    var pm_description = button.data('pm_description');
                    var pm_created_at_value = new Date(button.data('pm_created_at'));
                    var pm_updated_at_value = new Date(button.data('pm_updated_at'));


                    $('#pm_name').val(pm_name);
                    $('#pm_description').val(pm_description);
                    $('#pm_created_at').val(pm_created_at_value.toISOString().substr(0, 10));
                    $('#pm_updated_at').val(pm_updated_at_value.toISOString().substr(0, 10));

                } else {
                    $('#modal-title').html('Add New Payment Method');
                    $("#form_add_edit_pm").attr("action", "/paymentmethods/add");
                    $("#form_add_edit_pm input").val("");
                }

            });


        });
    </script>
@endsection

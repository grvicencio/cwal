<div class="modal fade" id="modal-add-module" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Form start -->
            <form class="form-horizontal" name="formmodule" id="formmodule" action="{{ route('roles.add') }}"
                  method="post">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Module</h4>
                </div>
                <div class="modal-body">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="module-input" class="col-sm-2 control-label">Module</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="module-input" name="module_name"
                                       placeholder="Module">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="controller-input" class="col-sm-2 control-label">Controller</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="controller-input" name="controller_name"
                                       placeholder="Controller">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description-input" class="col-sm-2 control-label">Description</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="description-input" name="module_description"
                                       placeholder="Module Description">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent-input" class="col-sm-2 control-label">Parent</label>

                            <div class="col-sm-10">
                                <select name="parent_id" id="parent-input" class="form-control">
                                    <option value="0">None</option>
                                    @foreach($modules as $module)
                                        <option value="{{ $module->module_id }}">{{ $module->module_name }}/{{ $module->controller_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'roles.add')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary">Save</button>
                    @endusercan
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Security</li>
        <li class="active">Users</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <!-- Box -->
            <div class="box">
                <div class="box-body">
                    <table id="users_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Last Login</th>
                            @if(Auth::guard('crm')->user()->canAccess('edit', 'assignuser.update') || Auth::guard('crm')->user()->canAccess('edit', 'assignuser.passwordReset') || Auth::guard('crm')->user()->canAccess('delete', 'assignuser.delete'))
                            <th>Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            @if(@$user->user_role->role->role_name)
                                <tr>
                                    <td>{{ $user->firstname }}</td>
                                    <td>{{ $user->lastname }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td data-role-id="{{ $user->user_role->role->role_id }}">{{ $user->user_role->role->role_name }}</td>
                                    <td data-status-id="{{ $user->status_id }}">{{ $user->status_name }}</td>
                                    <td>{{ $user->updated_at->diffForHumans() }}</td>
                                    @if(Auth::guard('crm')->user()->canAccess('edit', 'assignuser.update') || Auth::guard('crm')->user()->canAccess('edit', 'assignuser.passwordReset') || Auth::guard('crm')->user()->canAccess('delete', 'assignuser.delete'))
                                    <td>
                                        @usercan('edit', 'assignuser.update')
                                        <button type="button" class="btn-xs btn-warning btn-edit-user"
                                                data-id="{{ $user->id }}" data-img="{{ asset($user->image) }}"><i class="fa fa-edit" title="Edit"></i></button>
                                        @endusercan

                                        @usercan('edit', 'assignuser.passwordReset')
                                        <button type="button" class="btn-xs btn-info btn-user-password-reset"
                                                data-id="{{ $user->id }}"><i class="fa fa-key" title="Reset Password"></i></button>
                                        @endusercan

                                        {{--@usercan('delete', 'assignuser.delete')--}}
                                        {{--<button type="submit" class="btn-xs btn-danger delete_button" title="Delete" onclick="deleteUser({{ $user->id }})"><i--}}
                                                    {{--class="fa fa-trash"></i></button>--}}
                                        {{--@endusercan--}}
                                    </td>
                                    @endif
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                @usercan('add', 'assignuser.add')
                <div class="box-footer">
                    <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modal-add-users" title="Add"><i
                                class="fa fa-plus"></i></button>
                </div><!-- /.box-footer-->
                @endusercan
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    @include('CRM.security.user.forms.add')
    @include('CRM.security.user.forms.edit')
    @include('CRM.security.user.forms.password_reset')
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function () {
            $('#users_table').DataTable({
                "paging": false,
                "ordering": true,
                "searching": true,
                "responsive": true,
                columnDefs: [
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'assignuser.update') || Auth::guard('crm')->user()->canAccess('edit', 'assignuser.passwordReset') || Auth::guard('crm')->user()->canAccess('delete', 'assignuser.delete'))
                    {
                        targets: 6,
                        orderable: false
                    }
                    @endif
                ]
            });

            $("#modal-add-users").on("hidden.bs.modal", function () {
                var form = $('#formaddusers');
                form.trigger('reset');
                clearErr(form);
            });

            $('#formaddusers').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: form.serialize(),
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                            return;
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $("#modal-edit-users").on("hidden.bs.modal", function () {
                var form = $('#formeditusers');
                form.trigger('reset');
                clearErr(form);
            });

            $('#formeditusers').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: new FormData(form[0]),
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            swal("User Update", "Record Successfully Updated!", "success").then($res => {
                                location.reload();
                            });
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $('#users_table').on('click', 'button.btn-edit-user', function () {
                var modal = $('#modal-edit-users');
                var form = modal.find('form');
                form.attr('action', form.data('temp-action') + "/" + $(this).data('id'));

                form.find('input[name=firstname]').val($(this).closest('tr').find('td:eq(0)').text());
                form.find('input[name=lastname]').val($(this).closest('tr').find('td:eq(1)').text());
                form.find('select[name=status_id]').val($(this).closest('tr').find('td:eq(4)').data('status-id'));
                form.find('select[name=role_id]').val($(this).closest('tr').find('td:eq(3)').data('role-id'));
                form.find('input[name=email]').val($(this).closest('tr').find('td:eq(2)').text());
                if($(this).data('img'))
                    form.find('img#modal-img').prop('src', $(this).data('img'));

                modal.modal('show');
            });

            $('#users_table').on('click', 'button.btn-user-password-reset', function () {
                var modal = $('#modal-user-password-reset');
                var form = modal.find('form');
                form.attr('action', form.data('temp-action') + "/" + $(this).data('id') + form.data('append-action'));
                modal.modal('show');
            });

            $("#modal-user-password-reset").on("hidden.bs.modal", function () {
                var form = $('#formuserpasswordreset');
                form.trigger('reset');
                clearErr(form);
            });

            $('#formuserpasswordreset').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: form.serialize(),
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                            return;
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });

        {{--function deleteUser(id) {--}}
            {{--swal({--}}
                {{--title: '{{ trans('swal.user.delete.confirm.title') }}',--}}
                {{--html: '{{ trans('swal.user.delete.confirm.html') }}',--}}
                {{--type: '{{ trans('swal.user.delete.confirm.type') }}',--}}
                {{--showCancelButton: true,--}}
                {{--confirmButtonText: '{{ trans('swal.user.delete.confirm.confirmButtonText') }}'--}}
            {{--}).then(function (result) {--}}
                {{--if (result.value) {--}}
                    {{--$.ajax({--}}
                        {{--type: "DELETE",--}}
                        {{--url: "/admin/security/assignuser/delete/" + id,--}}
                        {{--data: {'_token': '{{ csrf_token() }}'},--}}
                        {{--success: function (response) {--}}
                            {{--if (response.status == '{{ config('response.type.success') }}') {--}}
                                {{--location.reload();--}}
                            {{--} else if (response.status == '{{ config('response.type.fail') }}') {--}}
                                {{--if (response.data.affected) {--}}
                                    {{--var html = '{{ trans('swal.user.delete.fail.html') }}';--}}

                                    {{--$.each(response.data.affected, function (key, val) {--}}
                                        {{--html += ' ' + val.count + ' ' + val.relation;--}}
                                    {{--});--}}

                                    {{--swal({--}}
                                        {{--title: '{{ trans('swal.user.delete.fail.title') }}',--}}
                                        {{--html: html,--}}
                                        {{--type: '{{ trans('swal.user.delete.fail.type') }}'--}}
                                    {{--});--}}
                                {{--}--}}
                            {{--}--}}
                        {{--}--}}
                    {{--});--}}
                {{--}--}}
            {{--});--}}
            {{--return false;--}}
        {{--}--}}
    </script>
@endsection


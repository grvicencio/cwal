<div class="modal fade" id="modal-add-users" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="formaddusers" id="formaddusers" action="{{ route('assignuser.add') }}"
                  method="POST">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Users</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="add-fname-input" class="col-sm-3 control-label">First Name</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="add-fname-input" name="firstname"
                                       placeholder="First name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-lname-input" class="col-sm-3 control-label">Last Name</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="add-lname-input" name="lastname"
                                       placeholder="Last name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-status-input" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="status_id" id="add-status-input">
                                    @foreach($statuses as $status)
                                        <option value="{{ $status->status_id }}">{{ $status->status_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-role-input" class="col-sm-3 control-label">Role</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="role_id" id="add-role-input">
                                    @foreach($roles as $role)
                                        <option value="{{ $role['role_id'] }}">{{ $role['role_name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-email-input" class="col-sm-3 control-label">Email</label>

                            <div class="col-sm-8">
                                <input type="email" class="form-control" id="add-email-input" name="email"
                                       placeholder="Email Address">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-pass-input" class="col-sm-3 control-label">Password</label>

                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="add-pass-input" name="password"
                                       placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-pass-confirmation-input" class="col-sm-3 control-label">Repeat Password</label>

                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="add-pass-confirmation-input"
                                       name="password_confirmation" placeholder="Repeat Password">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'assignuser.add')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary">Save changes</button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>
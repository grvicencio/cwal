<div class="modal fade" id="modal-edit-users" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="formeditusers" id="formeditusers"
                  data-temp-action="/admin/security/assignuser"
                  method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Users</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="image" class="col-sm-3 control-label"></label>

                            <div class="col-sm-8">
                                <img src="{{ asset('CRM/Capital7-1.0.0/img/user2-160x160.jpg') }}" id="modal-img" data-default-img="{{ asset('CRM/Capital7-1.0.0/img/user2-160x160.jpg') }}" width="100">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="image" class="col-sm-3 control-label">Image</label>

                            <div class="col-sm-8">
                                <input type="file" class="form-control" id="image" name="image" accept="image/x-png">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-fname-input" class="col-sm-3 control-label">First Name</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="edit-fname-input" name="firstname"
                                       placeholder="First name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-lname-input" class="col-sm-3 control-label">Last Name</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="edit-lname-input" name="lastname"
                                       placeholder="Last name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-status-input" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="status_id" id="edit-status-input">
                                    @foreach($statuses as $status)
                                        <option value="{{ $status->status_id }}">{{ $status->status_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-role-input" class="col-sm-3 control-label">Role</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="role_id" id="edit-role-input">
                                    @foreach($roles as $role)
                                        <option value="{{ $role['role_id'] }}">{{ $role['role_name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-email-input" class="col-sm-3 control-label">Email</label>

                            <div class="col-sm-8">
                                <input type="email" class="form-control" id="edit-email-input" name="email"
                                       placeholder="Email Address">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('edit', 'assignuser.update')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary">Save changes</button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>
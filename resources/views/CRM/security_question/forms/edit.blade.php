<!-- TODO: change this modal to ajax -->

<div class="modal fade" id="modal-edit-sec-question">

    <div class="modal-dialog">

        <div class="modal-content">
            <form class="form-horizontal" name="security_question" id="security_question_edit_form" action="/admin/security_question/update/" method="post">
                <div class="alert alert-danger print-error-msg" style="display:none">
                </div>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Security Question</h4>
                </div>
                <div class="modal-body">
                    <!-- /.box-header -->
                    <!-- form start -->
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="status-input" class="col-sm-2 control-label">Question</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="edit_sec_ques" name="security_question" placeholder="" required>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('edit', 'security_question.update')
                    <button type="submit" class="btn btn-primary" id="savestatus" >Save changes</button>
                    @endusercan
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
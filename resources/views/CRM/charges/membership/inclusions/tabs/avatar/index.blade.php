<div id="avatar" class="tab-pane fade">
    <table id="table-membership-avatar" class="table table-striped table-bordered" cellspacing="0"
           width="100%">
        <thead>
        <tr>
            <td>Avatar</td>
            <td>Description</td>
            @if(Auth::guard('crm')->user()->canAccess('edit', 'membership.avatars.update') || Auth::guard('crm')->user()->canAccess('delete', 'membership.avatars.destroy'))
                <td>Actions</td>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($membership_charges->membership_avatar as $membership_avatar)
            <tr data-membership-avatar-id="{{ $membership_avatar->membership_avatar_id }}">
                <td data-avatar-id="{{ $membership_avatar->avatar_id }}"
                    data-avatar="{{ str_replace('1024x1024', '100x100', $membership_avatar->avatar->avatar) }}"
                    data-name="{{ $membership_avatar->avatar->name }}"
                    title="{{ $membership_avatar->avatar->name }}">
                    <img src="{{ str_replace('1024x1024', '100x100', $membership_avatar->avatar->avatar) }}" height="25"
                         alt="{{ $membership_avatar->avatar->name }}">
                </td>
                <td>{{ $membership_avatar->description }}</td>
                @if(Auth::guard('crm')->user()->canAccess('edit', 'membership.avatars.update') || Auth::guard('crm')->user()->canAccess('delete', 'membership.avatars.destroy'))
                    <td>
                        @usercan('edit', 'membership.avatars.update')
                        <button type="button"
                                class="btn btn-warning btn-xs"
                                onclick="setMembershipAvatarForm(this);" title="Edit">
                            <i class="fa fa-edit"></i>
                        </button>
                        @endusercan

                        @usercan('delete', 'membership.avatars.destroy')
                        <button type="button" role="button"
                                class="btn btn-danger btn-xs btn-submit"
                                title="Delete"
                                onclick="deleteMembershipAvatar({{ $membership_avatar->membership_id }}, {{ $membership_avatar->membership_avatar_id }})">
                            <i class="fa fa-trash-o"></i>
                        </button>
                        @endusercan
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>

    <br>

    @usercan('add', 'membership.avatars.store')
    <button type="button" class="btn btn-success btn-xs" data-toggle="modal" id="add-membership-avatar-btn" title="Add"><i
                class="fa fa-plus"></i></button>
    @endusercan
</div>

@include('CRM.charges.membership.inclusions.tabs.avatar.forms.add')
@include('CRM.charges.membership.inclusions.tabs.avatar.forms.edit')
@include('CRM.customization.avatar.forms.select_avatar')

@section('style')
    @parent

    <style>
        .avatars .caption p {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

        .avatars img {
            height: 100px;
        }

        #modal-select-avatar .modal-dialog {
            overflow-y: initial !important
        }

        #modal-select-avatar .modal-body {
            height: 520px;
            overflow-y: auto;
        }

        #modal-select-avatar .modal-footer div {
            padding: 0;
        }

        #modal-select-avatar .modal-footer div .pagination {
            margin: 0;
        }
    </style>
@endsection

@section('script')
    @parent

    <script>
        var setMembershipAvatarForm = function (src) {
            var form = $('#form-edit-membership-avatar');
            var tr = $(src).closest('tr');
            form.attr('data-membership-avatar-id', tr.data('membership-avatar-id'));
            form.find('input[name=description]').val(tr.find('td:eq(1)').text());
            form.find('input[name=avatar_id]').val(tr.find('td:eq(0)').data('avatar-id'));
            form.find('.avatar-placeholder').prop('src', tr.find('td:eq(0)').data('avatar'));
            form.find('.avatar-placeholder').prop('alt', tr.find('td:eq(0)').data('name'));
            form.find('.avatar-placeholder').prop('title', tr.find('td:eq(0)').data('name'));
            var url = '{{ route('customization.avatars.json') }}?per_page=' + $('#avatars-per-page').val() + "&type=Membership&readonly=true&status_id={{ $active_id }}" + '&except=' + encodeURIComponent('{{ $excluded_avatar_ids }}');

            getAvatars(url, form.get(0)).done(function () {
                $('#modal-edit-membership-avatar').modal('show');
            });
        };

        function getAvatars(url, src) {
            $('#current-url').val(url);
            var avatars = $('#modal-select-avatar').find('.avatars');

            return $.getJSON(url, function (response) {
                if (response.status == '{{ config('response.type.success') }}') {
                    $('.avatars').html('<p class="text-center">' + '{!! trans('loading.processing') !!}' + '</p>');
                    avatars.data('form-src', $(src).prop('id'));
                    avatars.html(response.data.avatars.html);
                    $('#modal-select-avatar').find('.avatar-links').html(response.data.avatars.links);
                }
            });
        }

        $(function () {
            $('#add-membership-avatar-btn').on('click', function () {
                var url = '{{ route('customization.avatars.json') }}?per_page=' + $('#avatars-per-page').val() + "&type=Membership&readonly=true&status_id={{ $active_id }}" + '&except=' + encodeURIComponent('{{ $excluded_avatar_ids }}');

                getAvatars(url, $('#form-add-membership-avatar').get(0)).done(function () {
                    $('#modal-add-membership-avatar').modal('show');
                });
            });

            $('.select-avatar-btn').on('click', function () {
                $('#modal-select-avatar').modal('show');
            });

            $('.avatars').on('click', '.avatar-entry', function (e) {
                e.preventDefault();
                var form = $('#' + $(this).closest('.avatars').data('form-src'));
                form.find('input[name=avatar_id]').val($(this).data('avatar-id'));
                form.find('.avatar-placeholder').prop('src', $(this).data('avatar'));
                form.find('.avatar-placeholder').prop('alt', $(this).data('name'));
                form.find('.avatar-placeholder').prop('title', $(this).data('name'));
                $('#modal-select-avatar').modal('hide');
            });

            $("#avatars-search").on("keyup", function () {
                var form = $('#' + $(this).parent().parent().parent().parent().find('.avatars').data('form-src'));
                var url = '{{ route('customization.avatars.json') }}?search=' + $(this).val() + '&per_page=' + $('#avatars-per-page').val() + "&type=Membership&readonly=true&status_id={{ $active_id }}" + '&except=' + encodeURIComponent('{{ $excluded_avatar_ids }}');
                getAvatars(url, form.get(0));
            });

            $('.avatar-links').on('click', '.pagination a', function (e) {
                e.preventDefault();
                var form = $('#' + $(this).parent().parent().parent().parent().parent().find('.avatars').data('form-src'));
                var url_lvl1 = setParam($(this).prop('href'), 'per_page', $('#avatars-per-page').val());
                getAvatars(url_lvl1, form.get(0));
            });

            $("#avatars-per-page").on("change", function () {
                var form = $('#' + $(this).parent().parent().parent().parent().find('.avatars').data('form-src'));
                var url_lvl1 = setParam($('#current-url').val(), 'per_page', $(this).val());
                var url_lvl2 = setParam(url_lvl1, 'page', 1);
                getAvatars(url_lvl2, form.get(0));
            });

            $('#table-membership-avatar').DataTable({
                "paging": false,
                "ordering": true,
                "searching": true,
                "responsive": true,
                columnDefs: [
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'membership.avatars.update') || Auth::guard('crm')->user()->canAccess('delete', 'membership.avatars.destroy'))
                    {
                        targets: 2,
                        orderable: false
                    }
                    @endif
                ]
            });

            $('.form-delete-membership-avatar').find('.btn-submit').on('click', function () {
                if (confirm("Are you sure?")) {
                    $(this).closest('form').submit();
                }
            });

            $("#modal-add-membership-avatar").on("hidden.bs.modal", function () {
                var form = $('#form-add-membership-avatar');
                var img = form.find('.avatar-placeholder');
                form.find('.select-avatar-btn').prop('disabled', false);
                img.prop('src', img.data('src'));
                img.prop('alt', img.data('alt'));
                img.prop('title', img.data('title'));
                form.trigger('reset');
                clearErr(form);
            });

            $("#modal-edit-membership-avatar").on("hidden.bs.modal", function () {
                var form = $('#form-edit-membership-avatar');
                form.removeAttr('data-membership-avatar-id');
                var img = form.find('.avatar-placeholder');
                form.find('.select-avatar-btn').prop('disabled', false);
                img.prop('src', img.data('src'));
                img.prop('alt', img.data('alt'));
                img.prop('title', img.data('title'));
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-edit-membership-avatar').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                var membershipAvatarId = form.attr('data-membership-avatar-id');
                var url = form.data('temp-action') + '/' + membershipAvatarId;

                $.post(url, form.serialize(), function (response) {
                    if (response.status == '{{ config('response.type.success') }}') {
                        location.href = '#avatar';
                        location.reload();
                        return;
                    } else if (response.status == '{{ config('response.type.error') }}') {
                        assocErr(response.errors, form);
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $('#form-add-membership-avatar').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                var url = form.prop('action');

                $.post(url, form.serialize(), function (response) {
                    if (response.status == '{{ config('response.type.success') }}') {
                        location.href = '#avatar';
                        location.reload();
                        return;
                    } else if (response.status == '{{ config('response.type.error') }}') {
                        assocErr(response.errors, form);
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });

        function deleteMembershipAvatar(membership_id, membership_avatar_id) {
            swal({
                title: '{{ trans('swal.membership_avatar.delete.confirm.title') }}',
                html: '{{ trans('swal.membership_avatar.delete.confirm.html') }}',
                type: '{{ trans('swal.membership_avatar.delete.confirm.type') }}',
                showCancelButton: true,
                confirmButtonText: '{{ trans('swal.membership_avatar.delete.confirm.confirmButtonText') }}'
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: "/charges/membership/" + membership_id + "/avatars/" + membership_avatar_id,
                        data: {'_token': '{{ csrf_token() }}'},
                        success: function (response) {
                            if (response.status == '{{ config('response.type.success') }}') {
                                location.href = '#avatar';
                                location.reload();
                            } else if (response.status == '{{ config('response.type.fail') }}') {
                                if (response.data.affected) {
                                    var html = '{{ trans('swal.membership_avatar.delete.fail.html') }}';

                                    $.each(response.data.affected, function (key, val) {
                                        var str = ' ' + val.count + ' ' + val.relation;
                                        html += str.replace(/_/g, ' ');
                                    });

                                    swal({
                                        title: '{{ trans('swal.membership_avatar.delete.fail.title') }}',
                                        html: html,
                                        type: '{{ trans('swal.membership_avatar.delete.fail.type') }}'
                                    });
                                }
                            }
                        }
                    });
                }
            });
            return false;
        }
    </script>
@endsection

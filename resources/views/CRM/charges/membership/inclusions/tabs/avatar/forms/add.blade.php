<div class="modal fade" id="modal-add-membership-avatar" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- form start -->
            <form class="form-horizontal" name="form-add-membership-avatar" id="form-add-membership-avatar"
                  action="{{ route('membership.avatars.store', $membership_charges->membership_id) }}"
                  method="POST">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Avatar</h4>
                </div>
                <div class="modal-body">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="add-avatar-id-input" class="col-sm-3 control-label">Avatar</label>

                            <div class="col-sm-8">
                                <img src="http://via.placeholder.com/100/d3d3d3/696969?text=1024%20x%201024" alt="1024x1024"
                                     data-src="http://via.placeholder.com/100/d3d3d3/696969?text=1024%20x%201024" data-alt="1024x1024"
                                     data-title="1024x1024" title="1024x1024" style="width: 100px !important;"
                                     class="img-responsive avatar-placeholder">
                                <input type="hidden" name="avatar_id">
                                <br>
                                <button type="button" role="button"  class="btn btn-default select-avatar-btn">
                                    <i class="fa fa-smile-o"></i> Select avatar
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description-input" class="col-sm-3 control-label">Description</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="description"
                                       placeholder="Description">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'membership.avatars.store')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">
                        Save
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
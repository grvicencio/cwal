<div class="modal fade" id="modal-edit-membership-inclusion" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- form start -->
            <form class="form-horizontal" name="form-edit-membership-inclusion" id="form-edit-membership-inclusion"
                  data-temp-action="/charges/membership/{{ $membership_charges->membership_id }}/inclusions"
                  method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Inclusion</h4>
                </div>
                <div class="modal-body">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name-input" class="col-sm-2 control-label">Name</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name"
                                       placeholder="Inclusion Name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description-input" class="col-sm-2 control-label">Description</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="description"
                                       placeholder="Description">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-type-input" class="col-sm-2 control-label">Type</label>

                            <div class="col-sm-10">
                                <select id="edit-type-input" class="form-control" name="type">
                                    @foreach($types as $type)
                                        <option value="{{ $type }}">{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-currency-id-input" class="col-sm-2 control-label">Currency</label>

                            <div class="col-sm-10">
                                <select id="edit-currency-id-input" class="form-control" name="currency_id">
                                    @foreach($in_app_currencies as $currency)
                                        <option value="{{ $currency->currency_id }}" {{ boolval($currency->default_registration) ? ' selected' : '' }}>{{ $currency->currency }}</option>
                                    @endforeach
                                    <option value="">N/A</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="value-input" class="col-sm-2 control-label">Value</label>

                            <div class="col-sm-10">
                                <input type="number" step='0.01' value='0.00' class="form-control" name="value"
                                       placeholder="Value">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('edit', 'membership.inclusions.update')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">
                        Save changes
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
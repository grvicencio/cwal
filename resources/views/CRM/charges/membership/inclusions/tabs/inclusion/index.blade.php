<div id="inclusions" class="tab-pane fade">
    <table id="table-membership-inclusion" class="table table-striped table-bordered" cellspacing="0"
           width="100%">
        <thead>
        <tr>
            <td>Name</td>
            <td>Description</td>
            <td>Type</td>
            <td>Currency</td>
            <td>Value</td>
            @if(Auth::guard('crm')->user()->canAccess('edit', 'membership.inclusions.update') || Auth::guard('crm')->user()->canAccess('delete', 'membership.inclusions.destroy'))
                <td>Actions</td>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($membership_charges->membership_inclusions as $membership_inclusion)
            <tr data-inclusion-id="{{ $membership_inclusion->membership_inclusion_id }}">
                <td>{{ $membership_inclusion->name }}</td>
                <td>{{ $membership_inclusion->description }}</td>
                <td>{{ $membership_inclusion->type }}</td>
                <td data-currency-id="{{ is_null($membership_inclusion->currency_id) ? '' : $membership_inclusion->currency->currency_id }}">{{ is_null($membership_inclusion->currency_id) ? 'N/A' : $membership_inclusion->currency->currency }}</td>
                <td>{{ $membership_inclusion->value }}</td>
                @if(Auth::guard('crm')->user()->canAccess('edit', 'membership.inclusions.update') || Auth::guard('crm')->user()->canAccess('delete', 'membership.inclusions.destroy'))
                    <td>
                        @usercan('edit', 'membership.inclusions.update')
                        <button type="button"
                                class="btn btn-warning btn-xs btn-edit-membership-inclusion"
                                data-toggle="modal"
                                data-target="#modal-edit-membership-inclusion"
                                title="Edit">
                            <i class="fa fa-edit"></i>
                        </button>
                        @endusercan

                        @usercan('delete', 'membership.inclusions.destroy')
                        <button type="button" role="button"
                                class="btn btn-danger btn-xs btn-submit"
                                title="Delete"
                                onclick="deleteMembershipInclusion({{ $membership_inclusion->membership_id }}, {{ $membership_inclusion->membership_inclusion_id }});">
                            <i class="fa fa-trash-o"></i>
                        </button>
                        @endusercan
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>

    <br>

    @usercan('add', 'membership.inclusions.store')
    <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
            data-target="#modal-add-membership-inclusion" title="Add"><i class="fa fa-plus"></i></button>
    @endusercan
</div>

@include('CRM.charges.membership.inclusions.tabs.inclusion.forms.add')
@include('CRM.charges.membership.inclusions.tabs.inclusion.forms.edit')

@section('script')
    @parent

    <script>
        $(function () {
            $('#table-membership-inclusion').DataTable({
                "paging": false,
                "ordering": true,
                "searching": true,
                "responsive": true,
                columnDefs: [
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'membership.inclusions.update') || Auth::guard('crm')->user()->canAccess('delete', 'membership.inclusions.destroy'))
                    {
                        targets: 5,
                        orderable: false
                    }
                    @endif
                ]
            });

            $('.form-delete-membership-inclusion').find('.btn-submit').on('click', function () {
                if (confirm("Are you sure?")) {
                    $(this).closest('form').submit();
                }
            });

            $("#modal-add-membership-inclusion").on("hidden.bs.modal", function () {
                var form = $('#form-add-membership-inclusion');
                form.trigger('reset');
                clearErr(form);
            });

            $("#modal-edit-membership-inclusion").on("hidden.bs.modal", function () {
                var form = $('#form-edit-membership-inclusion');
                form.removeAttr('data-inclusion-id');
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-edit-membership-inclusion').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                var inclusionId = form.attr('data-inclusion-id');
                var url = form.data('temp-action') + '/' + inclusionId;

                $.post(url, form.serialize(), function (response) {
                    if (response.status == '{{ config('response.type.success') }}') {
                        location.hash = '#inclusions';
                        location.reload();
                        return;
                    } else if (response.status == '{{ config('response.type.error') }}') {
                        assocErr(response.errors, form);
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $('#form-add-membership-inclusion').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                var url = form.prop('action');

                $.post(url, form.serialize(), function (response) {
                    if (response.status == '{{ config('response.type.success') }}') {
                        location.hash = '#inclusions';
                        location.reload();
                        return;
                    } else if (response.status == '{{ config('response.type.error') }}') {
                        assocErr(response.errors, form);
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $('.btn-edit-membership-inclusion').on('click', function () {
                var form = $('#form-edit-membership-inclusion');
                form.attr('data-inclusion-id', $(this).closest('tr').data('inclusion-id'));
                form.find('input[name=name]').val($(this).closest('tr').find('td:eq(0)').text());
                form.find('input[name=description]').val($(this).closest('tr').find('td:eq(1)').text());
                form.find('select[name=type]').val($(this).closest('tr').find('td:eq(2)').text()).trigger('change');
                var currency = $(this).closest('tr').find('td:eq(3)');

                if (parseInt('{{ $in_app_currencies->count() }}') == 0) {
                    var select = form.find('select[name=currency_id]');
                    select.html('<option value="' + currency.data('currency-id') + '">' + currency.text() + '</option>');

                    if (currency.data('currency-id')) {
                        select.append('<option value="">N/A</option>');
                    }
                    select.val(currency.data('currency-id')).trigger('change');
                } else {
                    form.find('select[name=currency_id]').val(currency.data('currency-id') ? currency.data('currency-id') : null).trigger('change');
                }
                form.find('input[name=value]').val($(this).closest('tr').find('td:eq(4)').text());
            });
        });

        function deleteMembershipInclusion(membership_id, membership_inclusion_id) {
            swal({
                title: '{{ trans('swal.membership_inclusion.delete.confirm.title') }}',
                html: '{{ trans('swal.membership_inclusion.delete.confirm.html') }}',
                type: '{{ trans('swal.membership_inclusion.delete.confirm.type') }}',
                showCancelButton: true,
                confirmButtonText: '{{ trans('swal.membership_inclusion.delete.confirm.confirmButtonText') }}'
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: "/charges/membership/" + membership_id + "/inclusions/" + membership_inclusion_id,
                        data: {'_token': '{{ csrf_token() }}'},
                        success: function (response) {
                            if (response.status == '{{ config('response.type.success') }}') {
                                location.hash = '#inclusions';
                                location.reload();
                            } else if (response.status == '{{ config('response.type.fail') }}') {
                                if (response.data.affected) {
                                    var html = '{{ trans('swal.membership_inclusion.delete.fail.html') }}';

                                    $.each(response.data.affected, function (key, val) {
                                        var str = ' ' + val.count + ' ' + val.relation;
                                        html += str.replace(/_/g, ' ');
                                    });

                                    swal({
                                        title: '{{ trans('swal.membership_inclusion.delete.fail.title') }}',
                                        html: html,
                                        type: '{{ trans('swal.membership_inclusion.delete.fail.type') }}'
                                    });
                                }
                            }
                        }
                    });
                }
            });
            return false;
        }
    </script>
@endsection

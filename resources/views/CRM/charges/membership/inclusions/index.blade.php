@extends('CRM.layouts.dashboard')

@section('style')
    @parent

    <style>
        .nav-tabs {
            margin: 0 20px;
        }

        .tab-content {
            margin: 20px;
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Charges</li>
        <li><a href="/charges/membership">Membership</a></li>
        <li class="active">Inclusions</li>
    </ol>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ $membership_charges->name }}</h3>
        </div>
        <div class="box-body">
            <ul class="nav nav-tabs">
                @usercan('view', 'membership.inclusions.index')
                <li><a data-toggle="tab" href="#inclusions">Inclusions</a></li>
                @endusercan

                @usercan('view', 'membership.avatars.index')
                <li><a data-toggle="tab" href="#avatar">Avatar</a></li>
                @endusercan
                <li class="pull-right"><a href="{{ route('membership.index') }}"><i class="fa fa-long-arrow-left"></i>
                        Back to Memberships</a></li>
            </ul>
            <div class="tab-content">
                @usercan('view', 'membership.inclusions.index')
                @include('CRM.charges.membership.inclusions.tabs.inclusion.index')
                @endusercan

                @usercan('view', 'membership.avatars.index')
                @include('CRM.charges.membership.inclusions.tabs.avatar.index')
                @endusercan
            </div>
        </div>
    </div>
@endsection

@section('styles')
    @parent

    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">

    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/select2/dist/css/select2.min.css") }}">
@endsection

@section('scripts')
    @parent

    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>

    <!-- Select2 -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/select2/dist/js/select2.full.min.js") }}"></script>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>
    <script src="{{ asset("CRM/Capital7-1.0.0/js/params.js") }}"></script>

    <script>
        $(function () {
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');
        });
    </script>
@endsection
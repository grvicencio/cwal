<div class="modal fade" id="modal-edit-membership" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="form-edit-membership" id="form-edit-membership" method="POST"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Membership</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="edit-name-input" class="col-sm-3 control-label">Name</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="edit-name-input" name="name"
                                       placeholder="Membership Name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-description-input" class="col-sm-3 control-label">Description</label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="edit-description-input" name="description"
                                       placeholder="Description">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-logo-input" class="col-sm-3 control-label">Logo <span class="text-muted">(720 x 720)</span></label>

                            <div class="col-sm-8">
                                <input type="file" class="form-control" id="edit-logo-input" name="logo_file">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-currency-id-input" class="col-sm-3 control-label">Currency</label>

                            <div class="col-sm-8">
                                <select id="edit-currency-id-input" class="form-control" name="currency_id">
                                    @foreach($global_currencies as $currency)
                                        <option value="{{ $currency->currency_id }}" {{ boolval($currency->default_registration) ? ' selected' : '' }}>{{ $currency->currency }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-type-input" class="col-sm-3 control-label">Type</label>

                            <div class="col-sm-8">
                                <select id="edit-type-input" class="form-control" name="membership_type">
                                    @foreach($types as $type)
                                        <option value="{{ $type }}">{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-number-of-tables-input" class="col-sm-3 control-label">Number of
                                Tables</label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control col-sm-3"
                                       id="edit-number-of-tables-input"
                                       name="no_of_tables" placeholder="Number of Tables">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-rake-input" class="col-sm-3 control-label">Rake</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="number" class="form-control col-sm-3" id="edit-rake-input"
                                           name="banker_rake" placeholder="Rake" step='0.01'>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-price-input" class="col-sm-3 control-label">Price</label>

                            <div class="col-sm-8">
                                <input type="number" step='0.01' class="form-control"
                                       id="edit-price-input" name="price" placeholder="Price">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-status-input" class="col-sm-3 control-label">Status</label>

                            <div class="col-sm-8">
                                <select id="edit-status-input" class="form-control" name="status_id">
                                    @foreach($statuses as $status)
                                        <option value="{{ $status['status_id'] }}">{{ $status['status_name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-billing-cycle-input" class="col-sm-3 control-label">Billing Cycle</label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control col-sm-3" id="edit-billing-cycle-input"
                                       name="billing_cycle" placeholder="Billing Cycle">
                                <p class="text-muted">(0 for permanent membership)</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'membership.add')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">
                        Save
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>
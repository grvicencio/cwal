@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Status</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-xs-4'>
            <!-- Box -->
            <div class="box">
                <div class="box-body">
                    <div class="form-group">
                        <form name="form1" method="get">
                            <label for="sel"> Type:</label>
                            <select class="form-control" onchange='this.form.submit()' name='type'>
                                @foreach ($types as $type)
                                    <option value="{{ $type }}"
                                            @if ($type_selected == $type) selected @endif>{{ spaceCase($type) }}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>

                    <table id="module_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Status</th>
                            @if(Auth::guard('crm')->user()->canAccess('edit', 'status.update') && count(array_diff($secured, $statuses->pluck('status_name')->toArray())))
                                <th>Action</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($statuses as $status)
                            @continue($status->status_name == null)

                            <tr>
                                <td>{{ $status->status_name }}</td>
                                @if(Auth::guard('crm')->user()->canAccess('edit', 'status.update') && !in_array($status->status_name, $secured))
                                    <td>
                                        @usercan('edit', 'status.update')
                                        <button type="button" class="btn-xs btn-warning"
                                                id="{{$status->status_id}}"
                                                data-toggle="modal"
                                                data-target="modal-edit-module"
                                                title="Edit"><i
                                                    class="fa fa-edit"></i></button>
                                        @endusercan
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                @if(count(array_diff($secured, $statuses->pluck('status_name')->toArray())))
                    @usercan('add', 'status.store')
                    <div class="box-footer">
                        <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                                data-target="#modal-add-module"><i
                                    class="fa fa-plus"></i></button>
                    </div><!-- /.box-footer-->
                    @endusercan
                @endif
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    @include('CRM.status.forms.add')
    @include('CRM.status.forms.edit')
@endsection

@section('script')
    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function () {
            $("[data-target=modal-edit-module]").click(function (e) {
                var status_id = $(this).attr("id");
                var name = $(this).closest('tr').children('td:first').text();
                $("#statusmoduleupdate input[name=status_name]").val(name);
                $("#statusmoduleupdate input[name=status_id]").val(status_id);
                $('#modal-edit-module').modal({
                    show: 'false'
                });
            });

            $("#modal-add-module").on("hidden.bs.modal", function () {
                var form = $('#statusmodule');
                form.trigger('reset');
                clearErr(form);
            });

            $('#statusmodule').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: form.serialize(),
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                            return;
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $("#modal-edit-module").on("hidden.bs.modal", function () {
                var form = $('#statusmoduleupdate');
                form.trigger('reset');
                clearErr(form);
            });

            $('#statusmoduleupdate').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: form.serialize(),
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                            return;
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });
    </script>
@endsection


@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Email Templates</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <!-- Box -->
            <div class="box">
                <div class="box-body">
                    <table id="email-templates-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Template</th>
                            <th>Subject</th>
                            <th>Content</th>
                            <th>Updated At</th>
                            @usercan('edit', 'email_templates.update')
                            <th>Actions</th>
                            @endusercan
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(EmailTemplate::all() as $email_template)
                            <tr data-email-template-id="{{ $email_template->email_template_id }}">
                                <td data-type="{{ $email_template->email_type }}">{{ $email_template->email_type }}</td>
                                <td data-subject="{{ $email_template->subject }}">{{ $email_template->subject }}</td>
                                <td data-content="{{ $email_template->content }}"
                                    title="{{ $email_template->content }}"
                                    class="ellipted">{{ $email_template->content }}</td>
                                <td data-updated-at="{{ $email_template->updated_at }}">{{ $email_template->updated_at->diffForHumans() }}</td>
                                @usercan('edit', 'email_templates.update')
                                <td>
                                    <button type="button"
                                            onclick="editEmailTemplate(this);"
                                            data-variables="{{ getEmailTemplateVars($email_template->email_type) }}"
                                            class="btn-xs btn-warning"
                                            title="Edit">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </td>
                                @endusercan
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    @include('CRM.email_templates.forms.edit')
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('style')
    <style>
        .ellipted {
            max-width: 150px !important;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }
    </style>
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        function editEmailTemplate(src) {
            var tr = $(src).closest("tr");
            $("#modal-edit-email-template").find("#template-id").text(tr.find("td:eq(0)").text());
            $("#modal-edit-email-template").find("#edit-subject-input").val(tr.find("td:eq(1)").data("subject"));
            $("#modal-edit-email-template").find("#edit-content-textarea").text(tr.find("td:eq(2)").data("content"));
            $("#modal-edit-email-template").find("input[name=email_type]").val(tr.find("td:eq(0)").text());
            $('#form-edit-email-template').prop("action", "/admin/email_templates/" + tr.data("email-template-id"));

            $.each($(src).data("variables").split(','), function (key, value) {
                $("#email-variable-" + value).show();
            });

            $("#modal-edit-email-template").modal("show");
        }

        $(function () {
            $(".email-variable-button").hide();

            $('#email-templates-table').DataTable({
                "paging": true,
                "ordering": true,
                "searching": true,
                "responsive": true,
                columnDefs: [
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'email_templates.update'))
                    {
                        targets: 4,
                        orderable: false
                    }
                    @endif
                ]
            });

            $(".email-variable-button").on("click", function () {
                var txt = $("#edit-content-textarea");
                var caretPos = txt[0].selectionStart;
                var textAreaTxt = txt.val();
                var txtToAdd = $(this).data("value");
                txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos));
            });

            $("#modal-edit-email-template").on("hidden.bs.modal", function () {
                var form = $('#form-edit-email-template');
                $(".email-variable-button").hide();
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-edit-email-template').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop("method"),
                    url: form.prop("action"),
                    data: form.serialize(),
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                            return;
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });
    </script>
@endsection

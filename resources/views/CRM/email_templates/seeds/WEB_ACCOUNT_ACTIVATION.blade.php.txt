Hello {{$name}},

Please click the provided button below to activate your account.

{{$link}}

Note: This is auto generated email system. Do not Reply.

Thank you,

{{$app_name}} Team
Hello {{$receiver}},

{{$sender}} has sent you {{$amount}}.

Additional message from {{$sender}}:

{{$message}}

Please login to {{$app_url}} to see the content of your wallet.

Note: This is auto generated email system. Do not Reply.

Thank you,

{{$app_name}} Team
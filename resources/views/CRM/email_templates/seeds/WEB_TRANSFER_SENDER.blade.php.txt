Hello {{$sender}},

You sent {{$amount}} to {{$receiver}}.

Additional message from you:

{{$message}}

Please login to {{$app_url}} to see the content of your wallet.

Note: This is auto generated email system. Do not Reply.

Thank you,

{{$app_name}} Team
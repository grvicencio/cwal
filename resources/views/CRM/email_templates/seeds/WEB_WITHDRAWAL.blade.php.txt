Hello {{$name}},

Your request for withdrawal amounting {{$amount}} was {{$status}}.

{{$status}} notes:

{{$notes}}

Please login to {{$app_url}} to see the content of your wallet.

Note: This is auto generated email system. Do not Reply.

Thank you,

{{$app_name}} Team
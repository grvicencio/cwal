<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset((Auth::guard('crm')->user()->image) ? Auth::guard('crm')->user()->image : 'CRM/Capital7-1.0.0/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>

            <div class="pull-left info">
                <p>{{ Auth::guard('crm')->user()->firstname }} {{ Auth::guard('crm')->user()->lastname }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU</li>
            @usercan('view', 'crm.dashboard.index')
                <li class="@isset($dashboard_menu) active @endisset">
                    <a href="/admin/dashboard">
                        <i class="fa fa-dashboard" aria-hidden="true"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            @endusercan

            @usercan('view', 'accounts.index')
                <li class="@isset($accounts_menu) active @endisset">
                    <a href="/admin/accounts">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <span>Accounts</span>
                    </a>
                </li>
            @endusercan

            @usercan('view', 'ticketing.index')
                <li class="@isset($tickets_menu) active @endisset">
                    <a href="/admin/ticketing">
                        <i class="fa fa-tags" aria-hidden="true"></i>
                        <span>Tickets</span>
                    </a>
                </li>
            @endusercan

            @usercan('view', 'inquiry.index')
                <li class="@isset($inquiry_menu) active @endisset">
                    <a href="{{ route('inquiry.index') }}">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <span>Contact Inquiries</span>
                    </a>
                </li>
            @endusercan

            @usercan('view', 'transactions.index')
                <li class="@isset($transactions_menu) active @endisset">
                    <a href="/admin/transactions">
                        <i class="fa fa-exchange" aria-hidden="true"></i>
                        <span>Transactions</span>
                    </a>
                </li>
            @endusercan

            @usercan('view', 'status.index')
                <li class="@isset($status_menu) active @endisset">
                    <a href="/admin/status">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        <span>Status</span>
                    </a>
                </li>
            @endusercan

            @if(Auth::guard('crm')->user()->canAccess('view', 'roles.index') || Auth::guard('crm')->user()->canAccess('view', 'permissions.index') || Auth::guard('crm')->user()->canAccess('view', 'assignuser.index'))
                <li class="treeview @isset($security_menu) active menu-open @endisset" id="security-treeview">
                    <a href="javascript:void(0);">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                        <span>Security</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @usercan('view', 'roles.index')
                        <li class="@isset($roles_menu) active @endisset">
                            <a href="/admin/security/roles">
                                <i class="fa fa-cogs" aria-hidden="true"></i>
                                <span>Modules</span>
                            </a>
                        </li>
                        @endusercan

                        @usercan('view', 'permissions.index')
                        <li class="@isset($permission_menu) active @endisset">
                            <a href="/admin/security/permissions">
                                <i class="fa fa-user-secret" aria-hidden="true"></i>
                                <span>Roles</span>
                            </a>
                        </li>
                        @endusercan

                        @usercan('view', 'assignuser.index')
                        <li class="@isset($assignuser_menu) active @endisset">
                            <a href="/admin/security/assignuser">
                                <i class="fa fa-user-o" aria-hidden="true"></i>
                                <span>Users</span>
                            </a>
                        </li>
                        @endusercan
                    </ul>
                </li>
            @endif

            @if(Auth::guard('crm')->user()->canAccess('view', 'regular.index') || Auth::guard('crm')->user()->canAccess('view', 'membership.index') || Auth::guard('crm')->user()->canAccess('view', 'promotion.index'))
                <li class="treeview @isset($charges_menu)) active menu-open @endisset" id="charges-treeview">
                    <a href="javascript:void(0);">
                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                        <span>Charges</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @usercan('view', 'regular.index')
                        <li class="@isset($reg_menu) active @endisset">
                            <a href="/admin/charges/regular">
                                <i class="fa fa-user-circle" aria-hidden="true"></i>
                                <span>Regular</span>
                            </a>
                        </li>
                        @endusercan

                        @usercan('view', 'membership.index')
                        <li class="@isset($mem_menu) active @endisset">
                            <a href="/admin/charges/membership">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span>Membership</span>
                            </a>
                        </li>
                        @endusercan

                        {{-- @usercan('view', 'promotion.index') --}}
                        {{-- <li class="@isset($prootion_menu) active @endisset"> --}}
                            {{-- <a href="/admin/charges/promotion"> --}}
                                {{-- <i class="fa fa-thumbs-up" aria-hidden="true"></i> --}}
                                {{-- <span>Promotion</span> --}}
                            {{-- </a> --}}
                        {{-- </li> --}}
                        {{-- @endusercan --}}
                    </ul>
                </li>
            @endif

            @usercan('view', 'applications.index')
            <li class="@isset($application_menu) active @endisset">
                <a href="/admin/applications/">
                    <i class="fa fa-gamepad" aria-hidden="true"></i>
                    <span>Applications</span>
                </a>
            </li>
            @endusercan

            {{-- <li> --}}
                {{-- <a class="@isset($payment_methods) active @endisset " href="/admin/payment_methods"> --}}
                    {{-- <i class="fa fa-money" aria-hidden="true"></i> --}}
                    {{-- <span>Payment Methods</span> --}}
                {{-- </a> --}}
            {{-- </li> --}}

            @if(Auth::guard('crm')->user()->canAccess('view', 'currencies.index') || Auth::guard('crm')->user()->canAccess('view', 'wallet.transfer.transfer') || Auth::guard('crm')->user()->canAccess('view', 'exchange_rates.index') || Auth::guard('crm')->user()->canAccess('view', 'registrations.index') || Auth::guard('crm')->user()->canAccess('view', 'daily_bonuses.index') || Auth::guard('crm')->user()->canAccess('view', 'wallet.withdrawal.index') || Auth::guard('crm')->user()->canAccess('view', 'wallet.deposit.index') || Auth::guard('crm')->user()->canAccess('view', 'wallet.bankinformation.index'))
                <li class="treeview @isset($wallet_menu) active menu-open @endisset" id="wallet-treeview">
                    <a href="javascript:void(0);">
                        <i class="fa fa-id-card-o" aria-hidden="true"></i>
                        <span>Wallet</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @usercan('view', 'currencies.index')
                        <li class="@isset($currency_menu) active @endisset">
                            <a href="/admin/wallet/currencies">
                                <i class="fa fa-usd" aria-hidden="true"></i>
                                <span>Currency</span>
                            </a>
                        </li>
                        @endusercan

                        @usercan('view', 'exchange_rates.index')
                        <li class="@isset($exchange_rate_menu) active @endisset">
                            <a href="/admin/wallet/exchange_rates">
                                <i class="fa fa-exchange" aria-hidden="true"></i>
                                <span>Exchange Rate</span>
                            </a>
                        </li>
                        @endusercan

                        @usercan('view', 'wallet.transfer.transfer')
                        <li class="@isset($transfer_menu) active @endisset">
                            <a href="/admin/wallet/transfer">
                                <i class="fa fa-share-square-o" aria-hidden="true"></i>
                                <span>Transfer</span>
                            </a>
                        </li>
                        @endusercan

                        @usercan('view', 'wallet.deposit.index')
                        <li class="@isset($deposit_menu) active @endisset">
                            <a href="/admin/wallet/deposit">
                                <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
                                <span>Deposit</span>
                            </a>
                        </li>
                        @endusercan
                        @usercan('view', 'wallet.withdrawal.index')
                        <li class="@isset($withdrawal_menu) active @endisset">
                            <a href="/admin/wallet/withdrawal">
                                <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
                                <span>Withdrawal</span>
                            </a>
                        </li>
                        @endusercan
                        @usercan('view', 'wallet.bankinformation.index')
                        <li class="@isset($bankinformation_menu) active @endisset">
                            <a href="/admin/wallet/bankinformation">
                                <i class="fa fa-info" aria-hidden="true"></i>
                                <span>Bank Information</span>
                            </a>
                        </li>
                        @endusercan

                        @usercan('view', 'registrations.index')
                        <li class="@isset($registration_menu) active @endisset temp-hide">
                            <a href="/admin/wallet/registrations">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                <span>Registration</span>
                            </a>
                        </li>
                        @endusercan

                        @usercan('view', 'daily_bonuses.index')
                        <li class="@isset($daily_bonus_menu) active @endisset">
                            <a href="/admin/wallet/daily_bonuses">
                                <i class="fa fa-trophy" aria-hidden="true"></i>
                                <span>Daily Bonus</span>
                            </a>
                        </li>
                        @endusercan
                    </ul>
                </li>
            @endif

            @if(Auth::guard('crm')->user()->canAccess('view', 'marketplace_currency.index') || Auth::guard('crm')->user()->canAccess('view', 'marketplace_avatar.index'))
                <li class="treeview @isset($marketplace_menu) active menu-open @endisset" id="marketplace-treeview">
                    <a href="javascript:void(0);">
                        <i class="ion ion-briefcase" aria-hidden="true"></i>
                            <span>Marketplace</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>

                    <ul class="treeview-menu">
                        @usercan('view', 'marketplace_avatar.index')
                            <li class="@isset($marketplace_avatar_menu) active @endisset">
                                <a href="/admin/marketplace/avatar">
                                    <i class="fa fa-smile-o" aria-hidden="true"></i>
                                    <span>Avatar</span>
                                </a>
                            </li>
                        @endusercan

                        @usercan('view', 'marketplace_currency.index')
                            <li class="@isset($marketplace_currency_menu) active @endisset">
                                <a href="/admin/marketplace/marketplace_currency">
                                    <i class="fa fa-usd" aria-hidden="true"></i>
                                    <span>Currency</span>
                                </a>
                            </li>
                        @endusercan
                    </ul>
                </li>
            @endif

            @if(Auth::guard('crm')->user()->canAccess('view', 'avatars.index'))
                <li class="treeview @isset($customization_menu) active menu-open @endisset temp-hide" id="customization-treeview">
                    <a href="javascript:void(0);">
                        <i class="ion ion-wrench" aria-hidden="true"></i>
                        <span>Customization</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @usercan('view', 'avatars.index')
                        <li class="@isset($avatar_menu) active @endisset">
                            <a href="/admin/customization/avatars">
                                <i class="fa fa-smile-o" aria-hidden="true"></i>
                                <span>Avatar</span>
                            </a>
                        </li>
                        @endusercan
                    </ul>
                </li>
            @endif

            @usercan('view', 'security_question.index')
            <li class="@isset($security_question) active @endisset temp-hide">
                <a href="/admin/security_question">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    <span>Security Question</span>
                </a>
            </li>
            @endusercan

            @if(Auth::guard('crm')->user()->canAccess('view', 'categories.index') || Auth::guard('crm')->user()->canAccess('view', 'contents.index'))
                <li class="treeview @isset($cms_menu) active menu-open @endisset" id="cms-treeview">
                    <a href="javascript:void(0);">
                        <i class="fa fa-database" aria-hidden="true"></i>
                        <span>CMS</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @usercan('view', 'categories.index')
                        <li class="@isset($category_menu) active @endisset">
                            <a href="/admin/cms/categories">
                                <i class="fa fa-object-ungroup" aria-hidden="true"></i>
                                <span>Category</span>
                            </a>
                        </li>
                        @endusercan

                        @usercan('view', 'contents.index')
                        <li class="@isset($content_menu) active @endisset">
                            <a href="/admin/cms/contents">
                                <i class="fa fa-file-o" aria-hidden="true"></i>
                                <span>Content</span>
                            </a>
                        </li>
                        @endusercan
                    </ul>
                </li>
            @endif

            <li class="@isset($email_templates_menu) active @endisset">
                <a href="/admin/email_templates">
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    <span>Email Templates</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

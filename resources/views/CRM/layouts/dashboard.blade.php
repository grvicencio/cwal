<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>{{ $page_title or config('app.name') }}</title>

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/bootstrap/dist/css/bootstrap.min.css") }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/font-awesome/css/font-awesome.min.css") }}">

    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/Ionicons/css/ionicons.min.css") }}">

    @yield('styles')

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/dist/css/AdminLTE.min.css") }}">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/dist/css/skins/skin-blue.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/viewer.css") }}">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- ClubNine -->
    <link rel="stylesheet" href="{{ asset('CRM/Capital7-1.0.0/css/custom.css') }}">

    @yield('style')

    <style>
        @import url('https://fonts.googleapis.com/css?family=Ubuntu:100,300,400,500,600,700');

        span.logo-lg{ font-family: "Ubuntu", sans-serif !important; font-weight: 100 !important; }
            span.logo-lg strong{ font-weight: 500 !important; }
        .temp-hide{ display: none !important; }


         .centered{
             text-align: center;
         }

        .right_aligned{
            text-align: right;
        }

        button[data-balloon] {
            overflow: visible;
            word-wrap: break-word;
        }
        .detail {
            position: absolute;
            z-index: 9999999;
        }
        /* Style the Image Used to Trigger the Modal */
        #img-attachmen {
            border-radius: 5px;
            cursor: pointer;
            transition: 0.3s;
        }

        #img-attachmen:hover {opacity: 0.7;}


        /* Modal Content (Image) */
        .modal-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
        }

        /* Caption of Modal Image (Image Text) - Same Width as the Image */
        #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
        }

        /* Add Animation - Zoom in the Modal */
        .modal-content, #caption {
            animation-name: zoom;
            animation-duration: 0.6s;
        }

        @keyframes zoom {
            from {transform:scale(0)}
            to {transform:scale(1)}
        }


        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px){
            .modal-content {
                width: 100%;
            }
        }

    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

    <!-- Header -->
    @include('CRM.layouts.header')

    <!-- Sidebar -->
    @include('CRM.layouts.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Page Title" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- Breadcrumb -->
            @yield('breadcrumb')
        </section>

        <!-- Main Content -->
        <section class="content">
            <!-- Page Content -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('CRM.layouts.footer')
</div><!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/jquery/dist/jquery.min.js") }}"></script>


<!-- Bootstrap 3.3.7 -->
<script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/bootstrap/dist/js/bootstrap.min.js") }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset("CRM/AdminLTE-2.4.2/dist/js/adminlte.min.js") }}"></script>
@yield('scripts')

<!-- SweetAlert -->


<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
<script src="https://unpkg.com/sweetalert2@7.15.0/dist/sweetalert2.all.js"></script>

<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

<!-- Global Script -->
<script>
    $(function () {
        if ('{{ session()->has('swal') }}') {
            swal({
                title: '{{ session()->get('swal.title') }}',
                html: '{{ session()->get('swal.html') }}',
                type: '{{ session()->get('swal.type') }}'
            });
        }
    });
</script>

@yield('script')
</body>
</html>
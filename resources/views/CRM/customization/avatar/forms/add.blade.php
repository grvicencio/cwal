<div class="modal fade" id="modal-add-avatar" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form name="form-add-avatar" id="form-add-avatar"
                  action="{{ route('avatars.store') }}"
                  enctype="multipart/form-data" method="post">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Avatar</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="add-name-input" class="control-label">Name</label>
                            <input type="text" class="form-control" id="add-name-input" name="name"
                                   placeholder="Name">
                        </div>

                        <div class="form-group">
                            <label for="add-avatar-input" class="control-label">Avatar <span class="text-muted">(1024 x 1024)</span></label>
                            <input type="file" class="form-control" id="add-avatar-input" name="avatar">
                        </div>

                        <div class="form-group">
                            <label for="add-application-select" class="control-label">Application</label>
                            <select id="add-application-select" class="form-control" name="application_id">
                                @foreach($applications as $application)
                                    <option value="{{ $application->application_id }}">{{ $application->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="add-type-select" class="control-label">Type</label>
                            <select id="add-type-select" class="form-control" name="type">
                                @foreach($types as $type)
                                    <option value="{{ $type }}">{{ $type }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="add-status-select" class="control-label">Status</label>
                            <select id="add-status-select" class="form-control" name="status_id">
                                @foreach($statuses as $status)
                                    <option value="{{ $status->status_id }}">{{ $status->status_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'avatars.store')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">Save
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>
<?php

return [

    'sign_in' => '<i class="fa fa-pulse fa-spinner fa-fw"></i> Signing in',
    'please_wait' => '<i class="fa fa-pulse fa-spinner fa-fw"></i> Please wait',
    'default' => '<i class="fa fa-pulse fa-spinner fa-fw"></i> loading',
    'processing' => '<i class="fa fa-pulse fa-spinner fa-fw"></i> Processing...',

];

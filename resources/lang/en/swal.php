<?php

return [

    'wallet' => [
        'deposit' => [
            'success' =>[
            'title' => 'Update Success!',
            'html' => 'Note successfully saved.',
            'type' => 'success'
                ],
            'failed' =>[
                'title' => 'Update Success!',
                'html' => 'Error encountered while saving the note.',
                'type' => 'error'
            ],

        ],
        'withdrawal' => [
            'success' =>[
                'title' => 'Update Success!',
                'html' => 'Note successfully saved.',
                'type' => 'success'
            ],
            'failed' =>[
                'title' => 'Update Success!',
                'html' => 'Error encountered while saving the note.',
                'type' => 'error'
            ]
        ]
    ],

    'currency' => [
        'update' => [
            'confirm' => [
                'title' => 'Are you sure?',
                'html' => 'The default currency will be change.',
                'type' => 'warning',
                'confirmButtonText' => 'Yes, change it!'
            ]
        ]
    ],
    'transfer' => [
        'success' => [
            'title' => 'Transfer Success!',
            'html' => 'You have successfully transferred :amount to :link_to_route.',
            'type' => 'success'
        ]
    ],
    'account' => [
        'update' => [
            'success' => [
                'title' => 'Update Success!',
                'html' => 'Account information was successfully updated.',
                'type' => 'success'
            ]
        ]
    ],
    'exception' => [
        'title' => 'Error!',
        'type' => 'error'
    ],
    'permission' => [
        'forbidden' => [
            'title' => 'Error 403!',
            'html' => 'Forbidden: You don\'t have permission to access that URL on this server',
            'type' => 'error'
        ],
        'update' => [
            'success' => [
                'title' => 'Update Success!',
                'html' => 'Permissions was successfully updated.',
                'type' => 'success'
            ]
        ],
        'warning' => [
            'title' => 'Warning!',
            'html' => ':module requires <strong>View</strong> permission on :parent.',
            'type' => 'warning'
        ],
        'empty' => [
            'title' => 'Empty Selection!',
            'html' => 'Please select a permission.',
            'type' => 'info'
        ]
    ],
    'application' => [
        'create' => [
            'image' => [
                'error' => [
                    'title' => 'Error uploading file!',
                    'html' => 'File is either not valid or do not exist.',
                    'type' => 'error'
                ]
            ]
        ],
        'delete' => [
            'confirm' => [
                'title' => 'Are you sure?',
                'html' => 'You can not revert this action.',
                'type' => 'warning',
                'confirmButtonText' => 'Yes, delete it!'
            ],
            'fail' => [
                'title' => 'Delete Failed!',
                'html' => 'Model relationship:',
                'type' => 'error'
            ]
        ],
    ],
    'avatar' => [
        'create' => [
            'image' => [
                'error' => [
                    'title' => 'Error uploading file!',
                    'html' => 'File is either not valid or do not exist.',
                    'type' => 'error'
                ]
            ]
        ],
        'api' => [ //remove
            'image' => [
                'error' => [
                    'title' => 'Error uploading file!',
                    'type' => 'error'
                ]
            ]
        ],
        'update' => [
            'image' => [
                'error' => [
                    'title' => 'Error uploading file!',
                    'html' => 'File is either not valid or do not exist.',
                    'type' => 'error'
                ]
            ]
        ]
    ],
    'membership' => [
        'create' => [
            'logo' => [
                'error' => [
                    'title' => 'Error uploading file!',
                    'html' => 'File is either not valid or do not exist.',
                    'type' => 'error'
                ]
            ]
        ],
        'api' => [
            'logo' => [
                'error' => [
                    'title' => 'Error uploading file!',
                    'type' => 'error'
                ]
            ]
        ],
        'update' => [
            'logo' => [
                'error' => [
                    'title' => 'Error uploading file!',
                    'html' => 'File is either not valid or do not exist.',
                    'type' => 'error'
                ]
            ]
        ]
    ],
    'status' => [
        'delete' => [
            'confirm' => [
                'title' => 'Are you sure?',
                'html' => 'You can not revert this action.',
                'type' => 'warning',
                'confirmButtonText' => 'Yes, delete it!'
            ],
            'fail' => [
                'title' => 'Delete Failed!',
                'html' => 'Model relationship:',
                'type' => 'error'
            ]
        ],
    ],
    'module' => [
        'delete' => [
            'confirm' => [
                'title' => 'Are you sure?',
                'html' => 'You can not revert this action.',
                'type' => 'warning',
                'confirmButtonText' => 'Yes, delete it!'
            ],
            'fail' => [
                'title' => 'Delete Failed!',
                'html' => 'Model relationship:',
                'type' => 'error'
            ]
        ],
        'create' => [
            'module_not_exists' => [
                'title' => 'Create Failed!',
                'html' => 'Module :module does not exists.',
                'type' => 'error'
            ],
            'module_taken' => [
                'title' => 'Create Failed!',
                'html' => 'Module :module already taken.',
                'type' => 'error'
            ]
        ],
        'update' => [
            'module_not_exists' => [
                'title' => 'Update Failed!',
                'html' => 'Module :module does not exists.',
                'type' => 'error'
            ],
            'module_taken' => [
                'title' => 'Update Failed!',
                'html' => 'Module :module already taken.',
                'type' => 'error'
            ]
        ],
    ],
    'user' => [
        'delete' => [
            'confirm' => [
                'title' => 'Are you sure?',
                'html' => 'You can not revert this action.',
                'type' => 'warning',
                'confirmButtonText' => 'Yes, delete it!'
            ],
            'fail' => [
                'title' => 'Delete Failed!',
                'html' => 'Model relationship:',
                'type' => 'error'
            ]
        ],
    ],
    'charge' => [
        'delete' => [
            'confirm' => [
                'title' => 'Are you sure?',
                'html' => 'You can not revert this action.',
                'type' => 'warning',
                'confirmButtonText' => 'Yes, delete it!'
            ],
            'fail' => [
                'title' => 'Delete Failed!',
                'html' => 'Model relationship:',
                'type' => 'error'
            ]
        ],
    ],
    'membership_inclusion' => [
        'delete' => [
            'confirm' => [
                'title' => 'Are you sure?',
                'html' => 'You can not revert this action.',
                'type' => 'warning',
                'confirmButtonText' => 'Yes, delete it!'
            ],
            'fail' => [
                'title' => 'Delete Failed!',
                'html' => 'Model relationship:',
                'type' => 'error'
            ]
        ],
    ],
    'membership_avatar' => [
        'delete' => [
            'confirm' => [
                'title' => 'Are you sure?',
                'html' => 'You can not revert this action.',
                'type' => 'warning',
                'confirmButtonText' => 'Yes, delete it!'
            ],
            'fail' => [
                'title' => 'Delete Failed!',
                'html' => 'Model relationship:',
                'type' => 'error'
            ]
        ],
    ],
    'charts' => [
        'max_range' => [
            'title' => 'Info',
            'html' => 'Preview is limited to 90 days.',
            'type' => 'info'
        ],
    ],
    'category' => [
        'delete' => [
            'confirm' => [
                'title' => 'Are you sure?',
                'html' => 'You can not revert this action.',
                'type' => 'warning',
                'confirmButtonText' => 'Yes, delete it!'
            ],
            'fail' => [
                'title' => 'Delete Failed!',
                'html' => 'Model relationship:',
                'type' => 'error'
            ]
        ],
        'create' => [
            'confirm' => [
                'title' => 'Do you want to continue?',
                'html' => 'The position you selected is already taken by another category.',
                'type' => 'warning',
                'confirmButtonText' => 'Yes, continue!'
            ],
        ],
    ],
    'content' => [
        'delete' => [
            'confirm' => [
                'title' => 'Are you sure?',
                'html' => 'You can not revert this action.',
                'type' => 'warning',
                'confirmButtonText' => 'Yes, delete it!'
            ],
            'fail' => [
                'title' => 'Delete Failed!',
                'html' => 'Model relationship:',
                'type' => 'error'
            ],
            'default' => [
                'title' => 'Delete default error!',
                'html' => 'Deleting default content not allowed.',
                'type' => 'error'
            ],
        ],
        'set_as_default_error' => [
            'title' => 'Default required!',
            'html' => 'There must be at least one default content.',
            'type' => 'error'
        ],
    ],
    'marketplace_currency' => [
        'create' => [
            'image' => [
                'error' => [
                    'title' => 'Error uploading file!',
                    'html' => 'File is either not valid or do not exist.',
                    'type' => 'error'
                ]
            ]
        ],
        'update' => [
            'image' => [
                'error' => [
                    'title' => 'Error uploading file!',
                    'html' => 'File is either not valid or do not exist.',
                    'type' => 'error'
                ]
            ]
        ]
    ],
];

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
    return redirect('admin/login');
});
Route::namespace('CRM')->prefix('admin')->group(function(){
    Route::get('/', function(){
        return redirect('admin/login');
    });

    Route::get('images/{image}', 'ImageController@see')->name('images.see');

    Auth::routes();

    Route::group(['middleware' => 'auth'], function(){
        Route::get('players', 'PlayerController@index')->name('players.index');

        Route::namespace('Accounts')->group(function(){
            Route::resource('accounts.billing_addresses', 'BillingAddressController', ['only' => [
                'index', 'store', 'update'
            ]]);

            Route::prefix('security_question')->group(function(){
                Route::get('/', 'SecurityQuestionController@index')->name('security_question.index');
                Route::get('list', 'SecurityQuestionController@fetchSecurityQuestionDatable')->name('security_question.dataTable');
                Route::post('create', 'SecurityQuestionController@create')->name('security_question.create');
                Route::post('delete/{id}', 'SecurityQuestionController@delete')->name('security_question.delete');
                Route::post('update/{id}', 'SecurityQuestionController@update')->name('security_question.update');
            });

            Route::prefix('accounts')->group(function(){
                Route::get('/', 'AccountsController@index')->name('accounts.index');
                Route::get('datatable', 'AccountsController@dataTable')->name('accounts.dataTable');
                Route::get('{account}', 'AccountsController@details')->name('accounts.details');
                Route::put('{account}', 'AccountsController@update')->name('accounts.update');
            });
        });

        Route::namespace('Application')->group(function(){
            Route::resource('applications', 'ApplicationController', ['only' => [
                'index', 'store', 'update', 'destroy'
            ]]);

            Route::get('applications/{dir}/{size}/{image}', 'ApplicationController@getImage')->name('applications.getImage');
        });

        Route::namespace('Charges')->prefix('charges')->group(function(){
            Route::get('regular', 'RegularChargesController@index')->name('regular.index');
            Route::post('regular/add', 'RegularChargesController@add')->name('regular.add');
            Route::put('regular/edit/{charge}', 'RegularChargesController@edit')->name('regular.edit');
            Route::get('membership', 'MembershipChargesController@index')->name('membership.index');
            Route::post('membership/add', 'MembershipChargesController@add')->name('membership.add');
            Route::put('membership/{membership}', 'MembershipChargesController@update')->name('membership.update');
            Route::get('promotion', 'PromotionChargesController@index')->name('promotion.index');

            Route::resource('membership.inclusions', 'MembershipInclusionsController', ['only' => [
                'index', 'store', 'update', 'destroy'
            ]]);

            Route::resource('membership.avatars', 'MembershipAvatarController', ['only' => [
                'index', 'store', 'update', 'destroy'
            ]]);
        });

        Route::namespace('Charts')->prefix('charts')->group(function(){
            Route::get('user_charge_range/{from}/{to}', 'UserChargeController@user_charge_range')->name('charts.user_charge_range');
            Route::get('sales_range/{from}/{to}', 'SalesController@sales_range')->name('charts.sales_range');
            Route::get('registered_users_range/{from}/{to}', 'RegisteredUsersController@registered_users_range')->name('charts.registered_users_range');
        });

        Route::namespace('CMS')->prefix('cms')->group(function(){
            Route::get('categories/json', 'CategoryController@json')->name('categories.json');
            Route::get('categories/parent/{parent}/positions', 'CategoryController@positions')->name('categories.positions');
            Route::get('categories/datatable', 'CategoryController@dataTable')->name('categories.dataTable');

            Route::resource('categories', 'CategoryController', ['only' => [
                'index', 'store', 'update', 'destroy'
            ]]);

            Route::post('contents/image/upload', 'ContentController@upload')->name('contents.upload');
            Route::get('contents/datatable', 'ContentController@dataTable')->name('contents.dataTable');
            Route::resource('contents', 'ContentController', ['except' => ['show']]);
        });

        Route::namespace('Customization')->prefix('customization')->group(function(){
            Route::get('avatars/json', 'AvatarController@json')->name('customization.avatars.json');

            Route::resource('avatars', 'AvatarController', ['only' => [
                'index', 'store', 'update'
            ]]);
        });

        Route::namespace('Dashboard')->prefix('dashboard')->group(function(){
            Route::get('/', 'DashboardController@index')->name('crm.dashboard.index');
            Route::get('/deposit_tickets', 'DepositTicketsController@tickets')->name('crm.dashboard.deposit.tickets');
            Route::get('/withdrawal_tickets', 'WithdrawalTicketsController@tickets')->name('crm.dashboard.withdrawal.tickets');
        });

        Route::namespace('Marketplace')->prefix('marketplace')->group(function(){
            Route::prefix('marketplace_currency')->group(function(){
                Route::get('/', 'MarketplaceCurrencyController@index')->name('marketplace_currency.index');
                Route::get('/datatable', 'MarketplaceCurrencyController@dataTable')->name('marketplace_currency.dataTable');
                Route::post('/save', 'MarketplaceCurrencyController@store')->name('marketplace_currency.store');
                Route::put('/update', 'MarketplaceCurrencyController@update')->name('marketplace_currency.update');
            });

            Route::get('/avatar/json', 'MarketplaceAvatarController@json')->name('marketplace_avatar.json');
            Route::get('/avatar/selectavatars', 'MarketplaceAvatarController@selectAvatars')->name('marketplace_avatar.selectavatars');
            Route::get('/avatar/', 'MarketplaceAvatarController@index')->name('marketplace_avatar.index');
            Route::get('/avatar/datatable', 'MarketplaceAvatarController@dataTable')->name('marketplace_avatar.datatable');
            Route::post('/avatar/save', 'MarketplaceAvatarController@store')->name('marketplace_avatar.store');
            Route::put('/avatar/update', 'MarketplaceAvatarController@update')->name('marketplace_avatar.update');
        });

        Route::namespace('Profile')->prefix('profile')->group(function(){
            Route::get('/{id}', 'ProfileController@index')->name('user.profile');
            Route::put('image/update/{id}', 'ProfileController@update')->name('user.profile.image');
        });

        Route::namespace('Security')->prefix('security')->group(function(){
            Route::get('permissions', 'PermissionController@index')->name('permissions.index');
            Route::post('permissions/add', 'PermissionController@add')->name('permissions.add');
            Route::put('permissions/edit', 'PermissionController@edit')->name('permissions.edit');
            Route::delete('roles/delete/{module}', 'RoleController@delete')->name('roles.delete');
            Route::get('roles/{role}/json', 'RoleController@json')->name('roles.json');
            Route::get('roles', 'RoleController@index')->name('roles.index');
            Route::post('roles/add', 'RoleController@add')->name('roles.add');
            Route::put('roles/{module}', 'RoleController@edit')->name('roles.edit');
            Route::get('assignuser', 'AssignUserController@index')->name('assignuser.index');
            Route::post('assignuser/add', 'AssignUserController@add')->name('assignuser.add');
            Route::delete('assignuser/delete/{assignuser}', 'AssignUserController@delete')->name('assignuser.delete');
            Route::put('assignuser/{assignuser}', 'AssignUserController@update')->name('assignuser.update');
            Route::put('assignuser/{assignuser}/password/reset', 'AssignUserController@passwordReset')->name('assignuser.passwordReset');
            Route::get('userimg/{dir}/{image}', 'AssignUserController@getImage')->name('assignuser.getImage');
        });

        Route::namespace('Status')->group(function(){
            Route::get('status/', 'StatusController@index')->name('status.index');
            Route::post('status/save', 'StatusController@store')->name('status.store');
            Route::get('status/edit/{id}', 'StatusController@edit')->name('status.edit');
            Route::put('status/update', 'StatusController@update')->name('status.update');
        });

        Route::namespace('Ticketing')->prefix('ticketing')->group(function(){
            Route::get('/', 'TicketingController@index')->name('ticketing.index');
            Route::get('/datatable', 'TicketingController@datatable')->name('ticketing.datatable');
            Route::get('/details/{id}', 'TicketingController@ticketDetails')->name('ticketing.details');
            Route::post('/track', 'TicketingController@trackTicket')->name('ticketing.track');
            Route::post('/reward', 'TicketingController@crmTicketAction')->name('ticketing.action');
        });

        Route::namespace('Inquiry')->prefix('inquiry')->group(function(){
            Route::get('/', 'InquiryController@index')->name('inquiry.index');
            Route::get('/datatable', 'InquiryController@datatable')->name('inquiry.datatable');
            Route::post('/info', 'InquiryController@postInquiryInfo')->name('inquiry.info');
            Route::post('/crm/reply', 'InquiryController@postCRMReply')->name('inquiry.crm.reply');
            Route::post('/crm/replies', 'InquiryController@postCRMReplyHistory')->name('inquiry.crm.replies');
        });

        Route::namespace('Transactions')->prefix('transactions')->group(function(){
            Route::get('/', 'TransactionsController@index')->name('transactions.index');
            Route::post('add', 'TransactionsController@add')->name('transactions.add');
            Route::post('updateNotes/{id}', 'TransactionsController@updateNotes')->name('transactions.updateNotes');
            Route::get('accounts/{account}/activity/datatable', 'TransactionsController@activityDataTable')->name('transaction.activityDataTable');
        });

        Route::namespace('Wallet')->prefix('wallet')->group(function(){
            Route::get('registrations/avatars', 'RegistrationController@avatars')->name('wallet.registrations.avatars');

            Route::resource('registrations', 'RegistrationController', ['only' => [
                'index', 'store', 'update'
            ]]);

            Route::resource('daily_bonuses', 'DailyBonusController', ['only' => [
                'index', 'store', 'update'
            ]]);

            Route::prefix('exchange_rates')->group(function(){
                Route::get('datatable', 'ExchangeRateController@dataTable')->name('wallet.exchange_rates.dataTable');
            });

            Route::resource('exchange_rates', 'ExchangeRateController', ['only' => [
                'index', 'store', 'update'
            ]]);

            Route::prefix('currencies')->group(function(){
                Route::get('datatable', 'CurrencyController@dataTable')->name('wallet.currencies.dataTable');
            });

            Route::resource('currencies', 'CurrencyController', ['only' => [
                'index', 'store', 'update'
            ]]);
            

            Route::prefix('transfer')->group(function(){
                Route::get('/', 'TransferController@index')->name('wallet.transfer.index');
                Route::post('get_currencies', 'TransferController@get_currencies')->name('wallet.transfer.get_currencies');
                Route::post('/', 'TransferController@transfer')->name('wallet.transfer.transfer');
                Route::get('datatable', 'TransferController@dataTable')->name('wallet.transfer.dataTable');
            });

            Route::prefix('withdrawal')->group(function(){
                Route::get('/', 'WalletWithdrawalController@index')->name('wallet.withdrawal.index');
                Route::post('set_status', 'WalletWithdrawalController@set_status')->name('wallet.withdrawal.set_status');
                Route::post('set_notes', 'WalletWithdrawalController@set_notes')->name('wallet.withdrawal.set_notes');
                Route::post('set_attachments', 'WalletWithdrawalController@set_attachments')->name('wallet.withdrawal.set_attachments');
                Route::post('updatenote', 'WalletWithdrawalController@updateCrmNotes')->name('wallet.withdrawal.updatenote');
            });

            Route::prefix('deposit')->group(function(){
                Route::get('/', 'WalletDepositController@index')->name('wallet.deposit.index');
                Route::post('updatenote', 'WalletDepositController@updateCrmNotes')->name('wallet.deposit.updatenote');
                Route::post('/', 'WalletDepositController@chageStatus')->name('wallet.deposit.changeStatus');
                Route::get('datatable', 'WalletDepositController@dataTable')->name('wallet.deposit.dataTable');
                Route::post('updateStatus', 'WalletDepositController@processApproval')->name('wallet.deposit.updateStatus');
            });
            Route::prefix('bankinformation')->group(function(){
               Route::get('/','BankInformationController@index')->name('wallet.bankinformation.index');
               Route::get('datatable', 'BankInformationController@dataTable')->name('wallet.bankinformation.datatable');
               Route::post('store','BankInformationController@store')->name('wallet.bankinformation.store');
               Route::post('update','BankInformationController@update')->name('wallet.bankinformation.update');
               
            });

            Route::get('ledger/{ledger}/source-info', 'WalletController@ledgerSourceInfo')->name('wallet.ledgerSourceInfo');
            Route::get('{wallet}/datatable', 'WalletController@dataTable')->name('wallet.dataTable');
        });

        Route::namespace('EmailTemplate')->group(function(){
            Route::resource('email_templates', 'EmailTemplateController', ['only' => [
                'index', 'update'
            ]]);
        });
    });

    Route::get('logout', 'Auth\LoginController@logout');
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('testadmin', 'adminltesample@index');
    Route::get('testlogin', 'adminltesample@testlogin');
});

Route::namespace('common')->group(function()
{
    Route::get('/attachments/{action}/{id}/{file}', 'AttachmentsController@getFile')->name('attachments.getFile');
});


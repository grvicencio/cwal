<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



//Route::namespace('V1')->group(function()
//{        
Route::group(['namespace' => 'api'], function () {
Route::get('login', array('as' => 'login', function () {
    $response = array("error" => "Unauthenticated");
    return response(json_encode($response), 201);
}));
    
    
    Route::namespace('CRM')->group(function () {
        Route::prefix('avatars')->group(function () {
            Route::post('upload', 'AvatarController@avatarUpload');
            Route::get('{avatar_id}/{size}/{avatar}', 'AvatarController@getAvatar');
        });

        Route::prefix('membership/logo')->group(function () {
            Route::post('upload', 'MembershipLogoController@logoUpload');
            Route::get('{membership_id}/{size}/{logo}', 'MembershipLogoController@getLogo');
        });
    });

    Route::get('/stripe/index', 'samplecardController@index');

    Route::namespace('User')->prefix('user')->group(function () {
        
        Route::get('/online', 'UserActivityController@getOnline')->middleware('auth:api','online');
        Route::post('chat/{id}','UserActivityController@SendMessage')->middleware('auth:api','online');
        Route::get('getchat/{id}','UserActivityController@FetchMesage')->middleware('auth:api','online');
        Route::get('/inbox/{id}','UserActivityController@getUserInbox')->middleware('auth:api','online');
        Route::get('/isuseronline/{id}', 'UserActivityController@UserOnline')->middleware('auth:api','online');
        Route::get('/guest/{manufacturer}/{os}/{model}/{uuid}', 'UserController@Guest');
        Route::get('/profile', 'UserController@Profile')->middleware('auth:api','online');
        Route::get('/userprofile/{userid}', 'UserController@UserProfile')->middleware('auth:api', 'online');
        Route::post('/verify', 'UserController@Verify')->middleware('auth:api');
        Route::post('/login/{manufacturer?}/{os?}/{model?}/{uuid?}', 'UserController@Login')->middleware('online');
        Route::get('activate/{code}', 'UserController@Activation');
        Route::post('/web_register', 'UserController@WebRegister');
        Route::post('/register/{application}/{username}/{points}/{currency?}/{manufacturer?}/{os?}/{model?}', 'UserController@Register');
        Route::post('/refresh', 'UserController@Refresh')->middleware('auth:api','online');
        Route::post('/testdata', 'UserController@Testdata')->middleware('auth:api');
        Route::post('/logout', 'UserController@Logout')->middleware('auth:api');
        Route::post('/update', 'UserController@UpdateProfile')->middleware('auth:api','online');
        Route::post('/security', 'UserController@UpdatePassword')->middleware('auth:api','online');
        Route::post('/uploadprofile', 'UserController@UploadProfileImage')->middleware('auth:api','online');
        Route::get('/bank', 'BankInfoController@index')->middleware('auth:api','online');
        Route::get('/bank/{bank_id}', 'BankInfoController@show')->middleware('auth:api','online');
        Route::get('/bank/checkinfo/{user_id}', 'BankInfoController@checkIfUserHasBankInfo','online');
        Route::post('/bank/update', 'BankInfoController@update')->middleware('auth:api','online');
        Route::prefix('billing')->group(function() {
        Route::get('/', 'BillingController@index')->middleware('auth:api');
        Route::post('update', 'BillingController@Update')->middleware('auth:api','online');
        
        });
        
        Route::prefix('security_question')->group(function() {
            Route::get('/', 'UserController@GetSecurityQuestion')->middleware('auth:api','online');
            Route::post('/update', 'UserController@UpdateSecurityQuestion')->middleware('auth:api','online');
        });
    });

    Route::namespace('Membership')->prefix('membership')->group(function () {
        Route::get('/all', 'MembershipController@Index');
        Route::get('/detail/{id}', 'MembershipController@Detail');
        Route::get('/user', 'MembershipController@UserMembership')->middleware('auth:api');
        Route::get('/topup/{id}/{currency_name}/{application}', 'MembershipController@Topup')->middleware('auth:api');
    });
    Route::namespace('Contact')->prefix('contact')->group(function () {

        Route::post('submit/{source?}','ContactController@submit')->middleware('auth:api','online');
    });

    Route::namespace('Ticket')->prefix('tickets')->group(function () {
        Route::get('active', 'TicketController@active')->middleware('auth:api');
        Route::get('inactive', 'TicketController@inactive')->middleware('auth:api');
        Route::get('open', 'TicketController@openIndex')->middleware('auth:api');

        Route::prefix('{ticket}')->group(function () {
            Route::get('/', 'TicketController@showTicket')->middleware('auth:api');

            Route::prefix('offers')->group(function () {
                Route::get('/', 'TicketController@offers')->middleware('auth:api');
                Route::post('/', 'TicketController@setOffer')->middleware('auth:api');

                Route::prefix('{offer}')->group(function () {
                    Route::get('/', 'TicketController@showOffer')->middleware('auth:api');
                    Route::post('reject', 'TicketController@rejectOffer')->middleware('auth:api','online');
                    Route::post('approve', 'TicketController@approveOffer')->middleware('auth:api','online');
                    Route::post('pay', 'TicketController@offerPayment')->middleware('auth:api','online');
                    Route::post('payed', 'TicketController@offerPayed')->middleware('auth:api','online');
                    Route::post('close', 'TicketController@closeOffer')->middleware('auth:api','online');
                    Route::post('buyer_pay', 'TicketController@acceptAndPay')->middleware('auth:api');
                    Route::get('payment_info', 'TicketController@paymentInfo')->middleware('auth:api','online');
                    Route::get('transfer_info', 'TicketController@transferInfo')->middleware('auth:api','online');
                    Route::post('ticket_transfer', 'TicketController@ticket_transfer')->middleware('auth:api','online');
                    /* CP-206*/
                    Route::post('dispute','TicketController@dispute')->middleware('auth:api','online');
                    Route::get('disputemessage','TicketController@getDisputeMessage')->middleware('auth:api','online');
                    Route::post('disputetopaymentmade','TicketController@DisputeToPaymentMade')->middleware('auth:api','online');
                    /* CP-206*/

                });
            });
            Route::post('close', 'TicketController@close')->middleware('auth:api');
        });
    });

    Route::namespace('UserReview')->prefix('review')->group(function(){
        Route::post('/send', 'UserReviewController@postSend')->middleware('auth:api');
        Route::get('/overall/{userid}', 'UserReviewController@getComputedReview')->middleware('auth:api');
        Route::get('/get_review/{userid}', 'UserReviewController@getReview')->middleware('auth:api');
        Route::get('/get_reviews/{userid}', 'UserReviewController@getUserReview')->middleware('auth:api');
        Route::get('/review_detail/{userid}', 'UserReviewController@getReviewDetails')->middleware('auth:api');
    });

    Route::namespace('Charges')->prefix('charges')->group(function () {
        Route::get('/all', 'ChargesController@Index');
        Route::get('/detail/{id}', 'ChargesController@Detail');
    });

    Route::namespace('Application')->prefix('application')->group(function () {
        Route::get('/gamestatus/{application}', 'ApplicationController@MobileAppStatus')->middleware('auth:api');
        Route::get('/all', 'ApplicationController@Index');
        Route::get('/user', 'ApplicationController@UserApp')->middleware('auth:api');

        Route::prefix('screen_name')->group(function() {
            Route::post('change', 'ApplicationController@UpdateScreenName')->middleware('auth:api');
            Route::get('list', 'ApplicationController@GetScreenNames')->middleware('auth:api');
        });
        Route::post('/register', 'ApplicationController@RegisterApp')->middleware('auth:api');
    });

    Route::namespace('Payment')->prefix('payment')->group(function () {
        Route::post('/create', 'PaymentinfoController@Create')->middleware('auth:api');
        Route::get('/user', 'PaymentinfoController@UserDetail')->middleware('auth:api');
        Route::get('/all', 'PaymentinfoController@PaymentMethod')->middleware('auth:api');
        Route::post('/update', 'PaymentinfoController@UpdatePayment')->middleware('auth:api');
        Route::post('/charge/{chargeid}', 'PaymentChargesController@Create')->middleware('auth:api');
        Route::get('/transaction', 'PaymentChargesController@MyTransaction')->middleware('auth:api');
    });

    // unuse dont use this routes
    Route::namespace('Cards')->prefix('card')->group(function () {
        // Route::get('/start/{table_id}/{shoe_id}', 'CardController@Start');
        Route::get('/get', 'PlayCardController@GetCard');
        Route::post('/deal/save', 'PlayCardController@SaveDealResult');
        Route::get('/statistics/{table}/{shoe}', 'PlayCardController@GetTableStatistic');
    });

    // done
    Route::namespace('Game')->prefix('table')->group(function () {
        Route::get('/public', 'TableController@GetPublicTable');
        // Route::get('/get', 'PlayCardController@GetCard');
        // Route::post('/deal/save', 'CardController@SaveDealResult')->middleware('auth:api');
        Route::get('/statistics/{table}/{shoe}', 'TableController@GetTableStatistic')->middleware('auth:api');
        // Route::get('/create/shoe/{table_id}/{penetration}/{previousShoe_id}', 'CardController@RequestShoe')->middleware('auth:api');
        // Route::get('/card/start/{table_id}/{shoe_id}', 'CardController@Start')->middleware('auth:api');
        // Route::get('/shoe/close/{table_id}/{shoe_id}', 'CardController@closeShoe')->middleware('auth:api');
    });

    Route::namespace('Tradeprofile')->prefix('tradeprofile')->group(function(){
        Route::get('/all', 'TradeprofileController@getTradeProfiles')->middleware('auth:api');
        Route::get('/usertrade', 'TradeprofileController@getUserTradingProfile')->middleware('auth:api');
        Route::post('/store', 'TradeprofileController@store')->middleware('auth:api');
        Route::post('/update', 'TradeprofileController@update')->middleware('auth:api');
        Route::post('/delete', 'TradeprofileController@destroy')->middleware('auth:api');
    });

    Route::namespace('Wallet')->prefix('wallet')->group(function () {
        Route::get('/guestprofile/{uuid}', 'PointsController@GetGuestProfile')->middleware('auth:api','online'); // unused
        Route::get('mywallet', 'MyWallet@Wallet')->middleware('auth:api','online');
        Route::post('transferto', 'MyWallet@WalletStartTransfer')->middleware('auth:api','online');
        Route::post('transferprocess/{application}', 'MyWallet@WalletTransfer')->middleware('auth:api','online');
        Route::get('currency/{currency}', 'MyWallet@GetCurrency')->middleware('auth:api','online');
        Route::get('all/currency', 'MyWallet@getAll')->middleware('auth:api','online');
        Route::get('/guest/{application}', 'MyWallet@GetGuestWallet')->middleware('auth:api');
        Route::get('newaccount/{application}/{currency}','MyWallet@GenerateNewWalletAccount')->middleware('auth:api','online');
        Route::post('/deposit', 'DepositController@deposit')->middleware('auth:api','online');
        Route::get('/availablecurrency', 'MyWallet@GetOpenCurrency')->middleware('auth:api','online');
        Route::post('/withdrawal', 'WithdrawalController@withdrawal')->middleware('auth:api','online');
        Route::get('/check_balance/{currency_id}', 'WithdrawalController@checkBalance')->middleware('auth:api','online');
        Route::get('/generalwallet/{base}', 'ExchangeRateController@ExchangeRateWallet')->middleware('auth:api','online');
        Route::get('/currencyexchange/{base}', 'ExchangeRateController@GetCurrencyExchangeRate')->middleware('auth:api','online');
         Route::get('/exchangerate', 'ExchangeRateController@GetAllExchangeRate')->middleware('auth:api','online');
    });

    Route::namespace('Avatar')->prefix('avatar')->group(function () {
        Route::get('/default/{application}', 'DefaultController@DefaultAvatar')->middleware('auth:api');
        Route::get('/type/{type}/{application}', 'DefaultController@Index')->middleware('auth:api');
        Route::get('/get/{id}', 'DefaultController@GetAvatar')->middleware('auth:api');
        Route::get('/update/{id}/{application}', 'DefaultController@UpdateAvatar')->middleware('auth:api');
    });

    Route::namespace('Auth')->prefix('password')->group(function () {
        Route::post('/email', 'ForgotPasswordController@getResetToken');
        Route::post('/reset', 'ResetPasswordController@reset')->name('password.reset');
        
     /*   Route::post('/reset', [
                        'uses' => 'ResetPasswordController@reset',
                        'as' => 'password.reset'
                        ])->name('password.reset');*/
                        
        Route::get('/security_question_fetch', 'ResetPasswordController@fetchSecurityQuestion');
         Route::post('/checkIfTokenIsValid', 'ResetPasswordController@checkIfTokenIsValid');
        // throttle only for 5 times attempt per day (1440 min) to prevent brute force attack
        Route::post('/security_question_reset', 'ResetPasswordController@resetViaSecurityQuestion')->middleware('throttle:5,1440');;
    });

    Route::get('/market/getprofile', 'Market\UsersController@getUserProfile')->middleware('auth:api','online');

    Route::namespace('v2')->prefix('v2')->group(function () {
        Route::namespace('Wallet')->prefix('wallet')->group(function () {
            Route::get('/guestprofile/{uuid}', 'PointsController@GetGuestProfile')->middleware('auth:api','online'); // unused
            Route::get('mywallet', 'MyWallet@Wallet')->middleware('auth:api','online');
            Route::post('transferto', 'MyWallet@WalletStartTransfer')->middleware('auth:api','online');
            Route::post('transferprocess/{application}', 'MyWallet@WalletTransfer')->middleware('auth:api','online');
            Route::get('currency/{currency}', 'MyWallet@GetCurrency')->middleware('auth:api','online');
            Route::get('all/currency', 'MyWallet@getAll')->middleware('auth:api','online');
            Route::get('/guest/{application}', 'MyWallet@GetGuestWallet')->middleware('auth:api');
            Route::get('newaccount/{application}/{currency}','MyWallet@GenerateNewWalletAccount')->middleware('auth:api','online');
            Route::post('/deposit', 'DepositController@deposit')->middleware('auth:api','online');
            Route::get('/availablecurrency', 'MyWallet@GetOpenCurrency')->middleware('auth:api','online');
            Route::post('/withdrawal', 'WithdrawalController@withdrawal')->middleware('auth:api','online');
            Route::get('/check_balance/{currency_id}', 'WithdrawalController@checkBalance')->middleware('auth:api','online');
            Route::get('/generalwallet/{base}', 'ExchangeRateController@ExchangeRateWallet')->middleware('auth:api','online');
            Route::get('/currencyexchange/{base}', 'ExchangeRateController@GetCurrencyExchangeRate')->middleware('auth:api','online');
            Route::get('/exchangerate', 'ExchangeRateController@GetAllExchangeRate')->middleware('auth:api','online');
            Route::get('/exchangerate/forex', 'ExchangeRateController@forex')->middleware('auth:api','online');
            Route::post('/alias', 'MyWallet@updateAlias')->middleware('auth:api','online');
             Route::get('/mycurrency/{type}', 'MyWallet@getMyCurrency')->middleware('auth:api','online');


        });
        // the version 2 of user registration and login with 2fa

        Route::namespace('User')->prefix('user')->group(function () {
            Route::post('/login/{manufacturer?}/{os?}/{model?}/{uuid?}', 'UserController@Login')->middleware('online');
            Route::post('/web_register', 'UserController@WebRegister');
            Route::post('/google2fa', 'UserController@validategoogle2fa');
            Route::post('/fetchQR', 'UserController@fetchQR')->middleware('auth:api','online');
            Route::post('/validate2fa', 'UserController@validate2faKey')->middleware('auth:api','online');
            Route::post('/update_google2fa', 'UserController@updategoogle2fa')->middleware('auth:api','online');
         });
    });
//});
});
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();

redis.psubscribe('*', function(err,count){
	console.log('Redis: message-channel subscribed');
});

redis.on('pmessage', function(subscribed,channel, message){
	console.log('Redis: Message on ' + channel + ' received!');
	console.log(message);
	message = JSON.parse(message);
	console.log(channel, '======channel')
	console.log(message, '======message')

	io.emit(channel + ':' + message.event, message.data);
	//io.emit(channel,message)
});

http.listen(3000, function(){
	console.log('listening on *:3000');
});